#!/bin/bash
# Script to prepare riops polar 3D files for use in the drift tool
# Makes time the record dimension, converts files to netcdf4_classic
# Links forecasts to form a continuous time series
# Deletes data 10 days old
# Emails if previous day failed
#
# Usage: prepare_riops.sh [YYYYMMDD]
#        If no argument is given it will run with todays date.
# Author: Nancyh Soontiens
# Date: 2019-08-30

###### Set up directories #####################
date="$1"
if [[ -z "$date" ]]; then
  date=$(date +'%Y%m%d')
fi
# Source directory
src=/home/sdfo501/links/eccc_forecasts
model=riops_20191231/main/netcdf/prog/riops
SRC_DIR=${src}/${model}
# Save directories
save_dir=/home/nso001/data/work2/models/riops
SAVE_FCST_DIR=${save_dir}/forecast
SAVE_CONT_DIR=${save_dir}/continuous
# Only use FCST_HR defined below
FCST_HR=00
# Save sub directories
prefix=${date}${FCST_HR}
SAVE_FCST_DATE_DIR=${SAVE_FCST_DIR}/${prefix}
mkdir -p ${SAVE_FCST_DATE_DIR}
mkdir -p ${SAVE_CONT_DIR}
# Logfile
LOGFILE=${save_dir}/logs/${date}
##################################################

########## Check the destination #################
# If SAVE_FCST_DATE_DIR exists and is not empty, don't copy
if [[ -d "${SAVE_FCST_DATE_DIR}" ]]; then
    if [[ ! -z "$(ls ${SAVE_FCST_DATE_DIR})" ]]; then
        exit
    fi
fi
##################################################

########### Check the source files ###############
files=${SRC_DIR}/${prefix}*_3D*.nc
# Ensure that there are 49 files for the forecast
# Need to revise if we start splicing multiple forecasts.
if [[ "$(echo $files | grep -v '*' | wc -w)" -ne "49" ]]; then
    echo "All model files not available at $(date)." >> ${LOGFILE}
    echo "Try again later." >> ${LOGFILE}
    exit
fi
for file in ${files}; do
    base=$(basename "${file}")
    newfile=${SAVE_FCST_DATE_DIR}/${base}
    echo "Modifyng ${base}" >> ${LOGFILE}
    ncks --mk_rec_dmn time ${file} ${newfile}
    ncks -O --fl_fmt=netcdf4_classic $newfile $newfile
    # if first 24 hours, then also link
    hr=$(grep -oP '(?<=_).*(?=_3D)' <<< "${base}")
    echo ${hr} >> ${LOGFILE}
    if [[ "${hr}" < "024" ]]; then
       echo "Linking ${base}" >> ${LOGFILE}
       ln -s ${newfile} ${SAVE_CONT_DIR}/${base}
    fi
done

############## Delete data and logs 5 days old ####################
dremove=$(date -d "$date -6days" +'%Y%m%d')
dir_remove=${dremove}${FCST_HR}
echo "Removing data from ${SAVE_FCST_DIR}/${dir_remove}" >> ${LOGFILE}
rm -rf ${SAVE_FCST_DIR}/${dir_remove}
# Remove brokin links in continuous
echo "Removing broken symbolic links in ${SAVE_CONT_DIR}" >> ${LOGFILE}
find -L ${SAVE_CONT_DIR} -type l -exec rm -- {} +
# Delete logs
echo "Removing old log ${save_dir}/logs/${dremove}" >> ${LOGFILE}
rm -rf ${save_dir}/logs/${dremove}

######### Email if previous day failed ############################
# directory doesn't exist or is empty
yesterday=$(date -d "$date -1days" +'%Y%m%d')
# Directory doesn't exist
if [[ ! -d "${SAVE_FCST_DIR}/${yesterday}${FCST_HR}" ]]; then
    echo "${yesterday} failed" | mail -s "riops preprocess" nancy.soontiens@dfo-mpo.gc.ca
fi
# Directory is empty
if [[ -d "${SAVE_FCST_DIR}/${yesterday}${FCST_HR}" ]]; then
    if [[ -z "$(ls ${SAVE_FCST_DIR}/${yesterday}${FCST_HR})" ]]; then
        echo "${yesterday} failed" | mail -s "riops preprocess" nancy.soontiens@dfo-mpo.gc.ca
    fi
fi
####################################################################
