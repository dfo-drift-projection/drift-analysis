Scripts for processing RIOPS operational model for use in the drift tool.

hcron settings for running daily:
```bash
  as_user=nso001
  host=dfo-inter-ubuntu-18.04-amd64.science.gc.ca
  command=/home/nso001/code/drifters/drift-analysis/pre-process/riops/prepare_riops.sh
  notify_email=nancy.soontiens@dfo-mpo.gc.ca
  notify_message="running riops preprocess"
  when_month=*
  when_day=*
  when_hour=10
  when_minute=00
  when_dow=*
  template_name=
```