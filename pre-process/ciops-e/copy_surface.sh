# Add depth dimension for use in ariane
# - Ariane needs at least 2 vertical levels in order to finish successfully

date=201
filesU=/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-E/CIOPS-E_PA_502_SN1500_10d/*/NEMO/*1h*grid_U*${date}*.nc
filesV=/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-E/CIOPS-E_PA_502_SN1500_10d/*/NEMO/*1h*grid_V*${date}*.nc
outdir=$HOME/data/work2/models/ciops-e/CIOPSE_SN1500_2D/
mkdir -p $outdir

mesh=$HOME/data/work2/models/ciops-e/CIOPSE_InitialTest_Surface_mesh.nc

for f in $filesU; do
  echo $f
  basename=$(basename $f)
  ncap2 -s 'defdim("z",2);uos_new[$time_counter,$z,$y,$x]=uos;' $f $outdir/$basename
  ncks -A -v gdept_1d $mesh $outdir/$basename
  # Copy metadata for depth
  ncks -A -C -H -v gdept_1d $mesh $outdir/$basename
  ncks -O -x -v uos $outdir/$basename $outdir/$basename
  ncrename -v uos_new,uos $outdir/$basename
  ncrename -v gdept_1d,depth $outdir/$basename
  # Copy metadata for u
  ncks -A -C -H -v uos $f $outdir/$basename
done

for f in $filesV; do
  echo $f
  basename=$(basename $f)
  ncap2 -s 'defdim("z",2);vos_new[$time_counter,$z,$y,$x]= vos;' $f $outdir/$basename
  ncks -A -v gdept_1d $mesh $outdir/$basename
  # Copy metadata for depth
  ncks -A -C -H -v gdept_1d $mesh $outdir/$basename
  ncks -O -x -v vos $outdir/$basename $outdir/$basename
  ncrename -v vos_new,vos $outdir/$basename
  ncrename -v gdept_1d,depth $outdir/$basename
  # Copy metadata for v
  ncks -A -C -H -v vos $f $outdir/$basename
done
