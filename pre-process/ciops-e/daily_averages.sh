# Script to produce daily averages from hourly averages

files=$HOME/data/work2/models/ciops-e/orig-cgrid/*
outdir=$HOME/data/work2/models/ciops-e/daily_averages/
mkdir -p $outdir

for f in $files; do
  echo $f
  basename=$(basename $f)
  ncra $f $outdir/$basename
done
