#!/bin/bash

# Script to submit the hrdps pre-process job submission script.
# To be called by hcron
# Nancy Soontiens


rundate=$(date +'%Y%m%d')
HH=00
ciopsePath="/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608"
data_dir="${ciopsePath}/forecast/gridpt/prog/ciops"
results_dir="/home/nso001/data/work2/models/ciops-e_operational_forecasts/"
LOGFILE="${results_dir}/logs/${rundate}${HH}"

# Check that all source files are there before running
files=${data_dir}/${rundate}${HH}*
if [ "$(echo $files | grep -v '*' | wc -w)" -ne "49" ]; then
    echo "All model files not available at ${rundate}." >> ${LOGFILE}
    echo "Try again later." >> ${LOGFILE}
    exit
fi

# Don't submit job if output already exists
outdir=${results_dir}/${rundate}${HH}
if [ -d $outdir ]; then # directory exists
    if [ ! -z "$(ls -A ${outdir})" ]; then # there are files
	exit
    fi
else
    /fs/ssm/main/base/20190814/all/bin/jobsub -c gpsc4 /home/nso001/code/drifters/drift-analysis/pre-process/ciops-e/rpn2netcdf/ciopse_submit.job

     ############## Delete data and logs 5 days old ####################
     dremove=$(date -d "$date -6days" +'%Y%m%d')
     dir_remove=${dremove}${HH}
     echo "Removing data from ${results_dir}/${dir_remove}" >> ${LOGFILE}
     rm -rf ${results_dir}/${dir_remove}
     # Delete logs
     echo "Removing old log ${results_dir}/logs/${dremove}" >> ${LOGFILE}
     rm -f ${results_dir}/logs/${dremove}
     rm -f ${results_dir}/logs/ciopse*

     ######### Email if previous day failed ############################
     # directory doesn't exist or is empty
#     yesterday=$(date -d "$date -1days" +'%Y%m%d')
#     # Directory doesn't exist
#     if [[ ! -d "${results_dir}/${yesterday}${HH}" ]]; then
#        echo "${yesterday} failed" | mail -s "ciopse preprocess" nancy.soontiens@dfo-mpo.gc.ca
#     fi
#     # Directory is empty
#     if [[ -d "${results_dir}/${yesterday}${HH}" ]]; then
#        if [[ -z "$(ls ${results_dir}/${yesterday}${HH})" ]]; then
#            echo "${yesterday} failed" | mail -s "ciopse preprocess" nancy.soontiens@dfo-mpo.gc.ca
#        fi
#     fi
     ####################################################################
fi
