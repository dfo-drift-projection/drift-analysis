# Wrapper to call to_netcdf.sh on subdirectories of CIOPSE files
# Usage: bash netcdf_wrapper date
# where date is a date string (eg. YYYYMMHHFF)
# Nancy Soontiens July 2020


SRC_DIR=/home/nso001/data/eccc_forecasts/ciops_east_20200608/forecast/gridpt/prog/ciops
SAVE_DIR=/home/nso001/data/work2/models/ciops-e_operational_forecasts

date="$1"

files=$(ls ${SRC_DIR}/${date}*)
echo $files

outdir=${SAVE_DIR}/${date}
mkdir -p $outdir

bash to_netcdf.sh "${files}" $outdir dict_2d.nc dict_3d.nc
