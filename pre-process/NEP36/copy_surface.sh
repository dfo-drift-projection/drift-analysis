# Add depth dimension for use in ariane
# - Ariane needs at least 2 vertical levels in order to finish successfully

date=201503
filesU=$HOME/data/work/NEP36/output/*1h*grid_U*${date}*.nc
filesV=$HOME/data/work/NEP36/output/*1h*grid_V*${date}*.nc
outdir=$HOME/data/work/NEP36/surface_$date
mkdir -p $outdir

mesh=$HOME/data/work/NEP36/grid/mesh_mask_surface.nc

for f in $filesU; do
  echo $f
  basename=$(basename $f)
  ncap2 -s 'defdim("z",2);uos_new[$time_counter,$z,$y,$x]=uos;' $f $outdir/$basename
  ncks -A -v gdept_1d $mesh $outdir/$basename
  ncks -O -x -v uos $outdir/$basename $outdir/$basename
  ncrename -v uos_new,uos $outdir/$basename
  ncrename -v gdept_1d,depth $outdir/$basename
done

for f in $filesV; do
  echo $f
  basename=$(basename $f)
  ncap2 -s 'defdim("z",2);vos_new[$time_counter,$z,$y,$x]= vos;' $f $outdir/$basename
  ncks -A -v gdept_1d $mesh $outdir/$basename
  ncks -O -x -v vos $outdir/$basename $outdir/$basename
  ncrename -v vos_new,vos $outdir/$basename
  ncrename -v gdept_1d,depth $outdir/$basename
done
