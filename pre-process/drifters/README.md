ios/ and ubs/ contain scripts for pre-processing drifter data obtained from IOS and UBC.
* Convert from native format (csv or matlab) to netcdf
* Original and netcdf drifter data is found on trinity: /data/hdd/drifters/...

Scripts are separated into folders for IOS and UBC.

misc_readers/ contains reader/writer scripts for various static data sets (for example NOAA student drifter project). The scripts have varying levels of qc included and need to be formalized.
