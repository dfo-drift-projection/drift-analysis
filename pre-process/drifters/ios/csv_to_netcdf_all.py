# Script to convert csv drifter file to netcdf for all ios drifters

import datetime
import pandas as pd
import netCDF4 as nc
import numpy as np
import os
import glob
import dateutil.parser

DRIFTER_DATA='/data/hdd/drifters/ios_drifters/all/'
SAVEDIR='/data/hdd/drifters/ios_drifters/all'


def main():
    process_SCT(os.path.join(DRIFTER_DATA, 'SCT_clean'))
    process_Osker(os.path.join(DRIFTER_DATA,'OSKERS_cleaned_SP'))
        

def process_SCT(datadir):
    basename=os.path.basename(datadir)
    files = glob.glob(os.path.join(datadir,'*/*.csv'))
    for fname in files:
        print("Processing file: {}".format(fname))
        data = pd.read_csv(fname,header=None, names=['date', 'lat', 'lon'],
                           skiprows=[0])
        dates = [ dateutil.parser.parse(d) for d in data['date']]
        directory = os.path.join(SAVEDIR, basename, 'netcdf', dates[0].strftime('%Y%m'))
        if not os.path.exists(directory):
            os.makedirs(directory)
        fsave=os.path.join(directory,
                           '{}.nc'.format(os.path.basename(fname)[:-4]))
        save_netcdf_file(data, fsave)                                         
    

def process_Osker(datadir):
    basename=os.path.basename(datadir)
    files=glob.glob(os.path.join(datadir, '*.csv'))
    for fname in files:
        print("Processing file: {}".format(fname))
        data = pd.read_csv(fname,header=None,
                           names=['id', 'date', 'lat', 'lon'])
        dates = [ dateutil.parser.parse(d) for d in data['date']]
        directory = os.path.join(SAVEDIR, basename, 'netcdf', dates[0].strftime('%Y%m'))
        if not os.path.exists(directory):
            os.makedirs(directory)
        fsave=os.path.join(directory,
                           '{}.nc'.format(os.path.basename(fname)[:-4]))
        save_netcdf_file(data, fsave)
                                                                            

def save_netcdf_file(data, fname):
    dates = [ dateutil.parser.parse(d) for d in data['date']]
    dates = [d.replace(tzinfo=None) for d in dates]
    f = nc.Dataset(fname, 'w')
    # Dimensions
    time = f.createDimension('time', len(data['date']))
    # Global Attributes
    drift_id = os.path.basename(fname)[:-3]
    f.description = 'IOS drifter trajectory file'
    f.ios_id = drift_id
    epoch = int((dates[0] -
                 datetime.datetime.utcfromtimestamp(0)).total_seconds())
    f.unique_id = '{}_{}'.format(drift_id,
                                 epoch) 
    f.launchDate = dates[0].strftime('%Y-%m-%d %H:%M:%S')
    f.comment1 = 'Drifter data collected by the Institude of Ocean Sciences'
    f.comment2 = 'Files transferred by Hauke Blanken ' +\
                 '<hauke.blanken@dfo-mpo.gc.ca>'
    f.comment3 = 'netCDF file created by Nancy Soontiens ' +\
                 '<nancy.soontiens@dfo-mpo.gc.ca>'

    f.reference = os.path.realpath(__file__)
    # Variables
    times = f.createVariable('time', np.float64, ('time',))
    times[:] = nc.date2num(dates,'seconds since 1950-01-01 00:00:00')
    times.units='seconds since 1950-01-01 00:00:00'
    times.calendar='standard'
    times.origin='1950-01-01 00:00:00'
    
    lons = f.createVariable('longitude', np.float64, ('time',))
    lons[:] = data['lon'].values[:]
    lons.units='degrees_east'
    lons.long_name='Longitude'

    lats = f.createVariable('latitude', np.float64, ('time',))
    lats[:] = data['lat'].values[:]
    lats.units='degrees_north'
    lats.long_name='Latitude'
    f.close()

                      
if __name__ == '__main__':
    main()
