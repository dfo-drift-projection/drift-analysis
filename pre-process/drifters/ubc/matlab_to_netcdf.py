# Script to convert matlab drifter file to netcdf files for each drifter

import datetime
import scipy.io as sio
import netCDF4 as nc
import numpy as np
import os

DRIFTER_FILE='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/offShoreTracks_L3.mat'
SAVEDIR='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/'

design_dict = {'1': 'UBC',
               '2': 'IOS',
               '3': 'MicroStar',
               '4': 'Driftcard'}

def main():
    f = sio.loadmat(DRIFTER_FILE)
    drifters = f['drift']
    ndrifters=np.size(drifters['launchDate'])
    problems=0 # Record number of drifters that could not be saved
    for ind in range(ndrifters):
        data = load_drifter_data(drifters, ind)
        data = remove_offshore(data)
        directory = os.path.join(SAVEDIR, 'netcdf',
                                 data['launchDate'].strftime('%Y%m'))
        if not os.path.exists(directory):
            os.makedirs(directory)
        epoch=int((data['launchDate'] -
                   datetime.datetime.utcfromtimestamp(0)).total_seconds())
        fname = os.path.join(
            directory,
            '{0}{1}{2:03d}D{3}.nc'.format(data['id'], data['design'],
                                         epoch, data['launchDate'].strftime('%Y%m%d%H')))
        #exclude Driftcard design
        if data['design'] != design_dict['4']:
            try:
                save_netcdf_file(data, fname)
            except IndexError:
                print('Problem saving drifter {}'.format(ind+1))
                print(data['launchDate'], data['id'])
                print('time length: {}'.format(len(data['times'])))
                print('lon length: {}'.format(np.shape(data['lons'])))
                print('lats length: {}'.format(np.shape(data['lats'])))
                print('offShore length: {}'.format(np.shape(data['offShore'])))
                problems+=1
                os.remove(fname)
    print('Total problem drifters: {}'.format(problems))

    
def load_drifter_data(drifters, ind):
    # Gather relavant data
    data={}
    data['id'] = np.squeeze(drifters['id'])[ind][0,0]
    data['tzone'] = np.squeeze(drifters['tzone'])[ind][0]
    data['design'] = design_dict[str(np.squeeze(drifters['design'])[ind][0,0])]
    data['launchDate'] =\
        matlab2datetime(np.squeeze(drifters['launchDate'])[ind][0,0])
    data['times'] =\
        [matlab2datetime(t) for t in np.squeeze(drifters['mtime'])[ind][:,0]]
    data['lons'] = np.squeeze(drifters['lon'])[ind][:,0]
    data['lats'] = np.squeeze(drifters['lat'])[ind][:,0]
    data['atSea'] = np.squeeze(drifters['atSea'])[ind][:,0]
    return data


def remove_offshore(data):
    data_new=data.copy()
    sea = data['atSea']
    for key in data_new:
        var=data_new[key]
        if type(var) is np.ndarray:
            if var.size > 1:
                print(key)
                data_new[key] = var[sea == 1]
        if type(var) is list:
            var=np.array(var)
            if var.size > 1:
                print(key)
                data_new[key] = list(var[sea == 1])
    return data_new


def save_netcdf_file(data, fname):
    f = nc.Dataset(fname, 'w')
    # Dimensions
    time = f.createDimension('TIME', len(data['times']))
    # Global Attributes
    f.description = 'UBC drifter trajectory file'
    f.ubc_id = str(data['id'])
    f.design = str(data['design'])
    epoch = int((data['launchDate'] -
                 datetime.datetime.utcfromtimestamp(0)).total_seconds())
    f.buoyid = '{}{}{}D{}'.format(str(data['id']), data['design'],
                                  epoch, data['launchDate'].strftime('%Y%m%d%H')) 
    f.launchDate = data['launchDate'].strftime('%Y-%m-%d %H:%M:%S')
    f.comment1 = 'Drifter released by Rich Pawlowicz and team' +\
                 ' Contact: rpawlowicz@eoas.ubc.ca or kstankov@eoas.ubc.ca'
    f.comment2 = 'netCDF file created by Nancy Soontiens ' +\
                 '<nancy.soontiens@dfo-mpo.gc.ca>'
    f.approximate_drogue_depth='0'
    # Other useful attributes
    f.firsttime = data['times'][0].strftime('%Y-%m-%d %H:%M:%S')
    f.lasttime = data['times'][-1].strftime('%Y-%m-%d %H:%M:%S')

    f.geospatial_lat_min = str(data['lats'].min())
    f.geospatial_lat_max = str(data['lats'].max())
    f.geospatial_lon_min = str(data['lons'].min())
    f.geospatial_lon_max = str(data['lons'].max())

    f.time_coverage_start = np.array(data['times']).min().strftime('%Y-%m-%d %H:%M:%S')
    f.time_coverage_end = np.array(data['times']).max().strftime('%Y-%m-%d %H:%M:%S')

    f.first_date_observation = data['times'][0].strftime('%Y-%m-%d')
    f.last_date_observation =  data['times'][-1].strftime('%Y-%m-%d')
    f.last_latitude_observation = str(data['lats'][-1])
    f.last_longitude_observation = str(data['lons'][-1])
    
            
    # Variables
    times = f.createVariable('TIME', np.float64, ('TIME',))
    times[:] = nc.date2num(data['times'],'seconds since 1950-01-01 00:00:00')
    times.units='seconds since 1950-01-01 00:00:00'
    times.calendar='standard'
    times.origin='1950-01-01 00:00:00'
    times.timezone = data['tzone']
    
    lons = f.createVariable('LONGITUDE', np.float64, ('TIME',))
    lons[:] = data['lons']
    lons.units='degrees_east'
    lons.long_name='Longitude'

    lats = f.createVariable('LATITUDE', np.float64, ('TIME',))
    lats[:] = data['lats']
    lats.units='degrees_north'
    lats.long_name='Latitude'

    #u = f.createVariable('vx', np.float64, ('time',))
    #u[:] = data['u']
    #u.units='m/s'
    #u.long_name='Drifter X Velocity'
    
    #v = f.createVariable('vy', np.float64, ('time',))
    #v[:] = data['v']
    #v.units='m/s'
    #v.long_name='Drifter Y Velocity'

    atSea = f.createVariable('atSea', np.int32, ('TIME',))
    atSea[:] = data['atSea']
    atSea.long_name='Quality of drifter being at sea (1=good, 2-4=bad)'
    f.close()
    
    
def matlab2datetime(matlab_datenum):
    """From https://stackoverflow.com/questions/13965740/converting-matlabs-datenum-format-to-python"""
    day = datetime.datetime.fromordinal(int(matlab_datenum))
    dayfrac = datetime.timedelta(days=matlab_datenum%1) -\
              datetime.timedelta(days = 366)
    return day + dayfrac


                      
if __name__ == '__main__':
    main()
