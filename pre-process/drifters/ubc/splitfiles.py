# A script to split the CMEMS historical files into segments when time gap is > 1 day.
# Nancy Soontiens May 2020

import glob
import os
import subprocess

import numpy as np
import pandas as pd
import xarray as xr

data_dir='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/netcdf'
out_dir='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/netcdf-split/'

files=glob.glob(os.path.join(data_dir,'*/*.nc'))
files.sort()

shours=6 #number of hours to split files on
mintotalhours=24

for f in files:
    ds = xr.open_dataset(f)
    print('Processing drifter {}'.format(ds.buoyid))
    # Find indices where time gaps > s hours
    time_diff=ds.TIME.diff(dim='TIME')
    time_diff_in_seconds = time_diff.values.astype('timedelta64[s]')
    inds = np.where(time_diff_in_seconds.astype('float') >= shours*3600)[0]
    if inds.size == 0:
        newfile = os.path.join(out_dir, os.path.basename(f))
        print('...No time split. Copying file to {}'.format(newfile))
        subprocess.call(['cp', f, newfile])
        continue
    inds = inds.tolist() if not inds.size==0 else []
    inds = [ i+1 for i in inds]
    inds.insert(0,0)
    inds.append(ds.TIME.values.size)
    
    # Split up the file
    for s, e in zip(inds[0:-1], inds[1:] ):
        print(s,e)
        dsnew = ds.isel(TIME=slice(s,e))
        if dsnew.TIME.values.size < 2:
            print('...Time interval does not conatin at least two points. Do not process.')
            continue
        diff=(dsnew.TIME.values[-1] - dsnew.TIME.values[0]).astype('timedelta64[s]')
        if diff.astype('float') < mintotalhours*3600:
            print('...Drifter track not at least {} hours. Do not process.'.format(mintotalhours))
            continue

        print('...Splitting between {} and {} '.format(dsnew.TIME.values[0],
                                                       dsnew.TIME.values[-1]))
        # preparing metadate
        dsnew.attrs['launchdate'] = pd.to_datetime(str(dsnew.TIME.values[0])).strftime('%Y%m%d%H')
        dsnew.attrs['firsttime'] = pd.to_datetime(
            str(dsnew.TIME.values[0])).strftime('%Y-%m-%d %H:%M:%S')
        dsnew.attrs['lasttime'] = pd.to_datetime(
            str(dsnew.TIME.values[-1])).strftime('%Y-%m-%d %H:%M:%S')

        dsnew.attrs['geospatial_lat_min'] = str(dsnew.LATITUDE.values.min())
        dsnew.attrs['geospatial_lat_max'] = str(dsnew.LATITUDE.values.max())
        dsnew.attrs['geospatial_lon_min'] = str(dsnew.LONGITUDE.values.min())
        dsnew.attrs['geospatial_lon_max'] = str(dsnew.LONGITUDE.values.max())

        dsnew.attrs['time_coverage_start'] = str(dsnew.TIME.values.min())
        dsnew.attrs['time_coverage_end'] = str(dsnew.TIME.values.max())
        dsnew.attrs['first_date_observation'] =  pd.to_datetime(
            str(dsnew.TIME.values[0])).strftime('%Y%m%d')
        dsnew.attrs['last_date_observation'] =  pd.to_datetime(
            str(dsnew.TIME.values[-1])).strftime('%Y%m%d')
        dsnew.attrs['last_latitude_observation'] = str(dsnew.LATITUDE.values[-1])
        dsnew.attrs['last_longitude_observation'] = str(dsnew.LONGITUDE.values[-1])
        dsnew.attrs['qc_comment'] = \
            'Time gaps larger than {} hours are split into separate files'.format(shours)

        # Save
        buoyid = '{}D{}'.format(ds.attrs['buoyid'].split('D')[0],dsnew.attrs['launchdate'])
        dsnew.attrs['buoyid'] = buoyid
        newfile = os.path.join(out_dir, '{}.nc'.format(buoyid))
        print('...Saving {}'.format(newfile))
        dsnew.to_netcdf(newfile)

