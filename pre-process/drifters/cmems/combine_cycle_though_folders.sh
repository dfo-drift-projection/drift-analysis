#!/bin/bash

for f in $(ls -d /gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files/* | sort); do
    if [ -d "$f" ] 
        then echo $f 
        date
        /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/combine_cmemsdata_by_drifter.py $f >> /gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/combine_files_output.txt
        ret=$?
        if [ $ret -ne 0 ]; then
            exit
        fi
    fi 
done


