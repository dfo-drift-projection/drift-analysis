#!/bin/bash



for f in /space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/CMEMS/2018*; do 
    if [ -d "$f" ] 
        then echo $f 
        /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/create_daily_cmems_files.py $f >> /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/output_log_2018.txt
    fi 
done

#some extra cases
for f in /space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/CMEMS/20190[12345]*; do
    if [ -d "$f" ]
        then echo $f
        /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/create_daily_cmems_files.py $f >> /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/output_log_2018.txt
    fi
done

for f in /space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/CMEMS/2019060[123]*; do
    if [ -d "$f" ]
        then echo $f
        /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/create_daily_cmems_files.py $f >> /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/output_log_2018.txt
    fi
done


for f in /space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/CMEMS/20191001; do
    if [ -d "$f" ]
        then echo $f
        /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/create_daily_cmems_files.py $f >> /fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/output_log_2018.txt
    fi
done






