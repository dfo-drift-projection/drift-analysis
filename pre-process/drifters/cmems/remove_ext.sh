#!/bin/bash

for file in *_daily_reprocessed.nc; do
   mv $file $(echo $file | cut -d_ -f1).nc
done
