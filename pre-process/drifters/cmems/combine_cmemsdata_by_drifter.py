#!/usr/bin/env python

#############################################################
#combine_cmems_by_drifter.py
#
#This script takes the drift-tool daily files and appends them 
#to the previously created master files to create one file per 
#drifter for the entire track. The datadir is passed in as a 
#command line arguement and the data is saved to 
#/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/cmems_dataset/daily_cmems_files/master_files/ 
#(this folder should be updated in the future to a directory 
#that isn't in my user account). There is a huge flaw in this 
#script currently, since it doesn't yet handle the case where 
#data needs to be added to the middle of the files. It will run 
#fine as long as new data is being appended to the end of the 
#files, but breaks down if data needs to be slotted into the 
#middle instead.
#
# NOTE: This does not check if the time is in the middle of the 
# merged file. The script needs to be smarter, in case we need
# to feed in missed data after the fact :(
#############################################################

import os
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt
import shutil
from datetime import datetime, timedelta
import argparse
import sys

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

#add the datadir as a command line arguement
parser = argparse.ArgumentParser()
parser.add_argument("datadir", help="echo the string you use here")
args = parser.parse_args()
datadir = args.datadir

text_output_dir = '/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_dataset/text_output_files/'
#text_output_dir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/text_output_files/'
masterdir = '/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_dataset/master_files'
#masterdir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/master_files'

#copy the daily files into the merged directory
print()
print('***********************************************************************************************************************************************************************')
print('copying _daily files to master directory: ', str(os.path.basename(datadir)))
for dailyfile in glob.glob(os.path.join(datadir,'*_daily.nc')):
    shutil.copy(dailyfile, masterdir)
print()

daily_buoylist = []
#make a list of unique ids
for file in glob.glob(os.path.join(masterdir,'*_daily.nc')):
    bname = os.path.basename(file)
    buoyid = bname.split('_')[0].split('D')[0]
    daily_buoylist.append(buoyid)

#figure out what to do with each daily file (append or make a new file)
for buoyname in np.unique(daily_buoylist):
    print(buoyname)
    newlist = []

    #list all the files with matching wmo numbers
    for fname in os.listdir(path=masterdir):
        if fname.startswith(buoyname):
            newlist.append(fname)

    #if there's one daily + one merged - append
    #the script that creates the daily files checks to be sure that there isn't 2 daily files
    #associated with any one wmo number already, so shouldn't need to recheck.
    if len(newlist) >= 2: 
        print('At least two files found: ', newlist)

        if len(newlist) > 2:
            print('There is probably more than one master file (probably a reused wmo id). Determining which files to merge.')
            #only keep the daily.nc file and most recent master file in newlist.
            keep = []
            datelist = []
            #keep the daily file
            for f in newlist:
                if '_daily.nc' in f:
                    keep.append(f)
                    newlist.remove(f)
            #decide which master file is most recent
            for f in newlist:
                d = f.split('.')[0].split('D')[1]
                #print(d)
                d = datetime.strptime(d,'%Y%m%d')
                datelist.append(d)
            latestdate = max(d for d in datelist)
            keep.append(str(buoyname) + 'D' + str(latestdate.strftime("%Y%m%d")) + '.nc')
            #update newlist
            newlist = keep
        
        print('now using: ', newlist)
         
        #now that I'm back to just two files, continue.
        for f in newlist:
            if '_daily.nc' in f:
                dailyd = f
            else:
                masterd = f

        #open the two files as datasets
        masterds = xr.open_dataset(os.path.join(masterdir,masterd))
        masterenddate = masterds.last_date_observation
        dailyds = xr.open_dataset(os.path.join(masterdir,dailyd))
        dailystartdate = dailyds.time_coverage_start
        dailyenddate = dailyds.last_date_observation
        dailyendlat = dailyds.last_latitude_observation
        dailyendlon = dailyds.last_longitude_observation

        #check here if the master end date is within a month of the start date of the next file. If it is, append it. If not
        #start a new file. If the file is going to be appended, be sure to update to a new data end date, lat/lon end dates, 
        #but keep the title and buoyid the same.
        date_format = "%Y-%m-%dT%H:%M:%SZ"
        a = datetime.strptime(masterenddate, date_format)
        b = datetime.strptime(dailystartdate, date_format)
        delta = b - a #new daily date - older merged date
        delta = delta/timedelta(days=1) 

        #add the paths to the files in newlist and place them correctly in an ordered list
        mf_uselist = [os.path.join(masterdir,masterd),os.path.join(masterdir,dailyd)]

        if (delta > 0) and (delta < 30):
            print('The new file date is < 30 days after the master file. Appending the daily data to the master file')
            mf_ds = xr.open_mfdataset(mf_uselist)
            #grab the updated attributes from dailyds and masterds to reflect the combined data
            mf_ds.attrs['time_coverage_start'] = masterds.time_coverage_start
            mf_ds.attrs['time_coverage_end'] = dailyds.time_coverage_end
            mf_ds.attrs['pi_name'] = ";".join(np.unique(list(masterds.pi_name.split(";")) + list(dailyds.pi_name.split(";"))))
            mf_ds.attrs['last_date_observation'] = dailyds.last_date_observation
            mf_ds.attrs['last_latitude_observation'] = dailyds.last_latitude_observation
            mf_ds.attrs['last_longitude_observation'] = dailyds.last_longitude_observation
            mf_ds.attrs['buoyid'] = masterds.buoyid
            if 'original_datafile_prefix' not in mf_ds.attrs:
                mf_ds.attrs['original_datafile_prefix'] = str((masterds.id).rsplit('_',1)[0])
                del mf_ds.attrs['id']
            mf_ds.attrs['geospatial_lat_min'] = str(np.min([float(masterds.geospatial_lat_min), float(dailyds.geospatial_lat_min)]))
            mf_ds.attrs['geospatial_lat_max'] = str(np.max([float(masterds.geospatial_lat_max), float(dailyds.geospatial_lat_max)]))
            mf_ds.attrs['geospatial_lon_min'] = str(np.min([float(masterds.geospatial_lon_min), float(dailyds.geospatial_lon_min)]))
            mf_ds.attrs['geospatial_lon_max'] = str(np.max([float(masterds.geospatial_lon_max), float(dailyds.geospatial_lon_max)]))
            mf_ds.attrs['comment'] = 'modified netCDF file created by Fisheries and Oceans Canada, NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
            #write out the netcdf file
            keepname = os.path.join(masterdir,(mf_ds.attrs['buoyid'] + '.nc'))
            discardname = keepname + '_garbage'
            os.rename(keepname, discardname)
            mf_ds.to_netcdf(keepname, unlimited_dims=['TIME'], mode='w')
            os.remove(discardname)

        elif (delta < 0):
            #There shouldn't be any negative time deltas!!
            ptname = 'past_time_warning.txt'
            ptpath = os.path.join(text_output_dir, ptname)
            if os.path.exists(ptpath):
                append_write = 'a' # append if already exists
            else:
                append_write = 'w' # make a new file if not
            ptwarn = open(ptpath,append_write)
            ptwarn.write("####################################################################################################################################" + '\n')
            ptwarn.write(datadir + '\n')
            ptwarn.write("There is an error here - the time shouldn't be negative! Sadly, this converter can not yet handle inserting data into the middle of tracks :(" + '\n')
            ptwarn.write("currently on file: " + buoyname + '\n')
            ptwarn.write("the master file end time is:" + a + "the newfile start time is:" + b + '\n')
            ptwarn.write(newlist)
            ptwarn.write('\n')
            ptwarn.close()
            continue
            #sys.exit(1)
            #exit()
            #could I check here if the time falls fully between data gaps? If so, insert it but don't take any of the metadata except min/max lat/lon?


        #Add an elif here to handle delta == 0?
        #This would cover the case where the script sees a folder that it's already processed? 


        else:
            #change id to original_dataset_prefix
            print('Time delta > 30 days. The wmo number has probably been reused - making a new file from the daily file')
            print('the master file end time is:', a, 'the newfile start time is:', b)

            matching = [s for s in newlist if "_daily" in s]
            #check len(matching) == 0
            dailyname = matching[0].split('.')[0].split('_')[0] + '_daily.nc'
            dailypathname = os.path.join(masterdir, dailyname)
            newname = (os.path.basename(dailyname)).split('_')[0] + '.nc'
            newpathname = os.path.join(masterdir,newname)
            os.rename(dailypathname, newpathname)
            commentds = xr.open_dataset(newpathname)
            commentds.attrs['comment'] = 'modified netCDF file created by Fisheries and Oceans Canada, NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
            commentds.attrs['original_datafile_prefix'] = str((commentds.id).rsplit('_',1)[0])
            keepname = newpathname
            discardname = newpathname + '_garbage'
            os.rename(keepname, discardname)
            del commentds.attrs['id']
            commentds.to_netcdf(newpathname, unlimited_dims=['TIME'], mode='w')
            os.remove(discardname)


    #if there's only one file, we need to start a new master file
    else:
        #change id to original_dataset_prefix
        print('There is only one file in the folder - creating new file using daily file')
        dailyname = newlist[0].split('.')[0].split('_')[0] + '_daily.nc'
        dailypathname = os.path.join(masterdir, dailyname)
        newname = (os.path.basename(dailyname)).split('_')[0] + '.nc'
        newpathname = os.path.join(masterdir,newname)
        os.rename(dailypathname, newpathname)
        commentds = xr.open_dataset(newpathname)
        commentds.attrs['comment'] = 'modified netCDF file created by Fisheries and Oceans Canada, NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
        commentds.attrs['original_datafile_prefix'] = str((commentds.id).rsplit('_',1)[0])
        keepname = newpathname
        discardname = newpathname + '_garbage'
        os.rename(keepname, discardname)
        del commentds.attrs['id']
        commentds.to_netcdf(newpathname, unlimited_dims=['TIME'], mode='w')
        os.remove(discardname)
        
#remove the daily files
print("removing the unwanted daily files")
for unwanted_dailyfile in glob.glob(os.path.join(masterdir,'*_daily.nc')):
    os.remove(unwanted_dailyfile)



