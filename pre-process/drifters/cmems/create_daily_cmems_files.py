#!/usr/bin/env python

########################################################################################
#create_daily_cmems_files.py - This script takes patricia's daily file and converts it 
#into a daily version in the drift-tool format (one file per drifter per day). 
#The datadir is passed in as a command line arguement and the data is saved to 
#/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files 
#(this should be updated in the future to a directory that isn't in my user account)
########################################################################################

import os
import glob
import shutil
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt
import wget
import geopy
import geopy.distance
import argparse

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

#add the datadir as a command line arguement
parser = argparse.ArgumentParser()
parser.add_argument("datadir", help="echo the string you use here")
args = parser.parse_args()
datadir = args.datadir

#the codedir is where the wmo file will be saved
codedir = '/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems'
#tempdir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_test/daily_cmems_files/testcase/'
tempdir = '/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_test/daily_cmems_files/testcase/'
#tempdir = '/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/cmems_test/daily_cmems_files/testcase/'
#tempdir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files/'

#check for non-unique buoyids
buoylist = []
for uf in glob.glob(os.path.join(datadir,'*.nc')):
    buoylist.append(uf)
if len(buoylist) != len(np.unique(buoylist)):
    print('Stop! Error time!')
    print('There is a non-unique wmo idea in the buoylist')
    exit()

#Download the most recent list of wmo numbers
print('checking for todays wmo_list.txt')
output = os.path.join(codedir,'wmo_list.txt') 
url = 'ftp://ftp.jcommops.org/JCOMMOPS/GTS/wmo/wmo_list.txt' 
#check if the file was already downloaded today. If not, grab it.
today = dt.datetime.now().date()
#include a fall back in case the wmo file has been deleted
try:
    filetime = dt.datetime.fromtimestamp(os.path.getctime(output))
except:
    filetime = None
#determine if we need to grab a new file
if filetime is None or filetime.date() != today:
    print('grabbing the most recent wmo_list.txt')
    getfile = wget.download(url,output)
    # Overwrite file if already exists (should be yesterday's file)
    if os.path.exists(output):
        shutil.move(getfile,output)
    print()
else:
    print('already have todays list - skipping download')

#read in the WMO table
dbcpop = pd.read_csv(os.path.join(codedir,'wmo_list.txt'), skiprows=1, delimiter=";")

#for each file in the datadir
for fname in glob.glob(os.path.join(datadir,'*.nc')):

    print(fname)    
    ds = xr.open_dataset(fname)
    #find the matching buoyid from the wmo list
    extra_meta = dbcpop.loc[dbcpop['WMO'] == float(ds.attrs['wmo_platform_code'])]
    #ds.attrs['buoyid'] = 'db' + ds.attrs['platform_wmo_code']

    #if there's no entries in the wmo list, add 'unknown' as the wmo buoy type
    if extra_meta.empty == True:
        ds.attrs['wmo_type'] = 'unknown'
    else:
        ds.attrs['wmo_type'] = extra_meta.iloc[0]['PTFM TYPE']
    
    #if the platform type is missing from the original netcdf, add 'unknown'
    if ('platform_type' not in ds.attrs) or (ds.attrs['platform_type'] == ''): 
        ds.attrs['platform_type'] = 'unknown'

    #if the wmo files says it's an SVP, add that to the attrs
    if ('SVP' in ds.attrs['wmo_type']): 
        ds.attrs['platform_type'] = 'SVP'
   
    #the following could be cleaned up, but I'm keeping it around in case I want 
    #to start writing DB and SVP to separate folders again. 
    if ('SVP' in ds.attrs['platform_type']) or ('SVP' in ds.attrs['wmo_type']):
        #write to netcdf file in one folder
        subdir = 'SVP'
    else:
        #write to netcdf file in a different folder
        subdir = 'DB'
        continue

    #determine some dates 
    launchdate = pd.to_datetime(str(ds.TIME.values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(ds.TIME.values[0])).strftime('%Y-%m-%d %H:%M:%S')
    lasttime = pd.to_datetime(str(ds.TIME.values[-1])).strftime('%Y-%m-%d %H:%M:%S')

    #######################################################################################################

    #QC
    df = pd.DataFrame()    
    #convert each variable to a pandas series, then reset the index on each before adding to the dataframe. 
    #It is not possible to carry the temperature forward this way because it has multiple depths. I might need
    #to revisit this later to add the temperature at each layer as well.
    df['TIME'] = ds.TIME.to_series().reset_index(drop=True)
    df['LATITUDE'] = ds.LATITUDE.to_series().reset_index(drop=True)
    df['LONGITUDE'] = ds.LONGITUDE.to_series().reset_index(drop=True)
    df['TIME_QC'] = ds.TIME_QC.to_series().reset_index(drop=True)
    df['TIME_QC'] = df['TIME_QC'].astype('int')
    df['POSITION_QC'] = ds.POSITION_QC.to_series().reset_index(drop=True)
    df['POSITION_QC'] = df['POSITION_QC'].astype('int')

    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    df.drop_duplicates(subset=['time'], keep='first', inplace=True)

    #remove any values where the cmems QC flag isn't 1
    df.where((df['POSITION_QC']==1) & (df['TIME_QC']==1), inplace=True)
    df.dropna(inplace=True)
    df.drop('POSITION_QC', axis=1, inplace=True)
    df.drop('TIME_QC', axis=1, inplace=True)

    # Dropping bad points
    changed = True
    std = None
    mean = None
    while changed:
        df.reset_index(inplace=True)
        df.drop('index', axis=1, inplace=True)

        # Get geopy points for each lat,lon pair
        points = df[
            ['LATITUDE', 'LONGITUDE']
        ].apply(lambda x: geopy.Point(x[0], x[1]), axis=1)

        # get distances in nautical miles
        ends = points.shift(1)
        distances = []
        for idx, start in enumerate(points):
            try:
                distances.append(geopy.distance.distance(start, ends[idx]).nm)
            except ValueError:
                distances.append(np.nan)

        distances = np.ma.masked_invalid(distances)

        # get the time difference in hours
        times = df['time'].diff() / 3600.0

        # calculate speed in knots
        speed = distances / times

        # Drop anything where speed is 3 standard deviations from the mean and is > 10 knots
        if std is None:
            std = np.std(speed)
            mean = np.mean(speed)

        si = np.where((abs(speed - mean) > 3 * std) & (speed > 10))[0]

        if len(si) > 0:
            df.drop(points.index[si[0]], inplace=True)
            print("\tDropping point with speed=%0.1f knots" % speed[si[0]])
        else:
            changed = False

        del si

    df.drop('time', axis=1, inplace=True)
    df.set_index('TIME',inplace=True)
    df.drop('level_0', axis=1, inplace=True)

    '''
    #remove any values where the cmems QC flag isn't 1
    df.where((df['POSITION_QC']==1) & (df['TIME_QC']==1), inplace=True)
    df.dropna(inplace=True)
    '''
    #######################################################################################################

    #keep the old attributes from the un-qc'ed ds:
    attrsds = ds


    #convert the qc'ed data back into a new dataset
    qcds = xr.Dataset.from_dataframe(df)

    #rename the dataset for convienience
    ds = qcds
    #ds.rename({'TIME':'time'},inplace=True)
    #re-add some (but not all!) of the attrs from the original dataset
    #ds.attrs = attrsds.attrs
    ds.attrs['platform_code'] = attrsds.attrs['platform_code']
    ds.attrs['wmo_platform_code'] = attrsds.attrs['wmo_platform_code']
    ds.attrs['coriolis_platform_code'] = attrsds.attrs['coriolis_platform_code']
    ds.attrs['platform_name'] = attrsds.attrs['platform_name']
    ds.attrs['source'] = attrsds.attrs['source']
    ds.attrs['source_platform_category_code'] = attrsds.attrs['source_platform_category_code']
    ds.attrs['references'] = attrsds.attrs['references']
    ds.attrs['id'] = attrsds.attrs['id']
    ds.attrs['cdm_data_type'] = attrsds.attrs['cdm_data_type']
    ds.attrs['family_label'] = attrsds.attrs['family_label']
    ds.attrs['family_code'] = attrsds.attrs['family_code']
    ds.attrs['area'] = attrsds.attrs['area']
    ds.attrs['geospatial_lat_min'] = attrsds.attrs['geospatial_lat_min']
    ds.attrs['geospatial_lat_max'] = attrsds.attrs['geospatial_lat_max']
    ds.attrs['geospatial_lon_min'] = attrsds.attrs['geospatial_lon_min']
    ds.attrs['geospatial_lon_max'] = attrsds.attrs['geospatial_lon_max']
    ds.attrs['time_coverage_start'] = attrsds.attrs['time_coverage_start']
    ds.attrs['time_coverage_end'] = attrsds.attrs['time_coverage_end']
    ds.attrs['contact'] = attrsds.attrs['contact']
    ds.attrs['author'] = attrsds.attrs['author']
    ds.attrs['data_assembly_center'] = attrsds.attrs['data_assembly_center']
    ds.attrs['pi_name'] = attrsds.attrs['pi_name']            
    ds.attrs['original_title'] = attrsds.attrs['title']
    ds.attrs['citation'] = attrsds.attrs['citation']
    ds.attrs['last_date_observation'] = attrsds.attrs['last_date_observation']
    ds.attrs['last_latitude_observation'] = attrsds.attrs['last_latitude_observation']
    ds.attrs['last_longitude_observation'] = attrsds.attrs['last_longitude_observation']
    ds.attrs['wmo_type'] = attrsds.attrs['wmo_type']
    ds.attrs['platform_type'] = attrsds.attrs['platform_type']
    
    #add new versions of some attributes instead of using originals
    #ds.attrs['netcdf_version'] = ""  #unnecessary
    ds.attrs['distribution_statement'] = "These data are public and free of charge. User assumes all risk for use of data. User must display cmems specific citation in any publication or product using data. User must contact PI prior to any commercial use of data." 
    ds.attrs['original_data_source'] = "These data were collected and made freely available by the International Data Buoy Cooperation Program and the national programs that contribute to it (http://www.jcommops.org/dbcp)."
    ds.attrs['data_description'] = "The data format of these files has been modified from daily drifting buoy trajectory files produced by Coriolis and Copernicus data provider. Daily time series have been merged to cover full available drifter track. Attributes have been modified from the original and may no longer meet Copernicus standards."
    ds.attrs['description'] = "CMEMS Drifting Buoys"                

    #add some extra attributes
    ds.attrs['buoyid'] = 'db' + ds.attrs['wmo_platform_code'] + 'D' + launchdate
    droguetype = ds.attrs['platform_type']
    #if droguetype not in droguedict.keys():
    if droguetype not in 'SVP':
        droguetype = 'unknown'
    ds.attrs['comment'] = 'modified netCDF file created by Fisheries and Oceans Canada, NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
    ds.attrs['qc_comment'] = 'qc has been provided by the data originator according to the standards listed on http://marine.copernicus.eu/services-portfolio/access-to-products/?option=com_csw&view=details&product_id=INSITU_GLO_NRT_OBSERVATIONS_013_030. For the present dataset, we have retained only data with a time and position qc flag of 1 (good data).'
    ds.attrs['original_data_source_comment'] = 'The original dataset is available for download at http://marine.copernicus.eu/services-portfolio/access-to-products/?option=com_csw&view=details&product_id=INSITU_GLO_NRT_OBSERVATIONS_013_030'
    ds.attrs['approximate_drogue_depth'] = str(15) #the drogue depth is 15 for SVPs 
    ds.attrs['model'] = ds.attrs['wmo_type']
    ds.attrs['type'] = ds.attrs['platform_type']
    
    #add attributes for the variables
    ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
    ds.LATITUDE.attrs['units'] = 'degrees_north'
    ds.LONGITUDE.attrs['long_name'] = 'Longitude of observed trajectory'
    ds.LONGITUDE.attrs['units'] = 'degrees_east'

    #write out the file
    #output_file = '{}.nc'.format(ds.attrs['buoyid'])
    output_file = '{}_daily.nc'.format(ds.attrs['buoyid'])
   
    #outdir = os.path.join(datadir, subdir) #to add an extra folder for non-SVPs
    outdir = os.path.join(tempdir, launchdate)
    if not os.path.exists(os.path.join(outdir)):
        os.makedirs(os.path.join(outdir))
    output_file=os.path.join(outdir, output_file)

    ds.to_netcdf(output_file, unlimited_dims=['TIME'])
