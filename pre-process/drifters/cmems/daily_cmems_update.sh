#!/bin/bash

#######################################################################################################
#daily_cmems_update.sh
#
#This script handles running the above scripts as part of a daily cron job. It uses 'yesterday' as the 
#download date since the source files arrive each day for the previous day. It has some checks to be 
#sure that all steps are running correctly before moving to the next step and it writes out log files 
#to a log directory specified in the script. It also checks if the end of the day has been reached 
#without a successful run. If so, it makes a backup of the master files for that day and sends an 
#error email.
#######################################################################################################

download_date=$(date --date 'yesterday' "+%Y%m%d")
hour=$(date +%H)

basepath_new="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/"
PATH=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/sitestore/miniconda/bin:$PATH


codedir="/fs/vnas_Hdfo/dpnm/jeh326/drifters/drift-analysis/pre-process/drifters/cmems"
sdfofiledir="/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/CMEMS/"
mainfiledir=${basepath_new}"/cmems_dataset/"

dailyfiledir="${mainfiledir}/daily_cmems_files/"
logdir="${mainfiledir}/text_output_files/"

outputfile_daily="${logdir}/cron-status-daily_${download_date}.txt"
outputfile_combine="${logdir}/cron-status-combine_${download_date}.txt"
daily_script_log="${logdir}/dailyfiles_${download_date}.txt"
combine_script_log="${logdir}/combinefiles_${download_date}.txt"
update_log="${logdir}/cmems_successful_updates.txt"
data_download_status="/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/cmems/cmems_cron_logfiles/cron-download-check-20191023.txt"

cronrundate=$(date --date 'today' "+%Y-%m-%d %H:%M:%S")

echo "the cron job last ran at ${cronrundate}" > ${logdir}/runcheck_log.txt

#######################################################################################################
#Check if the whole process already completed successfully for the day or alternately if it looks like
#it is going to be a failure
#######################################################################################################

#check if it's later than 10pm. If it is, implement this emergency fall back to save me since my 
#scripts do not yet handle data gaps

function night_check {
    if [ "$hour" -ge 22 ]; then
    
        #it is after 10pm. If the backup folder exists, exit. If the backup folder does not already exist, 
        #create one, email me a warning and exit.
        for f in ${mainfiledir}/master_files_emergencybackup_${download_date}_*; do
            #if the backup folder has already been made, no need to do anything
            if [ -e "$f" ]; then 
                exit
            else
                cp -r ${mainfiledir}/master_files ${mainfiledir}/master_files_emergencybackup_${download_date}_${hour}
                echo "failed to add cmems daily files for ${download_date} to master on ${cronrundate}" | mail -s "cmems daily cron failure" jennifer.holden@dfo-mpo.gc.ca
                echo "failed to add cmems daily files for ${download_date} to master on ${cronrundate}" >> ${update_log}
                exit
            fi
        done

    fi
}

#if it's earlier than 10pm, check if the process has already run successfully
if [ -f "${outputfile_combine}" ]; then
    combinevar=$(<${outputfile_combine})
    if [ "${combinevar}" -eq "0" ]; then
        #if file has a successful exit code, nothing left to do since combine_cmems_files.py has already successfully run
        exit
    else
        #unsuccessful error code. Either exit due to the night check or continue to main script
        night_check
    fi

else
    #the file doesn't exit. 
    night_check
fi


#######################################################################################################
#Check to be sure that the files got copied to the sdfo000 account
#######################################################################################################

#if the cron job to download the data didn't work, don't bother to move on
if [ -f "${data_download_status}" ]; then
    datadownloadvar=$(<${data_download_status})
    if [ "${datadownloadvar}" -gt "0" ]; then
        #if file has an unsuccessful exit code, nothing left to do since the raw data shouldn't yet exist
        exit
    fi
fi

#This check is redundant, since I'm now looking at the exit status from the download. But, it does no harm
#to leave it and it may catch a case that I'm missing. Checks for the presence of files in the sdfo000 account
#Theoretically, if there is no data on the cmems ftp to download, the wget would still be successful without
#downloading anything.
if [ ! -d "${sdfofiledir}/${download_date}" ]; then
    echo "received successful download exit code but no raw data in sdfo000 folder. Unable to add cmems daily files for ${download_date} to master on ${cronrundate}. Error sent from line 98 of daily_cmems_update.sh" | mail -s "cmems daily download failure" jennifer.holden@dfo-mpo.gc.ca
    exit
fi

#######################################################################################################
#check for the daily output file:
#######################################################################################################

if [ -f "${outputfile_daily}" ]; then

    dailyvar=$(<${outputfile_daily})

    #if the file has a successful exit code, move to the next step. Otherwise:
    if [ "${dailyvar}" -gt "0" ]; then
        #if the exit code is not 0, re-run the script
        ${codedir}/cron_create_daily_cmems_files.py ${sdfofiledir}/${download_date} > ${daily_script_log} 2>&1
        echo "$?" > ${outputfile_daily}
    fi

else
    #if no output file is found, the script hasn't run yet, but should be run now
    ${codedir}/cron_create_daily_cmems_files.py ${sdfofiledir}/${download_date} > ${daily_script_log} 2>&1
    echo "$?" > ${outputfile_daily}

fi

#######################################################################################################
#check to be sure that the daily files exist. Should only get to this point in the daily script runs,
#but fails to complete successfully
#######################################################################################################

#if the folder exists but is empty, exit because there are no daily files
#if the folder exists and has files, move on to the next step
#if the folder doesn't exist, exit since there are no files to add

#if the folder exists:
if [ -d "${dailyfiledir}/${download_date}" ]; then          
    if [ -z "$(ls -A ${dailyfiledir}/${download_date})" ]; then 
        #if the folder is empty
        exit
    fi
else                                                            
    #if the folder doesn't exist    
    exit
fi

#extra check to be sure that the daily files weren't only partially created
#check the log file again to be sure that the daily files were created successfully
if [ -f "${outputfile_daily}" ]; then
    dailyvar=$(<${outputfile_daily})
    #if the file has the correct exit code, move to the next step. Otherwise, exit and wait until the cron job
    #runs again?
    if [ "${dailyvar}" -gt "0" ]; then
        exit
    fi
fi

#######################################################################################################
#if the daily files exist, add them to the master files
#if combining the master files worked, exit (ie, if the error code is >0)
#######################################################################################################

#make a temporary copy of the master directory, just in case the combine is unsuccessful 
for f in ${mainfiledir}/master_files_${download_date}_*; do
    if [ ! -e "$f" ]; then
        cp -r ${mainfiledir}/master_files ${mainfiledir}/master_files_${download_date}_${hour}
    fi    
done

#attempt and hopefully succeed to append the files to the master files
if [ -f "${outputfile_combine}" ]; then
    combinevar=$(<${outputfile_combine})
    if [ "${combinevar}" -eq "0" ]; then #redundant check?
        #if file has a successful exit code, nothing left to do since combine_cmems_files.py has already successfully run
        exit
    else
        #if the exit code is not 0, re-run the script
        ${codedir}/combine_cmemsdata_by_drifter.py ${dailyfiledir}/${download_date} > ${combine_script_log} 2>&1
        echo "$?" > ${outputfile_combine}
    fi

else
    #if no output file is found, the script hasn't run yet, but should be run now
    ${codedir}/combine_cmemsdata_by_drifter.py ${dailyfiledir}/${download_date} > ${combine_script_log} 2>&1
    echo "$?" > ${outputfile_combine}

fi

#if the combine step completed successfully, no point in keeping the backup directory
if [ -f "${outputfile_combine}" ]; then
    combinevar=$(<${outputfile_combine})
    if [ "${combinevar}" -eq "0" ]; then
        #if file has a successful exit code, nothing left to do since combine_cmems_files.py has already successfully run
        rm -r ${mainfiledir}/master_files_${download_date}_${hour}
        echo "cmems files for ${download_date} appended successfully at ${cronrundate}" >> ${update_log}
    fi
fi


#clean up the logfiles after the day has changed
clean_up_date=$(date --date '-2 day' "+%Y%m%d")
mv ${logdir}/*${clean_up_date}.txt ${logdir}/old_logs
#echo "moving ${logdir}/*${clean_up_date}.txt ${logdir}/old_logs"
