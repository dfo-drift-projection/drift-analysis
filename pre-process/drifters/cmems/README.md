The scripts in this folder can be used to convert the cmems files that Patricia is downloading daily on the GPSC into the per-drifter files that the drift-tool requires.

Main Scripts:

-create_daily_cmems_files.py - This script takes patricia's daily file and converts it into a daily version in the drift-tool format (one file per drifter per day). The datadir is passed in as a command line arguement and the data is saved to /gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files (this should be updated in the future to a directory that isn't in my user account)

-combine_cmemsdata_by_drifter.py - This script takes the drift-tool daily files and appends them to the previously created master files to create one file per drifter for the entire track. The datadir is passed in as a command line arguement and the data is saved to /gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files/master_files/ (this folder should be updated in the future to a directory that isn't in my user account). There is a huge flaw in this script currently, since it doesn't yet handle the case where data needs to be added to the middle of the files. It will run fine as long as new data is being appended to the end of the files, but breaks down if data needs to be slotted into the middle instead.

-daily_cmems_update.sh - This script handles running the above scripts as part of a daily cron job. It uses 'yesterday' as the download date since the source files arrive each day for the previous day. It has some checks to be sure that all steps are running correctly before moving to the next step and it writes out log files to a log directory specified in the script. It also checks if the end of the day has been reached without a successful run. If so, it makes a backup of the master files for that day and sends an error email.


Scripts used to process the backlog of data before we started running the cron job:

-cycle_through_folders.sh

-combine_cycle_though_folders.sh

These scripts both just loop over the main scripts to run though the backlog of folders.


A couple of useful bash short cut scripts:

-get_wmo_table.sh

-remove_ext.sh - this just removes _daily from filenames

-make_daily_files.job*

-rwxr-xr-x 1 jeh326 dfo_odis 7.1K Oct  8 12:50 daily_cmems_update.py*

