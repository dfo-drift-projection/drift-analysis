#!/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/sitestore/miniconda/envs/opendrift_update/bin/python

import os
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt

import geopy
import geopy.distance

#user_installed_python_path="/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/"
os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/sitestore/miniconda/share/proj"

'''
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap
'''

###################################################
#
###################################################

def gather_filelist(datatype, datadir, datafile=None):
    if datatype == 'wp':
        filelist = gather_wp_filelist(datadir, datafile=datafile)
    elif datatype == 'wptemp':
        filelist = gather_wptemp_filelist(datadir, datafile=datafile)
    else:
        print('Unknown raw data type. Can not convert to netcdf')
        exit()
    return filelist

def gather_wp_filelist(datadir, datafile=None):
    if datafile is None:
        filelist = []
        for fname in glob.glob(os.path.join(datadir,'*.drf')):
            filelist.append(fname)
    else:
        filelist = [os.path.join(datadir,datafile)]
    return filelist

def gather_wptemp_filelist(datadir, datafile=None):
    if datafile is None:
        filelist = []
        for fname in glob.glob(os.path.join(datadir,'*.csv')):
            filelist.append(fname)
    else:
        filelist = [os.path.join(datadir,datafile)]
    return filelist


#####################################################################
def read_raw_data(datatype, fname, droguedict):
    if datatype == 'wp':
        #maybe split this into read header and read data?
        df, hdrdict, buoyid = wp_datareader(fname,droguedict)
    elif datatype == 'wptemp':
        df, hdrdict, buoyid = wptemp_datareader(fname,droguedict)
    else:
        print('Unknown raw data type. Can not convert to netcdf')
        exit()
    return df, hdrdict, buoyid

#maybe also make a wp_hdrreader?
def wp_datareader(fname, droguedict):
    
    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0].split('_')[0]
        #bname = os.path.basename(fname).split('.')[0].split('_')[1]
        endrow = 0
        lines = []
        hdrd = {}
        table = {}
        intable = False

        #deal with the header
        for line in fp:
            if "*END OF HEADER" in line:
                endrow += 1
                break
            else:
                endrow +=1
                #write all the header lines to a string
                lines.append(line.replace('\n','').lstrip().rstrip())
 
                #keep the useful values in a dictionary
                if '$TABLE' in line:
                    intable=line.split(":")[1].lstrip().rstrip()
                    table[intable] = []
                    continue

                if '$END' in line:
                    intable = False

                if intable != False:

                    if line.split()[0].rstrip().lstrip().isdigit() == True:
                        table[intable].append(line.split()[1])
                    else:
                        continue

                if ":" in line:
                    keeplist = ["TYPE", "MODEL", "SERIAL NUMBER", "IMEI", 
                                "SPOT ESN", "AGENCY", "DATA DESCRIPTION", 
                                "NUMBER OF CHANNELS", "START TIME", "END TIME"]
                    if any(word in line for word in keeplist):
                        key = line.split(":",1)[0].rstrip().lstrip()
                        val = line.split(":",1)[1].rstrip().lstrip().replace('\n','')
                        hdrd[key] = val    

    hdrd['DROGUE_DEPTH'] = droguedict[hdrd['MODEL']]

    #read in the data
    df = pd.read_csv(fname, 
            skiprows=endrow, 
            names=table['CHANNELS'], 
            skip_blank_lines=False, 
            delimiter=r"\s+")

    #arrange the data for xarray
    df['DateTime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    launchtime = str(df['DateTime'][0])
    df.drop(labels=['Date', 'Time', 'Record_Number'], axis=1, inplace=True)
    #df['DateTime'] = (df['DateTime'] - df['DateTime'][0]).apply(lambda x: x.total_seconds())
    df.rename(columns={"DateTime":"TIME", "Latitude":"LATITUDE", "Longitude":"LONGITUDE"},inplace=True)

    #decide on a buoyid
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    if 'SPOT ESN' in hdrd.keys():
        buoyid = 'wp' + hdrd['SERIAL NUMBER'] + hdrd['SPOT ESN'] + 'D' + launchdate  
        #note: had to combine 'SPOT ESN' and 'SERIAL NUMBER' in the buoyid to remove dups 
    elif 'OSKER IMEI' in hdrd.keys():
        buoyid = 'wp' + hdrd['OSKER IMEI'] + 'D' + launchdate 
    else:
        print('no buoyid?')
        exit()
    print(os.path.basename(fname))
    hdrdict = {
                'source':str(hdrd['AGENCY']),
                'contact':'Roy.Hourston@dfo-mpo.gc.ca',
                'platform_type':'Drifting Buoy',
                'model':str(hdrd['MODEL']),
                'buoyid':str(buoyid),
                'qc_comment':'unspecified qc performed by the data provider',
                'nafc_qc_comment':'Additional quality control provided by NAFC Drift Group. '
                            'Tests include removal of points with physically unrealistic '
                            'speeds and removal of points on land. Tracks with time gaps '
                            'longer than 24 hours have been split into multiple tracks.',
                'original_data_source_comment':'The original dataset is available for download at waterproperties.ca',
                'approximate_drogue_depth':str(hdrd['DROGUE_DEPTH']), #str(15)
                'brand':str(hdrd['TYPE']),
                'serial_number':str(hdrd['SERIAL NUMBER']),
                'original_filename':os.path.basename(fname),
                'description':'Drifting Buoys',
                'comment':'modified netCDF file created by Fisheries and '
                            'Oceans Canada, NAFC Drift Group on ' + 
                            dt.datetime.today().strftime('%Y-%m-%d') +
                            '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                            'more details.',
                'launchdate':str(firsttime)
                }

                #'raw_track_start_time':hdrd['START TIME']
                #                        .replace('UTC ','').replace('/','-'),
                #'raw_track_end_time':hdrd['END TIME']
                #                        .replace('UTC ','').replace('/','-'),
                #'launchdate':firsttime
                #}

    if 'SPOT ESN' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['SPOT ESN']
        hdrdict['reference_number_type'] = 'SPOT ESN'
    elif 'OSKER IMEI' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['OSKER IMEI']
        hdrdict['reference_number_type'] = 'OSKER IMEI'
    else:
        hdrdict['reference_number'] = 'unknown'
        hdrdict['reference_number_type'] = 'unknown'

    #for k in hdrdict.keys():
    #    print(k,': ',hdrdict[k])
       
    return df, hdrdict, buoyid

def wptemp_datareader(fname, droguedict):

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]
        hdrd = {}

    hdrd['DROGUE_DEPTH'] = str(0) 

    #read in the data
    df = pd.read_csv(fname,
            skiprows=1,
            names=['Events','Date','Address','LatLng','Speed','Heading','Altitude','Via'],
            skip_blank_lines=False,
            delimiter=r",")

    #arrange the data for xarray

    df[['Date','Time']] = df.Date.str.split(" ",expand=True,)
    df['DateTime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    launchtime = str(df['DateTime'][0])

    df[['LATITUDE','LONGITUDE']] = df.LatLng.str.split(",",expand=True,)
    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    df.drop(labels=['Events', 'Address', 'Speed', 'Heading', 'Altitude', 'Via', 'LatLng', 'Date', 'Time'], axis=1, inplace=True)
    df.rename(columns={"DateTime":"TIME"},inplace=True)


    #decide on a buoyid
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    buoyid = bname
    hdrdict = {
                'platform_type':'Drifting Buoy',
                'buoyid':str(buoyid),
                'nafc_qc_comment':'Additional quality control provided by NAFC Drift Group. '
                            'Tests include removal of points with physically unrealistic '
                            'speeds and removal of points on land. Tracks with time gaps '
                            'longer than 24 hours have been split into multiple tracks.',
                'original_data_source_comment':'The original dataset is available for download at waterproperties.ca',
                'approximate_drogue_depth':str(hdrd['DROGUE_DEPTH']), #str(15)
                'original_filename':os.path.basename(fname),
                'description':'Drifting Buoys',
                'comment':'modified netCDF file created by Fisheries and '
                            'Oceans Canada, NAFC Drift Group on ' +
                            dt.datetime.today().strftime('%Y-%m-%d') +
                            '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                            'more details.',
                'launchdate':str(firsttime)
                }

    return df, hdrdict, buoyid



###################################################
#
###################################################

#need to add if buoylist is None to the arguement list 
def dup_buoyid_test(buoyid, buoylist):
    #to use as a check later
    if buoyid in buoylist:
        print('')
        print('Warning! There is a duplicate buoyid!')
        print(buoyid, fname)
        print('exiting because this really should be fixed!')
        exit()
    else:
        buoylist.append(buoyid)
    return buoylist
        
#realistic times (make sure times are increasing monotonically, no odd values)
#split on large time gaps
#test if on land. how?
#should probably add a plot to visually check

############################################################################

def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


def impossible_coordinate_test(df):
    df.loc[(df['LATITUDE'] > 90), 'LATITUDE'] = np.nan
    df.loc[(df['LATITUDE'] < -90), 'LATITUDE'] = np.nan
    df.loc[(df['LONGITUDE'] < -360), 'LONGITUDE'] = np.nan
    df.loc[(df['LONGITUDE'] > 360), 'LONGITUDE'] = np.nan
    df['LONGITUDE'] = wrap_to_180(df['LONGITUDE'])
  
    #drop rows where the lats or lons are set to NaN
    df.dropna(inplace=True)

    return df
############################################################################

def impossible_time_test(df, hdrdict):
   
    #This should be done per point, but right now it just checks that the min
    #and max points are realistic.
    #hdr_starttime = hdrdict['raw_track_start_time']
    #hdr_endtime = hdrdict['raw_track_end_time']

    #add a column (time) with seconds since start and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    df.drop_duplicates(subset=['time'], keep='first', inplace=True)

    #check if min/max data times match header start/end times
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    lasttime = pd.to_datetime(str(df['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')

    #hdr_starttime_num = dt.datetime.strptime(hdr_starttime, '%Y-%m-%d %H:%M:%S.%f')
    #hdr_endtime_num = dt.datetime.strptime(hdr_endtime, '%Y-%m-%d %H:%M:%S.%f')
    firsttime_num = dt.datetime.strptime(firsttime, '%Y-%m-%d %H:%M:%S')
    lasttime_num = dt.datetime.strptime(lasttime, '%Y-%m-%d %H:%M:%S')

    #startdiff = (hdr_starttime_num.date() - firsttime_num.date())
    #enddiff = (hdr_endtime_num.date() - lasttime_num.date())
    seconds_in_day = 24 * 60 * 60
    #diff_start_minutes,diff_start_secs = divmod(startdiff.days * seconds_in_day + startdiff.seconds, 60)
    #diff_end_minutes,diff_end_secs = divmod(enddiff.days * seconds_in_day + enddiff.seconds, 60)
  
    '''
    if diff_start_minutes != 0:
        print('Something wrong with the start date. Exiting.')
        print('....header start date = ', hdr_starttime)
        print('....data start date = ', firsttime)
        exit()
    if diff_end_minutes != 0:
        print('Something wrong with the data end date. Exiting.') 
        print('....header end date = ', hdr_endtime)
        print('....data end date = ', lasttime)
        exit()
    '''
  
    #check for future times?
    todaysdate = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    todaysdate_num = dt.datetime.strptime(todaysdate, '%Y-%m-%d %H:%M:%S')
    if todaysdate_num.date() < firsttime_num.date():
        print('The start date is later than today! Exiting.')
        print('....today = ', todaysdate)
        print('....data start date = ', firsttime)
        exit()
    if todaysdate_num.date() < lasttime_num.date():
        print('The end date is later than today! Exiting.')
        print('....today = ', todaysdate)
        print('....data end date = ', lasttime)
        exit()

    #check for dates earlier than 1970?
    sfr = '1970-01-01 00:00:00'
    sfr_num = dt.datetime.strptime(sfr, '%Y-%m-%d %H:%M:%S')
    if sfr_num.date() > firsttime_num.date():
        print('The start date is earlier than ',sfr,'! Unlikely, so exiting.')
        print('....data start date = ', firsttime)
        exit()
    if sfr_num.date() > lasttime_num.date():
        print('The end date is earlier than ',sfr,'! Unlikely, so exiting.')
        print('....data end date = ', lasttime)
        exit()
    df.drop('level_0', axis=1, inplace=True)
    df.drop('time', axis=1, inplace=True)

    return df
    #not returning anything right now. If there's a bad point, the program exits

def impossible_speed_test(df): 
 
    #manipulate the time and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    df.drop_duplicates(subset=['time'], keep='first', inplace=True)

    #determine some dates
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    lasttime = pd.to_datetime(str(df['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')

    # Dropping bad speed points
    changed = True
    std = None
    mean = None

    while changed:
        df.reset_index(inplace=True)
        df.drop('index', axis=1, inplace=True)

        # Get geopy points for each lat,lon pair
        points = df[
            ['LATITUDE', 'LONGITUDE']
        ].apply(lambda x: geopy.Point(x[0], x[1]), axis=1)

        # get distances in nautical miles
        ends = points.shift(1)
        distances = []

        for idx, start in enumerate(points):
            try:
                distances.append(geopy.distance.distance(start, ends[idx]).nm)
            except ValueError:
                distances.append(np.nan)

        #distances = np.ma.masked_invalid(distances)

        # get the time difference in hours
        times = df['time'].diff() / 3600.0
        #times = np.ma.masked_invalid(times)

        # calculate speed in knots
        speed = distances / times

        # Drop anything where speed is 3 standard deviations from the mean and is > 10 knots
        if std is None:
            std = np.std(speed)
            mean = np.mean(speed)

        #np.abs throws an error when trying to do np.abs([nan]) - I'm surpressing the error here.
        with np.errstate(invalid='ignore'):
            si = np.where((np.abs(speed - mean) > (3 * std)) & (speed > 10))[0]

        if len(si) > 0:
            #print('        ',df['TIME'].iloc[points.index[si[0]]-1],
            #        df['LATITUDE'].iloc[points.index[si[0]]-1],df['LONGITUDE'].iloc[points.index[si[0]]-1])
            #print('        ',df['TIME'].iloc[points.index[si[0]]],
            #        df['LATITUDE'].iloc[points.index[si[0]]],df['LONGITUDE'].iloc[points.index[si[0]]])
            #print('        ',df['TIME'].iloc[points.index[si[0]]+1],
            #        df['LATITUDE'].iloc[points.index[si[0]]+1],df['LONGITUDE'].iloc[points.index[si[0]]+1])
            df.drop(points.index[si[0]], inplace=True)
            print("\tDropping point with speed=%0.1f knots" % speed[si[0]])
        else:
            changed = False

        del si

    #clean up the un-needed QC variables
    df.drop('level_0', axis=1, inplace=True)
    df.drop('time', axis=1, inplace=True)
    df.set_index('TIME',inplace=True)

    return df

'''
def plot_track_test(df, metadata, datadir):

    print('........plotting drifter track')

    #for k in metadata:
    #    print(k,metadata[k])
    
    minlons = min(df['LONGITUDE']) - 1 
    maxlons = max(df['LONGITUDE']) + 1
    minlats = min(df['LATITUDE']) - 1
    maxlats = max(df['LATITUDE']) + 1

    #add the track to the map
    plt.figure(1)

    #set up the basemap
    map = Basemap(projection='merc', lat_0 = 57, lon_0 = -135, lat_ts = 57, 
                    resolution = 'l', area_thresh = 0.1, llcrnrlon=minlons, 
                    llcrnrlat=minlats, urcrnrlon=maxlons, urcrnrlat=maxlats)
    #map.etopo()
    #map.bluemarble()
    #map.drawcoastlines()
    map.fillcontinents(color = 'gray')
    #map.drawmapboundary()

    ## this needs to be modified before it's included. Maybe save the pickles somewhere common?
    ##import the ciopse domain pickle
    #import pickle
    #load_from_pickle='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/boundary-handling/domains/ciopsw_domain.pickle'
    #with open(load_from_pickle, 'rb') as f:
    #    domainspecs = pickle.load(f)
    #
    ##add the boundary
    #x_left,y_left = map(domainspecs['left_lon'], domainspecs['left_lat'])
    #map.plot(x_left, y_left, linewidth=0.5, color='r')
    #x_right,y_right = map(domainspecs['right_lon'], domainspecs['right_lat'])
    ##map.plot(x_right, y_right, 'o', markersize=1, color='r')
    #map.plot(x_right, y_right, linewidth=0.5,  color='r')
    #x_top,y_top = map(domainspecs['top_lon'], domainspecs['top_lat'])
    #map.plot(x_top, y_top, linewidth=0.5,  color='r')
    #x_bottom,y_bottom = map(domainspecs['bottom_lon'], domainspecs['bottom_lat'])
    #map.plot(x_bottom, y_bottom, linewidth=0.5,  color='r')

    map.fillcontinents(color = 'gray')

    # draw parallels and meridians (label parallels on right and top, meridians on bottom and left)
    parallels = np.arange(0.,81,5.)
    # labels = [left,right,top,bottom]
    map.drawparallels(parallels,labels=[False,True,True,False])
    meridians = np.arange(10.,351.,5.)
    map.drawmeridians(meridians,labels=[True,False,False,True])

    df.reset_index(inplace=True)
    lats = df['LATITUDE'].tolist()
    lons = df['LONGITUDE'].tolist()
    times = df['TIME'].tolist()

    x,y = map(lons, lats)

    map.plot(x,y,'ko',markersize=2,fillstyle='full')
    map.plot(x[0],y[0],'ro',markersize=4,fillstyle='full')

    titlestr = metadata['buoyid'] + '\n start: ' + str(metadata['time_coverage_start']) + '\n end: ' + str(metadata['time_coverage_end'])
    plt.title(titlestr)

    savenamejpg = '{}.jpg'.format(metadata['buoyid'])
    outdirnamestr = 'netcdf_' + str(dt.datetime.today().strftime('%Y-%m-%d'))
    savedir = os.path.join(datadir, outdirnamestr, 'drifter_track_plots')
    if not os.path.exists(os.path.join(savedir)):
        os.makedirs(os.path.join(savedir))
    savepath=os.path.join(savedir, savenamejpg)
    plt.savefig(savepath,bbox_inches='tight',dpi=300)
    plt.close()
'''

###################################################
#
###################################################
def determine_track_length(df, hdrdict, datadir, plot=True, write=True):

    #manipulate the time and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)

    #Split the tracks if longer than 24 hours gap
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    times = df['time'].diff() / 3600.0
    df.drop('time', axis=1, inplace=True)
    extents = df.index[times > 24].tolist()

    # prepend 0 to that list
    extents.insert(0, 0)

    # append the total size (of the column) to the end of the list
    extents.append(times.size)

    # make a sliding window of 2 to move along that list, that'll be the indices of the slice of the dataframe to select.
    extent_pairs = list(zip(extents[0:-1], extents[1:]))
    if len(extent_pairs) != 1:
        print('....splitting file into', str(len(extent_pairs)),'files')

    #break the df into sub dataframes for each time window
    for pair in extent_pairs:

        if len(extent_pairs) != 1:
            print('......working on subfile')            

        subdf = df[slice(*pair)]
        print('........time range:',subdf['TIME'].iloc[0],' to ',subdf['TIME'].iloc[-1], '(', len(subdf['TIME']),'points included)')

        ##just to visually confirm the dataranges:
        #horizontal_stack = pd.concat([df['TIME'], subdf['TIME']], axis=1)
        #with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
        #    print(horizontal_stack)

        if len(subdf['TIME']) < 2:
            print('........The subdf does not contain at least 2 points. Erroneous time or short track, moving onto next subdf without writing file.')
            #print('original time range',np.min(df['TIME']),np.max(df['TIME']))
            #print('subdf time range   ',np.min(subdf['TIME']),np.max(subdf['TIME']))
            ##just to visually confirm the dataranges:
            #horizontal_stack = pd.concat([df['TIME'], subdf['TIME']], axis=1)
            #with pd.option_context('display.max_rows', None, 'display.max_columns', None):  # more options can be specified also
            #    print(horizontal_stack)
            #exit()
            continue

        launchdate = pd.to_datetime(str(subdf['TIME'].values[0])).strftime('%Y%m%d')
        firsttime = pd.to_datetime(str(subdf['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
        lasttime = pd.to_datetime(str(subdf['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')

        geospatial_lat_min = np.min(subdf['LATITUDE']) 
        geospatial_lat_max = np.max(subdf['LATITUDE'])
        geospatial_lon_min = np.min(subdf['LONGITUDE'])
        geospatial_lon_max = np.max(subdf['LONGITUDE'])

        time_coverage_start = np.min(subdf['TIME'])
        time_coverage_end = np.max(subdf['TIME'])
        first_date_observation = pd.to_datetime(str(subdf['TIME'].iloc[0])).strftime('%Y%m%d') 
        last_date_observation = pd.to_datetime(str(subdf['TIME'].iloc[-1])).strftime('%Y%m%d') 
        last_latitude_observation = subdf['LATITUDE'].iloc[-1]
        last_longitude_observation = subdf['LONGITUDE'].iloc[-1] 

        #print('........',hdrdict['buoyid'].split('D')[0] + 'D' + launchdate)
        #print('........time range:',subdf['TIME'].iloc[0],' to ',subdf['TIME'].iloc[-1], '(', len(subdf['TIME']),'points included)')

        modhdrdict = {
                    'launchdate': str(launchdate),
                    'buoyid': hdrdict['buoyid'].split('D')[0] + 'D' + str(launchdate),
                    'dataStartDate': str(firsttime),
                    'geospatial_lat_min': str(geospatial_lat_min),
                    'geospatial_lat_max': str(geospatial_lat_max),
                    'geospatial_lon_min': str(geospatial_lon_min),
                    'geospatial_lon_max': str(geospatial_lon_max),
                    'time_coverage_start': str(time_coverage_start),
                    'time_coverage_end': str(time_coverage_end),
                    'first_date_observation': str(first_date_observation),
                    'last_date_observation': str(last_date_observation),
                    'last_latitude_observation': str(last_latitude_observation),
                    'last_longitude_observation': str(last_longitude_observation)
                    }


        '''
        print()
        print('modhdrdict')
        print(modhdrdict)
        print()
        print('subdf.head()')
        print(subdf.head())
        print()
        print('datadir')
        print(datadir)
        print()
        print('output_ds')
        print(output_ds)
        print()
        print('hdrdict')
        print(hdrdict)
        exit()
        '''

        '''
        #TODO
        if plot:
            subdf.set_index('TIME',inplace=True)
            plot_track_test(subdf, modhdrdict, datadir)
        '''

        if write:
            subdf.set_index('TIME',inplace=True)
            output_ds = subdf.to_xarray()
            write_drifttool_netcdf(output_ds,hdrdict,modhdrdict,datadir)


def write_drifttool_netcdf(ds,hdrdict,modhdrdict,datadir):

    #add attributes for the variables
    ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
    ds.LATITUDE.attrs['units'] = 'degrees_north'
    ds.LATITUDE.attrs['_FillValue'] = ds.LATITUDE.dtype.type(np.nan)
    ds.LONGITUDE.attrs['long_name'] ='Longitude of observed trajectory'
    ds.LONGITUDE.attrs['units'] = 'degrees_east'
    ds.LONGITUDE.attrs['_FillValue'] = ds.LONGITUDE.dtype.type(np.nan)

    #ds.TIME.attrs['units'] = 'seconds since ' + modhdrdict['launchdate'] 
    #ds.TIME.attrs['units'] = 'seconds since ' + hdrdict['launchdate'] 
    #ds.TIME.attrs['calendar'] = "standard" ;

    #add some attributes from the header metadata
    for var in hdrdict.keys():
        ds.attrs[var] = str(hdrdict[var])

    #Add attributes that are created when the file is split (or not). 
    #If the file is split, this will overwrite the older launchdate and buoyid.
    for newvar in modhdrdict.keys():
        ds.attrs[newvar] = str(modhdrdict[newvar])

    #write out the file
    output_file = '{}.nc'.format(ds.attrs['buoyid'])
    outdirnamestr = 'netcdf_' + str(dt.datetime.today().strftime('%Y-%m-%d'))
    outdir = os.path.join(datadir, outdirnamestr)
    if not os.path.exists(os.path.join(outdir)):
        os.makedirs(os.path.join(outdir))
    output_file=os.path.join(outdir, output_file)
    print('........writing',output_file)
    ds.to_netcdf(output_file,unlimited_dims=['TIME'])






#############################################################################
#end of functions
#############################################################################

'''
###############################################
# sample wp data file
###############################################
#sample filename: sct0752_20170901_20170915.drf
#sample header

*2019/02/14 12:42:46.07
*IOS HEADER VERSION 2.0      2016/04/28 2016/06/13 MATLAB

*FILE
    START TIME          : UTC 2017/09/01 15:34:56.000
    END TIME            : UTC 2017/09/15 06:51:56.000
    NUMBER OF RECORDS   : 3846
    DATA DESCRIPTION    : Drifting Buoy
    NUMBER OF CHANNELS  : 5

    $TABLE: CHANNELS
    ! No Name            Units        Minimum      Maximum
    !--- --------------- ------------ ------------ ----------
       1 Record_Number   n/a          1            3846
       2 Date            YYYY/MM/DD   n/a          n/a
       3 Time            HH:MM:SS     n/a          n/a
       4 Latitude        degrees      47.96659     48.68913
       5 Longitude       degrees      -125.94722   -124.72263
    $END

    $TABLE: CHANNEL DETAIL
    ! No  Pad        Start  Width  Format      Type  Decimal_Places
    !---  ---------  -----  -----  ----------  ----  --------------
       1  ' '        ' '        8  F           R4      1
       2  ' '        ' '    ' '    YYYY/MM/DD  D     ' '
       3  ' '        ' '    ' '    HH:MM:SS    T     ' '
       4  999.00000  ' '       10  F           R4      5
       5  999.00000  ' '       11  F           R4      5
    $END

*ADMINISTRATION
    MISSION             : 2017-09
    AGENCY              : IOS, Ocean Sciences Division, Sidney, B.C.
    COUNTRY             : Canada
    PROJECT             : La Perouse
    SCIENTIST           : Robert M.
    PLATFORM            : John P. Tully

*LOCATION
    GEOGRAPHIC AREA     : North-East Pacific
    LATITUDE            :  47  57.99540 N  ! (deg min)
    LONGITUDE           : 125  56.83320 W  ! (deg min)
    LATITUDE 2          :  48  41.34780 N  ! (deg min)
    LONGITUDE 2         : 124  43.35780 W  ! (deg min)

*INSTRUMENT
    TYPE                : Oceanetic Measurement
    MODEL               : Surface Circulation Tracker
    SERIAL NUMBER       : 752
    SPOT TYPE           : SPOT Trace
    SPOT ESN            : 3106854

*HISTORY

    $TABLE: PROGRAMS
    !   Name    Vers  Date       Time     Recs In   Recs Out
    !   ------- ----  ---------- -------- --------- ---------
        CSV2IOS 1.0   2019/02/14 12:42:46      3846      3846
    $END
    $REMARKS
        -CSV2IOS processing: 2019/02/14 12:42:46
    $END

*END OF HEADER
'''

