#!/usr/bin/env python

import os
import glob
import xarray as xr
#import numpy as np
import pandas as pd
import numpy as np
import datetime as dt
import geopy
import geopy.distance

"""
1 : identification number from the Global Drifter Program (see deployment log file at http://www.aoml.noaa.gov/phod/dac/deployed.html)
2 : year
3 : month
4 : day
5 : hour of the day (UTC, 0-23)
6 : longitude (degree East)
7 : latitude (degree North)
8 : longitude error (10^-5 degree, missing value: Inf)
9 : latitude error (10^-5 degree, missing value: Inf)
10 : zonal velocity (meter per second)
11 : meridional velocity (meter per second, missing value: Inf)
12 : zonal velocity error (meter per second, missing value: Inf)
13 : meridional velocity error (meter per second)
14 : length of interpolating gap in hours; it is the difference between the time of the first GPS fix forward in time minus the time of the last GPS fix backward in time
15 :  drogue status; 1 = drogue on, 0 = drogue off
"""

datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/NOAA_Hourly/individualgps_1.03/'

for fname in glob.glob(os.path.join(datadir,'*')):
    
    with open(fname) as fp:
        bname = os.path.basename(fname) #.split('.')[0]
        #print(bname)

        #read in the data
        df = pd.read_csv(fname,
            names=['ID','year','month','day','hour_of_the_day','longitude','latitude','longitude_error','latitude_error','zonal_velocity','meridional_velocity','zonal_velocity_error','meridonional_velocity_error','length_of_interpolation_gap_in_hours','drogue_status'],
            skip_blank_lines=False,
            delim_whitespace=True)

        #df['time'] = df['year'].astype(str) + (df['month'].apply(lambda x: '{0:0>2}'.format(x))).astype(str) + (df['day'].apply(lambda x: '{0:0>2}'.format(x))).astype(str)
        df['y'] = df['year'].astype(str)
        df['m'] = (df['month'].apply(lambda x: '{0:0>2}'.format(x))).astype(str) 
        df['d'] = (df['day'].apply(lambda x: '{0:0>2}'.format(x))).astype(str)
        df['time'] = df[['y', 'm', 'd']].apply(lambda x: ''.join(x), axis=1)
        df['time'] = pd.to_datetime(df['time'])
        launchtime = str(df['time'][0].strftime('%Y%m%d'))
        df.drop(labels=['year','month','day','hour_of_the_day','longitude_error','latitude_error','zonal_velocity','meridional_velocity','zonal_velocity_error','meridonional_velocity_error'], axis=1, inplace=True)
        #df.drop(labels=['longitude','latitude','length_of_interpolation_gap_in_hours','drogue_status'], axis=1, inplace=True)
        df['time'] = (df['time'] - df['time'][0]).apply(lambda x: x.total_seconds())
        #df.rename(columns={"LATITUDE":"latitude", "LONGITUDE":"longitude"},inplace=True)
        df.sort_values('time', inplace=True)

        df.drop_duplicates(subset=['time'], keep='first', inplace=True)

        # Dropping bad points
        changed = True
        std = None
        mean = None
        while changed:
            df.reset_index(inplace=True)
            df.drop('index', axis=1, inplace=True)

            # Get geopy points for each lat,lon pair
            points = df[
                ['latitude', 'longitude']
            ].apply(lambda x: geopy.Point(x[0], x[1]), axis=1)

            # get distances in nautical miles
            ends = points.shift(1)
            distances = []
            for idx, start in enumerate(points):
                #distances.append(geopy.distance.vincenty(start, ends[idx]).nm)
                try:
                    distances.append(geopy.distance.distance(start, ends[idx]).nm)
                except ValueError:
                    distances.append(np.nan)

            distances = np.ma.masked_invalid(distances)

            # get the time difference in hours
            times = df['time'].diff() / 3600.0

            # calculate speed in knots
            speed = distances / times

            # Drop anything where speed is 3 standard deviations from the mean and is > 10 knots
            if std is None:
                std = np.std(speed)
                mean = np.mean(speed)

            si = np.where((abs(speed - mean) > 3 * std) & (speed > 10))[0]

            if len(si) > 0:
                df.drop(points.index[si[0]], inplace=True)
                print("\tDropping point with speed=%0.1f knots" % speed[si[0]])
            else:
                changed = False

            del si

        df.set_index('time',inplace=True)
        obs = df.to_xarray()

        presize = df.size

        df = df[df.drogue_status == 1] #drop rows if the drogue has been lost
        
        midsize = df.size

        df = df[df.length_of_interpolation_gap_in_hours < 1] #drop rows where interpolation gap is too big

        postsize = df.size

        print(bname, presize, midsize, postsize)

        # Create dataset to save in netcdf
        ds = xr.Dataset(
            coords={'time': obs['time']},
            data_vars={'latitude': ('time', obs['latitude']),
                    'longitude': ('time', obs['longitude']),})

        #add attributes for the variables
        ds.latitude.attrs['long_name'] = 'Latitude of observed trajectory'
        ds.latitude.attrs['units'] = 'degrees_north'
        #ds.obs_lat.attrs['_FillValue'] = ds.obs_lat.dtype.type(np.nan)
        ds.longitude.attrs['long_name'] = 'Longitude of observed trajectory'
        ds.longitude.attrs['units'] = 'degrees_east'
        #ds.obs_lon.attrs['_FillValue'] = ds.obs_lon.dtype.type(np.nan)
        ds.time.attrs['units'] = 'seconds since ' + str(launchtime)
        ds.time.attrs['calendar'] = "standard" ;

        ds.attrs['source'] = 'data retrieved from NOAA at  https://www.aoml.noaa.gov/phod/gdp/hourly_data.php'
        ds.attrs['description'] = 'Global Hourly Drifter Locations from surface drifters tracked by Argos or GPS.'
        ds.attrs['buoyid'] = 'noaah' + bname + 'D' + launchtime
        ds.attrs['comment'] = 'netCDF file created by NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
        ds.attrs['approximate_drogue_depth'] = 15
        ds.attrs['model'] = 'SVP'
        ds.attrs['type'] = 'SVP'
        ds.attrs['launchDate'] = launchtime

        #write out the file
        output_file = '{}.qc.nc'.format(ds.attrs['buoyid'])
        outdir = os.path.join(datadir, 'netcdf')
        if not os.path.exists(os.path.join(outdir)):
            os.makedirs(os.path.join(outdir))
        output_file=os.path.join(outdir, output_file)
        #print('writing',output_file)
        ds.to_netcdf(output_file)

