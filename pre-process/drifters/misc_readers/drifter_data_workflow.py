#!/usr/bin/env python

import os
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt

import geopy
import geopy.distance

import drifter_data_readers as ddr

#print('finished imports')

###################################################
#
###################################################

#Are there standard drouge depths for various drifters?
droguedict = {
            "OSKER":"0",
            "SCT":"0", #20cm
            "Surface Circulation Tracker":"0", #same as SCT
            "ROBY":"0",
            "SVP":"15",
            "SLDMB":"0",
            "unknown":"unknown"
            }

#For the water properties data downloaded in February 2019:
datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/waterproperties_drifters/raw'

#collect all the filenames in a folder:
filelist = ddr.gather_filelist('wp',datadir)

#for just one file:
#datafile = 'sct0760_20171007_20171024.drf'
#datafile = 'garbage_threetimes_osker0448_20170715_20170723.drf'
#filelist = ddr.gather_filelist('wp', datadir, datafile)

buoylist = []

#For each file:
for fname in filelist:

    print( )
    #read in the data:
    df, hdrdict, buoyid = ddr.read_raw_data('wp', fname, droguedict)
    print('Working on file ', buoyid)

    #check for duplicate id
    buoylist = ddr.dup_buoyid_test(buoyid, buoylist)

    #impossible positions
    print('....checking for realistic coordinates')
    df = ddr.impossible_coordinate_test(df)
    
    #impossible times
    print('....checking for realistic times')
    df = ddr.impossible_time_test(df,hdrdict)
    
    #do the speed distance check
    print('....running speed distance check')
    df = ddr.impossible_speed_test(df) 

    #check the time range for breaks longer than a day,
    #then split the tracks if there are breaks. Write
    #out netcdf files of the finished data.
    #print('....writing out the files')
    ddr.determine_track_length(df,hdrdict,datadir,plot=False,write=True)

#check to be sure all the buoyids are unique
if len(buoylist) != len(np.unique(buoylist)):
    print('Error! There are repeated buoyids.')


