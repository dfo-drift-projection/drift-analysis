#!/usr/bin/env python

import os
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt

datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/waterproperties_drifters/raw'
buoylist = []

#for each file in the directory of raw data
for fname in glob.glob(os.path.join(datadir,'*.drf')):
    
    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0].split('_')[1]
        endrow = 0
        lines = []
        hdrdict = {}
        table = {}
        intable = False

        #deal with the header
        for line in fp:
            if "*END OF HEADER" in line:
                endrow += 1
                break
            else:
                endrow +=1
                #write all the header lines to a string
                lines.append(line.replace('\n','').lstrip().rstrip())
 
                #keep the useful values in a dictionary
                if '$TABLE' in line:
                    intable=line.split(":")[1].lstrip().rstrip()
                    table[intable] = []
                    continue

                if '$END' in line:
                    intable = False

                if intable != False:

                    if line.split()[0].rstrip().lstrip().isdigit() == True:
                        table[intable].append(line.split()[1])
                    else:
                        continue

                if ":" in line:
                    keeplist = ["TYPE", "MODEL", "SERIAL NUMBER", "IMEI", "SPOT ESN", "AGENCY", "DATA DESCRIPTION", "NUMBER OF CHANNELS"]
                    if any(word in line for word in keeplist):  
                        key = line.split(":")[0].rstrip().lstrip()
                        val = line.split(":")[1].rstrip().lstrip().replace('\n','')
                        hdrdict[key] = val    

    #Are there standard drouge depths for various drifters?
    droguedict = {
                "OSKER":"0",
                "SCT":"0", #20cm
                "Surface Circulation Tracker":"0", #same as SCT
                "ROBY":"0",
                "SVP":"15",
                "SLDMB":"0",
                "unknown":"unknown"
                }

    hdrdict['SOURCE']='retrieved from waterproperties.ca'
    hdrdict['DROGUE_DEPTH'] = droguedict[hdrdict['MODEL']]

    #read in the data
    df = pd.read_csv(fname, 
            skiprows=endrow, 
            names=table['CHANNELS'], 
            skip_blank_lines=False, 
            delimiter=r"\s+")

    #arrange the data for xarray
    df['DateTime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    launchtime = str(df['DateTime'][0])
    df.drop(labels=['Date', 'Time', 'Record_Number'], axis=1, inplace=True)
    df['DateTime'] = (df['DateTime'] - df['DateTime'][0]).apply(lambda x: x.total_seconds())
    df.rename(columns={"DateTime":"TIME", "Latitude":"latitude", "Longitude":"longitude"},inplace=True)
    df.set_index('TIME',inplace=True)
    obs = df.to_xarray()
    obs_lon = obs['longitude']
    obs_lat = obs['latitude']
    obs_time = obs['TIME']


    # Create dataset to save in netcdf
    ds = xr.Dataset(
        coords={'TIME': obs_time},
        data_vars={'LATITUDE': ('TIME', obs_lat),
                   'LONGITUDE': ('TIME', obs_lon),})

    #add attributes for the variables
    ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
    ds.LATITUDE.attrs['units'] = 'degrees_north'
    #ds.obs_lat.attrs['_FillValue'] =ds.obs_lat.dtype.type(np.nan)
    ds.LONGITUDE.attrs['long_name'] ='Longitude of observed trajectory'
    ds.LONGITUDE.attrs['units'] = 'degrees_east'
    #ds.obs_lon.attrs['_FillValue'] =ds.obs_lon.dtype.type(np.nan)
    ds.TIME.attrs['units'] = 'seconds since ' + str(launchtime) 
    ds.TIME.attrs['calendar'] = "standard" ;

    #decide on a buoyid
    if 'SPOT ESN' in hdrdict.keys():
        ds.attrs['buoyid'] = 'wp' + hdrdict['SERIAL NUMBER'] + hdrdict['SPOT ESN'] + 'D' + bname #had to combine 'SPOT ESN' and 'SERIAL NUMBER' to remove dups :/
    elif 'OSKER IMEI' in hdrdict.keys():
        ds.attrs['buoyid'] = 'wp' + hdrdict['OSKER IMEI'] + 'D' + bname
    else:
        print('no buoyid?')
        exit()

    #to use as a check later    
    if ds.attrs['buoyid'] in buoylist:
        print('')
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!duplicate buoyid!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print(ds.attrs['buoyid'], fname)
        print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!duplicate buoyid!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
        print('')
    #else:
    #    print(ds.attrs['buoyid'], fname)
    buoylist.append(ds.attrs['buoyid'])

    #add some attributes
    ds.attrs['description'] = hdrdict['DATA DESCRIPTION']
    ds.attrs['comment1'] = 'Drifter data collected by ' + hdrdict['AGENCY']
    ds.attrs['comment2'] = hdrdict['SOURCE']
    ds.attrs['comment3'] = 'netCDF file created by NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
    ds.attrs['approximate_drogue_depth'] = hdrdict['DROGUE_DEPTH']
    ds.attrs['model'] = hdrdict['MODEL']
    ds.attrs['type'] = hdrdict['TYPE']
    ds.attrs['serial_number'] = hdrdict['SERIAL NUMBER']
    ds.attrs['dataStartDate'] = launchtime
    ds.attrs['original_header'] = ','.join(lines)

    #write out the file
    output_file = '{}.nc'.format(ds.attrs['buoyid'])
    outdir = os.path.join(datadir, 'netcdf')
    if not os.path.exists(os.path.join(outdir)):
        os.makedirs(os.path.join(outdir))
    output_file=os.path.join(outdir, output_file)
    print('writing',output_file)
    ds.to_netcdf(output_file)

#check to be sure all the buoyids are unique
if len(buoylist) != len(np.unique(buoylist)):
    print('Error! There are repeated buoyids.')

    
'''
#sample filename: sct0752_20170901_20170915.drf
#sample header

*2019/02/14 12:42:46.07
*IOS HEADER VERSION 2.0      2016/04/28 2016/06/13 MATLAB

*FILE
    START TIME          : UTC 2017/09/01 15:34:56.000
    END TIME            : UTC 2017/09/15 06:51:56.000
    NUMBER OF RECORDS   : 3846
    DATA DESCRIPTION    : Drifting Buoy
    NUMBER OF CHANNELS  : 5

    $TABLE: CHANNELS
    ! No Name            Units        Minimum      Maximum
    !--- --------------- ------------ ------------ ----------
       1 Record_Number   n/a          1            3846
       2 Date            YYYY/MM/DD   n/a          n/a
       3 Time            HH:MM:SS     n/a          n/a
       4 Latitude        degrees      47.96659     48.68913
       5 Longitude       degrees      -125.94722   -124.72263
    $END

    $TABLE: CHANNEL DETAIL
    ! No  Pad        Start  Width  Format      Type  Decimal_Places
    !---  ---------  -----  -----  ----------  ----  --------------
       1  ' '        ' '        8  F           R4      1
       2  ' '        ' '    ' '    YYYY/MM/DD  D     ' '
       3  ' '        ' '    ' '    HH:MM:SS    T     ' '
       4  999.00000  ' '       10  F           R4      5
       5  999.00000  ' '       11  F           R4      5
    $END

*ADMINISTRATION
    MISSION             : 2017-09
    AGENCY              : IOS, Ocean Sciences Division, Sidney, B.C.
    COUNTRY             : Canada
    PROJECT             : La Perouse
    SCIENTIST           : Robert M.
    PLATFORM            : John P. Tully

*LOCATION
    GEOGRAPHIC AREA     : North-East Pacific
    LATITUDE            :  47  57.99540 N  ! (deg min)
    LONGITUDE           : 125  56.83320 W  ! (deg min)
    LATITUDE 2          :  48  41.34780 N  ! (deg min)
    LONGITUDE 2         : 124  43.35780 W  ! (deg min)

*INSTRUMENT
    TYPE                : Oceanetic Measurement
    MODEL               : Surface Circulation Tracker
    SERIAL NUMBER       : 752
    SPOT TYPE           : SPOT Trace
    SPOT ESN            : 3106854

*HISTORY

    $TABLE: PROGRAMS
    !   Name    Vers  Date       Time     Recs In   Recs Out
    !   ------- ----  ---------- -------- --------- ---------
        CSV2IOS 1.0   2019/02/14 12:42:46      3846      3846
    $END
    $REMARKS
        -CSV2IOS processing: 2019/02/14 12:42:46
    $END

*END OF HEADER
'''

