#!/usr/bin/env python

import os
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/cmems_drifters/'

#read in the WMO table
dbcpop = pd.read_csv(os.path.join(datadir,'dbcp_operational.csv'), delimiter=";")
buoylist = []

#for each file in the datadir
for fname in glob.glob(os.path.join(datadir,'*.nc')):
    
    #print(fname)

    ds = xr.open_dataset(fname)
    #find the matching buoyid from the wmo list
    extra_meta = dbcpop.loc[dbcpop['WMO'] == float(ds.attrs['platform_wmo_code'])]
    #ds.attrs['buoyid'] = 'db' + ds.attrs['platform_wmo_code']

    #if there's no entries in the wmo list, add 'unknown' as the wmo buoy type
    if extra_meta.empty == True:
        ds.attrs['wmo_type'] = 'unknown'
        #print(ds.attrs['platform_wmo_code'], ds.attrs['platform_type'], ds.attrs['wmo_type'])

    else:
        #ds.attrs['buoyid'] = 'cmems' + ds.attrs['platform_wmo_code'] 
        ds.attrs['wmo_type'] = extra_meta.iloc[0]['PTF_MODEL']
        #print(ds.attrs['platform_wmo_code'], ds.attrs['platform_type'], ds.attrs['wmo_type'])

    #if the platform type is missing from the original netcdf, add 'unknown'
    if ('platform_type' not in ds.attrs) or (ds.attrs['platform_type'] == ''): 
        ds.attrs['platform_type'] = 'unknown'

    #if the wmo files says it's an SVP, add that to the attrs
    if ('SVP' in ds.attrs['wmo_type']) and (ds.attrs['platform_type'] == 'DB'):
        ds.attrs['platform_type'] = 'SVP'
        
    #print(ds.attrs['platform_wmo_code'], ds.attrs['platform_type'], ds.attrs['wmo_type'])

    if 'SVP' in (ds.attrs['platform_type']) or ('SVP' in ds.attrs['wmo_type']):
        #write to netcdf file in one folder
        #print('..........has SVP')
        subdir = 'SVP'
    else:
        #write to netcdf file in another folder
        #print('..........not SVP')
        subdir = 'DB'

    #Are there standard drouge depths for various drifters?
    droguedict = {
                "OSKER":"0",
                "SCT":"0", #20cm
                "Surface Circulation Tracker":"0", #same as SCT
                "ROBY":"0",
                "SVP":"15",
                "SLDMB":"0",
                "unknown":"unknown"
                }
    
    launchdate = pd.to_datetime(str(ds.TIME.values[0])).strftime('%Y%m%d')

    ds.attrs['buoyid'] = 'db' + ds.attrs['platform_wmo_code'] + 'D' + launchdate
 
    droguetype = ds.attrs['platform_type']
    if droguetype not in droguedict.keys():
        droguetype = 'unknown'
    ds.attrs['SOURCE']='retrieved from cmems ftp'
    #add some attributes
    #ds.attrs['comment1'] = 'Drifter data collected by ' + hdrdict['AGENCY']
    #ds.attrs['comment2'] = hdrdict['SOURCE']
    ds.attrs['comment'] = 'netCDF file created by NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
    ds.attrs['approximate_drogue_depth'] = droguedict[droguetype]
    ds.attrs['model'] = ds.attrs['wmo_type']
    ds.attrs['type'] = ds.attrs['platform_type']
    ds.attrs['launchDate'] = launchdate
    ds.attrs['description'] = 'CMEMS db file'
    #remove extra dimensions from the file so that it will be more easily read by the drift tool
    ds.attrs['comment2'] = 'These data were collected and made freely available by the International Data Buoy Cooperation Program and the national programs that contribute to it (http://www.jcommops.org/dbcp).'    


    #write out the file
    output_file = '{}.nc'.format(ds.attrs['buoyid'])
    outdir = os.path.join(datadir, 'netcdf', subdir)
    if not os.path.exists(os.path.join(outdir)):
        os.makedirs(os.path.join(outdir))
    output_file=os.path.join(outdir, output_file)
    print('writing',output_file)
    ds.to_netcdf(output_file)

