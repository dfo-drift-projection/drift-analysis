#!/usr/bin/env python

import os
import glob
import xarray as xr
#import numpy as np
import pandas as pd
import numpy as np
import datetime as dt
import geopy
import geopy.distance

from datetime import datetime, timedelta

datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/ugar_drifters/'

for fname in glob.glob(os.path.join(datadir,'*.txt')):
    
    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]
        print(bname)

        #read in the data
        df = pd.read_csv(fname,
            skip_blank_lines=False,
            delimiter="\t")

        #Are there standard drouge depths for various drifters?
        droguedict = {
                    "OSKER":"0",
                    "SCT":"0", #20cm
                    "Surface Circulation Tracker":"0", #same as SCT
                    "ROBY":"0",
                    "SVP":"15",
                    "SLDMB":"0",
                    "unknown":"unknown",
                    "spot":"0.2"
                    }


        df.rename(columns=lambda x: x.strip(), inplace=True)


        def datenum2datetime(datenum):
            return datetime.fromordinal(int(datenum)) + timedelta(days=datenum%1) - timedelta(days = 366)

        df['realtime'] = df['time'].map(datenum2datetime, na_action='ignore')
        
        df.drop(labels=['time'], axis=1, inplace=True)
        df.rename(columns={"realtime":"time"}, inplace=True)

        launchtime = str(df['time'][0].strftime('%Y%m%d'))
        firsttime = str(df['time'][0].strftime('%Y-%m-%d %H:%M:%S'))
        df['time'] = (df['time'] - df['time'][0]).apply(lambda x: x.total_seconds())
        df['time'] = df['time'].astype(np.uint32)
        df.sort_values('time', inplace=True)

        df.drop_duplicates(subset=['time'], keep='first', inplace=True)

        # Dropping bad points
        changed = True
        std = None
        mean = None
        while changed:
            df.reset_index(inplace=True)
            df.drop('index', axis=1, inplace=True)

            # Get geopy points for each lat,lon pair
            points = df[
                ['latitude', 'longitude']
            ].apply(lambda x: geopy.Point(x[0], x[1]), axis=1)

            # get distances in nautical miles
            ends = points.shift(1)
            distances = []
            for idx, start in enumerate(points):
                #distances.append(geopy.distance.vincenty(start, ends[idx]).nm)
                try:
                    distances.append(geopy.distance.distance(start, ends[idx]).nm)
                except ValueError:
                    distances.append(np.nan)

            distances = np.ma.masked_invalid(distances)

            # get the time difference in hours
            times = df['time'].diff() / 3600.0

            # calculate speed in knots
            speed = distances / times

            # Drop anything where speed is 3 standard deviations from the mean and is > 10 knots
            if std is None:
                std = np.std(speed)
                mean = np.mean(speed)

            si = np.where((abs(speed - mean) > 3 * std) & (speed > 10))[0]

            if len(si) > 0:
                df.drop(points.index[si[0]], inplace=True)
                print("\tDropping point with speed=%0.1f knots" % speed[si[0]])
            else:
                changed = False

            del si

        df.rename(columns={'time':'TIME'}, inplace=True)
        df.set_index('TIME',inplace=True)
        obs = df.to_xarray()

        # Create dataset to save in netcdf
        ds = xr.Dataset(
            coords={'TIME': obs['TIME']},
            data_vars={'LATITUDE': ('TIME', obs['latitude']),
                    'LONGITUDE': ('TIME', obs['longitude']),})

        #add attributes for the variables
        ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
        ds.LATITUDE.attrs['units'] = 'degrees_north'
        #ds.obs_lat.attrs['_FillValue'] = ds.obs_lat.dtype.type(np.nan)
        ds.LONGITUDE.attrs['long_name'] = 'Longitude of observed trajectory'
        ds.LONGITUDE.attrs['units'] = 'degrees_east'
        #ds.obs_lon.attrs['_FillValue'] = ds.obs_lon.dtype.type(np.nan)
        ds.TIME.attrs['units'] = 'seconds since ' + str(firsttime)
        ds.TIME.attrs['calendar'] = "standard" ;

        ds.attrs['source'] = 'retrieved from Dany Dumont (UGAR)'
        ds.attrs['description'] = 'UGAR drifter data'
        ds.attrs['buoyid'] = 'ugar' + bname + 'D' + launchtime
        ds.attrs['comment'] = 'netCDF file created by NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
        ds.attrs['approximate_drogue_depth'] = droguedict['spot']
        ds.attrs['model'] = 'spot'
        ds.attrs['type'] = 'spot'
        #ds.attrs['launchDate'] = launchtime
        ds.attrs['dataStartDate'] = firsttime

        #write out the file
        output_file = '{}.nc'.format(ds.attrs['buoyid'])
        outdir = os.path.join(datadir, 'netcdf')
        if not os.path.exists(os.path.join(outdir)):
            os.makedirs(os.path.join(outdir))
        output_file=os.path.join(outdir, output_file)
        print('writing',output_file)
        ds.to_netcdf(output_file)

