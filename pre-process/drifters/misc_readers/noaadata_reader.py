#!/usr/bin/env python

import os
import glob
import xarray as xr
#import numpy as np
import pandas as pd
import datetime as dt
import numpy as np

datadir = '/fs/vnas_Hdfo/odis/jeh326/data/work/drifters/noaa_student_drifters/'

for fname in glob.glob(os.path.join(datadir,'*.csv')):
    
    with open(fname) as fp:
        #bname = os.path.basename(fname).split('.')[0]

        #read in the data
        df = pd.read_csv(fname,
            #names=['Received Date(GMT)','Sent Date(GMT)','Data Date(GMT)','LATITUDE','LONGITUDE','YEAR','DAY','HOUR','MIN','SST'],
            skiprows=[1],
            header=0,
            skip_blank_lines=False,
            delimiter=",")

        ##Are there standard drouge depths for various drifters?
        #droguedict = {
        #            "OSKER":"0",
        #            "SCT":"0", #20cm
        #            "Surface Circulation Tracker":"0", #same as SCT
        #            "ROBY":"0",
        #            "SVP":"15",
        #            "SLDMB":"0",
        #            "unknown":"unknown"
        #            }

        df['time'] = pd.to_datetime(df['time'])
        df['duptime'] = df['time']
        #####df['time'] = (df['time'] - df['time'][0]).apply(lambda x: x.total_seconds())
        df.sort_values('time', inplace=True)

        df.drop_duplicates(subset=['time'], keep='first', inplace=True)

        #####df.set_index('time',inplace=True)

        #find the unique ids in the master dataframe
        idlist = np.unique(df['id'])

        #sort by drifter
        for l in idlist:
            indvdf = df[df['id']==l].copy() 
            
            ddepth = abs(float(np.unique(indvdf['depth'])))

            indvdf.reset_index(drop=True,inplace=True)

            #####################
            indvdf['time'] = (indvdf['time'] - indvdf['time'][0]).apply(lambda x: x.total_seconds())

            indvdf.rename(columns={'time':'TIME'}, inplace=True)
            indvdf.set_index('TIME',inplace=True)
            #####################

            launchtime = str(indvdf['duptime'].iloc[0].strftime('%Y%m%d'))
            firsttime = str(indvdf['duptime'].iloc[0].strftime('%Y-%m-%d %H:%M:%S'))
            indvdf.drop(labels=['duptime'],axis=1,inplace=True)
            obs = indvdf.to_xarray()

            # Create dataset to save in netcdf
            ds = xr.Dataset(
                coords={'TIME': obs['TIME']},
                data_vars={'LATITUDE': ('TIME', obs['latitude']),
                        'LONGITUDE': ('TIME', obs['longitude']),})

            #add attributes for the variables
            ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
            ds.LATITUDE.attrs['units'] = 'degrees_north'
            #ds.obs_lat.attrs['_FillValue'] = ds.obs_lat.dtype.type(np.nan)
            ds.LONGITUDE.attrs['long_name'] = 'Longitude of observed trajectory'
            ds.LONGITUDE.attrs['units'] = 'degrees_east'
            #ds.obs_lon.attrs['_FillValue'] = ds.obs_lon.dtype.type(np.nan)
            ds.TIME.attrs['units'] = 'seconds since ' + str(firsttime)
            #ds.time.attrs['units'] = 'seconds since ' + str(launchtime)
            ds.TIME.attrs['calendar'] = "standard" ;

            ds.attrs['source'] = 'retrieved from noaa'
            ds.attrs['description'] = 'NOAA student drifter data'
            ds.attrs['buoyid'] = 'noaa' + str(l) + 'D' + launchtime
            ds.attrs['comment'] = 'netCDF file created by NAFC Drift Group on ' + dt.datetime.today().strftime('%Y-%m-%d')
            ds.attrs['approximate_drogue_depth'] = ddepth
            ds.attrs['model'] = 'NOAA student drifter'
            ds.attrs['type'] = 'NOAA student drifter'
            #ds.attrs['launchDate'] = launchtime
            ds.attrs['dataStartDate'] = firsttime

            #write out the file
            output_file = '{}.nc'.format(ds.attrs['buoyid'])
            outdir = os.path.join(datadir, 'netcdf_revised')
            if not os.path.exists(os.path.join(outdir)):
                os.makedirs(os.path.join(outdir))
            output_file=os.path.join(outdir, output_file)
            print('writing',output_file)
            ds.to_netcdf(output_file)

