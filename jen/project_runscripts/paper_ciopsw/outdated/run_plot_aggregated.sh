#!/bin/bash

MINICONDA_PATH=/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/envs/opendrift_update
drifter_code_dir=$HOME/drifters/drift-tool

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

#grab the folders again:
#/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/refresh-folders.sh

#plot the aggregated individual data:

############### CIOPSW Hourly ############################
#plot the aggregated output
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --skills_subplots_with_filled \
  --no-plot_ratio_and_track 


############### CIOPSW daily ############################
#plot the aggregated output
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --skills_subplots_with_filled \
  --no-plot_ratio_and_track


############### RIOPS daily ############################
#plot the aggregated output
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops-daily/' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --skills_subplots_with_filled \
  --no-plot_ratio_and_track


############### CIOPSW nowinds ############################
#plot the aggregated output
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --skills_subplots_with_filled \
  --no-plot_ratio_and_track

#plot the mapped skill scores
###################################################################
#ciopsw-hourly
/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 do_pickle
#ciopsw-daily
/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 do_pickle
#ciopsw-nowinds
/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 do_pickle
#riops-daily
/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops-daily/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 do_pickle

###########################################################
#crop the tracks to common time frame:
~/drifters/drift-analysis/jen/driftcompare_early-scripts/crop_common_netcdfs.py

# plot the comparison plots
~/drifters/drift-analysis/jen/driftcompare_early-scripts/compare_output.py


#############################################################
#crop the wind/no wind data here.





#  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' 
#os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"
