#!/bin/bash

MINICONDA_PATH=/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/envs/opendrift_update
drifter_code_dir=$HOME/drifters/drift-tool

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


###################################################################
# if I need to refresh the data, grab the folders again:
# (this lists the folders that I pulled from)
###################################################################
#refreshed 20200212 at 5pm
#/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/refresh-folders.sh


###################################################################
# plot the individual plots from the aggregated data:
# remove the no- prefix to make that plot
############### CIOPSW Hourly ############################

##plot the aggregated output
#echo "aggregated ciopsw hourly"
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' \
  --no-plot_track_indiv \
  --no-plot_track_persistance \
  --no-plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --no-plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_skills_subplots_with_filled \
  --no-plot_ratio_and_track \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd'

############### CIOPSW daily ############################
##plot the aggregated output
#echo "aggregated ciopsw daily"
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/' \
  --no-plot_track_indiv \
  --no-plot_track_persistance \
  --no-plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --no-plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_skills_subplots_with_filled \
  --no-plot_ratio_and_track \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd'

############### RIOPS daily ############################
##plot the aggregated output
#echo "aggregated riops daily"
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops-daily/' \
  --no-plot_track_indiv \
  --no-plot_track_persistance \
  --no-plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --no-plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_skills_subplots_with_filled \
  --no-plot_ratio_and_track \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd'

############### CIOPSW nowinds ############################
##plot the aggregated output
#echo "ciopsw no winds"
/fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/plot_aggregated_output.py \
  --data_dir='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/' \
  --no-plot_track_indiv \
  --no-plot_track_persistance \
  --no-plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --no-plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_skills_subplots_with_filled \
  --no-plot_ratio_and_track \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd'

################################################################################
#plot the individual mapped skill scores for the aggregated data
#
#need to update None to make_pickle if you are changing how the master files
#are created/stored or when running a new dataset!

#the program is:
#/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py
###############################################################################

##ciopsw-hourly
#echo "mapped ciopsw hourly"
/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/aggregated_plots/' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 make_pickle

##ciopsw-daily
#echo "mapped ciopsw daily"
#/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-daily/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 make_pickle

##ciopsw-nowinds
#echo "mapped ciopsw no winds"
#/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-nowinds/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 make_pickle

##riops-daily
#echo "mapped riops daily"
#/home/jeh326/drifters/drift-analysis/jen/mapped_skillscores/mapped_skillscores.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops-daily/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/riops-daily/aggregated_plots/mapped_skillscores' 'ciopsw' 'opendrift' '20160101' '20161231' 48 48 make_pickle


########################################################################################
#These are the plots for comparing datasets
#you need to update parameters INSIDE these files to run them.
########################################################################################

##crop the tracks to a common time frame:
#echo "cropping the datasets"
#~/drifters/drift-analysis/jen/driftcompare_early-scripts/crop_common_netcdfs.py

## plot the comparison plots
#echo "plotting cropped datasets"
#~/drifters/drift-analysis/jen/driftcompare_early-scripts/compare_output.py

