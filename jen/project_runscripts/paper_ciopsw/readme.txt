This output was created on Oct 29, 2020

I used the scripts in: 
/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/project_runscripts/paper_ciopsw/ 
to create the output:
/home/nso001/data/work2/OPP/salish-ciopsw-comparisons/cropped_datasets/CIOPSWBC12nowinds-RIOPSBC12nowinds/

To re-run the same additional plots (the map subplots and the line plot): 
1. Update ciopsw-paper_user-config_27Oct2020.yaml (best to make a copy, then update). 
2. Update the USER_CONFIG var in ciopsw-paper_run-analysis.sh.
3. Run ciopsw-paper_run-analysis.sh. 
   - This calls ciopsw-paper_extra-plots.py and passes it the user config an argument.
