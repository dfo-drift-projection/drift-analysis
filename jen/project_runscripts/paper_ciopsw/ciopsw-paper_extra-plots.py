"""
Plot workflow script
================
:Author: Jennifer Holden
:Created: 2020-07-07
"""

import os
import glob
import matplotlib
matplotlib.use('Agg')
from matplotlib import gridspec
import xarray as xr
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import sys
import argparse
from datetime import datetime
import shapely
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon

import driftutils
from driftutils import plotting_workflow as main_plotting_workflow
from driftutils import regional_analysis_functions as rafuncs
from driftutils import plotting_utils as putils
from driftutils import map_functions as mapfuncs
from driftutils import skillscore_functions as ssfuncs
from driftutils import qc_plot_functions as qcfuncs
from driftutils import utils
from driftutils import mpl_util

########################################################################
# Read in the data and run the workflow
########################################################################

#load the data from the original plotting workflow. This compiles all 
# the data for the skillscores as well.
zonedata = main_plotting_workflow.main()

########################################################################
# make the ciopsw plots
########################################################################

print('\nrunning the extra plotting')
for zone in zonedata.keys():
    p = zonedata[zone]

    map_subplots = True 
    skills_lineplot = True

    ################################################################
    # the maps subplot
    ################################################################

    if map_subplots:

        fig = plt.figure(figsize=(15,10))
        gs  = gridspec.GridSpec(1, 3, width_ratios=[0.925, 1, 1])
        ax0 = plt.subplot(gs[0])
        ax1 = plt.subplot(gs[1])
        ax2 = plt.subplot(gs[2])

        print('assembling drifter track data in case it was not run in the '
                'plotting workflow\n')
        all_drifter_tracks_dict = mapfuncs.create_tracks_dataframe(
                    datatype='dataset',
                    given_dslist=p['datasets_list']
                    )

        alldf = pd.concat([df for df in all_drifter_tracks_dict.values()])
        alldict = {}
        alldict['alltracks'] = alldf

        #find the geographical extremes across all the experiments
        tight_extremes = mapfuncs.return_map_extremes(alldict)
        minlon, maxlon, minlat, maxlat = tight_extremes['tight_extremes']
        all_lats = tight_extremes['all_lats']
        all_lons = tight_extremes['all_lons']

        extremes_with_buffer = mapfuncs.add_map_extent_buffer(
            minlon, maxlon, minlat, maxlat,
            lon_buff_percent=None,
            lat_buff_percent=None
            )
        map_extremes = extremes_with_buffer['buffered_extremes']
        mapext = map_extremes

        ###############################
        print('..plotting tracks')
        ###############################
        savestr = mapfuncs.plot_drifter_tracks(
                    all_drifter_tracks_dict['CIOPSWBC12nowinds'],
                    obs_tracks=True,
                    mod_tracks=True,
                    landmask_type=None,
                    landmask_files=p['landmask_files'],
                    etopo_file=None,
                    ax=ax0,
                    par_labels=[1,1,0,0],
                    mer_labels=[0,0,0,1],
                    map_extremes=mapext,
                    obs_color='k',
                    mod_color='gray',
                    map_projection='lambert',
                    show_legend=False
                    )
        ax0.text(0.08, 0.05, '(a)', horizontalalignment='center',verticalalignment='center', transform=ax0.transAxes)
        handles, labels = ax0.get_legend_handles_labels()
        ax0.legend(
            handles,
            ['observed tracks','modelled tracks'],
            loc='upper right',
            fontsize=8
        )


        ###############################
        print('\n..plotting hexbins for ciopsw hourly molcard')
        ###############################
        df_all = putils.drifter_to_binned_dataframes_list(p['datasets_dict'], 'molcard')
        mapstr, skilllabel = mapfuncs.plot_hexbins(
            'molcard',
            48,
            'CIOPSWBC12nowinds',
            df_all['CIOPSWBC12nowinds'],
            grid_spacing=80,
            landmask_type='ciopswbc12nowinds',
            landmask_files=p['landmask_files'],
            etopo_file=None,
            ax=ax1,
            par_labels=[0,0,0,0],
            mer_labels=[0,0,0,1],
            map_extremes=mapext,
            map_projection='lambert',
            cbar_label='Molcard skill'
            )
        ax1.text(0.08, 0.05, '(b)', horizontalalignment='center',verticalalignment='center', transform=ax1.transAxes)


        ###############################
        print('\n..plotting subtracted plot, hourly-daily ciopsw, sep')
        ###############################
        df_res_combined_dict = putils.drifter_to_binned_dataframes_list(p['datasets_dict'], 'sep') 
        df1 = df_res_combined_dict['CIOPSWBC12nowinds']
        df2 = df_res_combined_dict['RIOPSBC12nowinds']

        subsavestr = mapfuncs.plot_subtracted_hexbins(
                        'sep',
                        48,
                        df_res_combined_dict['CIOPSWBC12nowinds'],
                        df_res_combined_dict['RIOPSBC12nowinds'],
                        ['CIOPSWBC12nowinds','RIOPSBC12nowinds'],
                        zone='all',
                        landmask_type='riopsbc12nowinds',
                        landmask_files=p['landmask_files'],
                        etopo_file=None,
                        ax=ax2,
                        logplot=True,
                        par_labels=[0,0,0,0],
                        mer_labels=[0,0,0,1],
                        map_extremes=mapext,
                        map_projection='lambert',
                        cbar_label='Separation distance difference (km)'
                        )
        ax2.text(0.08, 0.05, '(c)', horizontalalignment='center',verticalalignment='center', transform=ax2.transAxes)

        multimapfilename = 'ciopsw-paper_mapped-skillscore-subplot.png'
        multimappathname = os.path.join(p['savedir'],multimapfilename)
        plt.savefig(multimappathname, bbox_inches='tight',format='png', dpi=300)
        print('saving ', multimappathname)

        plt.close()

    ################################################################
    # filled skillscores
    ################################################################

    if skills_lineplot:

        print('\nplotting average skill scores with filled range')

        fig, ax = plt.subplots(figsize=(8.5,5.25))
        ssfuncs.plot_skill_with_filled(
            p['datasets_dict'],
            'sep',
            p['plotcolors'],
            p['skill_label_params'],
            lwidth=2,
            filledrangetype='IQR',
            labs=True,
            leg=True,
            ax=ax,
            bounding_bars_only=False,
            pickledir=p['psavedir']
        )

        ax.legend(['CIOPS-W','RIOPS','Interquartile range','Interquartile range'])
        ax.set_xlabel('Hours since start')
        ax.set_ylabel('Average separation distance (km)')
        ax.set_xlim(left=0.0, right=48)
        fig.tight_layout(rect=[0, 0.03, 1, 0.95])

        combinedskfignamepng = "{}_{}_{}.png".format('ciopsw-paper_skill-score-lineplot','IQR', zone)
        plt.savefig(
            os.path.join(p['savedir'], combinedskfignamepng),
            bbox_inches='tight',
            dpi=300,
            papertype='letter',
            orientation='portrait'
        )
        print('saving', os.path.join(p['savedir'], combinedskfignamepng))

        plt.close()



