#!/bin/bash

# provide a default path the drift-tool miniconda install on the GPSC
MINICONDA_PATH=$HOME/work/software/miniconda/envs/dev_dtv2/
USER_CONFIG=ciopsw-paper_user-config_27Oct2020.yaml

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# Analyze and plot the drift-tool output
# --------------------------
python ciopsw-paper_extra-plots.py \
  --user_config $USER_CONFIG
