#!/usr/bin/env /gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/envs/opendrift_update/bin/python

#env python

import xarray as xr
import numpy as np
import pandas as pd
import os
os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

#import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

from dateutil.parser import parse
import datetime
import sys
import glob

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

from dateutil.parser import parse
import collections
from driftutils import mpl_util



def main():

    skills_list=['sep']

    #savedir="/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/two-percent-winds/map_plots/"
    savedir="/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/jen/paper_ciopsw/"

    cropped_input_one=('/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/'
                            'two-percent-winds/daily-datasets/RIOPS_daily-CIOPSW_daily/cropped_datasets/'
                            'CIOPSW_daily_cropped-dataset/')  
    cropped_input_two=('/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/'
                            'two-percent-winds/daily-datasets/RIOPS_daily-CIOPSW_daily/cropped_datasets/'
                            'RIOPS_daily_cropped-dataset/')
    datadirs={"RIOPS_daily":cropped_input_two,"CIOPSW_daily":cropped_input_one}

    #############################################################################
    # Find the common ids between folders
    #############################################################################

    #for each dictionary, make a list of the buoyids, then find the common ids
    def grab_buoyids(datadir):
        """ make a list of unique ids  """
        iname = []
        for fpath in glob.glob(os.path.join(datadir,'*.nc')):
            fname = os.path.basename(fpath)
            iname.append(fname.split('_')[-2])
        return np.unique(iname)

    buoyids = {}
    for run, dir in datadirs.items():
        buoyids[run] = grab_buoyids(dir)

    commonids = None
    for modelrun in buoyids.keys():
       if commonids is None:
           commonids = set(buoyids[modelrun])
       commonids.intersection_update(buoyids[modelrun])

    #############################################################################
    # plot average of means for each model WITH filled
    #############################################################################

    cwp_colors = {"CIOPSW_hourly":"firebrick","CIOPSW_daily":"firebrick","RIOPS_daily":"mediumblue"}

    cwp_skill_label_params = {
        'sep':{'ylab':'Average separation distance (km)',
                'yleg':'Average separation distance (km)'},
        'molcard':{'ylab':'Average Molcard skill',
                'yleg':'Average Molcard skill'},}

    lstyles = {"CIOPSW_hourly":"-.","CIOPSW_daily":"-","RIOPS_daily":"-"}

    frtypes = ['IQR'] 
    print('Plotting avg means with filled range')

    for skill in skills_list:
        print('....', skill)
        for filledrangetype in frtypes:
            print('.... creating filled range using', filledrangetype )
            fig, ax = plt.subplots(figsize=(8.5,5.25))
            #fig, ax = plt.subplots(figsize=(11,8.5))

            plot_combined_filled_skill(
                commonids, datadirs, savedir, skill, 
                cwp_colors, cwp_skill_label_params, 
                '1H', filledrangetype, True, ax=ax)
            plot_combined_skill(
                commonids, datadirs, savedir, skill, 
                cwp_colors, cwp_skill_label_params, 
                2, '1H', lstyle=lstyles, leg=True, ax=ax)

            ax.set_xlabel('Hours since Start')
            ax.set_xlim(left=0.0, right=48)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])

            filledavgskillfignamepng = "ciopsw-paper_comparison-line-plot_{}_{}_{}.png".format(str('-'.join(datadirs.keys())),skill,filledrangetype)
            #savedir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/two-percent-winds/map_plots/'
            plt.savefig(os.path.join(savedir,filledavgskillfignamepng),bbox_inches='tight',dpi=300) 
            print('saving ', os.path.join(savedir,filledavgskillfignamepng))
            plt.close()

    print('skills_subplots_with_filled',str(datetime.datetime.now()))
    print()

##############################################################################################################################################
# FUNCTIONS
##############################################################################################################################################

def bin_skills(ds, skill, sampling_freq):
    """ bin skill scores by drifter """

    #This part makes a binned average of the skill. 
    ds['time_since_start'] = ds['time'].astype(np.timedelta64)
    for m in ds.model_run.values:
        ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64) #ds['time'][m, 0]

    df = ds.to_dataframe()
    df = df.set_index('time_since_start')
    df_res = df[skill].dropna().sort_index().resample(sampling_freq, label='right').mean().interpolate()
    df_res = df_res.reset_index()
    df_res['hours since start'] = df_res.time_since_start.astype('timedelta64[h]')

    return df_res

################################################################################################################
def plot_combined_skill(commonids, datadirs, savedir, skill, lcolors, skill_label_params, lwidth, sampling_freq, lstyle=None, leg=False, ax=None):

    #for each id in commonids, run bin_skill
    #print("Plotting averaged skill scores")

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    #skill = 'sep'
    for modelrun in datadirs.keys(): #buoyids.keys():
        print("...... plotting average skill for ",str(modelrun))

        newmodel = True
        for drifterid in commonids:
            #print("............",str(drifterid))

            fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated.nc")
            #fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated_cropped.nc")
            with xr.open_dataset(fname) as ds:

                df_res = bin_skills(ds, skill, sampling_freq)
                if newmodel is True:
                    df_res_all = df_res
                    newmodel=False
                else:
                    df_res_all = pd.concat((df_res_all, df_res), axis=0)

        df_res_all = df_res_all.set_index('time_since_start')
        df_res_combined = df_res_all[skill].dropna().sort_index().resample('1H', label='right').mean().interpolate()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined['hours since start'] = df_res_combined.time_since_start.astype('timedelta64[h]')

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            df_res_combined[skill]=df_res_combined[skill]/1000.

        #add the plot
        if modelrun == 'CIOPSW_hourly':
            modelrunstr = 'CIOPS-W (hourly)'
        elif modelrun == 'CIOPSW_daily':
            modelrunstr = 'CIOPS-W'
        elif modelrun == 'RIOPS_daily':
            modelrunstr = 'RIOPS'
        else:
            modelrunstr = str(modelrun)

        if lstyle is None:
            lstyle == '-'
        else:
            lstyle=lstyle

        ax.plot(df_res_combined['hours since start'][1:-1], df_res_combined[skill][1:-1], lcolors[modelrun], linewidth=lwidth, linestyle=lstyle[modelrun], label=modelrunstr)
        ax.minorticks_on()
        ax.grid(which='major', alpha=0.4)
        ax.grid(which='minor', alpha=0.2)

        ylab = str(skill_label_params[skill]['ylab'])
        yleg = str(skill_label_params[skill]['yleg'])

        ax.set_xlabel('hours since start', fontsize=10)
        ax.set_ylabel(ylab, fontsize=10)
        #ax.set_title('Average of mean for all drifters', fontsize=14)

        if leg == True:
            ax.legend(fontsize=10)

    #save the plot or show it
    combinedskfignamepng="comparison_{}_{}.png".format(str('-'.join(datadirs.keys())), skill)
    if savefig:
        plt.savefig(os.path.join(savedir,combinedskfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        pass

################################################################################################################
def plot_combined_filled_skill(commonids, datadirs, savedir, skill, lcolors, skill_label_params, sampling_freq, filledrangetype, leg=False, ax=None):

    #for each id in commonids, run bin_skill
    print("...... calculating filled zones")

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if skill is 'logobsratio':
        plotlog=True
        skill = 'obsratio'
    elif skill is 'logmodratio':
        plotlog=True
        skill = 'modratio'
    else:
        plotlog=False

    ### For each set of data
    for modelrun in datadirs.keys(): 
        #print("...... plotting filled range for ",str(modelrun))

        ### for each drifter in the set of common drifter ids
        newdrifter = True 
        for drifterid in commonids:
            #print("............",str(drifterid))
            fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated.nc")
            #fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated_cropped.nc")
            with xr.open_dataset(fname) as ds:
                df_res = bin_skills(ds, skill, sampling_freq)

                if newdrifter is True:                    
                    df_res_all = df_res
                    newdrifter=False
                else:
                    df_res_all = pd.concat((df_res_all, df_res), axis=0)

        df_res_all = df_res_all.set_index('time_since_start')
        df_res_combined = df_res_all[skill].dropna().sort_index().resample('1H', label='right').mean().interpolate()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined['hours since start'] = df_res_combined.time_since_start.astype('timedelta64[h]')

        if filledrangetype == 'IQR':
            df_q1_quan = df_res_all[skill].dropna().sort_index().resample('1H', label='right').quantile(0.25).interpolate()
            df_q3_quan = df_res_all[skill].dropna().sort_index().resample('1H', label='right').quantile(0.75).interpolate()
            df_res_combined['upper'] = df_q3_quan.values
            df_res_combined['lower'] = df_q1_quan.values
            leg_str = 'Interquartile Range'
        elif filledrangetype == '1std':
            df_std = df_res_all[skill].dropna().sort_index().resample('1H', label='right').std().interpolate()
            df_std = df_std.reset_index()
            df_res_combined['std'] = df_std[skill]
            df_res_combined['upper'] = df_res_combined[skill]+df_res_combined['std']
            df_res_combined['lower'] = df_res_combined[skill]-df_res_combined['std']
            if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
                df_res_combined['std'] = df_res_combined['std']/1000.
            leg_str = '1std'
        elif filledrangetype == 'extremes':
            df_min_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').min().interpolate()
            #df_min_values = df_min_values.reset_index()
            df_max_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').max().interpolate()
            #df_max_values = df_max_values.reset_index()
            df_res_combined['upper'] = df_max_values.values
            df_res_combined['lower'] = df_min_values.values
            leg_str = 'extremes'
        else:
            print('you need to pass in a filledrangetype')

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            df_res_combined[skill]=df_res_combined[skill]/1000.
            df_res_combined['upper'] = df_res_combined['upper']/1000.
            df_res_combined['lower'] = df_res_combined['lower']/1000.

        #make the plot
        if plotlog is True:
            #ylab = 'log(' +  skill_label_params[skill['ylab']]+ ')'
            #ax.semilogy(df_res_combined['hours since start'][1:-2], df_res_combined[skill][1:-2], lcolors, label=yleg)
            print('I can not do this yet :(')
            exit()
        else:
            ax.fill_between(
                df_res_combined['hours since start'][1:-1],
                df_res_combined['lower'][1:-1],
                df_res_combined['upper'][1:-1],
                alpha=0.2,
                facecolor=lcolors[modelrun],
                label=leg_str)
            ax.plot(df_res_combined['hours since start'][1:-1],df_res_combined['upper'][1:-1],'k',linewidth=0.5, label='_nolegend_')
            ax.plot(df_res_combined['hours since start'][1:-1],df_res_combined['lower'][1:-1],'k',linewidth=0.5, label='_nolegend_')

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])
 
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('Hours since start', fontsize=10)
    ax.set_ylabel(ylab, fontsize=10)
    #ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=10)

    #save the plot or show it
    filledavgskillfignamepng = "comparison_{}_{}_{}.png".format(str('-'.join(datadirs.keys())),skill,filledrangetype)

    if savefig:
        print('saving', filledavgskillfignamepng)
        #print('saving ', os.path.join(savedir,filledavgskillfignamepng))
        plt.savefig(os.path.join(savedir,filledavgskillfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        pass



if __name__ == '__main__':
    main()


