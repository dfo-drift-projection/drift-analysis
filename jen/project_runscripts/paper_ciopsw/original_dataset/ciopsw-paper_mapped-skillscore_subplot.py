#!/usr/bin/env python

import os
os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt

import os, sys
import xarray as xr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap
import numpy as np
import numpy.random as npr

from datetime import datetime
from datetime import timedelta
import glob
import pandas as pd
import pickle

import argparse
import matplotlib.colors as cm

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)


def main():

    DATE_STR = '20160101'
    DATE_END = '20161231'
    exp_dur = 48
    tstep = 48
    leadt_list = range(tstep, exp_dur+1, tstep)
    usepickle =  'ignore_pickle' #'make_pickle' #'read_pickle'

    import pickle
    picklename = 'pickled_data' + '_' + 'CIOPSW-daily-cropped' + '_' + str(DATE_STR) + '_' + str(DATE_END) + '.pickle'
    picklename2 = 'pickled_data' + '_' + 'RIOPSW-daily-cropped' + '_' + str(DATE_STR) + '_' + str(DATE_END) + '.pickle'
    picklename3 = 'pickled_data' + '_' + 'CIOPSW-hourly-full' + '_' + str(DATE_STR) + '_' + str(DATE_END) + '.pickle'

    dir_basepath = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/two-percent-winds/daily-datasets/CIOPSW_daily-RIOPS_daily/cropped_datasets' 
    #pathoutfig = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/ciopsw-paper_comparison-plots/two-percent-winds/map_plots/'
    pathinroot=os.path.join(dir_basepath,'CIOPSW_daily_cropped-dataset/')
    pathinroot2=os.path.join(dir_basepath,'RIOPS_daily_cropped-dataset/')
    pathinroot3='/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/ciopsw/ciopsw_hourly_windage2/all_output/output_per_drifter/'
    pathoutfig='/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/jen/paper_ciopsw/'


    #including a check that will exist if the drift-tool output folder is not present
    #this could be due to an error or inadequate ocean/drifter data to run the tool.
    if os.path.isdir(pathinroot) == False:
        sys.exit('No DriftEval output exists so no further processing required')

    #making the savedir if it doesn't exist already
    if os.path.isdir(pathoutfig) == False:
        os.mkdir(pathoutfig)

    #for CIOPSW paper
    if usepickle == 'make_pickle':
        print('creating df1')
        df_all = create_master_dataframe(pathinroot)
        df_all.to_pickle(os.path.join(pathoutfig,picklename))
        print('creating df2')
        df_all2 = create_master_dataframe(pathinroot2)
        df_all2.to_pickle(os.path.join(pathoutfig,picklename2))
        print('creating df3')
        df_all3 = create_master_dataframe(pathinroot3)
        df_all3.to_pickle(os.path.join(pathoutfig,picklename3))
    elif usepickle == 'read_pickle':
        df_all = pd.read_pickle(os.path.join(pathoutfig,picklename))
        df_all2 = pd.read_pickle(os.path.join(pathoutfig,picklename2))
        df_all3 = pd.read_pickle(os.path.join(pathoutfig,picklename3))
    else:
        print('creating df1')
        df_all = create_master_dataframe(pathinroot)
        print('creating df2')
        df_all2 = create_master_dataframe(pathinroot2)
        print('creating df3')
        df_all3 = create_master_dataframe(pathinroot3)


    #df1: there are 88 unique buoyids and 1983 model runs over all the buoyids
    #df2: there are 88 unique buoyids and 1716 model runs over all the buoyids
    #df3: there are 103 unique buoyids and 2107 model runs over all the buoyids

    #############################################################################
    # common parameters
    #############################################################################

    grid_spacing = 80 #I think this can also be ['100','100'], etc
    edge_color = 'black' #can be a color like 'black' or 'face' for no color
    opacity_level = 1 #0.8

    #############################################################################
    # plot maps as subplots for ciopsw
    #############################################################################

    print("plotting map subplots")
    import matplotlib.pyplot as plt
    from matplotlib import gridspec

    fig = plt.figure(figsize=(15,10))
    #gs  = gridspec.GridSpec(1, 3, width_ratios=[1.1, 1.025, 0.9])
    gs  = gridspec.GridSpec(1, 3, width_ratios=[0.925, 1, 1])
    #gs  = gridspec.GridSpec(1, 3, width_ratios=[0.9, 1.025, 1])
    ax0 = plt.subplot(gs[0])
    ax1 = plt.subplot(gs[1])
    ax2 = plt.subplot(gs[2])

    lats = df_all['obs_lat'].tolist() + df_all['mod_lat'].tolist() + df_all2['obs_lat'].tolist() + df_all2['mod_lat'].tolist() + df_all3['obs_lat'].tolist() + df_all3['mod_lat'].tolist()
    lons = df_all['obs_lon'].tolist() + df_all['mod_lon'].tolist() + df_all2['obs_lon'].tolist() + df_all2['mod_lon'].tolist() + df_all3['obs_lon'].tolist() + df_all3['mod_lon'].tolist()
    minlon = np.nanmin(lons) - 0.5
    maxlon = np.nanmax(lons) + 0.5
    minlat = np.nanmin(lats) - 0.5
    maxlat = np.nanmax(lats) + 0.5

    print('..plotting tracks')
    plot_all_tracks(
        df_all3, DATE_STR, DATE_END,
        pathoutfig,
        obs_tracks=True, mod_tracks=True,
        start_dot=False, mod_start_dots=False,
        landmask_type=None,
        ax=ax0,
        par_labels=[1,1,0,0],
        mer_labels=[0,0,0,1],
        map_extremes=[minlon, maxlon, minlat, maxlat])
    #ax0.set_title('Observed Drifter Tracks 2016', fontsize=10)
    ax0.text(0.08, 0.05, '(a)', horizontalalignment='center',verticalalignment='center', transform=ax0.transAxes)

    print('..plotting hexbins for ciopsw hourly molcard')
    plot_hexbins(
        ['molcard'], [48], df_all3,
        pathoutfig, grid_spacing,
        edge_color, opacity_level,
        DATE_STR, DATE_END,
        landmask_type='ciopsw',
        ax=ax1,
        par_labels=[0,0,0,0],
        mer_labels=[0,0,0,1],
        map_extremes=[minlon, maxlon, minlat, maxlat])
    #ax1.set_title('Average Molcard Skill at 48h \n CIOPS-W (hourly data)', fontsize=10)
    ax1.text(0.08, 0.05, '(b)', horizontalalignment='center',verticalalignment='center', transform=ax1.transAxes)

    print('..plotting subtracted plot, hourly-daily ciopsw, sep')
    dataset_list = ['CIOPSW-daily','RIOPS-daily']
    plot_subtracted_hexbins(
        ['sep'], [48],
        df_all, df_all2,
        dataset_list, pathoutfig,
        grid_spacing, edge_color,
        opacity_level,
        DATE_STR, DATE_END,
        landmask_type='riops',
        ax=ax2, logplot=True,
        par_labels=[0,0,0,0],
        mer_labels=[0,0,0,1],
        map_extremes=[minlon, maxlon, minlat, maxlat])
    #ax2.set_title('Difference in average separation distance \n CIOPS-W (daily) - RIOPS (daily)', fontsize=10)
    ax2.text(0.08, 0.05, '(c)', horizontalalignment='center',verticalalignment='center', transform=ax2.transAxes)

    #fig.tight_layout(rect=[0, 0.03, 1, 0.95])
    #fig.suptitle(str('test'), fontsize=12)

    multimapfilename = 'ciopsw-paper_mapped-skillscore-subplot.png'
    multimappathname = os.path.join(pathoutfig,multimapfilename)
    plt.savefig(multimappathname, bbox_inches='tight',format='png', dpi=300)
    print('saving ', multimappathname)

    plt.close()



    #############################################################################
    # some individual plots
    #############################################################################

    #print()
    #print('plotting drifter tracks')
    ##for all the tracks
    ##mss.plot_all_tracks(
        #df_all3,DATE_STR, DATE_END,pathoutfig, 
        #obs_tracks=True, mod_tracks=True, 
        #start_dot=False, mod_start_dots=False, 
        #landmask_type='ciopsw', ax=None)

    #for one individual track
    #mss.plot_all_tracks(
        #df_all, obs_tracks=True, mod_tracks=True, start_dot=False, mod_start_dots=False, 
        #extraprefix='P2D-wp4962520867D20160810', landmask_type=None, ax=None)
 
    ##hexbins
    #mss.plot_hexbins(
        #['molcard'], leadt_list, df_all3, pathoutfig, grid_spacing, edge_color,opacity_level, 
        #DATE_STR, DATE_END, landmask_type='riops', ax=None)


    '''
    ##subtracted hexbins
    #print('make subtracted hexbin plots')

    dataset_list = ['CIOPSW-hourly','CIOPSW-daily']
    mss.plot_subtracted_hexbins(
        skill_list, leadt_list, 
        df_all, df_all2, 
        dataset_list, pathoutfig, 
        grid_spacing, edge_color, 
        opacity_level,
        DATE_STR, DATE_END, 
        landmask_type='riops', 
        ax=None, logplot=True)
    mss.plot_subtracted_hexbins(
        skill_list, leadt_list, 
        df_all, df_all2, 
        dataset_list, pathoutfig, 
        grid_spacing, edge_color, 
        opacity_level,
        DATE_STR, DATE_END,
        landmask_type='riops', 
        ax=None, logplot=False)
    '''


####################################################################################################################
# The necessary functions
# this was copied from /fs/vnas_Hdfo/odis/jeh326/drifters/drift-tool/src/driftutils/mapped_skillscores_functions.py
####################################################################################################################

def grab_buoyids(datadir):

    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir,'*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[-2])
    return np.unique(iname)


def setup_map(crnr=None, resolution='c', lat_coordinate=None, lon_coordinate=None, ax=None, par_labels=None, mer_labels=None, map_extremes=None):
    'Setup Basemap.'

    if crnr is None:
        bmap = Basemap(projection='robin', lon_0=0, resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 0]
        parallel_label = [0, 0, 0, 0]

    elif crnr == 'riops':
        d_crnr = {'lllon': -180, 'urlon': 180, 'lllat': 0, 'urlat': 90}
        bmap = Basemap(
            llcrnrlon=d_crnr['lllon'], 
            llcrnrlat=d_crnr['lllat'], 
            urcrnrlon=d_crnr['urlon'], 
            urcrnrlat=d_crnr['urlat'], 
            resolution=resolution, 
            ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    elif crnr == 'ciopsw':

        minlons = -142.28317260742188 -1
        minlats = 44.3333854675293  -1
        maxlons = -120.56839752197266 +1
        maxlats = 59.62149429321289 +1
        bmap = Basemap(projection='merc', 
            lat_0 = 57, 
            lon_0 = -135, 
            lat_ts = 57, 
            area_thresh = 0.1, 
            llcrnrlon=minlons, llcrnrlat=minlats, 
            urcrnrlon=maxlons, urcrnrlat=maxlats, 
            resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    elif crnr == 'data':
        if map_extremes is None:
            minlons = np.nanmin(lon_coordinate) - 0.5
            maxlons = np.nanmax(lon_coordinate) + 0.5
            minlats = np.nanmin(lat_coordinate) - 0.5
            maxlats = np.nanmax(lat_coordinate) + 0.5
        else:
            minlons = map_extremes[0]
            maxlons = map_extremes[1]
            minlats = map_extremes[2]
            maxlats = map_extremes[3]

        bmap = Basemap(projection='merc',
            lat_0 = 57,
            lon_0 = -135,
            lat_ts = 57,
            area_thresh = 0.1,
            llcrnrlon=minlons, llcrnrlat=minlats,
            urcrnrlon=maxlons, urcrnrlat=maxlats,
            resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 0]

    elif crnr == 'nps':
        bmap = Basemap(projection='npstere',boundinglat=30,lon_0=270,resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    elif crnr == 'lamb':
        bmap = Basemap(width=11000000,height=8000000,
            rsphere=(6378137.00,6356752.3142),\
            resolution=resolution,area_thresh=1000.,projection='lcc',\
            lat_1=45.,lat_2=55,lat_0=68,lon_0=-90., ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    else:
        d_crnr = {'lllon': -180, 'urlon': 180, 'lllat': -90, 'urlat': 90}
        for key in d_crnr.iterkeys():
            crnr.setdefault(key, d_crnr[key])
        bmap = Basemap(llcrnrlon=crnr['lllon'], llcrnrlat=crnr['lllat'], urcrnrlon=crnr['urlon'], urcrnrlat=crnr['urlat'], resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    #common formatting
    if mer_labels is not None:
        meridian_label = mer_labels
    if par_labels is not None:
        parallel_label = par_labels

    if crnr != 'data':
        minlons = bmap.llcrnrlon
        maxlons = bmap.urcrnrlon
        minlats = bmap.llcrnrlat
        maxlats = bmap.urcrnrlat

    if maxlons-minlons < 4:
        merfmt = '%.2f'
        merrotamt = 45
        merrange = (minlons, maxlons, 5)
    else:
        merfmt = '%.0f'
        merrotamt = 0 
        merrange = np.append(np.arange(np.ceil(minlons), np.floor(maxlons), 2), [np.floor(maxlons)])
        
    if maxlats-minlats < 4:
        parfmt = '%.2f'
        parrotamt = 45
        parrange = (minlats, maxlats, 5)
    else:
        parfmt = '%.0f'
        parrotamt = 90
        parrange = np.append(np.arange(np.ceil(minlats), np.floor(maxlats), 2), [np.floor(maxlats)])

    bmap.drawmeridians(merrange, dashes=[1,1],linewidth=0.1, fontsize=8,fmt=merfmt, labels=meridian_label,rotation=merrotamt)
    bmap.drawparallels(parrange, dashes=[2,2],linewidth=0.1,fontsize=8, fmt=parfmt,labels=parallel_label, rotation=parrotamt)

    return bmap

def create_master_dataframe(pathinroot):

    df_all = pd.DataFrame()
    pathin = pathinroot
    buoyids = grab_buoyids(pathin)
 
    print('there are ', len(buoyids), ' unique buoyids')
    mod_release_count = 0
    for buoyid in buoyids:  
        fname = os.path.join(pathin, buoyid+'_aggregated.nc')
        #print('the file to open is...',fname)
        with xr.open_dataset(fname) as ds:

            ds['time_since_start'] = ds['time'].astype(np.timedelta64)
            ds['initial_lat'] = ds['mod_lat'].astype(float)
            ds['initial_lon'] = ds['mod_lon'].astype(float)

            for m in ds.model_run.values:
                mod_release_count += 1
                ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64)
                ds['initial_lat'][m, :] = ds['mod_lat'][m, 0]
                ds['initial_lon'][m, :] = ds['mod_lon'][m, 0]

            df = ds.to_dataframe()
            df.insert(0,'buoyid', buoyid, True)
            df_all = pd.concat([df_all, df])

    print('there are ', mod_release_count, ' model runs over all the buoyids')
    #to round to the hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600))
    #to round to .1 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600, 1))
    #to round to .5 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 1800) / 2.0)
    #to round to the hour before:
    df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

    return df_all


def plot_land_mask(bmap, landmask_type=None):

    import matplotlib.colors as colors
    land_color="gray" #or maybe "silver"?
    water_color="white"

    if landmask_type is None:
        print('..drawing land from default basemap landmask')
        # draw coastlines.
        bmap.drawcoastlines(linewidth=0.5)
        bmap.drawmapboundary(linewidth=0.5, fill_color=water_color)
        bmap.fillcontinents(color="silver",lake_color=water_color)
    else:
        print('..drawing land from ',landmask_type, ' landmask')
        if landmask_type=='riops':
            ocean_mesh_file='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
            lon_var='nav_lon'
            lat_var='nav_lat'
            tmask_var='tmask'
        elif landmask_type=='ciopsw':
            ocean_mesh_file='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
            lon_var='nav_lon'
            lat_var='nav_lat'
            tmask_var='tmask'
        else:
            print('Unrecognized landmask_type specified (should be riops, ciopsw). Using default basemap to draw land instead.')
            bmap.drawcoastlines(linewidth=0.5)
            bmap.drawmapboundary(linewidth=0.5, fill_color=water_color)
            bmap.fillcontinents(color="silver",lake_color=water_color)

        #assuming a landmask was given:
        ds = xr.open_dataset(ocean_mesh_file)
        lons = ds[lon_var].values
        lats = ds[lat_var].values
        mask = np.squeeze(ds[tmask_var].values)
        while mask.ndim > 2:
            mask = mask[0, ...]
        mask_masked = np.ma.masked_values(mask, 1)
        x, y = bmap(lons, lats)
        cmap = colors.LinearSegmentedColormap.from_list("", [land_color,water_color])
        # turn on/off for filled land
        # WARNING: CIOPS land isn't filled entirely because of domain boundaries
        bmap.contourf(x,y,mask_masked, levels=[0,1],cmap=cmap)
        # turn on/off contour below to show landmask edges
        # WARNING: RIOPS mask is strange in Pacific Ocean so edges appear below 46N
        bmap.contour(x,y,mask,levels=[0,],colors='k',linewidths=0.5)


def plot_all_tracks(df_all,DATE_STR,DATE_END,pathoutfig, obs_tracks=True, mod_tracks=False, start_dot=False, mod_start_dots=False, extraprefix=None,landmask_type=None, ax=None, bmap=None, par_labels=None, mer_labels=None, map_extremes=None):
    
    buoyids = np.unique(df_all.buoyid.values)
    trackcounter = 0

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    lat_coor = df_all['obs_lat'].tolist() + df_all['mod_lat'].tolist()
    lon_coor = df_all['obs_lon'].tolist() + df_all['mod_lon'].tolist()

    #lat_coor = df_all['initial_lat'].tolist()
    #lon_coor = df_all['initial_lon'].tolist()

    #lat_coor = [52.5, 53]
    #lon_coor = [-131,-130]

    if bmap is None:
        bmap = setup_map(crnr='data', resolution='h', lat_coordinate=lat_coor, lon_coordinate=lon_coor, ax=ax, par_labels=par_labels, mer_labels=mer_labels,map_extremes=None)

    #set up the land
    plot_land_mask(bmap, landmask_type=landmask_type)

    if DATE_STR != DATE_END:
        titlestrextra = str(DATE_STR) + ' - ' + str(DATE_END)
    else:
        titlestrextra = pd.to_datetime(ndate[0]).strftime('%Y-%m-%d')
    
    savestr = ''
    if obs_tracks == True:
        savestr = savestr + '_obs'
    if mod_tracks == True:
        savestr = savestr + '_mod'
    if start_dot == True:
        savestr = savestr + '_stdot'
    if mod_start_dots == True:
        savestr = savestr + '_modstdot'

    obs_color = 'black'
    mod_color = 'dimgray'
    dot_color = 'green'
    mod_dot_color = 'blue'

    #for each obs drifter
    #for buoyid in ['P2D-wp4962520867D20160810']: # buoyids:
    for buoyid in buoyids:
        trackcounter += 1

        if trackcounter == 1:
            obs_label = 'observed tracks'
            mod_label = 'modelled tracks'
            st_label = 'obs deployment points'
            mod_st_label = 'mod deployment points'
        else:
            obs_label = '_nolabel_'
            mod_label = '_nolabel_'
            st_label = '_nolabel_'
            mod_st_label = '_nolabel_'

        #print('.... plotting track ', trackcounter, ' of ', len(buoyids))
        drifterdf = df_all.loc[df_all['buoyid'] == buoyid]

        if obs_tracks == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    obs_label = '_nolabel_'
                lats = drifterdf['obs_lat'].loc[drifterdf.index.get_level_values('model_run') == m].values
                lons = drifterdf['obs_lon'].loc[drifterdf.index.get_level_values('model_run') == m].values
                x,y = bmap(lons, lats)
                #bmap.scatter(x, y, 0.05, marker='o', zorder=2, label=obs_label) 
                #bmap.plot(x, y, linewidth=0.3, zorder=2, label=obs_label) 
                bmap.plot(x, y, color=obs_color, linewidth=0.3, zorder=2, label=obs_label) 

        if mod_tracks == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    mod_label = '_nolabel_'
                mlats = drifterdf['mod_lat'].loc[drifterdf.index.get_level_values('model_run') == m].values
                mlons = drifterdf['mod_lon'].loc[drifterdf.index.get_level_values('model_run') == m].values
                mx,my = bmap(mlons, mlats)
                #bmap.scatter(mx, my, 0.5, marker='o', color=mod_color, zorder=1, label=mod_label)
                bmap.plot(mx, my, color=mod_color,linewidth=0.3, zorder=1, label=mod_label)

        if mod_start_dots == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    mod_st_label = '_nolabel_'
                mlats = drifterdf['mod_lat'].loc[drifterdf.index.get_level_values('model_run') == m].values
                mlons = drifterdf['mod_lon'].loc[drifterdf.index.get_level_values('model_run') == m].values
                msdlat = mlats[0]
                msdlon = mlons[0]
                msdx,msdy = bmap(msdlon, msdlat)
                bmap.scatter(msdx, msdy, 1, marker='o', color=mod_dot_color, zorder=100, label=mod_st_label)

        if start_dot == True:
            origlat = drifterdf['obs_lat'][0,0]
            origlon = drifterdf['obs_lon'][0,0]
            orx,ory = bmap(origlon, origlat)
            bmap.scatter(orx, ory, 1, marker='o', color=dot_color, zorder=10, label=st_label)

    ax.legend(loc='upper right', fontsize=8)
    #ax.legend(loc='best', fontsize=10)

    if savefig:
        ax.set_title('all drifter tracks' + '\n' + titlestrextra, fontsize=12)

    if savefig:
        if extraprefix is None:
            extraprefixstr = ''
        else:
            extraprefixstr = extraprefix + '_'
        savestr = extraprefixstr +'all-drifter-tracks_'+'_'+savestr+'.png'
        #savestr = extraprefixstr +'all-drifter-tracks_'+nom_ocemodel+'_'+nom_driftmodel+savestr+'.png'
        outfile=os.path.join(pathoutfig,savestr)
        print('.... saving map')
        plt.savefig(outfile,bbox_inches='tight',format='png', dpi=100)
        plt.close()
    else:
        pass


def compute_score_per_leadtime(df_all, skill, leadt):

    df_lead = df_all.loc[df_all['hours_since_start'] == leadt]
    fdate = np.unique(df_lead['time'])
    print('..gathering scores for leadt ',leadt, 'h')
    lat = df_lead.groupby(['buoyid','mod_start_date'])['initial_lat'].mean().values
    lon = df_lead.groupby(['buoyid','mod_start_date'])['initial_lon'].mean().values
    score = df_lead.groupby(['buoyid','mod_start_date'])[skill].mean().values

    # Pad out the score when there aren't enough data points
    score_pad = np.zeros_like(lat)
    score_pad[:] = np.nan
    score_pad[:score.shape[0]] = score
    score = score_pad

    return (df_lead, lat, lon, score)


def plot_hexbins(skill_list, leadt_list, df_all, pathoutfig, grid_spacing, edge_color, opacity_level, DATE_STR, DATE_END, landmask_type=None, ax=None, bmap=None, par_labels=None,mer_labels=None,map_extremes=None):
    for skill in skill_list :
        for leadt in leadt_list:
            df_lead, lat, lon, score = compute_score_per_leadtime(df_all, skill, leadt)
            print('..plotting score hexbins')
            do_hexbin_plot(df_lead, lat, lon, score, pathoutfig, skill, leadt, grid_spacing, edge_color, opacity_level,DATE_STR,DATE_END,landmask_type=landmask_type, ax=ax, bmap=bmap, par_labels=par_labels, mer_labels=mer_labels,map_extremes=map_extremes)


def do_hexbin_plot(df_lead, lat, lon, score, pathoutfig, skill, leadt, grid_spacing, edge_color,opacity_level, DATE_STR, DATE_END, landmask_type=None, ax=None, bmap=None, par_labels=None, mer_labels=None,map_extremes=None):

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    lat_coor = lat #df_lead['obs_lat'].tolist() + df_lead['mod_lat'].tolist()
    lon_coor = lon #df_lead['obs_lon'].tolist() + df_lead['mod_lon'].tolist()

    if bmap is None:
        bmap = setup_map(crnr='data', resolution='h', lat_coordinate=lat_coor, lon_coordinate=lon_coor, ax=ax, par_labels=par_labels, mer_labels=mer_labels, map_extremes=map_extremes)

    lon_x, lat_y = bmap(lon, lat)

    plot_land_mask(bmap, landmask_type=landmask_type)

    if DATE_STR != DATE_END:
        titlestrextra = str(DATE_STR) + ' - ' + str(DATE_END) 
    else:
        titlestrextra = pd.to_datetime(ndate[0]).strftime('%Y-%m-%d')


    import matplotlib.colors as colors
    import cmocean
    cmap_value  = cmap.get_cmap('cmo.dense', 5)

    if skill == 'sep':
        cbar_extreme = max(score)
        vmin_value = 0 
        vmax_value = cbar_extreme*1e-3
        colorbarlabel = 'Separation distance (km)'
        skilllabel = 'Separation distance'
        scorevalues = score*1e-3
        colorbarstr = ['0 km','20 km', '40 km', '60 km','80 km','100 km']
    else:
        vmin_value = 0
        vmax_value = 1.0
        if skill == 'molcard':
            skillcap = 'Molcard'
        else:
            skillcap = skill
        colorbarlabel = ' '.join([skillcap,'skill']) 
        colorbarstr = ['0','0.2','0.4','0.6','0.8','1.0']
        skilllabel = skill
        scorevalues=score

    sd_bin = bmap.hexbin(
        lon_x, lat_y,
        C=scorevalues,
        vmin=vmin_value,
        vmax=vmax_value,
        reduce_C_function=np.nanmean,
        linewidths=0.2,
        edgecolor=edge_color,
        gridsize=grid_spacing,
        zorder=100,
        alpha=opacity_level,
        cmap=cmap_value)
    #cbar = bmap.colorbar(sd_bin, location='bottom', pad='5%', label=colorbarlabel)
    cbar = bmap.colorbar(sd_bin, location='right', pad='2%', label=colorbarlabel)
    cbar.ax.set_yticklabels(colorbarstr)
    ticklabs = cbar.ax.get_yticklabels()
    cbar.ax.set_yticklabels(ticklabs, fontsize=8)

    
    if savefig:
        ax.set_title(skilllabel + ' at ' + str(leadt).zfill(3)+' hr' + '\n' + titlestrextra, fontsize=12)

    outfile=os.path.join(
        pathoutfig,
        'skillscoremap_'+skill+'_'+str(leadt).zfill(3)+'hr.png')

    if savefig:
        plt.savefig(outfile,bbox_inches='tight',format='png', dpi=200)        
        plt.close()
    else:
        #plt.show()
        pass

############################################################################################

def compute_subtracted_score_per_leadtime(df_all, skill, leadt):

    df_all['buoyid_modcoords'] = df_all['buoyid'] + 'LON' + df_all['mod_start_lon'].astype(str) + 'LAT' + df_all['mod_start_lat'].astype(str)
    df_all.dropna(subset=[skill],inplace=True)
    df_all_grouped_tracks = df_all.groupby(['buoyid_modcoords','hours_since_start']).mean()
    df_all_grouped_tracks.reset_index(inplace=True)
    df_lead = df_all_grouped_tracks.loc[df_all_grouped_tracks['hours_since_start'] == leadt]

    return df_lead

def plot_subtracted_hexbins(skill_list, leadt_list, df_all, df_all2, dataset_list,pathoutfig, grid_spacing, edge_color, opacity_level, DATE_STR, DATE_END, landmask_type=None, ax=None, logplot=None, bmap=None, par_labels=None, mer_labels=None, map_extremes=None):
    for skill in skill_list :
        for leadt in leadt_list:
            print('..making subtracted hexbin plot for',skill, 'at', leadt, 'h')

            #the dataframes at this point are full concatonated dataframes with data binned by drifter and by leadt
            #This gives a dataframe that still has multiple values for each buoyid, to account for all the times
            #that fall into the hour before leadt

            #bin by mod start coordinates/buoyid and hours since start, then subtract
            df_lead = compute_subtracted_score_per_leadtime(df_all, skill, leadt)
            df_lead2 = compute_subtracted_score_per_leadtime(df_all2, skill, leadt)

            unique_buoymodruns1 = np.unique(df_lead['buoyid_modcoords'].values)
            unique_buoymodruns2 = np.unique(df_lead2['buoyid_modcoords'].values)

            commonids = [value for value in unique_buoymodruns1 if value in unique_buoymodruns2]

            uncommonids1 = [value for value in unique_buoymodruns1 if value not in unique_buoymodruns2]
            uncommonids2 = [value for value in unique_buoymodruns2 if value not in unique_buoymodruns1]
            uncommon_check1 = df_lead.loc[df_lead['buoyid_modcoords'].isin(uncommonids1)]
            uncommon_check2 = df_lead.loc[df_lead['buoyid_modcoords'].isin(uncommonids2)]
            if uncommon_check1.empty == False:
                print('Why are all the model runs not in the common set? They should be!')
            if uncommon_check2.empty == False:
                print('Why are all the model runs not in the common set? They should be!')

            df_all_common = df_lead.loc[df_lead['buoyid_modcoords'].isin(commonids)]
            df_all_common.reset_index(inplace=True)
            df_all2_common = df_lead2.loc[df_lead2['buoyid_modcoords'].isin(commonids)]
            df_all2_common.reset_index(inplace=True)

            ulat_check1 = ''.join(df_all_common['mod_start_lat'].astype(str).values)
            ulat_check2 = ''.join(df_all2_common['mod_start_lat'].astype(str).values)
            ulon_check1 = ''.join(df_all_common['mod_start_lon'].astype(str).values)
            ulon_check2 = ''.join(df_all2_common['mod_start_lon'].astype(str).values)
            buoymodcoords_check1 = ''.join(df_all_common['buoyid_modcoords'].astype(str).values)
            buoymodcoords_check2 = ''.join(df_all2_common['buoyid_modcoords'].astype(str).values)
            if (ulat_check1 not in ulat_check2):
                print('common lat columns are not the same length? why?')
            if (ulon_check1 not in ulon_check2):
                print('common lon columns are not the same length? why?')
            if (buoymodcoords_check1 not in buoymodcoords_check2):
                print('common lon columns are not the same length? why?')

            subdf = pd.DataFrame()
            subdf['lat'] = df_all_common['mod_start_lat'].values
            subdf['lon'] = df_all_common['mod_start_lon'].values
            subdf['score1'] = (df_all_common[skill]).astype(float)
            subdf['score2'] = (df_all2_common[skill]).astype(float)
            subdf.dropna(subset=['score1','score2'], inplace=True)
            subdf['score'] = subdf['score1'] - subdf['score2']

            sub_lat = subdf['lat'].values
            sub_lon = subdf['lon'].values
            sub_score = subdf['score'].values

            savefig = False
            if ax is None:
                fig, ax = plt.subplots(figsize=(11,8.5))
                savefig = True

            do_subtracted_hexbin_plot(
                sub_lat,
                sub_lon,
                sub_score,
                skill, leadt, 
                grid_spacing,
                edge_color,opacity_level,
                dataset_list,
                pathoutfig,
                landmask_type=landmask_type,
                ax=ax,
                logplot=logplot,
                bmap=bmap,
                subtracted=True,
                par_labels=par_labels,
                mer_labels=mer_labels,
                map_extremes=map_extremes)

            if savefig:
                if skill == 'sep':
                    ax.set_title(''.join(['Average Separation Distance at ',str(leadt),'h \n','-'.join(dataset_list)]), fontsize=14)
                elif skill == '':
                    ax.set_title(''.join(['Average ', skill, ' at ',str(leadt),'h \n','-'.join(dataset_list)]), fontsize=14)

            if logplot:
                extralogstr = 'log_'
            else:
                extralogstr = 'linear_'

            subtractedplotfilename = 'comparison_mapped-skillscore_' + extralogstr + '-'.join(dataset_list) + '_' + str(skill) + '.png'
            subtractedplotpath = os.path.join(pathoutfig,subtractedplotfilename)

            if savefig:
                plt.savefig(subtractedplotpath,bbox_inches='tight',format='png', dpi=300)
                plt.close()
            else:
                pass



def do_subtracted_hexbin_plot(lat, lon, score, skill, leadt, grid_spacing, edge_color,opacity_level, dataset_list, pathoutfig, landmask_type=None, ax=None, logplot=None, bmap=None, subtracted=None, par_labels=None, mer_labels=None, map_extremes=None):

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    lat_coor = lat #df_all['mod_start_lat'].tolist()
    lon_coor = lon #df_all['mod_start_lon'].tolist()

    if bmap is None:
        bmap = setup_map(crnr='data', resolution='h', lat_coordinate=lat_coor, lon_coordinate=lon_coor, ax=ax, par_labels=par_labels, mer_labels=mer_labels, map_extremes=map_extremes)
    lon_x, lat_y = bmap(lon, lat)

    plot_land_mask(bmap, landmask_type=landmask_type)

    #if plotting subtracted datasets:
    import matplotlib.colors as colors
    #if subtracted:
    import cmocean
    #cmap_value = cmocean.cm.balance
    cmap_value  = cmap.get_cmap('cmo.balance')
    #cmap_value = 'coolwarm'
    #cmap_value  = cmap.get_cmap('viridis', 5)
    if skill == 'sep':
        cbar_extreme = max(max(score),abs(min(score)))
        import math 
        exp = math.ceil(math.log10(cbar_extreme*1e-3))
        vmin_value = cbar_extreme*-1e-3
        vmax_value = cbar_extreme*1e-3
        colorbarlabel = 'Separation distance difference (km)'
        skilllabel = 'Separation distance difference (km)'
        scorevalues = score*1e-3
        norm_value=colors.SymLogNorm(linthresh=1, linscale=1,vmin=vmin_value, vmax=vmax_value)
    else:
        vmin_value = -1.0
        vmax_value = 1.0
        colorbarlabel = 'skill score'
        colorbarstr = ['-1.0','-0.5','0.0','0.5','1.0']
        skilllabel = skill
        scorevalues=score
        norm_value=colors.SymLogNorm(linthresh=0.1, linscale=1,vmin=vmin_value, vmax=vmax_value)

    #if making a log plot version
    if logplot:
        sd_bin = bmap.hexbin(
            lon_x, lat_y,
            C=scorevalues,
            vmin=vmin_value,
            vmax=vmax_value,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            #mincnt=1,
            cmap=cmap_value,
            norm=norm_value)

    else:
        #the non-log version
        sd_bin = bmap.hexbin(
            lon_x, lat_y,
            C=scorevalues,
            vmin=vmin_value,
            vmax=vmax_value,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            #mincnt=1,
            cmap=cmap_value)

    #cbar = bmap.colorbar(sd_bin, location='bottom', pad='5%', label=colorbarlabel)
    cbar = bmap.colorbar(sd_bin, location='right', pad='2%', label=colorbarlabel)
    ticklabs = cbar.ax.get_yticklabels()
    cbar.ax.set_yticklabels(ticklabs, fontsize=8)

    if savefig:
        if skill == 'sep':
            ax.set_title(''.join(['Average Separation Distance at ',str(leadt),'h \n','-'.join(dataset_list)]), fontsize=14)
        else:
            ax.set_title(''.join(['Average ', skill, ' at ',str(leadt),'h \n','-'.join(dataset_list)]), fontsize=14)

    if logplot == True:
        extralogstr = 'log_'
    else:
        extralogstr = 'linear_'

    subtractedplotfilename = 'comparison_mapped-skillscore_' + extralogstr + '-'.join(dataset_list) + '_' + str(skill) + '.png'
    subtractedplotpath = os.path.join(pathoutfig,subtractedplotfilename)
    if savefig:
        plt.savefig(subtractedplotpath,bbox_inches='tight',format='png', dpi=300)
        plt.close()
    else:
        pass


############################################################################################

if __name__ == '__main__':
    main()


