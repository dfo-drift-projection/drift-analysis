#!/bin/bash

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
#export PATH=$HOME/data/sitestore/miniconda/bin:$PATH
export PATH=$HOME/work/software/miniconda/envs/develop/bin:$PATH

drifter_code_dir=$HOME/dfo-drift-projection/drift-tool/ # path to drift-tool code

cropping_config=$drifter_code_dir/examples/comparisons/user_comparison_crop_config.yaml

crop_to_common_comparison_set \
  --config $cropping_config 

