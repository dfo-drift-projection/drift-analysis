#!/bin/bash

MINICONDA_PATH=$HOME/work/software/miniconda/envs/develop/

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

#paths to directories with configuration files
crop_config_dir=$HOME/dfo-drift-projection/drift-tool/examples/comparisons/
plot_config_dir=$HOME/dfo-drift-projection/drift-tool/examples/plotting/

#crop the datasets to a common timeframe and spatial range. 
cropping_config=$crop_config_dir/user_comparison_crop_config.yaml

crop_to_common_comparison_set \
  --config $cropping_config 

#plot the output
plotting_config=$plot_config_dir/plotting_config.yaml
user_plotting_config=$plot_config_dir/user_comparison_plotting_config.yaml

plotting_workflow \
  --config $plotting_config \
  --user_config $user_plotting_config



