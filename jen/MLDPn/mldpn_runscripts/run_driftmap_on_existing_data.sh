#!/bin/bash
# Script to call MLDPn 
# Jen Holden 
# run with environment from ~/.profile_ECCC

# Define constants
path=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_runscripts/
outpath=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output/
outdir=$outpath/driftmap_results/
namelist_file=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/mldpn_input/MLDPn.in
drifter_code_dir=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/src/driftutils

# Source the profile script
. $path/settings_ECCC.sh #alternately, use all_sdfo_profile_settings.sh

#Parameters
mldp_config_file=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/yaml/mldp.yaml

sdfo_requests=/gpfs/fs4/dfo/dpnm/dfo_dpnm/sdfo000/hnas/ECCC_forecast/requests/
atm_data_dir=$sdfo_requests/OceanNavigator/nat.eta
ocean_data_dir=$sdfo_requests/OceanNavigator/riops.native

experiment_dir=$outdir/mldpn_first_test/
#rpn_file_dir=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/preprocess_results/ #was meteo
rpn_file_dir=/home/nso001/data/work2/sab002/gpfs-fs4-odis/opp/mldp_dev/meteo

MLDPn=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn

first_start_date="2018-04-03"
last_start_date="2018-04-03"
drift_duration=24 # reduced from 240 for testing (NKS October 2020)
start_date_frequency='daily'
start_date_interval=1
ocean_model_name="RIOPS"
drifter_depth=1c
num_particles_x=4 # reduced from 10 for testing (NKS Ocotber 2020)
num_particles_y=4 # reduced from 10 for testing (NKS Ocobter 2020)
bbox="-78.18400573730469 34.87752914428711 -37.71416091918945 54.3026237487793"
#bbox="-142.28317260742188 44.3333854675293 -120.56839752197266 59.62149429321289"
windage=5

cd $path

if [ -d "$experiment_dir" ]; then
    rm -r $experiment_dir
else
    mkdir -p $experiment_dir
fi


# Call drift map
drift_map \
  --experiment-dir $experiment_dir \
  --ocean-data-dir $ocean_data_dir \
  --atm-data-dir $atm_data_dir \
  --rpn-file-dir $rpn_file_dir \
  --ocean-model-name $ocean_model_name \
  --drift-duration $drift_duration \
  --num-particles-x $num_particles_x \
  --num-particles-y $num_particles_y \
  --bbox "$bbox" \
  --first-start-date "$first_start_date" \
  --last-start-date "$last_start_date" \
  --wind-coefficient $windage \
  --drifter-depth $drifter_depth \
  --mldp-config-file $mldp_config_file \
  --mldp-exec $MLDPn \
  --start-date-frequency=$start_date_frequency \
  --start-date-interval=$start_date_interval


# Call plot_drift_map
# commented out for testing (NKS Ocobter 2020)
#plot_drift_data \
#  --data_dir $experiment_dir/output \
#  --bbox "$bbox" \
#  --etopo_file '/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
#  --plot_dir $experiment_dir/plots \
#  --plot_style 'talk'


