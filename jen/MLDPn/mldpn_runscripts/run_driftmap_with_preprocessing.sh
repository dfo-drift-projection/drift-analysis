
# Jen Holden 
# run with environment from ~/.profile_ECCC

# Define shared paths
path=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_runscripts/
outpath=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output/

# define the raw data for preprocessing
raw_ocean_data_rpn=/fs/vnas_Hdfo/dpnm/jeh326/work/projects/mldpn_implementation_output/raw_rpn_data/ciopse_forcast/
raw_meteo_data_rpn=/fs/vnas_Hdfo/dpnm/jeh326/work/projects/mldpn_implementation_output/raw_rpn_data/hrdps/
raw_data_outdir=$outpath/mldpn_input/
preprocessing_outdir=$outpath/preprocess_results/

# define paths for MLDPn
namelist_file=$path/MLDPn.in
drifter_code_dir=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/src/driftutils
mldp_config_file=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/yaml/mldp.yaml

#define paths for driftmap
experiment_dir=$outpath/driftmap_results/mldpn_second_test/

# Source the profile script (this loads the ECCC tools) .
. $path/settings_ECCC.sh 

########################################################################
# run the preprocessing
########################################################################

## clean up the preprocessing output folder
#if [ -d "$raw_data_outdir" ]; then
#    rm $raw_data_outdir/*
#else
#    mkdir -p $raw_data_outdir
#fi

#create_filelist=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/create_full_path_file_list.sh
#$create_filelist $raw_ocean_data_rpn $raw_data_outdir wetfiles
#$create_filelist $raw_meteo_data_rpn $raw_data_outdir metfiles

# clean up the preprocessing output folder
#if [ -d "$preprocessing_outdir" ]; then
#    rm $preprocessing_outdir/*
#else
#    mkdir -p $preprocessing_outdir
#fi

## to create a mask, set resmask to something other than 0 (ie, 25 or 100, etc)
#preprocessing_script=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/script/EERWetDB.tcl
#$preprocessing_script \
#    -bbox 48.0 -52.2 49.0 -51.0 \
#    -resmask 0 \
#    -metfiles $raw_data_outdir/metfiles \
#    -wetfiles $raw_data_outdir/wetfiles \
#    -out $preprocessing_outdir \
#    -interp \
#    -depth 0 \
#    -waves false

## At this point, the raw RPN wet/met files have been preprocessed and a 
## mask has been created.

MLDPn_executable=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
first_start_date="2020-12-07"
last_start_date="2020-12-07"
drift_duration=24 
start_date_frequency='daily'
start_date_interval=1
ocean_model_name="CIOPS-E"
drifter_depth=1c
num_particles_x=4  
num_particles_y=4 
bbox="-52.2 48.0 -51.0 49.0"
windage=5

cd $path

if [ -d "$experiment_dir" ]; then
    rm -r $experiment_dir
fi


########################################################################
# try running mldpn standalone

# Define constants
#path=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/
#outpath=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output/
#outdir=$outpath/mldpn_results/
#namelist_file=$path/mldpn_input/MLDPn.in
mkdir -p /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output//driftmap_results/mldpn_second_test//logs/
cd $path
MLDPn -i $namelist_file -o $experiment_dir -t 1 --nompi >$experiment_dir/logs/MLDPn.out 2>$experiment_dir/logs/MLDPn.err

########################################################################

## Call drift map
#drift_map \
#  --experiment-dir $experiment_dir \
#  --ocean-data-dir $raw_ocean_data_rpn \
#  --atm-data-dir $raw_meteo_data_rpn \
#  --rpn-file-dir $preprocessing_outdir \
#  --ocean-model-name $ocean_model_name \
#  --drift-duration $drift_duration \
#  --num-particles-x $num_particles_x \
#  --num-particles-y $num_particles_y \
#  --bbox "$bbox" \
#  --first-start-date "$first_start_date" \
#  --last-start-date "$last_start_date" \
#  --wind-coefficient $windage \
#  --drifter-depth $drifter_depth \
#  --mldp-config-file $mldp_config_file \
#  --mldp-exec $MLDPn_executable \
#  --start-date-frequency=$start_date_frequency \
#  --start-date-interval=$start_date_interval


# Call plot_drift_map
# commented out for testing (NKS Ocobter 2020)
#plot_drift_data \
#  --data_dir $experiment_dir/output \
#  --bbox "$bbox" \
#  --etopo_file '/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
#  --plot_dir $experiment_dir/plots \
#  --plot_style 'talk'
