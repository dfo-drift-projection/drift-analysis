#!/bin/bash
# Script to call MLDPn 
# Jen Holden 
# run with environment from ~/.profile_ECCC

# Define constants
path=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/
outpath=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output/
outdir=$outpath/mldpn_results/
namelist_file=$path/mldpn_input/MLDPn.in

# Source the profile script
. $path/settings_ECCC.sh #alternately, use all_sdfo_profile_settings.sh

cd $path

if [ -d "$outdir" ]; then
    rm $outdir/*  
else
    mkdir -p $outdir
fi

cd $path
MLDPn -i $namelist_file -o $outdir -t 1 --nompi >$outdir/logs/MLDPn.out 2>$outdir/logs/MLDPn.err


