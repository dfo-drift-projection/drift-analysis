# pre ===================================
. ssmuse-sh -x hpci/opt/hpcarchive-latest
. ssmuse-sh -x eccc/mrd/rpn/libs/19.5
. ssmuse-sh -d eccc/cmo/cmoi/base/20200123
. ssmuse-sh -d eccc/cmo/cmoe/eer/beta
. ssmuse-sh -x eccc/cmd/cmds/apps/grib/grib2-cvt/2.0
. ssmuse-sh -x eccc/cmd/cmds/apps/SPI/beta
export CMD_EXT_PATH=/fs/ssm/eccc/cmd/cmds/ext/20200129
. ssmuse-sh -x /fs/ssm/eccc/cmd/cmds/ext/20200129
. ssmuse-sh -d /fs/ssm/eccc/cmd/cmds/base/201911/01/default

# plat.ubuntu-18.04-amd64-64 ===================================
. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.2

# post ===================================
export OMP_STACKSIZE=4G
export SSM_DEV=/home/ords/cmod/cmoe/dev/jeh326/ssm
# where is MLDPn now?
#export MLDPn=/home/sdfo000/sitestores/gpfs-fs2/sdfo000/eccc/cmo/cmoe/eer/master/eerModel_4.2.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
export MLDPn=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
export EERWetDB=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/script/EERWetDB.tcl
export GEO_PATH=/space/group/dfo_odis/project/sdfo000/geo
export GDB_PATH=/space/group/dfo_odis/project/sdfo000/geo
export GDB_PATH=/space/group/dfo_odis/project/sdfo000/geo/DBGeo/data
typeset -fx chdir
umask 022

export KMP_AFFINITY=disabled
export OMPI_MCA_btl_openib_if_include=mlx5_0
export OMPI_MCA_orte_tmpdir_base=/run/shm

