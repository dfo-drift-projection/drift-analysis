#!/bin/bash
# Script to call EEWetDB for processing riops data for us in MLDP
# Jen Holden 
# run with environment from ~/.profile_ECCC

# Define constants
path=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/MLDPn/mldpn_standalone/
outpath=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/mldpn_implementation_output/
metfiles=$path/mldpn_input/metfiles
wetfiles=$path/mldpn_input/wetfiles
outdir=$outpath/preprocess_results/

# Source the profile script
. $path/settings_ECCC.sh #alternately, use all_sdfo_profile_settings.sh

cd $path
if [ -d "$outdir" ]; then
    rm $outdir/*
else
    mkdir -p $outdir
fi

script=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/script/EERWetDB.tcl

## from the sdfo000 sample run
$script \
    -bbox 47.9 -63.9 48.8 -62.3 \
    -resmask 100 \
    -metfiles $metfiles \
    -wetfiles $wetfiles \
    -out $outdir \
    -interp \
    -depth 0 \
    -waves false

## from samuel's script:
#res=1000       # resoltuion of output grid
#resmask=100    # resolution of mask
#depth=1        # depth in NEMO in m
#bbox='44.3333854675293 -142.28317260742188 47.62149429321289 -137.56839752197266' #bounding box
#$script \
#   -metfiles $metfiles \
#   -wetfiles $wetfiles \
#   -depth $depth \
#   -resmask 0 \
#   -bbox $bbox \
#   -out $out \
#   -waves false \
#   -interp \
#   -res $res \
#   -verbose DEBUG
