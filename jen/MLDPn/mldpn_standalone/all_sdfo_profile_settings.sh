#Intel Compiler
#. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
#. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
#. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.1

export TRUE_HOST=gpsc1
#----- HPCO compiler environment
#<gcc>
#. ssmuse-sh -x /fs/ssm/hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--gcc
#</gcc> <intel>
. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
#</intel>
. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.2
#----- HPCO compiler environment
#<gcc>
#. ssmuse-sh -x /fs/ssm/hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--gcc
#</gcc> <intel>
. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
#</intel>
. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.2
#Large grid fail on fstluk when using OpenMP
export OMP_STACKSIZE=4G
export SSM_DEV=/home/ords/cmod/cmoe/dev/${USER}/ssm
export MLDPn=/home/sdfo000/sitestores/gpfs-fs2/sdfo000/eccc/cmo/cmoe/eer/master/eerModel_4.2.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
export GEO_PATH=/space/group/dfo_odis/project/sdfo000/geo
export GDB_PATH=$GEO_PATH
export GDB_PATH=$GEO_PATH/DBGeo/data
typeset -fx chdir
umask 022

export TMPDIR=$(mktemp -d)
export TRUE_HOST="$ORDENV_TRUEHOST"
# HPCS
. ssmuse-sh -x hpci/opt/hpcarchive-latest

# RPN
. ssmuse-sh -x eccc/mrd/rpn/libs/19.5
#. ssmuse-sh -x eccc/mrd/rpn/utils/19.1

# CMOI
. ssmuse-sh -d eccc/cmo/cmoi/base/20200123

# CMOE
. ssmuse-sh -d eccc/cmo/cmoe/eer/beta

#CMDS
. ssmuse-sh -x eccc/cmd/cmds/apps/grib/grib2-cvt/2.0
#. ssmuse-sh -x eccc/cmd/cmds/apps/SPI/20191201
. ssmuse-sh -x eccc/cmd/cmds/apps/SPI/beta
export CMD_EXT_PATH=/fs/ssm/eccc/cmd/cmds/ext/20200129
#export CMD_EXT_PATH=/fs/ssm/eccc/cmd/cmds/ext/20190702
. ssmuse-sh -x $CMD_EXT_PATH
#. ssmuse-sh -x eccc/cmd/cmds/ext/20190702
. ssmuse-sh -d /fs/ssm/eccc/cmd/cmds/base/201911/01/default
export TMPDIR=$(mktemp -d)
export TRUE_HOST="$ORDENV_TRUEHOST"
. ssmuse-sh -x comm/eccc/all/base/20180130
. ssmuse-sh -x comm/eccc/all/exp/ordsoumet-1.19
export TMPDIR=$(mktemp -d)
export TRUE_HOST="$ORDENV_TRUEHOST"
# ------------------------------------
# Loading the maestro package and export an environment variable

#. ssmuse-sh -d /fs/ssm/eccc/cmo/isst/maestro/1.5.1
#export SEQ_MAESTRO_SHORTCUT=". ssmuse-sh -d eccc/cmo/isst/maestro/1.5.1"

# Maestro Ubuntu 18
. ssmuse-sh -d /fs/ssm/eccc/cmo/isst/maestro/1.5.3.2
export SEQ_MAESTRO_SHORTCUT=". ssmuse-sh -d eccc/cmo/isst/maestro/1.5.3.2"

#colors
export PS1="\[\e[01;33m\][\[\e[m\]\[\e[01;33m\]\u\[\e[m\]\[\e[01;33m\]@\[\e[m\]\[\e[01;33m\]\h\[\e[m\]:\[\e[01;36m\]\w\[\e[m\]\[\e[01;36m\]]\[\e[m\\$\[\e[m\] "

# -------------------------------------
# export

#export PS1='\u@\h:\w\$ '
export TERM=xterm
stty erase ^?
stty erase ''

# -------------------------------------
# some ls aliases
alias ll='ls -larth --color=auto'
alias lll='ls -lFh --color=auto'
alias ld='ls -ld --color=auto */'
alias lt='ls -lht --color=auto'
alias la='ls -AlFh --color=auto'

# -------------------------------------
# some other aliases

alias h='history'
alias dudi='du -c -h --max-depth=1 | sort -h'

