#!/bin/bash

########################################################################
# datasets downloaded from /home/sdfo501/links/eccc_forecasts/ on October 25, 2021
# Dates: 202110.... (00h forecasts)
# note: for the ciops netcdf files, I kept *00_000.tar
#
# hrdps_national_20191231
# -----------------------
# forecast (prog/lam/2p5/nat.eta)
# rpn from:    /gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/hrdps_national_20191231/gridpt/prog/lam/2p5/nat.eta
# netcdf from: converted via ~/dfo-drift-projection/drift-analysis/jen/projects/hrdps_convert/
#
# ciops_west_20200707 (forecast)
# ------------------------------
# rpn from:    /gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/gridpt/prog/ciops
# netcdf from: /gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/netcdf/prog
#
# ciops_east_20200608 (forecast)
# ------------------------------
# rpn from:    /gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/gridpt/prog/ciops
# netcdf from: /gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/netcdf/prog
#
# For both RPN and NetCDF files, I only kept files that ended in *00_*
# (extra notes on contents of forecast directories below)
#
# a good outdir_base=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/data_download_20211025/
########################################################################

usage() {
    ustr="\nUsage: grab_forecast_data.sh -d <datatype> -o <outdir_base>\n\n" 
    ustr="${ustr}NOTE: \"datatype\" will be one of ciopsw_rpn, ciopsw_netcdf," 
    ustr="$ustr ciopse_rpn, ciopse_netcdf, hrdps_rpn).\n\"outdir_base\" is" 
    ustr="$ustr the base directory where the output will be stored. The" 
    ustr="$ustr actual savedir will be \na subfolder inside this directory" 
    ustr="$ustr named ${datatype}."
    echo -e $ustr
    exit 1
}

# check for arguments being passed in at the command line.
while getopts "h?d:o:" args; do
case $args in
    h|\?)
        usage
        exit 1;;
    d) datatype=${OPTARG};;
    o) outdir_base=${OPTARG};;
    :) echo "Missing option argument for -$OPTARG"; exit 1;;
    *) echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# check for the existence of datatype and outdir_base. Both are required
# in order to run this script.
for VAR in datatype outdir_base; do
    if [ -z "${!VAR}" ]; then
        echo -e "\nThe argument $VAR was not found or was incorrect"
        usage 
    fi
done

# first, figure out which data to grab:
echo -e "\ngrabbing ${datatype} data"
case ${datatype} in
hrdps_national)
  indir=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/hrdps_national_20191231/gridpt/prog/lam/2p5/nat.eta ;;
ciopse_rpn)
  indir=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/gridpt/prog/ciops ;;
ciopse_netcdf)
  indir=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/netcdf/prog ;;
ciopsw_rpn)
  indir=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/gridpt/prog/ciops ;;
ciopsw_netcdf)
  indir=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/netcdf/prog/ ;;
all)
  echo -e "\nabout to rsync the forecast folders for all datatypes" ;;
*)
  echo -e "\nno datatype provided" ;;
esac

# define a function that will do the rsync:
grab_data() {
    datadir=$1
    outdir=$2
    echo -e "\npulling data from ${1}"
    echo -e "writing data to ${2}"
    num_infiles=$(ls $datadir/*00_* | wc -l)
    echo -e "Attempting to sync ${num_infiles} files\n"
    mkdir -p $outdir
    rsync -avHPSx ${datadir}/*00_* ${outdir}/
}

#log_dir=${outdir_base}/logs/
#mkdir -p $log_dir
#logfile=${log_dir}/forcast_data_transfer.txt
#da=$(date)
#echo $da >> ${logfile}

# actually start the transfer:
if [[ $datatype == "all" ]]; then
    grab_data $national_eta_rpn ${outdir_base}/hrdps_national_rpn/ #>> ${logfile}
    grab_data $ciopsw_rpn ${outdir_base}/ciopsw_rpn/ #>> ${logfile}
    grab_data $ciopsw_netcdf ${outdir_base}/ciopsw_netcdf/ #>> ${logfile}
    grab_data $ciopse_rpn ${outdir_base}/ciopse_rpn/ #>> ${logfile}
    grab_data $ciopse_netcdf ${outdir_base}/ciopse_netcdf/ #>> ${logfile}
else
    outdir=${outdir_base}/${datatype}
    grab_data $indir $outdir #>> ${logfile}
fi

########################################################################
# Extra notes on naming conventions
########################################################################
#
##HRDPS
#national_eta_rpn=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/hrdps_national_20191231/gridpt/prog/lam/2p5/nat.eta
#west_eta_rpn=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/hrdps_West_20191231/gridpt/prog/lam/1p0/west.eta
#
##CIOPSW
#ciopsw_rpn=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/gridpt/prog/ciops
#ciopsw_netcdf=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_west_20200707/forecast/netcdf/prog/
#
##CIOPSE
#ciopse_rpn=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/gridpt/prog/ciops
#ciopse_netcdf=/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/ciops_east_20200608/forecast/netcdf/prog
#
# The RPN files have names like:
#2021102400_000*  2021102400_018*  2021102400_036*  2021102406_005*  2021102406_023*  2021102406_041*  2021102412_010*  2021102412_028*  2021102412_046*  2021102418_015*  2021102418_033*
#2021102400_001*  2021102400_019*  2021102400_037*  2021102406_006*  2021102406_024*  2021102406_042*  2021102412_011*  2021102412_029*  2021102412_047*  2021102418_016*  2021102418_034*
#2021102400_002*  2021102400_020*  2021102400_038*  2021102406_007*  2021102406_025*  2021102406_043*  2021102412_012*  2021102412_030*  2021102412_048*  2021102418_017*  2021102418_035*
#2021102400_003*  2021102400_021*  2021102400_039*  2021102406_008*  2021102406_026*  2021102406_044*  2021102412_013*  2021102412_031*  2021102418_000*  2021102418_018*  2021102418_036*
#2021102400_004*  2021102400_022*  2021102400_040*  2021102406_009*  2021102406_027*  2021102406_045*  2021102412_014*  2021102412_032*  2021102418_001*  2021102418_019*  2021102418_037*
#2021102400_005*  2021102400_023*  2021102400_041*  2021102406_010*  2021102406_028*  2021102406_046*  2021102412_015*  2021102412_033*  2021102418_002*  2021102418_020*  2021102418_038*
#2021102400_006*  2021102400_024*  2021102400_042*  2021102406_011*  2021102406_029*  2021102406_047*  2021102412_016*  2021102412_034*  2021102418_003*  2021102418_021*  2021102418_039*
#2021102400_007*  2021102400_025*  2021102400_043*  2021102406_012*  2021102406_030*  2021102406_048*  2021102412_017*  2021102412_035*  2021102418_004*  2021102418_022*  2021102418_040*
#2021102400_008*  2021102400_026*  2021102400_044*  2021102406_013*  2021102406_031*  2021102412_000*  2021102412_018*  2021102412_036*  2021102418_005*  2021102418_023*  2021102418_041*
#2021102400_009*  2021102400_027*  2021102400_045*  2021102406_014*  2021102406_032*  2021102412_001*  2021102412_019*  2021102412_037*  2021102418_006*  2021102418_024*  2021102418_042*
#2021102400_010*  2021102400_028*  2021102400_046*  2021102406_015*  2021102406_033*  2021102412_002*  2021102412_020*  2021102412_038*  2021102418_007*  2021102418_025*  2021102418_043*
#2021102400_011*  2021102400_029*  2021102400_047*  2021102406_016*  2021102406_034*  2021102412_003*  2021102412_021*  2021102412_039*  2021102418_008*  2021102418_026*  2021102418_044*
#2021102400_012*  2021102400_030*  2021102400_048*  2021102406_017*  2021102406_035*  2021102412_004*  2021102412_022*  2021102412_040*  2021102418_009*  2021102418_027*  2021102418_045*
#2021102400_013*  2021102400_031*  2021102406_000*  2021102406_018*  2021102406_036*  2021102412_005*  2021102412_023*  2021102412_041*  2021102418_010*  2021102418_028*  2021102418_046*
#2021102400_014*  2021102400_032*  2021102406_001*  2021102406_019*  2021102406_037*  2021102412_006*  2021102412_024*  2021102412_042*  2021102418_011*  2021102418_029*  2021102418_047*
#2021102400_015*  2021102400_033*  2021102406_002*  2021102406_020*  2021102406_038*  2021102412_007*  2021102412_025*  2021102412_043*  2021102418_012*  2021102418_030*  2021102418_048*
#2021102400_016*  2021102400_034*  2021102406_003*  2021102406_021*  2021102406_039*  2021102412_008*  2021102412_026*  2021102412_044*  2021102418_013*  2021102418_031*
#2021102400_017*  2021102400_035*  2021102406_004*  2021102406_022*  2021102406_040*  2021102412_009*  2021102412_027*  2021102412_045*  2021102418_014*  2021102418_032*
#
# The NetCDF files have names like:
#2021102400_000.tar*  2021102406_000.tar*  2021102412_000.tar*  2021102418_000.tar*

