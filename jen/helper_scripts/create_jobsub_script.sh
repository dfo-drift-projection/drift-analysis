#!/bin/bash

########################################################################
# create_jobsub_script.sh -i "exp_ident" -r "runscript_path"
# ----------------------------------------------------------
# script to write a job submission file that can be submitted to the 
# job queue. The script deals with creating a log dir, naming the log
# files, etc.
#
# assumes the following:
# ---------------------
# -P dfo_dpnm-fa3
# -pe dev 1
# res_cpus=1
# res_mem=100000
# res_tmpfs=0
# res_image=dfo/dfo_all_default_ubuntu-18.04-amd64_latest
# h_rt=06:00:00
########################################################################

# provide usage information
usage() {
    ustr="Usage: create_jobsub_script.sh" 
    ustr="$ustr -i <experiment identifier used to label the job submission"
    ustr="$ustr script and log files>" 
    ustr="$ustr -r <full path to runscript that will be submitted to the queue>"
    echo $ustr
    # if the required arguments are not provided, exit.
    exit 1;
}

# check for arguments being passed in at the command line. 
while getopts "h?i:r:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    i) exp_ident=${OPTARG};;
    r) run_script=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

#check for experiment identifier
if [ -z "$exp_ident" ]; then
    echo "User did not provide an experiment identifier"
    usage
fi

#check for runscript
if [ -z "$run_script" ]; then
    lstr="User did not provide the name of a runscript or else"
    lstr="$lstr runscript is not found at the given path"
    echo $lstr
    usage
fi

# logdir:
#this will be the folder where the log files are stored. It needs to exist
#for the job to work, so it's created below if it doesn't exist already.
#this script is set up to write 2 log files *_screen_output.txt will have
#the output that would normally be printed to the screen, but it can be
#slow populating. *_joblog1.txt will have information about how the job is
#running on the queue. I'm not sure that *_joblog2.txt is actually needed
#for anything 
runpath=$(echo ${run_script} | cut -d" " -f1)
logdir="$(dirname $runpath)/logs/${exp_ident}/"
echo -e "\nThe log files will be saved in: ${logdir}"

#if the logdir does not exist, make it
mkdir -p $logdir

#define names for the output log file and the job submission script
LOG=$logdir/${exp_ident}_screen-output.txt
jobsub_file=${logdir}/${exp_ident}.job

########################################################################
# generate a job script and save it in the $logdir
# the part below under the line with "# Call your program" is where you
# will add what you want to do. This can be multiple lines, for example:
# cd $(dirname $run_script)/ 
# the_program_to_run.py
# etc
########################################################################
run_command="${run_script} >> $LOG 2>&1"

# create the job submission script
echo "#! /bin/bash" > $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -m bea" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -j y" >> $jobsub_file
echo "#$ -o $logdir/${exp_ident}_joblog1.txt" >> $jobsub_file
echo "#$ -e $logdir/${exp_ident}_joblog2.txt" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -P dfo_dpnm-fa3" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -pe dev 1" >> $jobsub_file
echo "#$ -l res_cpus=1" >> $jobsub_file
echo "#$ -l res_mem=100000" >> $jobsub_file
echo "#$ -l res_tmpfs=0" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -l res_image=dfo/dfo_all_default_ubuntu-18.04-amd64_latest" >> $jobsub_file
echo "" >> $jobsub_file
echo "#$ -l h_rt=06:00:00" >> $jobsub_file
echo "" >> $jobsub_file
echo "#NOTE: the directory where the log files will be stored must already exist!" >> $jobsub_file
echo "" >> $jobsub_file
echo "# Call your program" >> $jobsub_file
echo $run_command >> $jobsub_file

#########################################################################
## submit the job
#########################################################################
#echo "${jobsub_file} has been created"
#echo -e "\nTo submit to the job queue run:\njobsub -c gpsc4 ${jobsub_file}"
jobsub -c gpsc4 ${jobsub_file}

