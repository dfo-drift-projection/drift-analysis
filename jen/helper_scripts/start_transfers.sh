#!/bin/bash

runscript="/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/helper_scripts/grab_forecast_data.sh"
outdirbase="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/data_download_20211025/"

datatype="ciopse_rpn"
./create_jobsub_script.sh -i ${datatype} -r "${runscript} -d ${datatype} -o ${outdirbase}"

datatype="ciopse_netcdf"
./create_jobsub_script.sh -i ${datatype} -r "${runscript} -d ${datatype} -o ${outdirbase}"

datatype="ciopsw_rpn"
./create_jobsub_script.sh -i ${datatype} -r "${runscript} -d ${datatype} -o ${outdirbase}"

datatype="ciopsw_netcdf"
./create_jobsub_script.sh -i ${datatype} -r "${runscript} -d ${datatype} -o ${outdirbase}"

datatype="hrdps_national"
./create_jobsub_script.sh -i ${datatype} -r "${runscript} -d ${datatype} -o ${outdirbase}"
