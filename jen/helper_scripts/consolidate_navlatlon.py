#!/home/jeh326/work/software/miniconda/envs/mldpn_dtv2-env/bin/python

import argparse
import glob
import numpy as np
import os
import pandas as pd
import sys
import xarray as xr

########################################################################
# consolidate_navlatlon.py
#
# helper script to check if the nav_lat, nav_lon variables in the HRDPS
# output file are consistant across all the runs (they should be!). Some
# files have potentially been corrupted and contain bad data. This prevents
# prevents the drift tool from sucessfully running an experiment with 
# winds. 
#
# To use, update the following:
#
# - fix_files: True (this will go ahead and rename the refinal corrupted
#   file to file.nc.bad, then write out a new version with the 
#   appropriate nav_lat and/or nav_lon replaced with the reference set.
#   False: This will print out the bad values so that its possible to 
#   see what will be changed when set to True. Should use False by 
#   default to first make sure that the values that will be refered to 
#   are correct!
#
# - dir: The directory with the netcdf files
#
# - reference_file: This is the file that all the other files will be 
#   compared against. 
#
# - file_prefix: the common part of the filename in 'dir'
#
########################################################################

def str2bool(v):
    if isinstance(v, bool):
       return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

parser = argparse.ArgumentParser(description='Process some integers.')
helpstr1=("replace the corrupted value with the reference value")
helpstr2=("name of file that will be used for reference (path is not required - script assumes that the reference file and the data files are in the same folder)")
helpstr3=("optional. prefix to use to determine which files to grab. If undefined, the script grabs all the nc files in the current folder")
helpstr4=("optional. full path to the data directory. If undefined, the script chooses the current directory")
parser.add_argument("--fix_files", type=str2bool, nargs='?', const=True, default=False, help=helpstr1)
parser.add_argument("--reference_file", type=str, help=helpstr2)
parser.add_argument("--prefix", type=str, help=helpstr3)
parser.add_argument("--dir", type=str, help=helpstr4)
args = parser.parse_args()

fix_files = False
if args.fix_files:
    fix_files = True 

if args.dir:
    dir = args.dir
else:
    dir = os.getcwd()

if args.prefix:
    filesearch = args.prefix + "*.nc"
else:
    filesearch = "*.nc"

if args.reference_file:
    reference_file = os.path.join(dir, args.reference_file)
else:
    sys.exit("you must define a --reference_file")

# open the reference file and grab the nav_lat and nav_lon
with xr.open_dataset(os.path.join(dir, reference_file)) as f:
    reflat = f.nav_lat.values
    reflon = f.nav_lon.values
    refuwind = f.u_wind.values
    refvwind = f.v_wind.values

    # cycle though each of the other files in the folder and compare to
    # the reference file
    for filename in glob.glob(os.path.join(dir, filesearch)):

        bname = os.path.basename(filename)

        if bname == args.reference_file:
            print(bname, '- reference set')
            continue

        with xr.open_dataset(filename) as ds:
            lat = ds.nav_lat.values
            lon = ds.nav_lon.values
            uwind = ds.u_wind.values
            vwind = ds.v_wind.values

            # if you're ready to make the change to the file
            if fix_files:

                if not (reflat==lat).all() or not (reflon==lon).all():

                    print('fixing file: ', filename)
                    ds_new = ds
                
                    if not (reflat==lat).all():
                        print('changing the lat values to match the reference value')
                        ds_new['nav_lat'] = (('y','x'), reflat) 

                    if not (reflon==lon).all():
                        print('changing the lon values to match the reference value')
                        ds_new['nav_lon'] = (('y','x'), reflon)

                    new_filename = filename + '.bad'
                    os.rename(filename,new_filename)
                    ds_new.to_netcdf(filename, 'w')

            # if you just want to check out the differences
            else:

                findex = range(0, len(ds.nav_lon.values))
                fdf = pd.DataFrame(
                            np.nan, 
                            index=findex, 
                            columns=['reflat', 'corlat', 'reflon', 'corlon', 'ref_uwind', 'cor_uwind']
                        )

                if not (reflat==lat).all():
                    for l in range(0,len(lat)-1):
                        if not (lat[l]==reflat[l]).all():
                            list_lat = lon[l]
                            list_reflat = reflat[l]
                            for v in range(0, len(list_lat)-1):
                                if not (list_lat[v]==list_reflat[v]):
                                    fdf['reflat'].loc[v] = list_reflat[v]
                                    fdf['corlat'].loc[v] = list_lat[v]

                if not (reflon==lon).all():
                    for l in range(0,len(lon)-1):
                        if not (lon[l]==reflon[l]).all():
                            list_lon = lon[l]
                            list_reflon = reflon[l]
                            for v in range(0, len(list_lon)-1):
                                if not (list_lon[v]==list_reflon[v]):
                                    fdf['reflon'].loc[v] = list_reflon[v]
                                    fdf['corlon'].loc[v] = list_lon[v]

                if (refuwind==uwind).all():
                    print('The reference u_wind and the file u_wind match. There is something wrong with', bname)

                if (refvwind==vwind).all():
                    print('The reference v_wind and the file v_wind match. There is something wrong with', bname)


                fdf = fdf.dropna(how='all')
                if len(fdf.corlon.values) > 0:

                    latdf = fdf[['reflat','corlat']].copy()
                    latdf = latdf[latdf['corlat'].notna() == True].copy()
                    londf = fdf[['reflon','corlon']].copy()
                    londf = londf[londf['corlon'].notna() == True].copy()

                    #summary_print = ('\n' + bname + ' has '
                    #        + str(len(latdf.corlat.values)) + ' inconsistant '
                    #        'nav_lat values and ' + str(len(londf.corlon.values)) 
                    #        + ' inconsistant nav_lon values')
                    summary_print_str = ('\n' + bname + ' - '
                            + str(len(latdf.corlat.values)) + ' inconsistant '
                            'nav_lat values and ' + str(len(londf.corlon.values)) 
                            + ' inconsistant nav_lon values')
                    print(summary_print_str)

                    if not latdf.empty:
                        print('sample non-matching nav_lat (truncated):')
                        printlatdf = latdf.rename(
                                columns = {
                                    'reflat':'reference_lat',
                                    'corlat':'corrupted_lat'})
                        print(printlatdf.head(10).to_string(index=False))

                    if not londf.empty:
                        print('sample non-matching nav_lon (truncated):')
                        printlondf = londf.rename(
                                columns = {
                                    'reflon':'reference_lon',
                                    'corlon':'corrupted_lon'})
                        print(printlondf.head(10).to_string(index=False))

                    if (not londf.empty) or (not latdf.empty):
                        print(' ')

                else:
                    #print('The values in', bname, 'all seem ok')
                    print(bname, '- ok')
