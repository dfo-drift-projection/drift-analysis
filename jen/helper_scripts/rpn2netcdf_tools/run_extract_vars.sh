#-------------------------------------------------------
# Note, extract_variables expects to recieve: -i <full path to RPN input file>
# -o <optional. filename for NetCDF file> -d <optional. full path to 
# directory where NetCDF output file will be saved> and 
# -v <variable name if only extracting one variable>
#
# In the RPN files, uo/uos and vo/vos are given as UUW and VVW.
#-------------------------------------------------------

codedir="/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/helper_scripts/rpn2netcdf_tools/"
outdir="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_test_runs_output/extract_rpn_vars_test/"
infile="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/ciops/ciopse/selected_rpn_dates/2021011400/2021011400_043"

echo -e "\nExtracting the variables. Screen output redirected to: $outdir/extract_vars_$(date +"%Y%m%d").log"
# could also add argument "-o <output filename>" below to choose a specific name
# for example: -o "2021011400_043_uo" \

${codedir}/extract_variables.sh \
  -i $infile \
  -d $outdir \
  -v "UUW" \
  > $outdir/extract_vars_$(date +"%Y%m%d").log

${codedir}/extract_variables.sh \
  -i $infile \
  -d $outdir \
  -v "VVW" \
  >> $outdir/extract_vars_$(date +"%Y%m%d").log

echo -e "\nThe output files are store in: $outdir"
