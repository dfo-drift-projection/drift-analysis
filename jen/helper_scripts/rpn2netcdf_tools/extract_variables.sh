#!/bin/bash

u="Usage: extract_variables.sh -i <full path to RPN input file>"
u="$u -o <optional. filename for NetCDF file.>"
u="$u -d <optional. full path to directory where NetCDF output file will be saved>"
u="$u -v <optional. name of variable to extract (otherwise default all is used)>"
USAGE_STR=$u

usage() {
    echo $USAGE_STR
    exit 1;
}

# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?i:o:d:v:" args; do
case $args in
    h|\?)
        usage;
        exit 1;;
    i) INPUT_FILE=${OPTARG};;
    o) OUTPUT_FILENAME=${OPTARG};;
    d) OUTDIR=${OPTARG};;
    v) VAR=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

# if no input file is provided, exit.
if [ -z "$INPUT_FILE" ]; then
    echo $USAGE_STR
    exit 1;
fi

if [ -z "$OUTDIR" ]; then
    OUTDIR=$(dirname $INPUT_FILE)
fi

if [ -z "$OUTPUT_FILENAME" ]; then
    if [ -z "$VAR" ]; then
        OUTPUT_FILE=$OUTDIR/$(basename $INPUT_FILE)
    else
        OUTPUT_FILE=$OUTDIR/$(basename $INPUT_FILE)_$VAR
    fi
else
    OUTPUT_FILE=$OUTDIR/$OUTPUT_FILENAME
fi

# Load RPN libraries
. ssmuse-sh -x /fs/ssm/eccc/mrd/rpn/OCEAN/cstint-3.2.7/ 

# this won't have the u15/v15 info, we will need to get a new dictionary 
# file (maybe from the CIOPS suite?) once we switch to the new files
# original dictionary from:
DICTIONARY='/home/stt001/gpfs4/CONSTANTS//riops/v2.0.0/dict/dictionary_ops_nemo36_varops.nc'
#DICTIONARY='/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/misc'

if [ ! -d $OUTDIR ]; then
    mkdir $OUTDIR
fi

# Convert to netcdf. This conversion works, but is not great. If the file 
# converts and opens, but you can not use ncks on it (for example), try 
# ncks -3 $rest_of_call. That then makes a netcdf3 file, but that may 
# cause other problems down the road

if [ -z $VAR ]; then
    cstrpn2cdf -s $INPUT_FILE -d $OUTPUT_FILE -dict $DICTIONARY
else
    echo "extracting ${VAR} from ${INPUT_FILE} and saving in ${OUTPUT_FILE}"
    cstrpn2cdf -s $INPUT_FILE -d $OUTPUT_FILE -dict $DICTIONARY -nomv $VAR
fi


# TODO: clean up below
#filenames=$(ls $OUTDIR/*.nc)
#echo " "
#echo "folder contains:"
#for file in $filenames; do 
    ## this gives just the start of the name
    #short_name=$(echo "$file" | rev | cut -d_ -f2- | rev) 
#done


#filenames=$(ls $OUTPUT_FILE.nc.*)
#for file in $filenames; do 
#    echo " "
#    echo $file; 
#    #ncks -M -C -v depth $file; 
#    #ncks --trd -m $file | grep -E ': type' | cut -f 1 -d ' ' | sed 's/://' | sort
#done
#
#echo " "

#set -- $filenames
#ref_file=$1
#bname=$(basename $ref_file)
#dname=$(dirname $ref_file)
#sname=$(echo $bname | cut --delimiter="." -f 1)
#combined_file=$dname/$sname.nc
#cp $ref_file $combined_file
#
##count=$(ls $OUTPUT_FILE.nc.* | wc -l)
##echo $count
#
#for file in $filenames; do
#    if [ $file == $ref_file ]; then
#        echo "$file: ref_file. skipping"
#    else
#        echo "$file: not ref_file. appending"
#        ncks -A $file $combined_file
#    fi
#done 


