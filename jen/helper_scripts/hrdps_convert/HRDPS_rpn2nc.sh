#!/bin/bash
# convert HRDPS rpn file to netcdf and extract the sub-domains for archive for a given date
# usage:
#       HRDPS_rpn2nc.sh cDate cHour
# e.g.,
#       HRDPS_rpn2nc.sh 20190902 00
# history  :
#   2019-09: sub-domains and temporal blending (xianmin@ualberta.ca)
#   2019-12: compute hourly precipitation based on the accumulated PR, rotate GEM wind to earth coordinate,
#            modified diction file (to convert wind and air temperature units), add surface latent heat flux field, 
#            and also consider the special filename before 20151215 (xianmin@ualberta.ca)
# contributors:
#   temporal blending algorithm: retrieved from CIOPS-W Maestro suite by Taylor Stephanne [BIO]
#   rpn2nc: Bourgault-Brunelle Corinne [EC] & Sarah MacDermid
#   many detailed and technical discussions with Michael Dunphy and others involved in the mailist
#   adapt to forecasts with no temporal blending or subdomains: Nancy Soontiens

if [ $# -eq 0 ]; then
   echo "usage:"
   echo "      HRDPS_rpn2nc.sh yyyymmdd hh"
   exit
elif [ $# -eq 2 ]; then
    cDate=$1
    cHr=$2
   [ ${#cDate} -ne 8 ] && echo "input cDate must be in yyyymmdd format.  EXIT" && exit
elif [ $# -eq 2 ]; then
   startDay=$1; endDay=$2
   cDate=$1
   while [ ${cDate} -le ${endDay} ]
   do
         HRDPS_rpn2nc.sh ${cDate}
         cDate=`date --date="${cDate} +1 day" +"%Y%m%d%"`
   done
   exit
else
   echo "usage:"
   echo "      HRDPS_rpn2nc.sh yyyymmdd hh"
   exit
fi

# Directory for HRDPS forecasts:
YYYY=${cDate:0:4};  MM=${cDate:4:2}; DD=${cDate:6:2}; HH=${cHr};
isOV=0
cDomain="nat"
isDebug=0

hrdpsPath="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/hrdps/hrdps_national_20191231"  #"/gpfs/fs2/dfo/hpcmc/gridpt/sdfo501/hubs/suites/ops/hrdps_national_20191231/"
hrdpsf_std="${hrdpsPath}/gridpt/prog/lam/2p5/nat.eta"
hrdpsf_std="${hrdpsPath}/rpn/"
ncOutputPath="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/hrdps/hrdps_national_20191231/netcdf/${cDate}${HH}"
#dictFile="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/convert_hrdps/dict_hrdps_eta.nc"
#anglePath="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/sample_rpn-nc_dataset/convert_hrdps/grid"
dictFile="/home/sdfo502/storage4/archive/atmo/mshydro/grid/dict_hrdps_eta.nc"
anglePath="/home/sdfo502/storage4/archive/atmo/mshydro/grid"

tairOffset=`ncdump -h ${dictFile} | grep "tair:stdoffset" | awk -F= '{print $2}'`
if [ "${tairOffset}" == "0.f" -o "${tairOffset}" == "0.0" ]; then
   isDegC=1
else
   isDegC=0
fi

[ ! -d ${hrdpsPath} ] && echo "HRDPS source folder is not found: ${hrdpsPath}   EXIT!!!" && exit
[ ! -f ${dictFile} ] && echo  "dict file is not found: ${dictFile}   EXIT!!!" && exit
[ ! -d ${ncOutputPath} ] && mkdir -p ${ncOutputPath}

if [ ${isOV} -eq 0 ]; then
  isFound=1
  fileCombined="HRDPS_${cDomain}_ps2.5km_y${YYYY}m${MM}d${DD}.nc"
  [ ! -s ${ncOutputPath}/${fileCombined} ] && isFound=0
  if [ $isFound -eq 1 ]; then
     echo "y${YYYY}m${MM}d${DD} has been processed. SKIP"
     exit 0
  fi
fi

rpnVarList=(UU VV TT); ncVarList=(u_wind v_wind tair)
cfNameList=(eastward_wind northward_wind air_temperature)

tmpWKDIR="tmp${cDate}${HH}"
[ ! -d ${tmpWKDIR} ] && mkdir ${tmpWKDIR}
cd ${tmpWKDIR}

# To load the RPN libraries and utilities:
. ssmuse-sh -x /fs/ssm/eccc/cmd/cmde/ocean/tools/20190903/


for ((nv=0; nv<3; nv++));
do
    cRPNVar=${rpnVarList[$nv]}
    var=${ncVarList[$nv]}

    cFileNC="HRDPS_${cDomain}_${var}_ps2.5km_y${YYYY}m${MM}d${DD}h${HH}"

    # extract the hourly file
    cHour=0
    # 000 to 048 
    for FHour in {000..048}; 
    do
	FHourStr="${FHour}"	  
        cRPNFile="${cDate}${HH}_${FHourStr}"
        srcRPN=${hrdpsf_std}
        cHour=`expr $cHour + 1`
        cHourStr=`printf "%02d" ${cHour}`
        if [ -s ${srcRPN}/${cRPNFile} ]; then
            # rpn file exists
            ncOutFile="${cFileNC}_${FHour}"
            [ -f ${ncOutFile}.nc ] && rm -f ${ncOutFile}.nc
            if [ $isDebug -eq 1 ]; then
               # select the diagnostic level (Sarah MacDermid: 10m for winds and 1.5m for temperature and humidity) if needed
               if [ "${cRPNVar}" == "TT" -o "${cRPNVar}" == "UU" -o "${cRPNVar}" == "VV" -o "${cRPNVar}" == "HU" ]; then
                   echo "d.cstrpn2cdf -s ${srcRPN}/${cRPNFile} -d ${ncOutFile}.nc -nomv ${cRPNVar} -dict ${dictFile} -ip1 12000"
               else
                   echo "d.cstrpn2cdf -s ${srcRPN}/${cRPNFile} -d ${ncOutFile}.nc -nomv ${cRPNVar} -dict ${dictFile}"
               fi
            else
               if [ "${cRPNVar}" == "TT" -o "${cRPNVar}" == "UU" -o "${cRPNVar}" == "VV" -o "${cRPNVar}" == "HU" ]; then
                   eval "d.cstrpn2cdf -s ${srcRPN}/${cRPNFile} -d ${ncOutFile}.nc -nomv ${cRPNVar} -dict ${dictFile} -ip1 12000 >> cstrpn2cdfLog"
               else
                   eval "d.cstrpn2cdf -s ${srcRPN}/${cRPNFile} -d ${ncOutFile}.nc -nomv ${cRPNVar} -dict ${dictFile} >> cstrpn2cdfLog"
               fi
            fi
          else
            [ ! -e ${srcRPN}/${cRPNFile} ] && echo "rpn file is not found: ${cRPNFile}"
            exit
         fi
         [ ${isDebug} -eq 1 ] && echo "     "
         #seconds since 1950-01-01 00:00:00
	 dDate=`date -d "${cDate} ${HH}:00:00" +%s`
         cSeconds=$((`date -d "${cDate} ${HH}:00:00" +%s` - `date -d "19500101" +%s` + ${cHour}*3600 - 3600))
         cSecondsAVE=$((`date -d "${cDate} ${HH}:00:00" +%s` - `date -d "19500101" +%s` + ${cHour}*3600 - 1800))
         if [ ${isDebug} -ne 1 ]; then
              ncap2 -A -s "time_counter=${cSeconds}.0; time_average=time_counter; time_average=${cSecondsAVE}.0" ${ncOutFile}.nc
         else
            echo ncap2 -A -s "time_counter=${cSeconds}.0; time_average=${cSecondsAVE}.0" ${ncOutFile}.nc
         fi
    done # loop of Fhour
    echo "-------------done ${var}----------------"
    
    # Final touches on file metadate
    subdom="${cDomain}"
    for FHour in {000..048}; do
      ncOutFile="${cFileNC}_${FHour}"
     if [ $isDebug -eq 1 ]; then
        echo "ncks -4 -L 4  ${ncOutFile}.nc ${ncOutFile}_withZ.nc"
        # remove z-dimension
        echo "ncwa -O -a z ${ncOutFile}_withZ.nc ${ncOutFile}.nc"
        # add more information
        echo ncatted -O -h -a source,global,o,c,"HRDPS-${subdom} forecasts ${cDate}${HH} Fhour ${Fhour}" ${ncOutFile}.nc
        echo ncatted -O -h -a comment,global,o,c,"`date` : created by HRDPS_rpn2nc.sh (xianmin@ualberta.ca)" ${ncOutFile}.nc
     else
       eval "ncks -4 -L 4 ${ncOutFile}.nc ${ncOutFile}_withZ.nc"
       # remove z-dimension
       eval "ncwa -O -a z ${ncOutFile}_withZ.nc ${ncOutFile}.nc"
       [ $? -eq 0 ] && rm -f ${ncOutFile}_withZ.nc
       # add more information
       ncatted -O -h -a standard_name,${var},o,c,"${cfNameList[$nv]}" ${ncOutFile}.nc
       ncatted -O -h -a source,global,o,c,"HRDPS-${subdom} forecasts ${cDate}${HH} Fhour ${Fhour}" ${ncOutFile}.nc
       if [ "${cRPNVar}" == "TT" ]; then
          ncatted -O -h -a comment,global,o,c,"`date` : created by HRDPS_rpn2nc.sh (xianmin@ualberta.ca)" ${ncOutFile}.nc
          ncatted -a units,tair,o,c,"K" ${ncOutFile}.nc
          if [ $isDegC -eq 1 ]; then
             ncap2 -O -s 'tair=tair+273.15;' ${ncOutFile}.nc ${ncOutputPath}/${ncOutFile}.nc
             [ $? -eq 0 ] && rm -f ${ncOutFile}.nc 
          else
             mv ${ncOutFile}.nc ${ncOutputPath}/${ncOutFile}.nc
          fi
       else
          ncatted -O -h -a comment,global,o,c,"`date` : created by HRDPS_rpn2nc.sh (xianmin@ualberta.ca)" ${ncOutFile}.nc
          [ $? -eq 0 ] && mv ${ncOutFile}.nc ${ncOutputPath}/${ncOutFile}.nc
       fi
     fi
     # delete depth variable
     ncks -O -x -v depth ${ncOutputPath}/${ncOutFile}.nc ${ncOutputPath}/${ncOutFile}.nc
    done
    echo "============ done ${cDate}${HH}: ${var} ================="
done # loop of nv
# remove temporary files
cd -
[ $? -eq 0 -a ${isDebug} -eq 0 ] && rm -rf ${tmpWKDIR}
# rotate wind
cd ${ncOutputPath}
if [ ${isDebug} -eq 1 ]; then
   echo "rotate wind here. No detailed debug info here"
else
    for Fhour in {000..048}; do
       fileU="HRDPS_${cDomain}_u_wind_ps2.5km_y${YYYY}m${MM}d${DD}h${HH}_${Fhour}.nc"
       [ ! -s ${fileU} ] && echo "${fileU} is not found! EXIT." && exit
       fileV="HRDPS_${cDomain}_v_wind_ps2.5km_y${YYYY}m${MM}d${DD}h${HH}_${Fhour}.nc"
       [ ! -s ${fileV} ] && echo "${fileV} is not found! EXIT." && exit
       ncrename -v u_wind,uraw ${fileU}
       ncrename -v v_wind,vraw ${fileV}
       ncks -A -v vraw ${fileV} ${fileU}
       angleFile="${anglePath}/HRDPS_nat_rotation_angle.nc"
       [ ! -s ${angleFile} ] && echo "${angleFile} is not found! EXIT." && exit
       ncks -A -v gsint,gcost ${angleFile} ${fileU}
       ncap2 -O -s 'u_wind=uraw*gcost-vraw*gsint; v_wind=vraw*gcost+uraw*gsint' ${fileU} ${fileV}
       fileWinds="HRDPS_${cDomain}_winds_ps2.5km_y${YYYY}m${MM}d${DD}h${HH}_${Fhour}.nc"
       if [ $? -eq 0 ]; then
          rm -f ${fileU} && mv ${fileV} ${fileWinds}
       fi
       ncks -O -x -v uraw,vraw,gsint,gcost ${fileWinds} ${fileWinds}
    done
fi
cd -
