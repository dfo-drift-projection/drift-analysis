#!/gpfs/fs7/dfo/dpnm/jeh326/software/miniconda/envs/rotary_spectra_env/bin/python3.9

import xarray as xr
import os
import numpy as np
import cartopy.crs as ccrs
import cartopy.feature as cfeature
from cartopy.mpl.gridliner import LONGITUDE_FORMATTER, LATITUDE_FORMATTER
import matplotlib.pyplot as plt
import pickle
import glob


def setup_plot(edge_lons, edge_lats):
    fig = plt.figure(figsize=(16, 12))
    ax = fig.add_subplot(1, 1, 1, projection=ccrs.PlateCarree())
    ax.add_feature(cfeature.COASTLINE)
    # plot the ocean domain edges
    plt.plot(edge_lons, edge_lats, color='blue', linestyle='-', linewidth=1)
    # add lables to the plot
    gl = ax.gridlines(crs=ccrs.PlateCarree(), draw_labels=True, linewidth=0.5,
                      color='gray', alpha=0.5, linestyle='--')
    gl.xformatter = LONGITUDE_FORMATTER
    gl.yformatter = LATITUDE_FORMATTER
    return ax


def add_tracks_to_plot(ax, lons, lats, col, lab='__no_label__'):
    plt.plot(lons, lats, color=col, linestyle='-', linewidth=1, label=lab)
    plt.plot(lons[0], lats[0], 'go', markersize=1)
    return ax


def load_pickled_file(file):
    pickle_file = open(file, 'rb')
    pickled_data = pickle.load(pickle_file)
    pickle_file.close()
    return pickled_data


def create_fake_drifter_data(
    orlons,
    orlats,
    edge_lon=None,
    edge_lat=None,
    reverse=False
):
    if reverse:
        orlons = orlons[::-1]
        orlats = orlats[::-1]

    lat_offset = [(edge_lat - orlats[0]) if edge_lat is not None else 0]
    lon_offset = [(edge_lon - orlons[0]) if edge_lon is not None else 0]

    fake_lons = orlons + lon_offset
    fake_lats = orlats + lat_offset

    return fake_lons, fake_lats


def create_fake_file(origfile, fake_lons, fake_lats, outfile):
    with xr.open_dataset(origfile) as drifter:
        new_lons = np.array(fake_lons)
        new_lats = np.array(fake_lats)
        drifter.variables['LONGITUDE'].values = new_lons
        drifter.variables['LATITUDE'].values = new_lats

        print(drifter)

        notestr = ('This is a fake drifter file! It was created by modifying '
                   + 'the data from file: ' + str(os.path.basename(origfile)))
        approximate_drogue_depth = drifter.attrs['approximate_drogue_depth']
        new_attrs = {
            'note': notestr,
            'buoyid': str(os.path.basename(outfile)).split('.')[0],
            'approximate_drogue_depth': approximate_drogue_depth,
            'original_filename': drifter.attrs['original_filename'],
            'launchdate': drifter.attrs['launchdate'], 
            'dataStartDate': drifter.attrs['dataStartDate'], 
            'time_coverage_start': drifter.attrs['time_coverage_start'], 
            'time_coverage_end': drifter.attrs['time_coverage_end'], 
            'first_date_observation': drifter.attrs['first_date_observation'], 
            'last_date_observation': drifter.attrs['last_date_observation'],
        }

        for attr in list(drifter.attrs.keys()):
            del drifter.attrs[attr]

        for attr in list(new_attrs.keys()):
            drifter.attrs[attr] = new_attrs[attr]

        drifter.to_netcdf(outfile)


# set some paths:
drifter_dir = ('/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/'
               + 'drift-analysis/jen/projects/create_fake_drifter_files')

origfile = os.path.join(drifter_dir, 'wp4142510193D20160604.nc')
with xr.open_dataset(origfile) as orig:
    origlons = orig.LONGITUDE.values
    origlats = orig.LATITUDE.values
    origattrs = orig.attrs

# load the mesh edges from pickle files
edge_lons = load_pickled_file(os.path.join(drifter_dir,
                                           'ciopsw_edge_lons.pickle'))
edge_lats = load_pickled_file(os.path.join(drifter_dir,
                                           'ciopsw_edge_lats.pickle'))

# Set up a plot and add the mesh edges
print('creating plot')
ax = setup_plot(edge_lons, edge_lats)

# add the original track
print('...adding the original track')
ax = add_tracks_to_plot(ax, origlons, origlats, 'orange', lab='original track')

lon_adjust = [-140.8 , -141.3, -140.2, -141.8, -142, -139]
lat_adjust = [None , 50, 55, 47.5, 45.5, 57.5]
colors = ['red', 'purple', 'magenta', 'teal', 'green', 'pink']
#lon_adjust = [-140.8]
#lat_adjust = [None]
#colors = ['red']
inds = range(0, len(lon_adjust))
for elon, elat, col, ind in zip(lon_adjust, lat_adjust, colors, inds):
    # create the fake drifter data file:
    fake_lons, fake_lats = create_fake_drifter_data(origlons,
                                                    origlats,
                                                    edge_lon=elon,
                                                    edge_lat=elat,
                                                    reverse=True)
    outfile = os.path.join(drifter_dir, ('fake_drifter_' + str(ind) + '.nc'))
    create_fake_file(origfile, fake_lons, fake_lats, outfile)

    # add the fake drifter track to the plot
    print('...adding track for drifter_' + str(ind))
    ax = add_tracks_to_plot(ax, fake_lons, fake_lats, col,
                            lab=('fake_drifter_' + str(ind)))

# save out the plot
plt.legend()
plt.savefig(os.path.join(drifter_dir, 'fake_drifter_tracks_ciopsw.png'))

# couple of extra checks:
print('\nThe original file:')
print('starting point: ' + str([origlons[0], origlats[0]]))
print('attributes: ' + str(list(origattrs.keys())))

print('\nThe fake files:')
fakefiles = glob.glob(os.path.join(drifter_dir, 'fake_drifter_*.nc'))
for fakefile in fakefiles:
    with xr.open_dataset(os.path.join(drifter_dir, fakefile)) as ds:
        fake_start_point = [ds.LONGITUDE.values[0], ds.LATITUDE.values[0]]
        print('starting point: ' + str(fake_start_point))
        print('attributes: ' + str(list(ds.attrs.keys())))
        print('buoyid:', ds.attrs['buoyid'])
        approximate_drogue_depth = ds.attrs['approximate_drogue_depth']
        print('approximate_drogue_depth: ', approximate_drogue_depth)
        print('original_filename: ', ds.attrs['original_filename'])
        print('note: ', ds.attrs['note'])
        print('')
