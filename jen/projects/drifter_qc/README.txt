This folder contains scripts to convert datasets of observed drifter data
from their original format (.csv, .drf, .txt, etc) to the NetCDF format
used by the drift tool. It's very incomplete at the moment, but it can 
be used in a pinch to convert raw data to the drift-tool format 
(https://gitlab.com/dfo-drift-projection/drift-workflow-tool/-/wikis/observed-drifter-data)

To process a dataset:

- create a workflow script by copying one of the sample *_workflow.py 
  scripts included in the folder. In this script you'll need to define
  the path to the raw data, the dataset type, etc.

- if the dataset is different from the sets that already have readers 
  you'll need to add a new reader to drifter_qc_readers.py (ie, either 
  the normal output from waterproperties.ca (*.drf), or else
  one of the one-off sets that are already created). You need to add this
  reader to drifter_qc_readers.py

- if you want to add a new test, you can do so in the drifter_qc_tests.py
  script. This script does not include tests for things like grounded
  drifters, stuck drifters, etc. It would be good to test for those!

Notes:

- the current workflow steps though files and organizes them based on 
  datasets. It would probably be better to have the data be processed by
  drifter types instead. That way a few common drifter type readers could
  work instead of writing a new reader for each dataset!

- there's a file called 'epsg' in the folder. This file is used by 
  Basemap and so it's necessary to keep it in the same folder as the scipts.

- there's a folder called sample_data/ that has a few raw drifter files to 
  use for testing. 


