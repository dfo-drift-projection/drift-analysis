#!/bin/python

import os
import sys
import glob
import pandas as pd
import numpy as np
import datetime as dt
from datetime import datetime, timedelta
import xarray as xr

def gather_filelist(datatype, datadir, datafile=None, ext_type=None):
    """ Creates a list of files. Assumes the only files with the specific 
    extension will be drifter files to be converted """

    if not os.path.isdir(datadir):
        datadir_logstr = datadir + ' does not exist!'
        print(datadir_logstr)
        sys.exit()
    
    ext_dict = {
        'wp': '*.drf', 
        'wptemp': '*.csv',
        'wp_flag1': '*.drf',
        'uqar': '*.txt',
        'extrasvps': '*.csv',
        'cmems_svps': '*.nc',
        'meopar2020DFO': '*.csv',
        }

    if ext_type is not None:
        # add the wildcard if needed
        if '*' not in ext_type:
            ext_type = '*' + ext_type
        # add the extension to the dictionary
        ext_dict[datatype] = str(ext_type)

    if datatype in ext_dict.keys():
        if datafile is None:
            filelist = []
            for fname in glob.glob(os.path.join(datadir,ext_dict[datatype])):
                filelist.append(fname)
        else:
            filelist = [os.path.join(datadir,datafile)]
    else:
        print('Unknown raw data type. Can not convert to netcdf')
        exit()

    if len(filelist) < 1:
        printstr = ('There are no files of type ' + (ext_type) 
                                     + ' found in ' + str(datadir))
        print(printstr)

    return filelist


def read_raw_data(datatype, fname, droguedict=None):
    """Runs the correct reader for the particular raw data type."""

    # a dictionary with depths associated with each drifter.
    droguedict_default = {
            "OSKER":"0",
            "SCT":"0.2",
            "Surface Circulation Tracker":"0.2", 
            "ROBY":"0",
            "SVP":"15",
            "SLDMB":"0",
            "spot": "0.2",
            "unknown":"unknown",
            "IOS SCT":"0.39",
            "Xeos OSKER":"0",
            "Xeos OSKER with Drogue":"0.72",
            "IOS CODE/Davis":"1.34",
            "MetOcean CODE/Davis":"1.0",
            "IOS WIND":"0.09",
            "Pacific Gyre CARTHE":"0.40",
            "IOS MER 10 inch":"0.25",
            "IOS MER 12 inch":"0.30",
            "IOS MER Concrete":"0.15",
            "IOS MER Concrete with Drogue":"1.0",
            "IOS SVP 8 M":"13.00",
            "IOS SVP 18 M":"23.00",
            "CARTHE":"unknown",
            "CODE/DAVIS":"1.34",
            "iSVP":"15",
            "Marine Emergency Response 10IN":"0.25",
            "Marine Emergency Response 12IN":"0.30",
            "Marine Emergency Response Concrete":"0.15",
            "Marine Emergency Response Concrete with drogue":"1.0",
            "OSKER":"0",
            "OSKER with drogue":"0.72",
            "Surface Circulation Tracker":"0.2",
            "Surface Circulation Tracker/Stokes":"unknown",
            "Stokes":"unknown",
            "SVP8M":"13.00",
            "SVP18M":"23.00",
            "WIND":"unknown",
            "MER Cement":"0.15",
            "DAVIS":"1.34",
            "MER12INCH":"0.30",
            "MER12IN":"0.30",
            "MER10INCH":"0.25",
            "MER10IN":"0.25",
            "MER Cement with drogue":"1.0",
            }

    #this is a temporary band-aid to accommodate for the fact that the 
    #drift tool currently isn't smart about the drifter depth. For example,
    #if an experiment with depth=0c is being run, drifters with 
    #approximate_drogue_depth = 0.30 will be ignored. I need to fix this
    #in the tool itself, but this will work for now.
    for key, val in droguedict_default.items():
        if val == 'unknown':
            continue
        if float(val) < 2:
            droguedict_default[key] = str(0)
        if (float(val) > 11) & (float(val) < 19):
            droguedict_default[key] = str(15)

    #if there were drogue depths passed in as arguements, overwrite the
    #default values from the dictionary.
    ddict = droguedict_default
    if droguedict and droguedict is not None:
        for key,val in droguedict.items():
            ddict[key] = str(val)

    print(datatype)

    if datatype == 'wp':
        df, hdrdict, buoyid = wp_datareader(fname,ddict)
    elif datatype == 'wptemp':
        df, hdrdict, buoyid = wptemp_datareader(fname,ddict)
    elif datatype == 'wp_flag1':
        df, hdrdict, buoyid = wp_flag1_datareader(fname,ddict)
    elif datatype == 'uqar':
        df, hdrdict, buoyid = uqar_datareader(fname, ddict)
    elif datatype == 'extrasvps':
        df, hdrdict, buoyid = extrasvps_datareader(fname, ddict)
    elif datatype == 'cmems_svps':
        df, hdrdict, buoyid = cmems_svps_datareader(fname, ddict)
    elif datatype == 'meopar2020DFO':
        df, hdrdict, buoyid = meopar2020DFO_datareader(fname, ddict)
    else:
        print('Unknown raw data type. Can not convert to netcdf')
        exit()
    return df, hdrdict, buoyid


########################################################################
# individual data readers can be added below. Currently includes:
#   wp_datareader - the normal output from waterproperties.ca
#   wptemp_datareader - a one-off set from Roy Hourston
#   uqar_datareader - a one-off static set from UQAR
#
# would be better to convert this to readers by drifter type instead.
########################################################################

def wp_datareader(fname, droguedict):
    """ reads in raw drifter data files from waterproperties.ca with the
    extension *.drf"""

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0].split('_')[0]
        print('original filename: ', fname)
        endrow = 0
        lines = []
        hdrd = {}
        table = {}
        intable = False

        #deal with the header
        for line in fp:
            if "*END OF HEADER" in line:
                endrow += 1
                break
            else:
                endrow +=1
                #write all the header lines to a string
                lines.append(line.replace('\n','').lstrip().rstrip())

                #keep the useful values in a dictionary
                if '$TABLE' in line:
                    intable=line.split(":")[1].lstrip().rstrip()
                    table[intable] = []
                    continue

                if '$END' in line:
                    intable = False

                if intable != False:

                    if line.split()[0].rstrip().lstrip().isdigit() == True:
                        table[intable].append(line.split()[1])
                    else:
                        continue

                if ":" in line:
                    keeplist = ["TYPE", "MODEL", "SERIAL NUMBER", "IMEI",
                                "SPOT ESN", "AGENCY", "DATA DESCRIPTION",
                                "NUMBER OF CHANNELS", "START TIME", "END TIME",
                                "ID", "DEVICE ID"]
                    if any(word in line for word in keeplist):
                        key = line.split(":",1)[0].rstrip().lstrip()
                        val = line.split(":",1)[1].rstrip().lstrip().replace('\n','')
                        hdrd[key] = val

    hdrd['DROGUE_DEPTH'] = droguedict[hdrd['MODEL']]
    #read in the data
    df = pd.read_csv(fname,
            skiprows=endrow,
            names=table['CHANNELS'],
            skip_blank_lines=False,
            delimiter=r"\s+")

    #arrange the data for xarray
    df['DateTime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    launchtime = str(df['DateTime'][0])
    df.drop(labels=['Date', 'Time', 'Record_Number'], axis=1, inplace=True)
    df.rename(
        columns={"DateTime":"TIME", "Latitude":"LATITUDE", "Longitude":"LONGITUDE", "Flag:At_Sea": 'QUALITY_FLAG'},
        inplace=True
            )

    #decide on a buoyid
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    launchhour = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%dH%H')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')

    if 'SPOT ESN' in hdrd.keys():
        # note: had to combine 'SPOT ESN' and 'SERIAL NUMBER' in the
        # buoyid to remove dups
        buoyid = 'wp' + hdrd['SERIAL NUMBER'] + hdrd['SPOT ESN'] + 'D' + launchhour
    elif 'OSKER IMEI' in hdrd.keys():
        buoyid = 'wp' + hdrd['OSKER IMEI'] + 'D' + launchhour
    elif 'DEVICE ID' in hdrd.keys():
        buoyid = 'wp' + hdrd['DEVICE ID'] + bname.split('_')[0] + 'D' + launchhour
    else:
        print(hdrd)
        print('no buoyid?')

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
            'source':str(hdrd['AGENCY']),
            'contact':'Roy.Hourston@dfo-mpo.gc.ca',
            'platform_type':'Drifting Buoy',
            'model':str(hdrd['MODEL']),
            'buoyid':str(buoyid),
            'qc_comment':'unspecified qc performed by the data provider',
            'nafc_qc_comment': nafc_comment,
            'original_data_source_comment':'The original dataset is available '
                        'for download at waterproperties.ca',
            'approximate_drogue_depth':str(hdrd['DROGUE_DEPTH']),
            'brand':str(hdrd['TYPE']),
            #'serial_number':str(hdrd['SERIAL NUMBER']),
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' +
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            'quality_flags':'0 = Not classified, ' +
                            '1 = Good (at sea, freely floating, valid), ' +
                            '2 = Bad (at sea but trapped in rocky intertidal, ie floating but not free), ' +
                            '3 = Bad (on land, ie grounded, test data, etc.), ' +
                            '4 = Bad (at sea, ie large GPS error, on ship, etc.).'
            }
    if 'SPOT ESN' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['SPOT ESN']
        hdrdict['reference_number_type'] = 'SPOT ESN'
    elif 'OSKER IMEI' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['OSKER IMEI']
        hdrdict['reference_number_type'] = 'OSKER IMEI'
    else:
        hdrdict['reference_number'] = 'unknown'
        hdrdict['reference_number_type'] = 'unknown'

    return df, hdrdict, buoyid



def wp_flag1_datareader(fname, droguedict):
    """ reads in raw drifter data files from waterproperties.ca with the 
    extension *.drf"""

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0].split('_')[0]
        print('original filename: ', fname)
        endrow = 0
        lines = []
        hdrd = {}
        table = {}
        intable = False

        #deal with the header
        for line in fp:
            if "*END OF HEADER" in line:
                endrow += 1
                break
            else:
                endrow +=1
                #write all the header lines to a string
                lines.append(line.replace('\n','').lstrip().rstrip())
 
                #keep the useful values in a dictionary
                if '$TABLE' in line:
                    intable=line.split(":")[1].lstrip().rstrip()
                    table[intable] = []
                    continue

                if '$END' in line:
                    intable = False

                if intable != False:

                    if line.split()[0].rstrip().lstrip().isdigit() == True:
                        table[intable].append(line.split()[1])
                    else:
                        continue

                if ":" in line:
                    keeplist = ["TYPE", "MODEL", "SERIAL NUMBER", "IMEI", 
                                "SPOT ESN", "AGENCY", "DATA DESCRIPTION", 
                                "NUMBER OF CHANNELS", "START TIME", "END TIME",
                                "ID", "DEVICE ID"]
                    if any(word in line for word in keeplist):
                        key = line.split(":",1)[0].rstrip().lstrip()
                        val = line.split(":",1)[1].rstrip().lstrip().replace('\n','')
                        hdrd[key] = val    

    hdrd['DROGUE_DEPTH'] = droguedict[hdrd['MODEL']]

    #read in the data
    rawdf = pd.read_csv(fname, 
            skiprows=endrow, 
            names=table['CHANNELS'], 
            skip_blank_lines=False, 
            delimiter=r"\s+")

    #arrange the data for xarray
    rawdf['DateTime'] = pd.to_datetime(rawdf['Date'] + ' ' + rawdf['Time'])
    launchtime = str(rawdf['DateTime'][0])
    rawdf.drop(labels=['Date', 'Time', 'Record_Number'], axis=1, inplace=True)
    rawdf.rename(
        columns={"DateTime":"TIME", "Latitude":"LATITUDE", "Longitude":"LONGITUDE", "Flag:At_Sea": 'QUALITY_FLAG'},
        inplace=True
            )

    #only keeping "good data"
    df = rawdf.loc[rawdf['QUALITY_FLAG'] == 1].copy()

    if len(list(df['QUALITY_FLAG'])) < 3:
        buoyid = 'ignore'
        hdrdict = {}
        df = pd.DataFrame()
        return df, hdrdict, buoyid

    df.drop(labels=['QUALITY_FLAG'], axis=1, inplace=True)

    #decide on a buoyid
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    launchhour = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%dH%H')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')

    if 'SPOT ESN' in hdrd.keys():
        # note: had to combine 'SPOT ESN' and 'SERIAL NUMBER' in the
        # buoyid to remove dups
        buoyid = 'wp' + hdrd['SERIAL NUMBER'] + hdrd['SPOT ESN'] + 'D' + launchhour  
    elif 'OSKER IMEI' in hdrd.keys():
        buoyid = 'wp' + hdrd['OSKER IMEI'] + 'D' + launchhour
    elif 'DEVICE ID' in hdrd.keys():
        buoyid = 'wp' + hdrd['DEVICE ID'] + bname.split('_')[0] + 'D' + launchhour
    else:
        print(hdrd)
        print('no buoyid?')
   
    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
            'source':str(hdrd['AGENCY']),
            'contact':'Roy.Hourston@dfo-mpo.gc.ca',
            'platform_type':'Drifting Buoy',
            'model':str(hdrd['MODEL']),
            'buoyid':str(buoyid),
            'qc_comment':'unspecified qc performed by the data provider',
            'nafc_qc_comment': nafc_comment,
            'original_data_source_comment':'The original dataset is available '
                        'for download at waterproperties.ca',
            'approximate_drogue_depth':str(hdrd['DROGUE_DEPTH']),
            'brand':str(hdrd['TYPE']),
            #'serial_number':str(hdrd['SERIAL NUMBER']),
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' + 
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            'data_quality':'This file has been modified to only keep data that ' +
                            'has been flagged "Good (at sea, freely floating, valid)" ' +
                            'by the data provider. Original data containing '
                            'all flags is available on waterproperties.ca' 
            }

    if 'SPOT ESN' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['SPOT ESN']
        hdrdict['reference_number_type'] = 'SPOT ESN'
    elif 'OSKER IMEI' in hdrd.keys():
        hdrdict['reference_number'] = hdrd['OSKER IMEI']
        hdrdict['reference_number_type'] = 'OSKER IMEI'
    else:
        hdrdict['reference_number'] = 'unknown'
        hdrdict['reference_number_type'] = 'unknown'

    return df, hdrdict, buoyid


def wptemp_datareader(fname, droguedict):
    """Reader to handle a temporary data set aquired from Roy Hourston"""

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]
        hdrd = {}

    hdrd['DROGUE_DEPTH'] = str(0) 

    #read in the data
    df = pd.read_csv(fname,
            skiprows=1,
            names=['Events','Date','Address','LatLng','Speed','Heading','Altitude','Via'],
            skip_blank_lines=False,
            delimiter=r",")

    #arrange the data for xarray
    df[['Date','Time']] = df.Date.str.split(" ",expand=True,)
    df['DateTime'] = pd.to_datetime(df['Date'] + ' ' + df['Time'])
    launchtime = str(df['DateTime'][0])

    df[['LATITUDE','LONGITUDE']] = df.LatLng.str.split(",",expand=True,)
    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    df.drop(
        labels=['Events', 'Address', 'Speed', 'Heading', 'Altitude', 'Via', 'LatLng', 'Date', 'Time'], 
        axis=1, 
        inplace=True
        )
    df.rename(columns={"DateTime":"TIME"},inplace=True)

    #decide on a buoyid
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    buoyid = bname

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
        'platform_type':'Drifting Buoy',
        'buoyid':str(buoyid),
        'nafc_qc_comment': nafc_comment,
        'original_data_source_comment':'The original dataset is available for download at waterproperties.ca',
        'approximate_drogue_depth':str(hdrd['DROGUE_DEPTH']), #str(15)
        'original_filename':os.path.basename(fname),
        'description':'Drifting Buoys',
        'comment':'modified netCDF file created by Fisheries and '
                    'Oceans Canada, NAFC Drift Group on ' +
                    dt.datetime.today().strftime('%Y-%m-%d') +
                    '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                    'more details.',
        'launchdate':str(firsttime)
                }

    return df, hdrdict, buoyid



def uqar_datareader(fname, droguedict):

    # I think these drifters were SCT?
    drifter_model = 'SCT' #'spot'

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]

    #read in the data
    df = pd.read_csv(fname,
        skip_blank_lines=False,
        delimiter="\t")

    df.rename(columns=lambda x: x.strip(), inplace=True)

    def datenum2datetime(datenum):
        return datetime.fromordinal(int(datenum)) + timedelta(days=datenum%1) - timedelta(days = 366)

    df['realtime'] = df['time'].map(datenum2datetime, na_action='ignore')

    df.drop(labels=['time'], axis=1, inplace=True)
    df.rename(columns={"realtime":"time"}, inplace=True)

    launchtime = str(df['time'][0].strftime('%Y%m%d'))
    launchhour = str(df['time'][0].strftime('%Y%m%dH%H'))
    firsttime = str(df['time'][0].strftime('%Y-%m-%d %H:%M:%S'))
    df.rename(
        columns={"time":"TIME", "latitude":"LATITUDE", "longitude":"LONGITUDE"},
        inplace=True
        )

    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    #decide on a buoyid
    buoyid = 'uqar' + bname + 'D' + launchhour

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
            'source':'L\'Université du Québec à Rimouski (UQAR)',
            'platform_type':'Drifting Buoy',
            'model': drifter_model,
            'buoyid':str(buoyid),
            'nafc_qc_comment': nafc_comment, 
            'original_data_source_comment':'retrieved from Dany Dumont (UGAR)',
            'approximate_drogue_depth':str(droguedict[drifter_model]), 
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' +
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            }


    return df, hdrdict, buoyid

def extrasvps_datareader(fname, droguedict):

    # I think these drifters were SCT?
    drifter_model = 'SVP' #'spot'

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]

    #read in the data
    df = pd.read_csv(fname,
        skip_blank_lines=False,
        delimiter=",")


    header_line_str = ('Data Date (UTC), LATITUDE, LONGITUDE, FMTID, YEAR, '
                        'MONTH, DAY, HOUR, MIN, SST, RANGE, VBAT, GPSDELAY, '
                        'SNR, TTFF, SBDTIME, Report Body')

    droplist = ['FMTID', 'SST', 'RANGE', 'VBAT', 'GPSDELAY', 'SNR', 'TTFF', 'SBDTIME', 'Report Body', 'YEAR', 'MONTH', 'DAY', 'HOUR', 'MIN']

    for k in df.keys():
        df.rename(columns=lambda x: x.strip(), inplace=True)

    for dl in droplist:
        if dl in list(df.keys()):
            df.drop(labels=[dl], axis=1, inplace=True)

    def datenum2datetime(datenum):
        return datetime.fromordinal(int(datenum)) + timedelta(days=datenum%1) - timedelta(days = 366)

    df['realtime'] = pd.to_datetime(df['Data Date (UTC)'])
    #df['realtime'] = df['Data Date (UTC)'].map(datenum2datetime, na_action='ignore')

    df.drop(labels=['Data Date (UTC)'], axis=1, inplace=True)
    df.rename(columns={"realtime":"time"}, inplace=True)


    launchtime = str(df['time'][0].strftime('%Y%m%d'))
    launchhour = str(df['time'][0].strftime('%Y%m%dH%H'))
    firsttime = str(df['time'][0].strftime('%Y-%m-%d %H:%M:%S'))
    df.rename(
        columns={"time":"TIME", "latitude":"LATITUDE", "longitude":"LONGITUDE"},
        inplace=True
        )

    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    #decide on a buoyid
    buoyid = bname + 'D' + launchhour

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
            'platform_type':'Drifting Buoy',
            'model': drifter_model,
            'buoyid':str(buoyid),
            'nafc_qc_comment': nafc_comment,
            'original_data_source_comment':'retrieved from Doug Schillinger (BIO)',
            'approximate_drogue_depth':str(droguedict[drifter_model]),
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' +
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            }

    return df, hdrdict, buoyid


def meopar2020DFO_datareader(fname, droguedict):

    # I think these drifters were SCT?
    drifter_model = 'SVP' #'spot'

    with open(fname) as fp:
        bname = os.path.basename(fname).split('.')[0]

    #read in the data
    df = pd.read_csv(fname,
        skip_blank_lines=False,
        delimiter=",")


    header_line_str = ('Data Date (UTC), LATITUDE, LONGITUDE, FMTID, YEAR, '
                        'MONTH, DAY, HOUR, MIN, SST, RANGE, VBAT, GPSDELAY, '
                        'SNR, TTFF, SBDTIME, Report Body')
    
    droplist = ['FMTID', 'SST', 'RANGE', 'VBAT', 'GPSDELAY', 'SNR', 'TTFF', 'SBDTIME', 'Report Body', 'YEAR', 'MONTH', 'DAY', 'HOUR', 'MIN']

    for k in df.keys():
        df.rename(columns=lambda x: x.strip(), inplace=True)

    for dl in droplist:
        if dl in list(df.keys()):
            df.drop(labels=[dl], axis=1, inplace=True)

    def datenum2datetime(datenum):
        return datetime.fromordinal(int(datenum)) + timedelta(days=datenum%1) - timedelta(days = 366)

    df['realtime'] = pd.to_datetime(df['Data Date (UTC)'])
    #df['realtime'] = df['Data Date (UTC)'].map(datenum2datetime, na_action='ignore')

    df.drop(labels=['Data Date (UTC)'], axis=1, inplace=True)
    df.rename(columns={"realtime":"time"}, inplace=True)


    launchtime = str(df['time'][0].strftime('%Y%m%d'))
    launchhour = str(df['time'][0].strftime('%Y%m%dH%H'))
    firsttime = str(df['time'][0].strftime('%Y-%m-%d %H:%M:%S'))
    df.rename(
        columns={"time":"TIME", "latitude":"LATITUDE", "longitude":"LONGITUDE"},
        inplace=True
        )

    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    #decide on a buoyid
    buoyid = bname + 'D' + launchhour

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    hdrdict = {
            'platform_type':'Drifting Buoy',
            'model': drifter_model,
            'buoyid':str(buoyid),
            'nafc_qc_comment': nafc_comment,
            'original_data_source_comment':'retrieved from Doug Schillinger (BIO)',
            'approximate_drogue_depth':str(0), #str(droguedict[drifter_model]),
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' +
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            }

    return df, hdrdict, buoyid

def cmems_svps_datareader(fname, droguedict):

    drifter_model = 'SVP' 

    with xr.open_dataset(fname) as ds:
        bname = os.path.basename(fname).split('.')[0].split('D')[0]

    ds_orig = ds
    df = ds.to_dataframe()

    df.reset_index(inplace=True)

    launchtime = str(df['TIME'][0].strftime('%Y%m%d'))
    launchhour = str(df['TIME'][0].strftime('%Y%m%dH%H'))
    firsttime = str(df['TIME'][0].strftime('%Y-%m-%d %H:%M:%S'))

    df = df.astype({"LATITUDE": float, "LONGITUDE": float})

    #decide on a buoyid
    buoyid = bname + 'D' + launchhour

    nafc_comment = ('Additional quality control provided by NAFC Drift Group. '
                    'Tests include removal of points with physically '
                    'unrealistic times and speeds. Tracks with long time '
                    'gaps have been split into multiple tracks.')

    origdict = {}
    attr_list = ds_orig.attrs
    for key, val in attr_list.items():
        origdict[key] = str(val)

    hdrdict = {
            'buoyid':str(buoyid),
            'original_filename':os.path.basename(fname),
            'description':'Drifting Buoys',
            'comment':'modified netCDF file created by Fisheries and '
                        'Oceans Canada, NAFC Drift Group on ' +
                        dt.datetime.today().strftime('%Y-%m-%d') +
                        '. Please contact Jennifer.Holden@dfo-mpo.gc.ca for '
                        'more details.',
            'launchdate':str(firsttime),
            }

    for key, val in hdrdict.items():
        origdict[key] = str(val)

    return df, hdrdict, buoyid

