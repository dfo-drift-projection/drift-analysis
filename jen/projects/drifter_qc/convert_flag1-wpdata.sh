#!/bin/bash

#pass in an identifier for the batch of data.This just gets used to name
#the job submission script and log file. The data itself is saved as a 
#subfolder inside the datadir given in the workflow script.
exp_ident="flag1-wpdata-test"
codedir="/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/drifter_qc/"
logdir="${codedir}/qc_logs/${exp_ident}/"
workflow_script="${codedir}/flag1-wpdata_qc_workflow.py"

#if the logdir does not exist, make it
mkdir -p $logdir

#define names for the output log file and the job submission script
LOG=$logdir/${exp_ident}_screen-output.txt
jobsub_file=${logdir}/${exp_ident}.job

########################################################################
# generate a job script and submit the job
########################################################################
run_command="${workflow_script} >> $LOG 2>&1"

# create the job submission script
echo "#! /bin/bash" > $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -m bea" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -j y" >> $jobsub_file
echo "#$ -o $logdir/${exp_ident}_joblog1.txt" >> $jobsub_file
echo "#$ -e $logdir/${exp_ident}_joblog2.txt" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -P dfo_dpnm-fa3" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -pe dev 1" >> $jobsub_file
echo "#$ -l res_cpus=1" >> $jobsub_file
echo "#$ -l res_mem=100000" >> $jobsub_file
echo "#$ -l res_tmpfs=0" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -l res_image=dfo/dfo_all_default_ubuntu-18.04-amd64_latest" >> $jobsub_file
echo "" >> $jobsub_file
echo "#$ -l h_rt=06:00:00" >> $jobsub_file
echo "" >> $jobsub_file
echo "#NOTE: the directory where the log files will be stored must already exist!" >> $jobsub_file
echo "" >> $jobsub_file
echo "# Call your program" >> $jobsub_file
echo "cd $codedir" >> $jobsub_file
echo $run_command >> $jobsub_file

########################################################################
# submit the job
########################################################################
jobsub -c gpsc4 ${jobsub_file}

