#!/usr/bin/env python

import os
import glob

import xarray as xr

import drifter_qc_readers as readers
import drifter_qc_tests as tests


#For the MEOPAR TREX data already in netCDF format:
datadir = '/home/nso001/code/drifters/TREX-collab/data/observations/L3/netcdf'
outdir = '/home/nso001/data/work7/OPP/DriftEval-portmodels/st-lawrence/drifter-data/all-TREX-2020/netcdf_qcd'

# Collect the filenames for all the files in a folder. If just using
# one file instead, can add the datafile='path/to/file.ext' argument 
# to readers.gather_filelist(). If the extension is diffent from the 
# default for the data type, can add ext_type='.ext'
filelist = glob.glob(os.path.join(datadir, '*.nc*'))
filelist.sort()

# For each file:
buoylist = []
for fname in filelist:

    print('\nWorking on file ', os.path.basename(fname).split('.')[0])

    #read in the data. Note: if the droguedepth is different than the 
    # default or if there are new drifter types not in the default dict, 
    # can add droguedict={driftertype: depth} 
    # ex, droguedict={'Osker': 0, 'SCT':0}
    ds = xr.open_dataset(fname)
    buoyid = ds.attrs['buoyid']
    hdrdict = ds.attrs
    df = ds.to_dataframe()
    #df, hdrdict, buoyid = readers.read_raw_data('uqar', fname, droguedict=None)
    
    #check for duplicate id
    buoylist = tests.dup_buoyid_test(buoyid, buoylist)

    #impossible positions
    print('....checking for realistic coordinates')
    df = tests.impossible_coordinate_test(df)
    
    #impossible times
#    print('....checking for realistic times')
#    df = tests.impossible_time_test(df,hdrdict)
    
    #do the speed distance check
#    print('....running speed distance check')
#    df = tests.impossible_speed_test(df) 

    # Check for time gaps longer than a day and split the tracks if any
    # exist. Write netcdf files of the finished data and create track plots.
    tests.determine_track_length(
                        df, 
                        hdrdict, 
                        datadir, 
                        outdir=outdir, 
                        time_gap=2,
                        resample=True,
                        min_points=20, 
                        plot=False, 
                        write=True
                        )

#check to be sure all the buoyids are unique
tests.check_unique_buoyids(buoylist)
