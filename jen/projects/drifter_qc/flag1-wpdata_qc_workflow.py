#!/usr/bin/env python

import os
import drifter_qc_readers as readers
import drifter_qc_tests as tests

#For the water properties data downloaded in May 2021:
#datadir = '/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/drifter_qc/waterproperties_sample_data/'
datadir = '/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/drifter_qc/waterproperties_sample_data/'

# Collect the filenames for all the files in a folder. If just using
# one file instead, can add the datafile='path/to/file.ext' argument 
# to readers.gather_filelist(). If the extension is diffent from the 
# default for the data type, can add ext_type='.ext'
filelist = readers.gather_filelist('wp_flag1', datadir, datafile=None, ext_type=None)
#filelist = readers.gather_filelist('wp_flag1', datadir, datafile="codedavis534060167710_20200803_20200825.drf", ext_type=None)
#filelist = readers.gather_filelist('wp_flag1', datadir, datafile="osker0455_20200903_20200923.drf", ext_type=None)

# For each file:
buoylist = []
for fname in filelist:

    print('\nWorking on file ', os.path.basename(fname).split('.')[0])

    #read in the data. Note: if the droguedepth is different than the 
    # default or if there are new drifter types not in the default dict, 
    # can add droguedict={driftertype: depth} 
    # ex, droguedict={'Osker': 0, 'SCT':0}
    df, hdrdict, buoyid = readers.read_raw_data('wp_flag1', fname, droguedict=None)

    #adding this as a band-aid to handle the case where all the data in 
    #a file is listed as non-flag1 data. This should be pushed into the
    #drifter tests script, but I don't have time right now!
    if buoyid == 'ignore':
        print("file does not contain at least 3 points with quality flag = 1")
        continue
    
    #check for duplicate id
    buoylist = tests.dup_buoyid_test(buoyid, buoylist)

    #impossible positions
    print('....checking for realistic coordinates')
    df = tests.impossible_coordinate_test(df)
    
    #impossible times
    print('....checking for realistic times')
    df = tests.impossible_time_test(df,hdrdict)
    
    #do the speed distance check
    print('....running speed distance check')
    df = tests.impossible_speed_test(df) 

    # Check for time gaps longer than a day and split the tracks if any
    # exist. Next, resample the data to a 1H frequency if necessary 
    #(resample=False and resampe_freq='1H' by default. Finally, write 
    #netcdf files of the finished data and create track plots.
    tests.determine_track_length(
                        df, 
                        hdrdict, 
                        datadir, 
                        outdir=None, 
                        time_gap=24, 
                        min_points=20,
                        resample=True, 
                        plot=True, 
                        write=True
                        )

#check to be sure all the buoyids are unique
tests.check_unique_buoyids(buoylist)
