#!/bin/python

###############################################################################
# QA/QC Tests.
#
# tests to add:
# -------------
#   realistic times (make sure times are increasing monotonically, no odd values)
#   grounded drifters (too complex to implement at the moment)
#   stuck drifters (check to see if the lat/lon is actually changing)
#   visual check (commented below due to environment issues)
#
# Currently includes:
# -------------------
#   duplicate buoyid
#   impossible coordinates
#   impossible times
#   impossible speed
#   determine_track_length (splits track on long time gaps)

###############################################################################

import os
import sys
import glob
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt
import geopy
import geopy.distance

# This silences an unnecessary matplotlib deprecation warning. It is bad
# practice to supress warnings like this and so it should be removed (or
# at least better targeted in the future!!)
import warnings
warnings.filterwarnings("ignore", category=UserWarning)

# The following is needed to run basemap (otherwise you get a missing 
# epsg file error). In case the file is not found in cwd, there should 
# still be a copy left in: 
# /fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/drifter_qc
#os.environ["PROJ_LIB"] = os.getcwd()
os.environ["PROJ_LIB"] = '/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/drifter_qc/'
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

#########################################################################
# Begin function definitions
#########################################################################

def dup_buoyid_test(buoyid, buoylist):
    """Checks for duplicate buoyids in a given list. 
    Should add case where buoylist is None to the arguement list"""
    #to use as a check later
    if buoyid in buoylist:
        print('')
        print('Warning! There is a duplicate buoyid!')
        print(buoyid, fname)
        print('exiting because this really should be fixed!')
        exit()
    else:
        buoylist.append(buoyid)
    return buoylist
        

def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


def impossible_coordinate_test(df):
    """Check to be sure that the coordinates are physically possible."""
    df.loc[(df['LATITUDE'] > 90), 'LATITUDE'] = np.nan
    df.loc[(df['LATITUDE'] < -90), 'LATITUDE'] = np.nan
    df.loc[(df['LONGITUDE'] < -360), 'LONGITUDE'] = np.nan
    df.loc[(df['LONGITUDE'] > 360), 'LONGITUDE'] = np.nan
    df['LONGITUDE'] = wrap_to_180(df['LONGITUDE'])
    #drop rows where the lats or lons are set to NaN
    df.dropna(inplace=True)
    return df


def impossible_time_test(df, hdrdict):
    """Checks for impossible times. Right now this checks that the min
    and max points are realistic, but it should be done per point instead"""

    #add a column (time) with seconds since start and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    df.drop_duplicates(subset=['time'], keep='first', inplace=True)

    #check if min/max data times match header start/end times
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    lasttime = pd.to_datetime(str(df['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')
    firsttime_num = dt.datetime.strptime(firsttime, '%Y-%m-%d %H:%M:%S')
    lasttime_num = dt.datetime.strptime(lasttime, '%Y-%m-%d %H:%M:%S')

    #check for future times?
    todaysdate = dt.datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    todaysdate_num = dt.datetime.strptime(todaysdate, '%Y-%m-%d %H:%M:%S')
    if todaysdate_num.date() < firsttime_num.date():
        print('The start date is later than today! Exiting.')
        print('....today = ', todaysdate)
        print('....data start date = ', firsttime)
        exit()
    if todaysdate_num.date() < lasttime_num.date():
        print('The end date is later than today! Exiting.')
        print('....today = ', todaysdate)
        print('....data end date = ', lasttime)
        exit()

    #check for dates earlier than 1900?
    sfr = '1900-01-01 00:00:00'
    sfr_num = dt.datetime.strptime(sfr, '%Y-%m-%d %H:%M:%S')
    if sfr_num.date() > firsttime_num.date():
        print('The start date is earlier than ',sfr,'! Unlikely, so exiting.')
        print('....data start date = ', firsttime)
        exit()
    if sfr_num.date() > lasttime_num.date():
        print('The end date is earlier than ',sfr,'! Unlikely, so exiting.')
        print('....data end date = ', lasttime)
        exit()
    df.drop('level_0', axis=1, inplace=True)
    df.drop('time', axis=1, inplace=True)

    return df
    #not returning anything right now. If there's a bad point, the program exits


def impossible_speed_test(df): 
    """Check for impossible speeds. The function calculates the speed, 
    then drop any points where speed is greater than 3 standard deviations 
    from the mean AND greater than 10 knots""" 

    #manipulate the time and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    df.drop_duplicates(subset=['time'], keep='first', inplace=True)

    #determine some dates
    launchdate = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y%m%d')
    firsttime = pd.to_datetime(str(df['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
    lasttime = pd.to_datetime(str(df['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')

    # Dropping bad speed points
    changed = True
    std = None
    mean = None

    while changed:
        df.reset_index(inplace=True)
        df.drop('index', axis=1, inplace=True)

        # Get geopy points for each lat,lon pair
        points = df[
            ['LATITUDE', 'LONGITUDE']
        ].apply(lambda x: geopy.Point(x[0], x[1]), axis=1)

        # get distances in nautical miles
        ends = points.shift(1)
        distances = []

        for idx, start in enumerate(points):
            try:
                distances.append(geopy.distance.distance(start, ends[idx]).nm)
            except ValueError:
                distances.append(np.nan)
        #distances = np.ma.masked_invalid(distances)

        # get the time difference in hours
        times = df['time'].diff() / 3600.0
        #times = np.ma.masked_invalid(times)

        # calculate speed in knots
        speed = distances / times

        # Drop anything where speed is 3 standard deviations from the mean 
        # AND > 10 knots
        if std is None:
            std = np.std(speed)
            mean = np.mean(speed)

        # np.abs throws an error when trying to do np.abs([nan]) - I'm 
        # surpressing the error here.
        with np.errstate(invalid='ignore'):
            si = np.where((np.abs(speed - mean) > (3 * std)) & (speed > 10))[0]

        if len(si) > 0:
            df.drop(points.index[si[0]], inplace=True)
            print("\tDropping point with speed=%0.1f knots" % speed[si[0]])
        else:
            changed = False

        del si

    #clean up the un-needed QC variables
    df.drop('level_0', axis=1, inplace=True)
    df.drop('time', axis=1, inplace=True)
    df.set_index('TIME',inplace=True)

    return df


def plot_track(df, metadata, datadir, coastline_resolution='l', df_resampled=None):
    """Plots the drifter track per segment. The resolution of the basemap
    is currently set to l (low) to increase speed, but can be increased.
    Even at high resolution however, the coastlines should NOT be trusted 
    as a means of filtering out stuck or grounded points."""

    #print('........plotting drifter track')
    minlons = min(df['LONGITUDE']) - 0.5 #1 #- 0.5 
    maxlons = max(df['LONGITUDE']) + 0.5 #1 #+ 0.5
    minlats = min(df['LATITUDE']) - 0.5 #1 #- 0.5
    maxlats = max(df['LATITUDE']) + 0.5 #1 #+ 0.5

    #add the track to the map
    plt.figure(1)

    #set up the basemap
    map = Basemap(projection='merc', 
                lat_0 = 57, lon_0 = -135, lat_ts = 57, 
                resolution = coastline_resolution, area_thresh = 0.1, 
                llcrnrlon=minlons, llcrnrlat=minlats, 
                urcrnrlon=maxlons, urcrnrlat=maxlats)

    map.etopo()
    map.bluemarble()
    map.drawcoastlines()
    map.fillcontinents(color = 'gray')
    map.drawmapboundary()
    map.fillcontinents(color = 'gray')

    df.reset_index(inplace=True)
    lats = df['LATITUDE'].tolist()
    lons = df['LONGITUDE'].tolist()
    times = df['TIME'].tolist()
    x,y = map(lons, lats)

    if df_resampled is not None:
        df_resampled.reset_index(inplace=True)
        lats_resampled = df_resampled['LATITUDE'].tolist()
        lons_resampled = df_resampled['LONGITUDE'].tolist()
        times_resampled = df_resampled['TIME'].tolist()
        x_resampled,y_resampled = map(lons_resampled, lats_resampled)
        map.plot(x_resampled, y_resampled, 'ro', markersize=0.5, fillstyle='full', zorder=1, label='resampled data')
        map.plot(x, y, 'bo', markersize=0.5, fillstyle='full', zorder=100, label='original data')
        plt.legend()

    else:
        map.plot(x, y, 'k', linewidth=0.5)
        map.plot(x[0], y[0], 'ro', markersize=1, fillstyle='full')


    titlestr = (metadata['buoyid'] 
                + '\n start: ' + str(metadata['time_coverage_start']) 
                + '\n end: ' + str(metadata['time_coverage_end'] 
                + '\n num of measurements: ' + str(len(lats))))

    if df_resampled is not None:
        titlestr = titlestr + '\n num of resampled measurements: ' + str(len(lats_resampled))

    plt.title(titlestr, fontsize=8)

    if df_resampled is not None:
        savenamejpg = '{}_with-resampled.jpg'.format(metadata['buoyid'])
        outdirnamestr = 'netcdf_' + str(dt.datetime.today().strftime('%Y-%m-%d'))
        savedir = os.path.join(datadir, outdirnamestr, 'drifter_track_plots_with_resampled_data')
    else:
        savenamejpg = '{}.jpg'.format(metadata['buoyid'])
        outdirnamestr = 'netcdf_' + str(dt.datetime.today().strftime('%Y-%m-%d'))
        savedir = os.path.join(datadir, outdirnamestr, 'drifter_track_plots')

    if not os.path.exists(os.path.join(savedir)):
        os.makedirs(os.path.join(savedir))
    savepath=os.path.join(savedir, savenamejpg)
    plt.savefig(savepath,bbox_inches='tight',dpi=300)
    print('........plotting', savepath)
    plt.close()


def determine_track_length(df, 
                        hdrdict, 
                        datadir, 
                        outdir=None, 
                        time_gap=24, 
                        min_points=20, 
                        resample=False,
                        resample_freq='1H',
                        plot=True,
                        write=True,
                        ):
    """Tests for gaps in time longer than 24 hours (should probably 
    default to 6 hours instead). Each segment can then be written to a
    NetCDF file and/or plotted if the segment is longer than min_points.

    Parameters
    ----------
    df : Pandas.dataframe
        Dataframe created by running drifter_qc_readers.read_raw_data(). 
        This dataframe also changes as it passes though the various tests.
    hdrdict : dict
        Dictionary created by running drifter_qc_readers.read_raw_data()
    datadir : str
        Path to the directory containing the drifter files. Currently, the
        output NetCDF files and plots are saved to a subdirectory in datadir
        by default.
    outdir : str
        Full path to an alternate directory in which to save the output 
        Netcdf files and plots. Default of None uses a subdirectory of 
        datadir for output files.
    time_gap : int
        Number of hours allowed between points before separating track into
        individual segments
    min_points : int
        Minimum number of points allowed in a segment in order to write it
        to a separate NetCDF and/or plot
    plot : boolean
        If True, a plot is created for each segment. Calls function plot_track
    write : boolean
        If True, writes the data to a separate NetCDF file for each segment.
        Calls function write_drifttool_netcdf
    """

    #manipulate the time and drop duplicates
    df.reset_index(inplace=True)
    df.sort_values('TIME', inplace=True)
    df.reset_index(inplace=True)
    df.drop('index', axis=1, inplace=True)

    #Split the tracks if longer than the given time_gap
    df['time'] = (df['TIME'] - df['TIME'][0]).apply(lambda x: x.total_seconds())
    times = df['time'].diff() / 3600.0
    df.drop('time', axis=1, inplace=True)
    extents = df.index[times > time_gap].tolist()

    # prepend 0 to that list
    extents.insert(0, 0)

    # append the total size (of the column) to the end of the list
    extents.append(times.size)

    # make a sliding window of 2 to move along that list, that'll be the
    # indices of the slice of the dataframe to select.
    extent_pairs = list(zip(extents[0:-1], extents[1:]))
    if len(extent_pairs) != 1:
        print('....splitting file into', str(len(extent_pairs)),'files')

    #break the df into sub dataframes for each time window
    for pair in extent_pairs:

        if len(extent_pairs) != 1:
            print('......working on subfile')            

        subdf = df[slice(*pair)]
        pstr = ('........time range:' + str(subdf['TIME'].iloc[0]) + ' to ' 
                + str(subdf['TIME'].iloc[-1]) + ' (' + str(len(subdf['TIME'])) 
                + ' points included)')
        print(pstr)

        if len(subdf['TIME']) < 2:
            pstr2 = ('........The subdf does not contain at least 2 points. '
                    'Erroneous time or short track, moving onto next subdf '
                    'without writing file.')
            print(pstr2)
            continue

        launchdate = pd.to_datetime(str(subdf['TIME'].values[0])).strftime('%Y%m%d')
        firsttime = pd.to_datetime(str(subdf['TIME'].values[0])).strftime('%Y-%m-%d %H:%M:%S')
        lasttime = pd.to_datetime(str(subdf['TIME'].values[-1])).strftime('%Y-%m-%d %H:%M:%S')
        launchhour = pd.to_datetime(str(subdf['TIME'].values[0])).strftime('%Y%m%dH%H')

        geospatial_lat_min = np.min(subdf['LATITUDE']) 
        geospatial_lat_max = np.max(subdf['LATITUDE'])
        geospatial_lon_min = np.min(subdf['LONGITUDE'])
        geospatial_lon_max = np.max(subdf['LONGITUDE'])

        time_coverage_start = np.min(subdf['TIME'])
        time_coverage_end = np.max(subdf['TIME'])
        first_date_observation = pd.to_datetime(str(subdf['TIME'].iloc[0])).strftime('%Y%m%d') 
        last_date_observation = pd.to_datetime(str(subdf['TIME'].iloc[-1])).strftime('%Y%m%d') 
        last_latitude_observation = subdf['LATITUDE'].iloc[-1]
        last_longitude_observation = subdf['LONGITUDE'].iloc[-1] 

        modhdrdict = {
                    'launchdate': str(launchdate),
                    #'buoyid': hdrdict['buoyid'].split('D')[0] + 'D' + str(launchhour),
                    'buoyid': hdrdict['buoyid'][0:-12]  + 'D' + str(launchhour), #hdrdict['buoyid'].split('D')[0] + 'D' + str(launchhour),
                    'dataStartDate': str(firsttime),
                    'geospatial_lat_min': str(geospatial_lat_min),
                    'geospatial_lat_max': str(geospatial_lat_max),
                    'geospatial_lon_min': str(geospatial_lon_min),
                    'geospatial_lon_max': str(geospatial_lon_max),
                    'time_coverage_start': str(time_coverage_start),
                    'time_coverage_end': str(time_coverage_end),
                    'first_date_observation': str(first_date_observation),
                    'last_date_observation': str(last_date_observation),
                    'last_latitude_observation': str(last_latitude_observation),
                    'last_longitude_observation': str(last_longitude_observation)
                    }

        if outdir is None:
            savedir = datadir
        else:
            savedir = outdir

        if len(subdf['LATITUDE']) < 20:
            print('........too few points to save or plot')
            continue

        subdf.set_index('TIME',inplace=True)
        output_ds = subdf.to_xarray()        
        output_ds_orig = output_ds

        if resample:

            try:
                # '5T' is 5 min bins, '30S' is 30 sec bins, '1H' is one
                # hour bins, etc
                dsnew = output_ds.resample({'TIME': resample_freq}).interpolate('linear').dropna('TIME')
                modhdrdict['processing_comment'] = 'Data resampled to ' + resample_freq + ' frequency'
                output_ds = dsnew
                newsubdf = dsnew.to_dataframe()
            except:
                errorstr = '....could not resample the data to ' + resample_freq
                print(errorstr)
                continue

        if plot:
            #plot_track(newsubdf, modhdrdict, os.path.join(savedir,'resampled_only'))
            plot_track(subdf, modhdrdict, savedir)
            plot_track(subdf, modhdrdict, os.path.join(savedir), df_resampled=newsubdf)

        if write:
            write_drifttool_netcdf(output_ds, hdrdict, modhdrdict, savedir)


def write_drifttool_netcdf(ds, hdrdict, modhdrdict, datadir):
    """writes data to a NetCDF file in the format required by the 
    drift-tool"""

    #add attributes for the variables
    ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
    ds.LATITUDE.attrs['units'] = 'degrees_north'
    ds.LATITUDE.attrs['_FillValue'] = ds.LATITUDE.dtype.type(np.nan)
    ds.LONGITUDE.attrs['long_name'] ='Longitude of observed trajectory'
    ds.LONGITUDE.attrs['units'] = 'degrees_east'
    ds.LONGITUDE.attrs['_FillValue'] = ds.LONGITUDE.dtype.type(np.nan)

    #add some attributes from the header metadata
    for var in hdrdict.keys():
        ds.attrs[var] = str(hdrdict[var])

    #Add attributes that are created when the file is split (or not). 
    #If the file is split, this will overwrite the older launchdate and buoyid.
    for newvar in modhdrdict.keys():
        ds.attrs[newvar] = str(modhdrdict[newvar])

    #write out the file
    output_file = '{}.nc'.format(ds.attrs['buoyid'])
    outdirnamestr = 'netcdf_' + str(dt.datetime.today().strftime('%Y-%m-%d'))
    outdir = os.path.join(datadir, outdirnamestr)
    if not os.path.exists(os.path.join(outdir)):
        os.makedirs(os.path.join(outdir))
    output_file=os.path.join(outdir, output_file)
    print('........writing',output_file)
    ds.to_netcdf(output_file,unlimited_dims=['TIME'])


def check_unique_buoyids(buoylist):
    """Helper script to check for non-unique buoyids"""
    if len(buoylist) != len(np.unique(buoylist)):
        print('Error! There are repeated buoyids.')




