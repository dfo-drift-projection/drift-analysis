#!/bin/bash

# all the output will be store in $outdir/${m}_${s}-${e}/
# example: /output_directory/giops_20181101-20180103/
outdir=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/single_drifter_test/output/

# this is where your run_drift_eval.sh script is stored.
codedir=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-tool/examples/single_drifter/runs/

# The required inputs for submit_job.sh are:
# m: model name (ciopse, giops, riops)
# s: start date (ie, 2018-11-01)
# e: end date (ie, 2018-11-03)
# c: config file minus the dates
# o: output directory as described above
# d: code directory as described above

# for the drifter Canso_SVP-4-300234066033380-20200226T193146UTCD20181101H16.nc:
# The drifter runs from 2018-11-01 to 2020-02-14, which is 471 days.

########################################################################
# run the CIOPSE data first because it's fastest
########################################################################
start_date="2018-11-01"
interval=7
total_num_of_days=30
model="ciopse"

for i in $(seq 0 $interval $total_num_of_days); do

    start=$(date -d "$start_date + $i days" +%Y-%m-%d)
    end=$(date -d "$start_date + $interval days + $i days" +%Y-%m-%d)

    ${codedir}/submit_job.sh -m ${model} -s $start -e $end -c ${codedir}/config_seed_${model}.yaml -o ${outdir} -d ${codedir}
    sleep 5m

done

########################################################################
# next run the GIOPS data
########################################################################

start_date="2018-11-01"
interval=7
total_num_of_days=471
model="giops"

for i in $(seq 0 $interval $total_num_of_days); do

    start=$(date -d "$start_date + $i days" +%Y-%m-%d)
    end=$(date -d "$start_date + $interval days + $i days" +%Y-%m-%d)

    ${codedir}/submit_job.sh -m ${model} -s $start -e $end -c ${codedir}/config_seed_${model}.yaml -o ${outdir} -d ${codedir}
    sleep 5m

done

