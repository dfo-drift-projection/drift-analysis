The scripts in this folder will let you automate running the drift tool 
with multiple models for multiple dates. You should only need to update
paths in "handle_jobs.sh" and the drifter-data-dir in the "config_seed_*"
files to get started. Once the jobs are complete, you'll need to copy 
the output to a common directory, then run a script to aggregate the output
(the aggregation script included in this folder is just a copy of the script
found in the regular drift-tool code). Finally, update paths in 
"handle_plotting_jobs.sh" and "config-plotting_*.yaml" to do the analysis.

Running the simulations:
------------------------
The scripts for this part are in the run/ directory.

First, create a config seed for each model. These are just the regular
config files, but do not contain the start and end dates. For example:
- config_seed_ciopse.yaml
- config_seed_giops.yaml
- config_seed_riops.yaml

Then, update "handle_jobs.sh" to call whatever dates you need. We are only
able to run jobs that are less than 6 hours long on the GPSC, so we need
to divide long runs up into chunks. I usually run 7 days at a time to 
keep things simple (even for GIOPS this should be less than 6 hours for 
10-20 drifters). You can submit multiple jobs at a time.
- handle_jobs.sh

"handle_jobs.sh" calls "run_drift_eval_without_aggregation.sh". This script is 
just a copy of "run_drift_eval.sh" with the portion of the code that 
aggregates the output turned off. 

Creating the aggregated output:
-------------------------------

Once the jobs are all completed, you'll need to copy the output/ 
directory from all the individual job submissions to a single folder per 
experiment (ie, per model). So, you need to make a folder called 
/experiment/output/ and copy all the output from all the dates into it. 
Some of the dates will overlap because of the way that the drift-tool 
deals with start and end dates. The data in both versions of the files 
will be the same in these cases though, so just let the data files be 
overwritten in those cases. 

then run:
- aggregate_output.sh <full path to the folder containing the output>

Plotting the data:
------------------
The scripts for this part are in the analysis/ directory

At this point you'll have the "output_per_drifter" folder sitting one 
level higher than your original output. For example, if your full path 
pointed at experiment/output/, the new folder will be in 
experiment/output_per_drifter/. This folder will have the per drifter 
files that you'll need for creating the plots.

next, make a plotting config file like:
- config-plotting_ciopse.yaml  

update "handle_plotting_jobs.sh" with the same output path that you included
in the config file, then you should be able to just run it. 
handle_plotting_jobs.sh calls submit_plotting_job.sh, which creates a job
submission script and calls run_drift-tool_analysis.sh.
- handle_plotting_jobs.sh 
- run_drift-tool_analysis.sh  
- submit_plotting_job.sh

Comparison plots:
-----------------
The scripts for this part are in the comparison_analysis/ directory, but
the plotting run script calls a script from the analysis/ directory.

First you need to crop the data sets to a common set. Update the paths in
user_comparison_crop_config.yaml, then run 
"run_comparison_cropping.sh -c <config>"

This will give you a new data directory that contains a cropped set of data.
Next, update the "user_comparison_plotting_config.yaml" and update then run 
"run_comparison_plotting.sh".
