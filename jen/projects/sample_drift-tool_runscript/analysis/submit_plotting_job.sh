#!/bin/bash

########################################################################
# user input path
########################################################################
usage() {
    u="Usage: run_drift_eval.sh"
    u="$u -c <yaml config file>"
    u="$u -m <model name (giops, riops, ciope, etc)>"
    u="$u -o <Full path to directory in which to save output>"
    u="$u -d <location of code to be run, namely submit_plotting_job.sh>"
    echo $u
    exit 1
}

# check for arguments being passed in at the command line. Users are
# required to provide a CONFIG file and are optionally able to define a
# path to a miniconda install if they do not wish to use the default
# provided on the GPSC
while getopts "h?m:c:d:o:" args; do
  case $args in
    h|\?)
        usage;
        exit 1;;
    m) model=${OPTARG};;
    c) plot_config=${OPTARG};;
    d) codedir=${OPTARG};;
    o) outdir=${OPTARG};;
    : )
        echo "Missing option argument for -$OPTARG"; exit 1;;
    *  )
        echo "Unimplemented option: -$OPTARG"; exit 1;;
  esac
done

exp_ident="plots-${model}"
miniconda_path="/home/sdfo000/sitestore4/opp_drift_fa3/software/drift-tool-miniconda"
experiment_dir="${outdir}/${exp_ident}/"
logdir=${outdir}/logs

# if expdir exists, delete it
if [ -d $experiment_dir ]; then
    rm -r $experiment_dir
fi

# make the log dir if it does not exist (needed for submitting as job)
if [ ! -d $logdir ]; then
    mkdir -p $logdir
fi

########################################################################
# generate a job script and submit the job
########################################################################
jobsub_file=$outdir/${exp_ident}.job
LOG=$logdir/${exp_ident}_screen-output.txt
#run_command="${codedir}/run_drift_eval.sh -c ${config} -o ${experiment_dir} -m ${miniconda_path} >> $LOG 2>&1"
run_command="${codedir}/run_drift-tool_analysis.sh -c $plot_config -m $miniconda_path >> $LOG 2>&1"

# create the file
echo "#! /bin/bash" > $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -m bea" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -j y" >> $jobsub_file
echo "#$ -o $logdir/${exp_ident}_joblog1.txt" >> $jobsub_file
echo "#$ -e $logdir/${exp_ident}_joblog2.txt" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -P dfo_dpnm-fa3" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -pe dev 1" >> $jobsub_file
echo "#$ -l res_cpus=1" >> $jobsub_file
echo "#$ -l res_mem=100000" >> $jobsub_file
echo "#$ -l res_tmpfs=0" >> $jobsub_file
echo "#" >> $jobsub_file
echo "#$ -l res_image=dfo/dfo_all_default_ubuntu-18.04-amd64_latest" >> $jobsub_file
echo "" >> $jobsub_file
echo "#$ -l h_rt=06:00:00" >> $jobsub_file
echo "" >> $jobsub_file
echo "#NOTE: the directory where the log files will be stored must already exist!" >> $jobsub_file
echo "" >> $jobsub_file
echo "# Call your program" >> $jobsub_file
echo "cd $codedir" >> $jobsub_file
echo $run_command >> $jobsub_file

########################################################################
# submit the job
########################################################################

jobsub -c gpsc4 ${jobsub_file}
