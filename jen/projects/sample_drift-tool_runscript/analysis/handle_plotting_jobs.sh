#!/bin/bash

# all the output will be store in $outdir/
# example: /output_directory/giops_20181101-20180103/
#outdir=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/single_drifter_test/output/ciopse_all/
outdir=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/MEOPAR_TREX/output/ciopse_all/
# this is where your submit_plotting_job.sh script is stored.
codedir=/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/sample_drift-tool_runscript/analysis

# The required inputs:
# m: model name (ciopse, giops, riops)
# c: config file 
# o: output directory as described above
# d: code directory as described above
${codedir}/submit_plotting_job.sh -m ciopse -c ${codedir}/meopar_plotting_config.yaml -o ${outdir} -d ${codedir}
