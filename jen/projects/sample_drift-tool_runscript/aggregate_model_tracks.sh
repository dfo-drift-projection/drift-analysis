#!/bin/bash

########################################################################
# combine all the output into aggregated files
########################################################################

indir=$1

# Set environment 
# ---------------
MINICONDA_PATH='$HOME/work/software/miniconda/envs/mldpn_DTv2_implementation/'
#MINICONDA_PATH='/home/sdfo000/sitestore4/opp_drift_fa3/software/drift-tool-miniconda/'
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# Combine the output into per drifter files
# -----------------------------------------
combine_track_segments \
  --data_dir=$indir
