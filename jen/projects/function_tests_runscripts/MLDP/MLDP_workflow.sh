#!/bin/bash

########################################################################
# user defined variables and paths
########################################################################

# the directory where your scripts are stored (it should contain 
# standalone_mldp_incl_simulations.sh and MLDP_seed.in)
codedir="/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/function_tests_runscripts/MLDP/"

# the directory where the output will be stored. The script will create two
# subfolders (preprocessing_output/ and output/) while running.
savedir="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/mldp_test_$(date +"%Y%m%d")/"

# choose whether or not to run the preprocessing. If true, any data in the 
# preprocessing_output folder will be deleted. If false, the script will 
# expect preprocessed data to already exist in $savedir/preprocessing_output.
run_preprocessing=true

# the bbox and the drifter postions in the MLDP_seed.in were chosen to 
# work with the sample rpn data in the raw_rpn_data folders.
bbox="45.0 -49.0 48.0 -45.0"

#datadir="/fs/vnas_Hdfo/dpnm/sdfo000/sitestore4/opp_drift_fa3/share_drift/sample_rpn_dataset/"
datadir="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/input_data/"
raw_ocean_data_rpn=${datadir}/ciopse/
raw_meteo_data_rpn=${datadir}/hrdps/

# This is the start of a sample namelist. There's a bit of code at the 
# bottom of this script that merges this starter file plus the paths to 
# the preprocessed data. If you want to change the particle starting 
# locations, you should be able to change them in this seed file. 
# Otherwise, you shouldn't need to create a namelist file for MLDP to run.
namelist_seed="${codedir}/MLDP_seed.in"

########################################################################
# Set up directories for the preprocessing output and experiment output,
# These will all be subfolders under the defined savedir
########################################################################
preprocessing_outdir=${savedir}/preprocessing_output/
experiment_dir=${savedir}/output/
echo -e "\nOutput will be stored in ${savedir}"

mkdir -p $savedir/
# experiment_dir needs to exist for MLDP to run properly
rm -rf $experiment_dir
mkdir -p $experiment_dir
# only clean up the preprocessing_outdir if re-running the preprocessing
if $run_preprocessing ; then
    rm -rf $preprocessing_outdir/
    mkdir -p $preprocessing_outdir
fi

########################################################################
# Source the profile script (this loads the ECCC tools). As of 
# April 30, 2021, this profile script calls:
# EERWetDB.tcl=/fs/vnas_Hdfo/dpnm/sdfo000/sitestore4/MLDP_temp/EERWetDB.tcl
# MLDPn=/fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
########################################################################
#-------------------------------------------------------
# Source the ECCC profile script (this loads the ECCC tools).
#-------------------------------------------------------
source_log=$savedir/load_eccc_env_var_output.txt
echo -e "\nSetting up the ECCC environment variables by sourcing ${codedir}/MLDP_source_packages.sh. "
echo -e "Any resulting error messages will be displayed in $source_log"
source ${codedir}/MLDP_source_packages.sh >> $source_log 2>&1

########################################################################
# run the preprocessing. This combines the met and wet files together
# and creates a mask. Creating the mask takes a LONG time (from 10 minutes
# up to hours depending on the coastline enclosed by the bbox). At the 
# mask creation stage, the process just seems to hang, but will eventually
# complete. This port of the process isn't complete until a mask.in file
# is generated.
########################################################################

if $run_preprocessing ; then

    echo -e "\nRunning preprocessing. Screen output can be found ${savedir}/EERWetDB_screen-output.log"

    # create lists of the raw files to pre-process
    find $raw_ocean_data_rpn/* > $preprocessing_outdir/wetfiles
    find $raw_meteo_data_rpn/* > $preprocessing_outdir/metfiles

    # to create a mask, set resmask to something other than 0 (ie, 25 or 100, etc)
    EERWetDB.tcl \
        -bbox $bbox \
        -resmask 500 \
        -metfiles $preprocessing_outdir/metfiles \
        -wetfiles $preprocessing_outdir/wetfiles \
        -out $preprocessing_outdir \
        -interp \
        -depth 0 \
        -waves false \
        2>&1 > $savedir/EERWetDB_screen-output.log
else
    echo -e "\nUsing previously pre-processed data from ${preprocessing_outdir}"
fi
 
########################################################################
# create the namelist using the files that were created in the 
# preprocessing stage
########################################################################

namelist=$savedir/MLDP.in

# point at the grid and landmask
cat $namelist_seed > $namelist
echo "   OUT_GRID       = ${preprocessing_outdir}/grid.in # Output gr$" >> $namelist
echo " " >> $namelist
echo "   #----- Meteorological parameters" >> $namelist
echo "   MET_BOTTOM = 0.9999     # Bottom reflection level of particles in the atmosphere [hybrid|eta|sigma]" >> $namelist
echo "   MET_MASK   = ${preprocessing_outdir}/mask.in" >> $namelist
echo "   MET_FILES  =           # Meteorological input files" >> $namelist

# write the paths for the rpn preprocessed files to the namelist
cd $preprocessing_output
find "$(cd ${preprocessing_outdir}; pwd)" -type f \
  -not -name "wetfiles" \
  -not -name "metfiles" \
  -not -name "*.nc" \
  -not -name "*.in" \
  -not -name "*.prj" \
  -not -name "*.dbf" \
  -not -name "*.shp" \
  -not -name "*.shx" \
  | sort >> $namelist

echo -e "\nRunning MLDP. Screen output can be found ${savedir}/MLDPn.*"
# run MLDP
MLDPn \
  -i $namelist \
  -o $experiment_dir \
  -t 1 \
  --nompi >$savedir/MLDPn.out 2>$savedir/MLDPn.err

# copy the job submission logscripts into the savedir if they exist
logdir=$savedir/job-submission-logs/
mkdir $logdir
cp $(dirname $savedir)/logs/* $logdir 2>/dev/null

