# pre ===================================
# HPCS
. ssmuse-sh -x hpci/opt/hpcarchive-latest

# RPN
. ssmuse-sh -d eccc/mrd/rpn/utils/19.6.0

# CMOI
. ssmuse-sh -d eccc/cmo/cmoi/base/20190712

# CMOE
. ssmuse-sh -d eccc/cmo/cmoe/eer/beta

# CMDS
export CMD_EXT_PATH=/fs/ssm/eccc/cmd/cmds/ext/20200129
. ssmuse-sh -x $CMD_EXT_PATH
. ssmuse-sh -d /fs/ssm/eccc/cmd/cmds/base/201911/01/default
. ssmuse-sh -x eccc/cmd/cmds/apps/grib/grib2-cvt/2.0
. ssmuse-sh -d eccc/cmd/cmds/apps/grib/deprecated/grib1-cvt/3.5.1
. ssmuse-sh -x eccc/cmd/cmds/apps/SPI/beta

# plat.ubuntu-18.04-amd64-64 ===================================
#----- HPCO compiler environment
#<intel>
. ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
. ssmuse-sh -x hpco/exp/openmpi/openmpi-3.1.2--hpcx-2.4.0-mofed-4.6--intel-19.0.3.199
#</intel>
. ssmuse-sh -x hpco/exp/openmpi-setup/openmpi-setup-0.1

# post ===================================

typeset -fx chdir
umask 022

# Special override
export PATH="/fs/vnas_Hdfo/dpnm/sdfo000/sitestore4/MLDP_temp:$PATH"
. ssmuse-sh -x eccc/mrd/rpn/vgrid/6.5.0
. r.load.dot /fs/ssm/eccc/mrd/rpn/libs/19.6.0

export KMP_AFFINITY=disabled
export OMPI_MCA_btl_openib_if_include=mlx5_0
export OMPI_MCA_orte_tmpdir_base=/run/shm
export GEO_PATH=/fs/vnas_Hdfo/dpnm/sdfo000/sitestore4/hnas/geo
