This code runs a sample test of MLDP (both the pre-processing and simulations). It can be used to confirm that MLDP itself is still working correctly if the drift-tool version stops working for some reason.

## requires:
* MLDP_workflow.sh
* MLDP_seed.in
* MLDP_jobsub_script.job
* MLDP_source_packages.sh 
* Raw RPN ocean and atmosphere files 

## to run:

To submit as a job:
```
jobsub -c gpsc4 MLDP_jobsub_script.job
```
or run interactively:
```
./MLDP_workflow.sh
```

## NOTES:

* **run_preprocessing** in MLDP_workflow.sh is a boolean (true/false) defining if the raw data should be pre-preprocessed. If true, the workflow will run EERWetDB.tcl on the provided raw ocean and atm RPN format data. If false, the workflow will look in $savedir/preprocessing_output/ for previously processed data instead, which is significantly faster. Presently, the pre-processing takes approximately a couple of minutes and the simulations themselves are created in under a minute (the landmask creation portion of the pre-processing can be significantly slower for larger bounding boxes however).
* I've made a backup of the expected output and stored it here: /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/successful_test_20210928.tar.gz
* There's a backup of the required input data and settings file here: /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/input_data.tar.gz
* At the time when I wrote these scripts, the settings source file called the following: /fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/script/EERWetDB.tcl and /fs/ssm/eccc/cmo/cmoe/eer/master/eerModel_4.3.0-intel-19.0.3.199_ubuntu-18.04-amd64-64/bin/MLDPn
* Log files are created from the .job submission script and at other stages in the workflow. The log files created by the job submission are store here: /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/logs/. The rest of the log files are added to the $savedir.


## Sample screen output for a successful run:
```
jeh326@dfo-inter-ubuntu-18/fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/function_tests_runscripts/MLDP $ ./MLDP_workflow.sh

Output will be stored in /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/mldp_test_20210928/

Setting up the ECCC environment variables by sourcing /fs/vnas_Hdfo/dpnm/jeh326/dfo-drift-projection/drift-analysis/jen/projects/function_tests_runscripts/MLDP//MLDP_source_packages.sh.
Any resulting error messages will be displayed in /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/mldp_test_20210928//load_eccc_env_var_output.txt

Running preprocessing. Screen output can be found /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/mldp_test_20210928//EERWetDB_screen-output.log

Running MLDP. Screen output can be found /gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/function_tests/standalone_mldp/mldp_test_20210928//MLDPn.*
```
