# Add depth dimension for use in ariane
# - Ariane needs at least 2 vertical levels in order to finish successfully
# - original file written by Nancy
# updated by Jennifer to include decimating the mesh file to two layers.

###################################################
#need to update raw_data_dir, outdir, full_mesh

raw_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/SNDIST/CIOPSW_2D_1h/

full_mesh=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc

outdir=/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/copying_surface_ciopsw/
#mkdir -p $outdir

###################################################

date=201
filesU=${raw_data_dir}/*1h*grid_U*${date}*.nc
filesV=${raw_data_dir}/*1h*grid_V*${date}*.nc

#decimate the mesh file to two depth layers
mesh=${outdir}/duplicated_surface_mesh.nc
ncks -d z,0,1 $full_mesh ${mesh}

#for Ufile 
for Ufile in ${raw_data_dir}/*1h*grid_U_2D*.nc; do

    basename=$(basename $Ufile)
    echo $basename
    ncap2 -s 'defdim("z",2);uos_new[$time_counter,$z,$y,$x]=uos;' $Ufile $outdir/$basename
    #copy metadata for depth
    ncks -A -v gdept_1d $mesh $outdir/$basename
    ncks -O -x -v uos $outdir/$basename $outdir/$basename
    ncrename -v uos_new,uos $outdir/$basename
    ncrename -v gdept_1d,depth $outdir/$basename
    # Copy metadata for u
    ncks -A -C -H -v uos $f $outdir/$basename
    
done

#for Vfile
for Vfile in ${raw_data_dir}/*1h*grid_V_2D*.nc; do

    basename=$(basename $Vfile)
    echo $basename
    ncap2 -s 'defdim("z",2);vos_new[$time_counter,$z,$y,$x]=vos;' $Vfile $outdir/$basename
    #copy metadata for depth
    ncks -A -v gdept_1d $mesh $outdir/$basename
    ncks -O -x -v vos $outdir/$basename $outdir/$basename
    ncrename -v vos_new,vos $outdir/$basename
    ncrename -v gdept_1d,depth $outdir/$basename
    # Copy metadata for v
    ncks -A -C -H -v vos $f $outdir/$basename

done

#rm $mesh
