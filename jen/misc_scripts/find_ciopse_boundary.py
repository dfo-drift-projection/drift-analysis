#!/usr/bin/env python

import os
import glob
#import shutil
import xarray as xr
import numpy as np
#import pandas as pd
#import datetime as dt
import pickle

#user_installed_python_path="/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/"
os.environ["PROJ_LIB"] = "/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/share/proj/"

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

##some pandas parameters
#pd.set_option('display.max_rows', 500)
#pd.set_option('display.max_columns', 500)
#pd.set_option('display.width', 1000)
#pd.set_option('display.colheader_justify', 'left')

savedir = '/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/jen/misc_scripts'

use_pickle = 'yes' #yes or no

if use_pickle == 'yes':
    load_from_pickle=os.path.join(savedir,'ciopse_domain.pickle')
    with open(load_from_pickle, 'rb') as f:
        ciopse_domain = pickle.load(f)

else:
    datafile = '/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/models/ciops-e/CIOPSE_SN1500_3D/NEMO_RPN_1d_grid_V_20170108-20170108.nc'

    ds = xr.open_dataset(datafile)

    lengx = len(ds.x)-1
    lengy = len(ds.y)-1

    bottom_lat = ds.nav_lat[1,:].values
    top_lat = ds.nav_lat[lengy,:].values
    left_lat = ds.nav_lat[:,1].values
    right_lat = ds.nav_lat[:,lengx].values

    bottom_lon = ds.nav_lon[1,:].values
    top_lon = ds.nav_lon[lengy,:].values
    left_lon = ds.nav_lon[:,1].values
    right_lon = ds.nav_lon[:,lengx].values

    minlons=-78.18400573730469-4
    maxlons=-37.71416091918945+4
    minlats=34.87752914428711-4
    maxlats=54.3026237487793+4

    ciopse_domain = {
        "left_lon":left_lon,
        "right_lon":right_lon,
        "top_lon":top_lon,
        "bottom_lon":bottom_lon,
        "left_lat":left_lat,
        "right_lat":right_lat,
        "top_lat":top_lat,
        "bottom_lat":bottom_lat,
        "plot_minlons":minlons,
        "plot_minlats":minlats,
        "plot_maxlons":maxlons,
        "plot_maxlats":maxlats
    }

    #save a pickle of the domain
    with open(os.path.join(savedir,'ciopse_domain.pickle'), 'wb') as f:
        pickle.dump(ciopse_domain, f)



##print the min/max
#print('left_lon',min(left_lon),max(left_lon))
#print('left_lat',min(left_lat),max(left_lat))
#print('bottom_lon',min(bottom_lon),max(bottom_lon))
#print('bottom_lat',min(bottom_lat),max(bottom_lat))
#print('right_lon',min(right_lon),max(right_lon))
#print('right_lat',min(right_lat),max(right_lat))
#print('top_lon',min(top_lon),max(top_lon))
#print('top_lat',min(top_lat),max(top_lat))

print('max top lat:    ',max(ciopse_domain['top_lat']))
print('min bottom lat: ',min(ciopse_domain['bottom_lat']))
print('min left lon:   ',min(ciopse_domain['left_lon']))
print('max right lon:  ',max(ciopse_domain['right_lon']))

#add the track to the map
plt.figure(1)

#set up the basemap
map = Basemap(projection='merc', lat_0 = 57, lon_0 = -135, lat_ts = 57, resolution = 'l', area_thresh = 0.1, llcrnrlon=ciopse_domain['plot_minlons'], llcrnrlat=ciopse_domain['plot_minlats'], urcrnrlon=ciopse_domain['plot_maxlons'], urcrnrlat=ciopse_domain['plot_maxlats'])
map.bluemarble()
map.drawcoastlines()
map.fillcontinents(color = 'gray')

# draw parallels and meridians.
map.drawparallels(np.arange(-90.,91.,5.),dashes=[1,1],linewidth=0.1,labels=[True,True,False,False]) 
map.drawmeridians(np.arange(-180.,181.,10.),dashes=[1,1],linewidth=0.1,labels=[False,False,False,True]) 
#map.drawmeridians(np.arange(-180.,181.,10.),dashes=[1,1],linewidth=0.1,labelstyle="+/-",labels=[False,False,False,True]) 

#add the boundary
x_left,y_left = map(ciopse_domain['left_lon'], ciopse_domain['left_lat'])
map.plot(x_left, y_left, linewidth=0.5, color='gainsboro')
#map.plot(x_left, y_left, marker='.', markersize=0.02, color='gainsboro')

x_right,y_right = map(ciopse_domain['right_lon'], ciopse_domain['right_lat'])
map.plot(x_right, y_right, linewidth=0.5,  color='gainsboro')
#map.plot(x_right, y_right, marker='.', markersize=0.02,  color='gainsboro')

x_top,y_top = map(ciopse_domain['top_lon'], ciopse_domain['top_lat'])
map.plot(x_top, y_top, linewidth=0.5,  color='gainsboro')
#map.plot(x_top, y_top, marker='.', markersize=0.02,  color='gainsboro')

x_bottom,y_bottom = map(ciopse_domain['bottom_lon'], ciopse_domain['bottom_lat'])
map.plot(x_bottom, y_bottom, linewidth=0.5,  color='gainsboro')
#map.plot(x_bottom, y_bottom, marker='.', markersize=0.02,  color='gainsboro')

plt.title('CIOPSE domain')

savenamejpg = 'ciopse_domain.jpg'
plt.savefig(os.path.join(savedir,savenamejpg),bbox_inches='tight',dpi=300)


