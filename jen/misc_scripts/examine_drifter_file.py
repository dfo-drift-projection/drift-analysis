#!/usr/bin/env python

import os
from os.path import join as joinpath

#user_installed_python_path="/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/"
os.environ["PROJ_LIB"] = "/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/share/proj/" 

import datetime
import glob

import xarray as xr
import numpy as np
import pandas as pd
#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

#import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', 
                    type=str, 
                    default = '/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/drifters/DriftTool_Sample_Dataset/east_coast_drifters', 
                    help='path to observed drifter netcdfs',
                    )
parser.add_argument('--data_file',
                    type=str,
                    )
args = (parser.parse_args())
data_dir=args.data_dir
data_file=args.data_file

savedir = '/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/jen/driftcompare_early-scripts/testplots_short/'

dflist = []

if args.data_file is not None:
    globarg = args.data_file
else:
    globarg = '.nc'


for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith(globarg):
            #if not filename.endswith('0_1.nc'):
                continue
            
            drifter_filename = os.path.join(dirpath, filename)
            
            if not os.path.isabs(drifter_filename):
                # Determine path of drifter file relative to directory of
                # output file.
                odir = os.path.dirname(os.path.abspath(output_file))
                drifter_filename = os.path.relpath(drifter_filename, odir)

            ds = xr.open_dataset(drifter_filename)
           
            #print(drifter_filename)
            df = ds.to_dataframe()
            df['filename'] = filename
            df['full_filename'] = drifter_filename
            #dflengthtest = len(df['longitude'])
            df['depth'] = ds.approximate_drogue_depth
           
            #print(df['LATITUDE']) 
            #df.set_index('time', inplace=True)
            #print(df.head())
            
            if 'obs_lat' in df.keys():
                df.rename(inplace=True, columns={'obs_lat':'latitude','obs_lon':'longitude'})
            if 'lat' in df.keys():
                df.rename(inplace=True, columns={'lat':'latitude','lon':'longitude'})
            if 'Latitude' in df.keys():
                df.rename(inplace=True, columns={'Latitude':'latitude', 'Longitude':'longitude'})
            if 'LATITUDE' in df.keys():
                df.rename(inplace=True, columns={'LATITUDE':'latitude', 'LONGITUDE':'longitude'})
            if 'LAT' in df.keys():
                df.rename(inplace=True, columns={'LAT':'latitude', 'LON':'longitude'})
    
            if 'TIME' in df.keys():
                df.rename(inplace=True, columns={'TIME':'time'})

            #######################################################################################
            # to add a year range, uncomment below
            #######################################################################################

            df = df[df.index.year==2019]
            df = df[df.index.month.isin(range(9,10))]

            #df = df[df.index.month.isin(range(5,10))]
            #print(df.index)


            print(filename)
            print(df)


   

            


