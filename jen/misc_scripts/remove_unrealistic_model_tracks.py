#!/usr/bin/env python
#--------------------------------------------------------------------------
# Author: Yukie Hata ECCC-CMC CMDE
# Original Written: October 7, 2019
# The latest update: October 7, 2019 Y. Hata
#
# The following packages are needed to run this script
#. ssmuse-sh -p eccc/crd/ccmr/EC-CAS/master/fstd2nc_0.20180821.1_all
#. r.load.dot eccc/mrd/rpn/MIG/ENV/py/2.7/rpnpy/2.0.4
#
#------------------------------------------------------------------------

import os

os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os, sys
import xarray as xr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap
import numpy as np
import numpy.random as npr
from datetime import datetime
from datetime import timedelta
import glob
import pandas as pd
import pickle
import argparse
#some pd parameters
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

def grab_buoyids(datadir):
    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir,'*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[-2])
    return np.unique(iname)

def create_master_dataframe(pathinroot):

    df_all = pd.DataFrame()
    pathin = pathinroot
    buoyids = grab_buoyids(pathin)

    for buoyid in buoyids :
        fname = os.path.join(pathin, buoyid+'_aggregated.nc')
        with xr.open_dataset(fname) as ds:

            print()
            print(buoyid)

            for m in ds.model_run.values:
                print('model run number', m)

                modtime = np.datetime64(ds['mod_start_date'].values[m], format='%Y-%m-%d %H:%M:%S')
                obstime = ds['time'][m, 0].values
                timediff = (modtime - obstime) / np.timedelta64(1, 'm')
                timediffmins = (modtime - obstime) / np.timedelta64(1, 'h')
                origsep = ds['sep'][m, 0].values

                if np.abs(float(timediff)) > 60: 
                    print('    big time gap:  starting points',int(np.abs(float(timediff))), 'minutes (', int(np.abs(float(timediffmins))),'hours) apart')

                if origsep > 150:  
                    print('    distance gap:  starting points separated by', int(origsep), 'meters')


#########################################

pathinroot = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/'

create_master_dataframe(pathinroot)
