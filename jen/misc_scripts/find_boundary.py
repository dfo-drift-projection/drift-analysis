#!/usr/bin/env python

import os
#import glob
import xarray as xr
import numpy as np
import pickle

os.environ["PROJ_LIB"] = "/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/share/proj/"

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
#######################################

#savedir = '/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/jen/misc_scripts'
savedir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/boundary-handling/domains/'

modelname = 'ciopse' #ciopse or ciopsw
use_pickle = 'no' #yes or no


#######################################
picklename = modelname + '_domain.pickle'

if use_pickle == 'yes':
    load_from_pickle=os.path.join(savedir,picklename)
    with open(load_from_pickle, 'rb') as f:
        domainspecs = pickle.load(f)

else:
    if modelname == 'ciopse':
        datafile = '/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/models/ciops-e/CIOPSE_SN1500_3D/NEMO_RPN_1d_grid_V_20170108-20170108.nc'
    elif modelname == 'ciopsw':
        datafile = '/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-W/nep36_SNDIST_03/NEMO_705600/DI03_1d_grid_V_20170319-20170319.nc'    
    elif modelname =='salish':
        datafile = '/home/nso001/data/work/OceanModels/salishsea/grid/mesh_mask201702.nc'
    else:
        print("no specified datafile for this modelname")
        exit()

    ds = xr.open_dataset(datafile)

    lengx = len(ds.x)-1
    lengy = len(ds.y)-1

    bottom_lat = ds.nav_lat[1,:].values
    top_lat = ds.nav_lat[lengy,:].values
    left_lat = ds.nav_lat[:,1].values
    right_lat = ds.nav_lat[:,lengx].values

    bottom_lon = ds.nav_lon[1,:].values
    top_lon = ds.nav_lon[lengy,:].values
    left_lon = ds.nav_lon[:,1].values
    right_lon = ds.nav_lon[:,lengx].values

    domainspecs = {
        "left_lon":left_lon,
        "right_lon":right_lon,
        "top_lon":top_lon,
        "bottom_lon":bottom_lon,
        "left_lat":left_lat,
        "right_lat":right_lat,
        "top_lat":top_lat,
        "bottom_lat":bottom_lat
    }

    print('creating ' + picklename)
    print('')

    #save a pickle of the domain
    with open(os.path.join(savedir,picklename), 'wb') as f:
        pickle.dump(domainspecs, f)


##print the min/max
print('max top lat:    ',max(domainspecs['top_lat']))
print('min bottom lat: ',min(domainspecs['bottom_lat']))
print('min left lon:   ',min(domainspecs['left_lon']))
print('max right lon:  ',max(domainspecs['right_lon']))

minlons=min(domainspecs['left_lon'])-4
maxlons=max(domainspecs['right_lon'])+4
minlats=min(domainspecs['bottom_lat'])-4
maxlats=max(domainspecs['top_lat'])+4

#add the track to the map
plt.figure(1)

#set up the basemap
map = Basemap(projection='merc', lat_0 = 57, lon_0 = -135, lat_ts = 57, resolution = 'l', area_thresh = 0.1, llcrnrlon=minlons, llcrnrlat=minlats, urcrnrlon=maxlons, urcrnrlat=maxlats)
map.bluemarble()
map.drawcoastlines()
map.fillcontinents(color = 'gray')

# draw parallels and meridians.
map.drawparallels(np.arange(-90.,91.,5.),dashes=[1,1],linewidth=0.1,labels=[True,True,False,False]) 
map.drawmeridians(np.arange(-180.,181.,10.),dashes=[1,1],linewidth=0.1,labels=[False,False,False,True]) 
#map.drawmeridians(np.arange(-180.,181.,10.),dashes=[1,1],linewidth=0.1,labelstyle="+/-",labels=[False,False,False,True]) 

#add the boundary
x_left,y_left = map(domainspecs['left_lon'], domainspecs['left_lat'])
map.plot(x_left, y_left, linewidth=0.5, color='gainsboro')

x_right,y_right = map(domainspecs['right_lon'], domainspecs['right_lat'])
map.plot(x_right, y_right, linewidth=0.5,  color='gainsboro')

x_top,y_top = map(domainspecs['top_lon'], domainspecs['top_lat'])
map.plot(x_top, y_top, linewidth=0.5,  color='gainsboro')

x_bottom,y_bottom = map(domainspecs['bottom_lon'], domainspecs['bottom_lat'])
map.plot(x_bottom, y_bottom, linewidth=0.5,  color='gainsboro')

plt.title(modelname + ' domain')

savenamejpg = modelname + '_domain.jpg'
plt.savefig(os.path.join(savedir,savenamejpg),bbox_inches='tight',dpi=300)


