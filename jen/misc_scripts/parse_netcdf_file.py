#!/usr/bin/env python

import os

#user_installed_python_path="/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/"
os.environ["PROJ_LIB"] = "/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/share/proj/"

import datetime
import glob

import xarray as xr
import numpy as np
import pandas as pd
#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

#import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

indir = "/fs/vnas_Hdfo/odis/jeh326/garbage_todelete"

for filename in glob.glob(os.path.join(indir,'*.nc')):
    print(filename)
    file = os.path.join(indir, filename)

    ds = xr.open_dataset(file)

    print('----------------------------------------------------------------------------')
    print(len(ds.time),len(ds.obs_lat),len(ds.obs_lon),len(ds.mod_lat),len(ds.mod_lon))
    print('time',ds.time.values)
    print('obs_lat',ds.obs_lat.values)
    print('obs_lon',ds.obs_lon.values)
    print('mod_lat',ds.mod_lat.values)
    print('mod_lon',ds.mod_lon.values)

    print()
