#!/usr/bin/env python

import os

os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"

import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
import os, sys
import xarray as xr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap
import numpy as np
import numpy.random as npr
from datetime import datetime
from datetime import timedelta
import glob
import pandas as pd
import pickle
import argparse
#some pd parameters
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

def grab_buoyids(datadir):
    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir,'*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[-2])
    return np.unique(iname)

def setup_map(crnr=None, resolution='c', df=None, lat_coordinate=None, lon_coordinate=None, ax=None):
    'Setup Basemap.'
    minlons = np.nanmin(lon_coordinate) - 0.5
    maxlons = np.nanmax(lon_coordinate) + 0.5
    minlats = np.nanmin(lat_coordinate) - 0.5
    maxlats = np.nanmax(lat_coordinate) + 0.5
    bmap = Basemap(projection='merc',
        lat_0 = 57,
        lon_0 = -135,
        lat_ts = 57,
        area_thresh = 0.1,
        llcrnrlon=minlons, llcrnrlat=minlats,
        urcrnrlon=maxlons, urcrnrlat=maxlats,
        resolution=resolution, ax=ax)
    meridian_label = [0, 0, 0, 1]
    parallel_label = [1, 0, 0, 0]
    #common formatting
    if crnr == 'data':
        bmap.drawparallels(np.linspace(minlats, maxlats, 7),dashes=[2,2],linewidth=0.1,fontsize=5,fmt='%.2f',labels=parallel_label)
        bmap.drawmeridians(np.linspace(minlons, maxlons, 5),dashes=[2,2],linewidth=0.1,fontsize=5,fmt='%.2f',labels=meridian_label, rotation=45)
    else:
        bmap.drawparallels(np.linspace(bmap.llcrnrlat, bmap.urcrnrlat, 7),dashes=[1,1],linewidth=0.1,fontsize=5,labels=parallel_label)
        bmap.drawmeridians(np.linspace(bmap.llcrnrlon, bmap.urcrnrlon, 7),dashes=[1,1],linewidth=0.1,fontsize=5,labels=meridian_label,rotation=45)

    # draw coastlines.
    bmap.drawcoastlines(linewidth=0.1)
    bmap.drawmapboundary(linewidth=0.2, fill_color='white')
    bmap.fillcontinents(color='lightgray',lake_color='white')

    return bmap

def create_master_dataframe(pathinroot, drifterid):

    df_all = pd.DataFrame()
    pathin = pathinroot
    buoyid = str(drifterid)

    mod_release_count = 0
    fname = os.path.join(pathin, buoyid+'_aggregated.nc')
    with xr.open_dataset(fname) as ds:

        ds['time_since_start'] = ds['time'].astype(np.timedelta64)
        ds['initial_lat'] = ds['mod_lat'].astype(float)
        ds['initial_lon'] = ds['mod_lon'].astype(float)

        for m in ds.model_run.values:
            mod_release_count += 1
            ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64)
            ds['initial_lat'][m, :] = ds['mod_lat'][m, 0]
            ds['initial_lon'][m, :] = ds['mod_lon'][m, 0]

        df = ds.to_dataframe()
        df.insert(0,'buoyid', buoyid, True)
        df_all = pd.concat([df_all, df])

    print('there are ', mod_release_count, ' model runs saved in ', drifterid, '_aggregated.nc')
    #to round to the hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600))
    #to round to .1 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600, 1))
    #to round to .5 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 1800) / 2.0)
    #to round to the hour before:
    df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

    return df_all

def examine_buoy(drifterdf, drifterid, savedir):

    printloclist = ['buoyid','time','mod_start_date','obs_lat','obs_lon','mod_lat','mod_lon']
    printskilllist = ['buoyid','time','liu','molcard','sep','obs_dist','obs_disp',
        'mod_dist','mod_disp','sutherland','obsratio','modratio','pers_lat','pers_lon']
    printextralist = ['buoyid','time','mod_start_date','original_filename',
        'time_since_start','initial_lat','initial_lon','hours_since_start']
    printalllist = ['buoyid','time','obs_lat','obs_lon','mod_lat','mod_lon',
        'liu','molcard','sep','obs_dist','obs_disp','mod_dist','mod_disp',
        'sutherland','obsratio','modratio','pers_lat','pers_lon',
        'mod_start_date','original_filename','time_since_start',
        'initial_lat','initial_lon','hours_since_start']

    #drifterdf = df_all.loc[df_all['buoyid'] == str(drifterid)]
    #drifterdf = df_all

    print(np.unique(drifterdf.index.get_level_values('model_run').values))

    for m in np.unique(drifterdf.index.get_level_values('model_run').values):  #range(0, len(drifterdf.index.get_level_values('model_run'))):

        modrundf = drifterdf.loc[drifterdf.index.get_level_values('model_run') == m]
        print(modrundf[printloclist].head())
        print()


    '''
    for m in np.unique(drifterdf.index.get_level_values('model_run').values):  #range(0, len(drifterdf.index.get_level_values('model_run'))):

        modrundf = drifterdf.loc[drifterdf.index.get_level_values('model_run') == m]
        print(modrundf[printloclist].head())
        print()

        times = modrundf.index.get_level_values('timestep')

        fig, ax = plt.subplots()
        modrundf.plot(figsize=(15,4))
        modrundf.plot(subplots=True, figsize=(15,6))
        modrundf.plot(x=times, y=['obs_lat', 'mod_lat'], style='.')

        savestr = 'qc-check_lat_' + str(drifterid) + '-' + str(m) + '.png'
        outfile=os.path.join(savedir,savestr)
        plt.savefig(outfile,bbox_inches='tight',format='png', dpi=100)
        plt.close()
    '''

    '''
    times = drifterdf.index.get_level_values('timestep') 
    fig, ax = plt.subplots()
    print('plotting lats')
    drifterdf.plot(figsize=(15,4))
    drifterdf.plot(subplots=True, figsize=(15,6))
    drifterdf.plot(x=times, y=['obs_lat', 'mod_lat'], style='.', markersize='0.2')
    savestr = str(drifterid) + '_qc-check_lat' +  '.png'
    outfile=os.path.join(savedir,savestr)
    plt.savefig(outfile,bbox_inches='tight',format='png', dpi=100)
    plt.close()

    fig, ax = plt.subplots()
    print('plotting lons')
    drifterdf.plot(figsize=(15,4))
    drifterdf.plot(subplots=True, figsize=(15,6))
    drifterdf.plot(x=times, y=['obs_lon', 'mod_lon'], style='.', markersize='0.2')
    savestr = str(drifterid) + '_qc-check_lon' +  '.png'
    outfile=os.path.join(savedir,savestr)
    plt.savefig(outfile,bbox_inches='tight',format='png', dpi=100)
    plt.close()
    '''

    '''
    #not particularly helpful
    startdf = drifterdf[drifterdf.index.get_level_values('timestep')==0]
    starttimes = startdf.index.get_level_values('timestep')
    fig, ax = plt.subplots()
    print('plotting starting lons')
    startdf.plot(figsize=(15,4))
    startdf.plot(subplots=True, figsize=(15,6))
    startdf.plot(x=starttimes, y=['obs_lon', 'mod_lon'], style='.', markersize='0.2')
    savestr = str(drifterid) + '_qc-check_starting-lons' +  '.png'
    outfile=os.path.join(savedir,savestr)
    plt.savefig(outfile,bbox_inches='tight',format='png', dpi=100)
    plt.close()
    '''

##########################################
'''
parser = argparse.ArgumentParser(description = '(an example of command: )')

parser.add_argument('PTH_IN', help = 'path to the input data', type = str)
parser.add_argument('PTH_OUT', help = 'path to the output figures', type = str)
parser.add_argument('OCE_MODEL', help = 'name of ocean model', type = str)
parser.add_argument('DRIFT_MODEL', help = 'name of drift model', type = str)
#parser.add_argument('READ_DATA', help = 'either do_pickle or None', type = str)
parser.add_argument('BUOYID', help = '', type = str)

args = parser.parse_args()

pathinroot     = args.PTH_IN           # '/home/yuh002/data/site2/verif_ocevel_cmems/store_drifteval'
pathoutfig     = args.PTH_OUT          # '/home/yuh002/data/site2/verif_ocevel_cmems/store_drifteval/figures'
nom_ocemodel   = args.OCE_MODEL        # 'giops' 'riops'
nom_driftmodel = args.DRIFT_MODEL      # 'mldp'
#do_pickle      = args.READ_DATA        # do_pickle or None
drifterid      = args.BUOYID
'''

pathinroot = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' 
pathoutfig = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/aggregated_plots/qc_plots' 
nom_ocemodel = 'ciopsw' 
nom_driftmodel = 'opendrift' 
drifterid = 'P2D-wp4322522904D20160626'

# read data
#import pickle
#picklename = 'pickled-dataframe_' + drifterid + '_' + str(nom_ocemodel) + '_' + str(nom_driftmodel) + '.pickle'
#if do_pickle == 'do_pickle':
#    df_all = create_master_dataframe(pathinroot, drifterid)
#    df_all.to_pickle(os.path.join(pathoutfig,picklename))
#else:
#    df_all = pd.read_pickle(os.path.join(pathoutfig,picklename))

#df_test = df_all.loc[(df_all.time.dt.month == 10) & (df_all['hours_since_start'] == 24)]
#print(df_test[['buoyid','time','obs_lat','obs_lon','mod_lat','mod_lon','sep','mod_start_date','hours_since_start','initial_lat']])

drifterdf = create_master_dataframe(pathinroot, drifterid)
examine_buoy(drifterdf, drifterid, pathoutfig)

#sample run command:
#./examine_aggregated_output_file.py '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/' '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/paper_ciopsw/exp-output/ciopsw-hourly/aggregated_plots/qc_plots' 'ciopsw' 'opendrift' P2D-wp4962520867D20160810

