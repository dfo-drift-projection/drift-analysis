#!/usr/bin/env python

import os
from os.path import join as joinpath

#user_installed_python_path="/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/"
os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj" 
#os.environ["PROJ_LIB"] = "/fs/hnas1-evs1/Ddfo/dfo_odis/jeh326/miniconda/share/proj/" 

import datetime
import glob

import xarray as xr
import numpy as np
import pandas as pd
#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt

#import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

import pickle
import argparse

#example run command
# ./examine_observed_drifters.py --data_dir /space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/DriftTool_Sample_Dataset/datasets/waterproperties_drifters/netcdf/ --year 2016 

parser = argparse.ArgumentParser()
parser.add_argument('--data_dir', 
                    type=str, 
                    help='path to observed drifter netcdfs',
                    )
parser.add_argument('--data_file',
                    type=str,
                    )
parser.add_argument('--from_pickle',
                    type=str,
                    )
parser.add_argument('--year',
                    type=int,
                    )
parser.add_argument('--desc',
                    type=str,
                    )
args = (parser.parse_args())
data_dir=args.data_dir
data_file=args.data_file
useyear=args.year

if args.desc is not None:
    picklename = args.desc
else:
    picklename = 'reprocessed-wp'


savedir = '/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/plot_obs_data/'
#picklename = 'pacific-2016'
#picklename = 'water-properties-drifters-2016'
#picklename = 'water-properties-drifters-all'

dflist = []
dflist_picklename = 'dflist_' + picklename + '.pkl'
driftertrack_picklename = 'pickled_driftertracks_' + picklename + '.pkl'
pickle_savedir = savedir + '/pickles/'

if args.data_file is not None:
    globarg = args.data_file
else:
    globarg = '.nc'


if args.from_pickle is not None:
    load_from_pickle=os.path.join(pickle_savedir,args.from_pickle)
    drifter_tracks_df = pd.read_pickle(load_from_pickle)
    with open(os.path.join(pickle_savedir,dflist_picklename), 'rb') as f:
        dflist = pickle.load(f)

else:
    for dirpath, dirnames, filenames in os.walk(data_dir, followlinks=True):
            for filename in filenames:
                if not filename.endswith(globarg):
                #if not filename.endswith('0_1.nc'):
                    continue
                print(filename) 
                drifter_filename = os.path.join(dirpath, filename)
                
                if not os.path.isabs(drifter_filename):
                    # Determine path of drifter file relative to directory of
                    # output file.
                    odir = os.path.dirname(os.path.abspath(output_file))
                    drifter_filename = os.path.relpath(drifter_filename, odir)

                ds = xr.open_dataset(drifter_filename)
                #df = pd.DataFrame()
                #df['TIME'] = ds.TIME.to_series().reset_index(drop=True)
                #df['LATITUDE'] = ds.LATITUDE.to_series().reset_index(drop=True)
                #df['LONGITUDE'] = ds.LONGITUDE.to_series().reset_index(drop=True)

                #df = ds.to_dataframe()

                '''
                if 'obs_lat' in df.keys():
                    df.rename(inplace=True, columns={'obs_lat':'latitude','obs_lon':'longitude'})
                if 'lat' in df.keys():
                    print('test')
                    df.rename(inplace=True, columns={'lat':'latitude','lon':'longitude'})
                if 'Latitude' in df.keys():
                    df.rename(inplace=True, columns={'Latitude':'latitude', 'Longitude':'longitude'})
                if 'LATITUDE' in df.keys():
                    df.rename(inplace=True, columns={'LATITUDE':'latitude', 'LONGITUDE':'longitude'})
                if 'LAT' in df.keys():
                    df.rename(inplace=True, columns={'LAT':'latitude', 'LON':'longitude'})

                if 'TIME' in df.keys():
                    df.rename(inplace=True, columns={'TIME':'time'})
                '''

                if 'obs_lat' in ds.keys():
                    ds.rename(inplace=True, columns={'obs_lat':'latitude','obs_lon':'longitude'})
                if 'lat' in ds.keys():
                    #ds.rename_vars(inplace=True, columns={'lat':'latitude','lon':'longitude'})
                    ds['latitude'] = ds['lat']
                    ds['longitude'] = ds['lon']
                if 'Latitude' in ds.keys():
                    ds.rename(inplace=True, columns={'Latitude':'latitude', 'Longitude':'longitude'})
                if 'LATITUDE' in ds.keys():
                    #ds.rename(inplace=True, columns={'LATITUDE':'latitude', 'LONGITUDE':'longitude'})
                    ds['latitude'] = ds['LATITUDE']
                    ds['longitude'] = ds['LONGITUDE']

                if 'LAT' in ds.keys():
                    ds.rename(inplace=True, columns={'LAT':'latitude', 'LON':'longitude'})
                if 'TIME' in ds.keys():
                    #ds.rename(inplace=True, columns={'TIME':'time'})
                    ds['time'] = ds['TIME']


                df = pd.DataFrame()

                df['time'] = ds.time.to_series().reset_index(drop=True)
                df['latitude'] = ds.latitude.to_series().reset_index(drop=True)
                df['longitude'] = ds.longitude.to_series().reset_index(drop=True)


                df['filename'] = filename
                df['full_filename'] = drifter_filename

                if 'approximate_drogue_depth' not in ds.attrs:
                    df['depth'] = 0
                    print('....Warning! added dummy drogue depth of 0')
                else:
                    df['depth'] = ds.approximate_drogue_depth

                '''

                if 'obs_lat' in df.keys():
                    df.rename(inplace=True, columns={'obs_lat':'latitude','obs_lon':'longitude'})
                if 'lat' in df.keys():
                    df.rename(inplace=True, columns={'lat':'latitude','lon':'longitude'})
                if 'Latitude' in df.keys():
                    df.rename(inplace=True, columns={'Latitude':'latitude', 'Longitude':'longitude'})
                if 'LATITUDE' in df.keys():
                    df.rename(inplace=True, columns={'LATITUDE':'latitude', 'LONGITUDE':'longitude'})
                if 'LAT' in df.keys():
                    df.rename(inplace=True, columns={'LAT':'latitude', 'LON':'longitude'})
        
                if 'TIME' in df.keys():
                    df.rename(inplace=True, columns={'TIME':'time'})
                '''

                #df['latitude'] = df['latitude'].apply(lambda x: [y if y <= 900 else np.nan for y in x])
                #df['longitude'] = df['longitude'].apply(lambda x: [y if y <= 900 else np.nan for y in x])

                df['latitude'].values[df['latitude'] > 900] = np.nan
                df['longitude'].values[df['longitude'] > 900] = np.nan

                df['latitude'].values[df['latitude'] < -900] = np.nan
                df['longitude'].values[df['longitude'] < -900] = np.nan

                #if latmean < -90:
                #    df['latitude'] = df['latitude'] + 180

                latmean = df['latitude'].mean()
                latstd = df['latitude'].std()
                lonmean = df['longitude'].mean()
                lonstd = df['longitude'].std()

                #if latmean < -90:
                #    df['latitude'] = df['latitude'] + 180

                #df = (df.loc[(df['longitude']>lonmean-1*lonstd) & (df['longitude']<lonmean+1*lonstd)])
                #df = (df.loc[(df['latitude']>latmean-1*latstd) & (df['latitude']<latmean+1*latstd)])

                if args.year is not None:
                    df = df[df['time'].dt.year==useyear]
                
                if not df.empty:

                    if len(df['time']) > 4:

                        dflist.append(df)
                        if df['latitude'].max() > 900:
                            print(df['latitude'].max(),df['longitude'].max())
                            exit()
                        if df['longitude'].max() > 900:
                            print(df['latitude'].max(),df['longitude'].max())
                            exit()
                        print('....adding data to plotting dataframe')

                #to display number of points per month
                if df.empty:
                    continue
                else:
                    pass
                    #for i in range(1,12):
                        #print(str(i),len(df[df.index.month==i]))
                        

    #concatonate the dataframes and pickle the entire set
    drifter_tracks_df = pd.concat(dflist)
    drifter_tracks_df.to_pickle(os.path.join(pickle_savedir,driftertrack_picklename))
    #pickle dflist too
    with open(os.path.join(pickle_savedir,dflist_picklename), 'wb') as f:
        pickle.dump(dflist, f)


#################################################################################
#start the plotting here
#################################################################################

print()
print('plotting map')


minlons = min(drifter_tracks_df['longitude']) - 2
maxlons = max(drifter_tracks_df['longitude']) + 2
minlats = min(drifter_tracks_df['latitude']) - 2
maxlats = max(drifter_tracks_df['latitude']) + 2



#ciopsw
#minlons = -142.28317260742188 -1 
#minlats = 44.3333854675293  -1
#maxlons = -120.56839752197266 +1
#maxlats = 59.62149429321289 +1


#minlons = -80
#minlats = 33
#maxlons = -15
#maxlats = 60

#ciops
#minlons=-78.18400573730469 
#maxlons=-37.71416091918945
#minlats=34.87752914428711 
#maxlats=54.3026237487793



#add the track to the map
plt.figure(1)

#set up the basemap
map = Basemap(projection='merc', lat_0 = 57, lon_0 = -135, lat_ts = 57, resolution = 'l', area_thresh = 0.1, llcrnrlon=minlons, llcrnrlat=minlats, urcrnrlon=maxlons, urcrnrlat=maxlats)
#map.etopo()
#map.bluemarble()
#map.drawcoastlines()
map.fillcontinents(color = 'gray')
#map.drawmapboundary()

#import the ciopse domain pickle
import pickle
load_from_pickle='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/boundary-handling/domains/ciopsw_domain.pickle'
with open(load_from_pickle, 'rb') as f:
    domainspecs = pickle.load(f)

#add the boundary
x_left,y_left = map(domainspecs['left_lon'], domainspecs['left_lat'])
map.plot(x_left, y_left, linewidth=0.5, color='r')
x_right,y_right = map(domainspecs['right_lon'], domainspecs['right_lat'])
#map.plot(x_right, y_right, 'o', markersize=1, color='r')
map.plot(x_right, y_right, linewidth=0.5,  color='r')
x_top,y_top = map(domainspecs['top_lon'], domainspecs['top_lat'])
map.plot(x_top, y_top, linewidth=0.5,  color='r')
x_bottom,y_bottom = map(domainspecs['bottom_lon'], domainspecs['bottom_lat'])
map.plot(x_bottom, y_bottom, linewidth=0.5,  color='r')

map.fillcontinents(color = 'gray')


# draw parallels and meridians (label parallels on right and top, meridians on bottom and left)
parallels = np.arange(0.,81,5.)
# labels = [left,right,top,bottom]
map.drawparallels(parallels,labels=[False,True,True,False])
meridians = np.arange(10.,351.,5.)
map.drawmeridians(meridians,labels=[True,False,False,True])

'''
#define the polygon zones
Juan_de_Fuca = np.array([[48.4,-123.6],[48.62,-124.7],[48.37,-124.7],[48.14,-124.],[48.1,-123.5],[48.4,-123.6]])
Puget_Sound = np.array([[48.4,-123.6],[48.1,-123.5],[48.,-122.8],[48.25,-122.72],[48.38,-122.63],[48.47,-123.],[48.42,-123.32],[48.48,-123.46],[48.4,-123.6]])
Gulf_Islands = np.array([[48.47,-123.],[48.38,-122.63],[48.5,-122.25],[48.85,-122.6],[48.7,-122.9],[48.965,-123.53],[49.18,-123.8],[49.2,-124.],[48.4,-123.6],[48.48,-123.46],[48.67,-123.42],[48.67,-123.15],[48.47,-123.]])
Haro_Strait = np.array([[48.47,-123.],[48.67,-123.15],[48.67,-123.42],[48.48,-123.46],[48.42,-123.32],[48.47,-123.]])
SOG_north = np.array([[49.34,-123.38],[49.41,-123.55],[50.,-124.],[50.2,-125.],[50.,-125.4],[49.4,-124.8],[49.2,-124.],[49.18,-123.8],[49.34,-123.38]])
SOG_south = np.array([[48.7,-122.9],[48.85,-122.6],[49.1,-122.85],[49.3,-123.],[49.25,-123.3],[49.34,-123.38],[49.18,-123.8],[48.965,-123.53],[48.7,-122.9]])
Van_Harbour = np.array([[49.25,-123.3],[49.34,-123.38],[49.35,-123.25],[49.3,-123.],[49.25,-123.3]])
Howe_Sound = np.array([[49.34,-123.38],[49.41,-123.55],[49.53,-123.55],[49.71,-123.15],[49.5,-123.1],[49.35,-123.25],[49.34,-123.38]])
WCVI_Shelf = np.array([[48.37,-124.7],[48.62,-124.7],[49.,-125.],[49.3,-126.],[50.9,-128.75],[50.9,-129.5],[50.5,-129.5],[48.,-125.7],[46.2,-125.],[46.2,-124.],[48.37,-124.7]])

#plot the polygon zones
for polyname in [Juan_de_Fuca, Puget_Sound, Gulf_Islands, Haro_Strait, SOG_north, SOG_south, Van_Harbour, Howe_Sound, WCVI_Shelf]:
    map.plot(polyname[:,1], polyname[:,0], latlon=True, color='k', linewidth=0.35)
'''

x0 = []
y0 = []

#for each drifter in the list of dataframes:
print("adding individual plots")
pcount = []
for plotdf in dflist:

    try:
        ddepth = float(np.unique(plotdf['depth'])[0])
    except:
        print("Depth not a float, setting to 0")
        ddepth = 0.

    fname = str(np.unique(plotdf['filename'])[0])
    pcount.append(fname) 

    #plotdf = plotdf.resample('360T').mean()
    
    #Convert the lat and lon to projection co-ordinates and plot
    lats = plotdf['latitude'].tolist()
    lons = plotdf['longitude'].tolist()
    x,y = map(lons, lats)
    #map.scatter(x, y, 10, marker='o', color='b')

    #print(fname,fname[:2],ddepth)

    ddepth = abs(ddepth)

    colorlookupdict = {'na':'r','no':'b','cg':'g'}

    ffname = fname[:2]
   
    if ffname == 'cg':
        ddepth = 15

    if ddepth <= 5:
        colo = 'r'
    elif ddepth <=10 and ddepth >5:
        colo = 'b'
    elif ddepth <= 15 and ddepth >10:
        colo = 'g'
    else:
        colo = 'y'

    #d<=5 (r), 5<d<=10 (b), 10<d<=15 (g), d>15 (y)
        
 
    '''
    if ffname == 'na':
        continue
    elif ffname == 'cg':
        continue
    else:
        map.plot(x, y,color=colorlookupdict[ffname])
        #print(plotdf)
    '''

    #map.plot(x, y,color=colo)


    ####map.plot(x, y, color='k',linewidth=0.5) 
    #map.plot(x[0], y[0], 'r.',markersize=1)
    #print(x[0],y[0])
    x0.append(x[0])
    y0.append(y[0]) 
    #map.plot(x, y, '.',color='k',markersize=0.5) 
    map.plot(x,y,'k.',markersize=0.1)

    #map.plot(x,y,linewidth=0.7)
    #map.plot(x,y,linewidth=0.5)
    #plt.show()


map.plot(x0, y0, 'r.',markersize=1)

#a few general plot extras
#plt.title("2016 drifters (NAFC, NOAA Student Drifters, CG)\nd<=5 (r), 5<d<=10 (b), 10<d<=15 (g), d>15 (y)")
plt.title(picklename)
#plt.show()

sname = 'driftertracks_' + picklename 
savenamepdf = sname + '.pdf'
savenamepng = sname + '.png'
savenamejpg = sname + '.jpg'
#plt.savefig(os.path.join(savedir,'scttest.jpg'), dpi=600)
#plt.savefig(os.path.join(savedir,'scttest.png'),bbox_inches='tight',dpi=300, transparent=True)
plt.savefig(os.path.join(savedir,savenamepdf),bbox_inches='tight',dpi=300, transparent=True)
plt.savefig(os.path.join(savedir,savenamejpg),bbox_inches='tight',dpi=300)

print('plotted ',len(pcount), 'total drifters')
#for pc in range(0,len(pcount)):
#    print(pcount[pc])
