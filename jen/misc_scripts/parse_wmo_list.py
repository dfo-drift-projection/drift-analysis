#!/usr/bin/env python

########################################################################################
#create_daily_cmems_files.py - This script takes patricia's daily file and converts it
#into a daily version in the drift-tool format (one file per drifter per day).
#The datadir is passed in as a command line arguement and the data is saved to
#/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/cmems_dataset/daily_cmems_files
#(this should be updated in the future to a directory that isn't in my user account)
########################################################################################

import os
import glob
import shutil
import xarray as xr
import numpy as np
import pandas as pd
import datetime as dt
import wget
import geopy
import geopy.distance
import argparse

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

wmofile='/fs/vnas_Hdfo/odis/jeh326/drifters/drift-analysis/pre-process/drifters/cmems/wmo_list.txt'

#['WMO', 'TELECOM ID', 'TELECOM SYSTEM', 'PTFM NAME', 'PTFM FAMILY', 'PTFM TYPE', 'CONTACT NAME', 'EMAIL', 'PROGRAM', 'ROLE', 'AGENCY', 'COUNTRY', 'ALLOC_DATE', 'DEALLOC_DATE', 'AR  GOS_PROG']

#read in the WMO table
dbcpop = pd.read_csv(wmofile, skiprows=1, delimiter=";")

print(dbcpop.keys())
print()
print(dbcpop[['WMO', 'TELECOM ID', 'PTFM NAME', 'PTFM FAMILY', 'PTFM TYPE']].head())
print()
#print('number of wmo numbers = ',len(np.unique(dbcpop.WMO)))
print()
#print('unique PTFM NAME = ', dbcpop['PTFM NAME'].unique())
print()
#print('unique PTFM FAMILY = ', dbcpop['PTFM FAMILY'].unique())
print()
#print('unique PTFM TYPE = ', dbcpop['PTFM TYPE'].unique())

print()
print()

SVPdf = dbcpop.loc[dbcpop['PTFM TYPE'] == 'SVP']
print(SVPdf[['WMO', 'TELECOM ID', 'PTFM NAME', 'PTFM FAMILY', 'PTFM TYPE']])

exit()

print()
print('unique PTFM NAME = ', SVPdf['PTFM NAME'].unique())
print()
print('unique PTFM TYPE = ', SVPdf['PTFM TYPE'].unique())



'''
#find the matching buoyid from the wmo list
extra_meta = dbcpop.loc[dbcpop['WMO'] == float(ds.attrs['wmo_platform_code'])]

#if there's no entries in the wmo list, add 'unknown' as the wmo buoy type
if extra_meta.empty == True:
    ds.attrs['wmo_type'] = 'unknown'
else:
    ds.attrs['wmo_type'] = extra_meta.iloc[0]['PTFM TYPE']
'''
