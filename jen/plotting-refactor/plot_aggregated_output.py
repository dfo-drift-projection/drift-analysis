
import os
import glob
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import argparse
import sys
import yaml

#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

#import driftutils.aggregated_plotting_functions as aggplots
import aggregated_plotting_functions as aggplots
import mapped_skillscores_functions as mss

'''
def load_yaml_config(config_file):
    with open(config_file, "r") as ymlfile:
        try:
            return yaml.safe_load(ymlfile)
        except yaml.YAMLError as exc:
            print(exc)
'''

def main():

    #load any default parameters for plotting
    cfg = aggplots.load_yaml_config("aggregated_plotting_configuration.yaml")
    skill_label_params = cfg["skill_label_params"]
    lcolors = cfg["lcolors"]

    #load the user defined configuation file
    usercfg = aggplots.load_yaml_config("user_aggregated_plotting_config.yaml")
    datadir = usercfg['datadir']
    rawsavedir = usercfg['rawsavedir']
    etopo_file = usercfg['etopo_file']
    duration = usercfg['duration']
    skills_list = usercfg['skills_list']
    dataset_id = datadir.split('/')[-1]
    if dataset_id == '':
        dataset_id = datadir.split('/')[-2]

    #read which plots to create from the user defined yaml file
    plot_selection = usercfg['plot_selection']
    #add default False for any plot options that aren't chosen

    plot_options_list = [
        'plot_track_indiv', 'plot_drifter_skillscores', 'plot_average_of_mean_skills',
        'plot_skills_subplots_with_filled', 'plot_skills_subplots_withindv', 'plot_all_drifter_tracks',
        'plot_all_hexbin_skillscores', 'plot_all_drifter_skillscores', 'plot_skills_plus_map',
        'plot_track_persistance', 'plot_ratio_combined', 'plot_ratio_and_track']

    '''
    plot_options_list = [
        'plot_track_indiv', 'plot_drifter_skillscores', 'plot_average_of_mean_skills',
        'plot_skills_subplots_with_filled', 'plot_skills_subplots_withindv', 'plot_all_drifter_tracks',
        'plot_all_hexbin_skillscores', 'plot_all_drifter_skillscores', 'plot_skills_plus_map',
        'plot_track_persistance', 'plot_ratio_combined', 'plot_ratio_and_track','plot_skills_subplots_withindv',
        'plot_skills_subplots_with_filled', 'plot_subtracted_skillscores', 'plot_indv_subtracted_skillscores',
        'plot_subtracted_hexbins']
    '''

    for val in plot_options_list:
        if val not in plot_selection.keys():
            plot_selection[val] = False

    filled_calculation_method = usercfg['filled_calculation_method']
    if filled_calculation_method is None:
        filled_range_type = 'IQR'
    else:
        filled_range_type = filled_calculation_method  # filled_calculation_method.split(',')

    label_list = aggplots.get_label_list(datadir)

    #including a check that will exit if the drift-tool output folder is not present
    #this could be due to an error or inadequate ocean/drifter data to run the tool.
    if os.path.isdir(datadir) == False:
        sys.exit('No DriftEval output exists so no further processing required')

    #############################################################################
    # make sure the savedir exists. Prep for making subfolders below if
    # user chooses to make plots for individual drifters
    #############################################################################

    savedir = os.path.join(rawsavedir, dataset_id)
    if os.path.isdir(savedir) is False:
        os.makedirs(savedir)

    '''
    def setup_savedir(fname):
        subdir = os.path.join(savedir,str(fname))
        if os.path.isdir(subdir) is False:
            os.makedirs(subdir)
        return subdir
    '''

    #############################################################################
    # make a list of the buoyids from nc files in the datadir
    #############################################################################

    '''
    def grab_buoyids(datadir):
        """ make a list of unique ids  """
        iname = []
        for fpath in glob.glob(os.path.join(datadir,'*.nc')):
            fname = os.path.basename(fpath)
            iname.append(fname.split('_')[-2])
        return np.unique(iname)
    '''

    buoyids = aggplots.grab_buoyids(datadir)

    #############################################################################
    # Plot the individual drifter tracks
    #############################################################################
    if plot_selection['plot_track_indiv'] is True:
        tracks_savedir = aggplots.setup_savedir(savedir,'tracks_per_drifter')
        print('assembling drifter data set')
        all_drifter_tracks_df = mss.create_tracks_dataframe(datadir)
        print("Plotting individual tracks per drifter")
        for drifterid in buoyids: #['P2D-wp3772515323D20160526']: #buoyids:
            print(drifterid)
            mss.plot_all_tracks(
                all_drifter_tracks_df,
                savedir=tracks_savedir,
                startdate=None, enddate=None,
                obs_tracks=True, mod_tracks=True,
                start_dot=True, mod_start_dots=True,
                landmask_type='SalishSea', #None,
                etopo_file=etopo_file,
                ax=None,
                par_labels=None, mer_labels=None,
                map_extremes=None,
                obs_color=None, mod_color=None,
                dot_color=None, mod_dot_color='orange',
                map_projection='lambert',
                drifter_prefix=drifterid
                )

    #############################################################################
    # Plot all drifter tracks in set
    #############################################################################
    if plot_selection['plot_all_drifter_tracks'] is True:
        print('assembling drifter data set')
        all_drifter_tracks_df = mss.create_tracks_dataframe(datadir)
        mss.plot_all_tracks(
            all_drifter_tracks_df,
            savedir=savedir,
            startdate=None, enddate=None,
            obs_tracks=True, mod_tracks=True,
            start_dot=True, mod_start_dots=False,
            landmask_type='SalishSea', #None,
            etopo_file=etopo_file,
            ax=None,
            par_labels=None, mer_labels=None,
            map_extremes=None,
            obs_color=None, mod_color=None,
            dot_color=None, mod_dot_color=None,
            map_projection='lambert',
            )

    #############################################################################
    # Plot the individual skills plus drifter tracks
    #############################################################################
    if plot_selection['plot_skills_plus_map'] is True:
        QC_savedir = aggplots.setup_savedir(savedir,'QC_plots')
        all_drifter_tracks_df = mss.create_tracks_dataframe(datadir)

        for drifterid in buoyids: #['P2D-wp3122542700D20160524','P2D-wp4372522492D20160617','P2D-wp4362522943D20160617']: # buoyids:
            print(drifterid)
            fig, ax = plt.subplots(2, 2, squeeze=False, figsize=(11, 8.5))

            drifterds = aggplots.drifter2datasets(datadir, drifterid)
            for modelrun in drifterds.keys():
                aggplots.plot_skills(drifterid, drifterds[modelrun], 'sep', 'm', skill_label_params, 1, True, False, ax=ax[0][0])
                aggplots.plot_skills(drifterid, drifterds[modelrun], 'molcard', 'r', skill_label_params, 1, True, False, ax=ax[1][0])
                aggplots.plot_skills(drifterid, drifterds[modelrun], 'liu', 'b', skill_label_params, 1, True, False, ax=ax[0][1])

            all_drifter_tracks_df = mss.create_tracks_dataframe(datadir)
            mss.plot_all_tracks(
                all_drifter_tracks_df, savedir=None,
                startdate=None, enddate=None,
                obs_tracks=True, mod_tracks=True,
                start_dot=True, mod_start_dots=True,
                landmask_type=None, #'SalishSea'
                etopo_file=None, #etopo_file,
                ax=ax[1][1],
                par_labels=None, mer_labels=None,
                map_extremes=None,
                lambert_aspect_ratio_value=0.3, # subplot width/height
                obs_color=None, mod_color=None,
                dot_color=None, mod_dot_color=None,
                show_legend=False,
                map_projection='lambert', #must be lambert to create a square plot
                drifter_prefix=drifterid
                )

            fig.suptitle(str(drifterid), fontsize=14)
            fig.suptitle(str(drifterid), fontsize=14)
            multiskillmapfignamepng = "{}_skills_plus_map.png".format(str(drifterid))
            plt.savefig(os.path.join(QC_savedir, multiskillmapfignamepng), bbox_inches='tight', dpi=300)
            print('saving', os.path.join(QC_savedir, multiskillmapfignamepng))
            plt.close()

    #############################################################################
    # Plot all hexbin plots
    #############################################################################
    if plot_selection['plot_all_hexbin_skillscores'] is True:
        df_all = mss.create_master_dataframe(datadir)
        # hexbin plots
        grid_spacing = 80  # I think this can also be ['100','100'], etc
        edge_color = 'black'  # can be a color like 'black' or 'face' for no color
        opacity_level = 1  # 0.8
        #skills_list = ['sep','molcard']
        leadt_list = [duration]
        for skill in skills_list:
            for leadt in leadt_list:
                print('\n plotting hexbins')
                mss.plot_hexbins(
                    skill, leadt, df_all, savedir,
                    grid_spacing, edge_color, opacity_level,
                    startdate=None, enddate=None,
                    landmask_type=None, ax=None,
                    par_labels=None, mer_labels=None,
                    map_extremes=None, map_projection='lambert')

    #############################################################################
    # plot skill scores on subplots
    #############################################################################

    if plot_selection['plot_drifter_skillscores'] is True:
        skills_savedir = aggplots.setup_savedir(savedir,'skills_per_drifter')
        #for each common id, plot the skill scores together
        print("Plotting skill score subplots for each unique drifter")
        for drifterid in buoyids:
            print(drifterid)
            dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids=[drifterid])
            plotcolors = aggplots.assign_colors(dataset_labels)
            drifterds = datasets_dict
            fig, ax = plt.subplots(3, 1, squeeze=False, figsize=(8.5, 11), sharex='all')
            for setname in dataset_labels:
                subdrifterds = drifterds[setname][0]
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'sep', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[0][0])
                handles, labels = ax[0][0].get_legend_handles_labels()
                newlabels = list(label_list)
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'molcard', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[1][0])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'liu', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[2][0])
            #ax[0][0].legend(handles, newlabels, loc='center left', bbox_to_anchor=(1, 0.5), fontsize=10)
            ax[0][0].legend(handles, newlabels, loc='best', fontsize=10)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
            fig.suptitle(str(drifterid), fontsize=14)
            ax[0][0].set_xlabel('')
            ax[1][0].set_xlabel('')
            plt.subplots_adjust(wspace=0, hspace=0.05)
            multiskillfignamepng = "{}_skills.png".format(str(drifterid))
            plt.savefig(os.path.join(skills_savedir, multiskillfignamepng), bbox_inches='tight', dpi=300)
            plt.close()


    #############################################################################
    # plot ALL skill scores on subplots
    #############################################################################
    if plot_selection['plot_all_drifter_skillscores'] is True:
        all_skills_savedir = aggplots.setup_savedir(savedir,'all_skills_per_drifter')
        #for each common id, plot the skill scores together
        print("Plotting all skill score subplots for each unique drifter")

        for drifterid in buoyids:
            print(drifterid)
            dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids=[drifterid])
            plotcolors = aggplots.assign_colors(dataset_labels)
            drifterds = datasets_dict
            fig, ax = plt.subplots(2, 3, squeeze=False, figsize=(11,8.5))
            for setname in dataset_labels:
                subdrifterds = drifterds[setname][0]
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'sep', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[0][0])
                handles, labels = ax[0][0].get_legend_handles_labels()
                newlabels = list(label_list)
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'molcard', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[1][0])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'liu', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[0][1])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'sutherland', plotcolors[setname], skill_label_params, 1,True, False, ax=ax[1][1])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'logobsratio', plotcolors[setname], skill_label_params, 1,True, False, ax=ax[0][2])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'logmodratio', plotcolors[setname], skill_label_params, 1,True, False, ax=ax[0][2])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'obsratio', plotcolors[setname], skill_label_params, 1,True, False, ax=ax[1][2])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'modratio', plotcolors[setname], skill_label_params, 1,True, False, ax=ax[1][2])

            ax[0][0].legend(handles, newlabels, loc='best', fontsize=10)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
            fig.suptitle(str(drifterid), fontsize=14)
            ax[0][0].set_xlabel('')
            ax[1][0].set_xlabel('')
            #plt.subplots_adjust(wspace=0, hspace=0.05)
            allmultiskillfignamepng = "{}_allskills.png".format(str(drifterid))
            plt.savefig(os.path.join(all_skills_savedir,allmultiskillfignamepng),bbox_inches='tight',dpi=300)
            print('saving', all_skills_savedir,allmultiskillfignamepng)
            plt.close()

    #############################################################################
    # plot average of means for each model
    #############################################################################

    if plot_selection['plot_average_of_mean_skills'] is True:
        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir)
        plotcolors = aggplots.assign_colors(dataset_labels, lcolors)
        for skill in skills_list:
            fig, ax = plt.subplots(figsize=(11, 8.5))
            for setname in datasets_dict.keys():
                all_mean, all_drifters_all_tracks = aggplots.bin_skills_all_drifters(datasets_dict[setname], skill)
                aggplots.plot_average_skill(all_mean, setname, savedir, skill, plotcolors[setname], skill_label_params, 1, leg=True, ax=ax)
            combinedskfignamepng = "aggregated_mean_{}.png".format(skill)
            plt.savefig(os.path.join(savedir, combinedskfignamepng), bbox_inches='tight', dpi=300, papertype='letter', orientation='portrait')
            plt.close()

    #############################################################################
    # plot average of means for each model WITH individual means
    #############################################################################
    if plot_selection['plot_skills_subplots_withindv'] is True:
        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir)
        plotcolors = aggplots.assign_colors(dataset_labels)
        for skill in skills_list: #'liu', 'sutherland', 'obsratio', 'modratio', 'logobsratio', 'logmodratio'
            aggplots.plot_skill_with_indv(buoyids, datadir, savedir, skill, duration, plotcolors[dataset_labels[0]], skill_label_params, 2, '1H',True, False, ax=None)
            exit()

    #############################################################################
    # plot average of means for each model WITH filled
    # IQR, 1std, extremes (min/max values)
    #############################################################################
    if plot_selection['plot_skills_subplots_with_filled'] is True:
        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids)
        plotcolors = aggplots.assign_colors(dataset_labels, lcolors)
        for skill in ['sep','molcard']: #'liu', 'sutherland', 'obsratio', 'modratio', 'logobsratio', 'logmodratio'
            print(skill)
            for fill_range_type in ['IQR','extremes','1std']:
                print(fill_range_type)
                aggplots.plot_skill_with_filled(datasets_dict, dataset_labels, savedir, skill, plotcolors,
                                                skill_label_params, 2, fill_range_type, lab=True, leg=True, ax=None)


'''
    ############################################################################
    # plot the ratio plots
    ############################################################################
    if plot_selection['plot_ratio_and_track'] is True:
        ratio_and_track_savedir = aggplots.setup_savedir(savedir,'ratio_and_track')
        print('assembling drifter data set')
        all_drifter_tracks_df = mss.create_tracks_dataframe(datadir)
        for drifterid in buoyids:
            print("......",str(drifterid))
            fig, ax = plt.subplots(1, 2, squeeze=False, figsize=(11,8.5))
            aggplots.plot_skills(drifterid, datadir, 'logobsratio', duration,'r', skill_label_params, 1, '1H', True, False, ax=ax[0][0])
            aggplots.plot_skills(drifterid, datadir, 'logmodratio', duration,'b', skill_label_params, 1, '1H', True, False, ax=ax[0][0])
            #aggplots.plot_aggregated_tracks(drifterid, datadir, etopo_file, minlon=None, maxlon=None, minlat=None, maxlat=None, domainname="data", tracktype=None, ax=ax[0][1])
            print(ratio_and_track_savedir)
            mss.plot_all_tracks(
                all_drifter_tracks_df,
                pathoutfig=ratio_and_track_savedir,
                startdate=None,
                enddate=None,
                obs_tracks=True,
                mod_tracks=True,
                start_dot=True,
                mod_start_dots=True,
                landmask_type=None,  # 'SalishSea', #None,
                etopo_file=None,  # etopo_file,
                ax=ax[0][1],
                par_labels=None,
                mer_labels=None,
                map_extremes=None,
                lambert_aspect_ratio_value=0.3,
                obs_color=None,
                mod_color=None,
                dot_color=None,
                mod_dot_color=None,
                show_legend=False,
                map_projection='lambert',
                drifter_prefix=drifterid
                )
            ratioandtrackfignamepng = "{}_ratioandtrack.png".format(str(drifterid))
            plt.savefig(os.path.join(ratio_and_track_savedir,ratioandtrackfignamepng),bbox_inches='tight',dpi=300)
            plt.close()

    #############################################################################
    # Plot obs ratio and mod ratio on the same axis
    #############################################################################
    if plot_selection['plot_ratio_combined'] is True:
        fig, ax = plt.subplots(figsize=(11,8.5))

        aggplots.plot_average_skill(buoyids, datadir, savedir, 'obsratio', duration, lcolors, skill_label_params, 2, '1H', leg=True, ax=ax)
        aggplots.plot_average_skill(buoyids, datadir, savedir, 'modratio', duration, lcolors, skill_label_params, 2, '1H', leg=True, ax=ax)

        #deplots.plot_combined_skill(commonids, datadirs, savedir, 'obsratio', lcolors, skill_label_params, 2, '1H', True, ax=ax)
        #deplots.plot_combined_skill(commonids, datadirs, savedir, 'modratio', lcolors, skill_label_params, 2, '1H', True, ax=ax)

        fig.tight_layout(rect=[0, 0.03, 1, 0.95])
        ratiofignamepng = "combined_ratios.png"
        plt.savefig(os.path.join(savedir,ratiofignamepng),bbox_inches='tight',dpi=300) 
        plt.close()
'''

if __name__ == '__main__':
    main()

