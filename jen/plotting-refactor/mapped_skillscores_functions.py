#--------------------------------------------------------------------------
# Author: Jennifer Holden
# Contributed to by Yukie Hata ECCC-CMC CMDE
#
#------------------------------------------------------------------------

#import os
#os.environ["PROJ_LIB"] = "/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/sitestore/miniconda/share/proj"

import matplotlib
matplotlib.use("Agg")
import os, sys
import xarray as xr
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap
import numpy as np
#from datetime import datetime
#from datetime import timedelta
import glob
import pandas as pd
import matplotlib.colors as colors
import cmocean
import math
import collections

import mpl_util

#some pd parameters
pd.set_option('display.max_rows', 5000)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import aggregated_plotting_functions as aggplots

import warnings
warnings.simplefilter(action='ignore', category=pd.errors.PerformanceWarning)

def grab_buoyids(datadir):

    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir,'*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[-2])
    return np.unique(iname)

'''
def create_master_dataframe(pathinroot):

    df_all = pd.DataFrame()
    pathin = pathinroot
    buoyids = grab_buoyids(pathin)
    print('there are ', len(buoyids), ' unique buoyids')
    mod_release_count = 0
    for buoyid in buoyids:
        fname = os.path.join(pathin, buoyid+'_aggregated.nc')
        with xr.open_dataset(fname) as ds:

            ds['time_since_start'] = ds['time'].astype(np.timedelta64)
            ds['initial_lat'] = ds['mod_lat'].astype(float)
            ds['initial_lon'] = ds['mod_lon'].astype(float)

            for m in ds.model_run.values:
                mod_release_count += 1
                ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64)
                ds['initial_lat'][m, :] = ds['mod_lat'][m, 0]
                ds['initial_lon'][m, :] = ds['mod_lon'][m, 0]

            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            df = ds[vars_to_keep].to_dataframe()
            df.insert(0,'buoyid', buoyid, True)
            df_all = pd.concat([df_all, df])

    print('there are ', mod_release_count, ' model runs over all the buoyids')
    #to round to the hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600))
    #to round to .1 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600, 1))
    #to round to .5 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 1800) / 2.0)
    #to round to the hour before:
    df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

    return df_all
'''

def create_master_dataframe(datadir, buoyids):
    #buoyids = grab_buoyids(datadir)
    print('there are ', len(buoyids), ' unique buoyids')
    mod_release_count = 0
    masterdict = {}
    for buoyid in buoyids:
        drifterdict = aggplots.drifter2datasets(datadir, buoyid)
        for setnameplace in range(0,len(drifterdict.keys())):
            s = list(drifterdict.keys())[setnameplace]

            #for each dataset for each model, feed into bin skill, df comes out. add to a list of dfs, then concat
            ds = drifterdict[s]

            ds['time_since_start'] = ds['time'].astype(np.timedelta64)
            ds['initial_lat'] = ds['mod_lat'].astype(float)
            ds['initial_lon'] = ds['mod_lon'].astype(float)

            for m in ds.model_run.values:
                mod_release_count += 1
                ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64)
                ds['initial_lat'][m, :] = ds['mod_lat'][m, 0]
                ds['initial_lon'][m, :] = ds['mod_lon'][m, 0]

            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            df = ds[vars_to_keep].to_dataframe()
            df.insert(0,'buoyid', buoyid, True)

            df['hours_since_start'] = df['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

            if s in masterdict.keys():
                masterdict[s].append(df)
            else:
                masterdict[s] = [df]

    # need result to be per model. combine all the lists of dataframes into one dataframe
    for key, value in masterdict.items():
        masterdict[key] = pd.concat(value)

    print('there are ', mod_release_count, ' model runs over all the buoyids')

    #to round to the hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600))
    #to round to .1 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600, 1))
    #to round to .5 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 1800) / 2.0)
    #to round to the hour before:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

    return masterdict #this should now be a list of dataframes

def create_tracks_dataframe(datadir, drifterid=None):
    df_all_tracks = pd.DataFrame()
    if drifterid is not None:
        buoyids = [drifterid]
    else:
        buoyids = grab_buoyids(datadir)
    mod_release_count = 0
    for buoyid in buoyids:
        fname = os.path.join(datadir, buoyid+'_aggregated.nc')
        with xr.open_dataset(fname) as ds:

            ds['initial_lat'] = ds['mod_lat'].astype(float)
            ds['initial_lon'] = ds['mod_lon'].astype(float)
            for m in ds.model_run.values:
                mod_release_count += 1
                ds['initial_lat'][m, :] = ds['mod_lat'][m, 0]
                ds['initial_lon'][m, :] = ds['mod_lon'][m, 0]

            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            df = ds[vars_to_keep].to_dataframe()
            df.insert(0,'buoyid', buoyid, True)
            df_all_tracks = pd.concat([df_all_tracks, df])

    return df_all_tracks

#######################################################################################
def grab_comparison_buoyids(datadir):

    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir,'*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[0])
    return np.unique(iname)

def create_comparison_master_dataframe(pathinroot):

    df_all = [] #pd.DataFrame()
    df = []
    pathin = pathinroot
    print(pathin)
    buoyids = grab_buoyids(pathin)
    print('there are ', len(buoyids), ' unique buoyids')
    mod_release_count = 0
    for buoyid in buoyids:
        print(buoyid)
        fname = os.path.join(pathin, buoyid+'*.nc')
        with xr.open_dataset(fname) as ds:

            for d in ds:
                ds[d]['time_since_start'] = ds[d]['time'].astype(np.timedelta64)
                ds[d]['initial_lat'] = ds[d]['mod_lat'].astype(float)
                ds[d]['initial_lon'] = ds[d]['mod_lon'].astype(float)

                for m in ds.model_run.values:
                    mod_release_count += 1
                    ds[d]['time_since_start'][m, :] = ds[d]['time'][m, :] - ds[d]['mod_start_date'][m].astype(str).astype(np.datetime64)
                    ds[d]['initial_lat'][m, :] = ds[d]['mod_lat'][m, 0]
                    ds[d]['initial_lon'][m, :] = ds[d]['mod_lon'][m, 0]

                vars_to_keep = [var for var in ds[d].data_vars if 'DEPTH' not in ds[d][var].dims]
                df[d] = ds[d][vars_to_keep].to_dataframe()
                df[d].insert(0,'buoyid', buoyid, True)
                df_all[d] = pd.concat([df_all[d], df[d]])

    print('there are ', mod_release_count, ' model runs over all the buoyids')
    #to round to the hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600))
    #to round to .1 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 3600, 1))
    #to round to .5 of an hour:
    #df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.round(x.total_seconds() / 1800) / 2.0)
    #to round to the hour before:
    df_all['hours_since_start'] = df_all['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))

    return df_all

def create_comparison_tracks_dataframe(datadir, drifterid=None):

    if drifterid is not None:
        buoyids = [drifterid]
    else:
        buoyids = grab_comparison_buoyids(datadir)
    mod_release_count = 0

    buoydict = {}
    testdict = {}
    print('converting the data to a list of dataframes')
    for buoyid in buoyids:
        #this assumes that there's only one file with that filename.
        for fname in glob.glob(os.path.join(datadir, buoyid+'*.nc')):
            with xr.open_dataset(fname) as ds:
                vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
                for setnameplace in range(0, len(ds.setname.values)):
                    s = ds.setname.values[setnameplace]
                    df = ds[vars_to_keep].sel(setname=s).to_dataframe()
                    df.insert(0, 'buoyid', buoyid, True)
                    #add the data frame to a list of dataframes
                    if s in buoydict.keys():
                        buoydict[s].append(df)
                    else:
                        buoydict[s] = [df]
    #combine all the lists of dataframes into one dataframe so that the output
    for key, value in buoydict.items():
        buoydict[key] = pd.concat(value)

    print('adding some extra columns to dataframes')
    #add a few extra variables
    alltracksdict = {}
    for b in buoydict.keys():
        #for each of the datasets
        indvdf = buoydict[b]
        tracksdf = pd.DataFrame()
        for buoyid in buoyids:
            initial_lat_list = []
            initial_lon_list = []
            tempdf = indvdf[indvdf['buoyid'] == buoyid]
            for m in np.unique(tempdf.index.get_level_values('model_run').values):
                d = tempdf[tempdf.index.get_level_values('model_run') == m]
                mod_release_count += 1
                timestep_length = len(d.index.get_level_values('timestep').values)
                starting_lat_value = d['mod_lat'][(d.index.get_level_values('model_run') == m) & (
                        d.index.get_level_values('timestep') == 0)].values
                starting_lon_value = d['mod_lon'][(d.index.get_level_values('model_run') == m) & (
                        d.index.get_level_values('timestep') == 0)].values
                initial_lat_list.extend([starting_lat_value[0]]*timestep_length)
                initial_lon_list.extend([starting_lon_value[0]]*timestep_length)
            tempdf.insert(0, 'initial_lat', initial_lat_list, True)
            tempdf.insert(0, 'initial_lon', initial_lon_list, True)
            tracksdf = pd.concat([tracksdf, tempdf])

        alltracksdict[b] = tracksdf

    return alltracksdict
#######################################################################################

def setup_map(
        crnr=None, resolution='c', lat_coordinate=None, lon_coordinate=None,
        ax=None, par_labels=None, mer_labels=None, map_extremes=None,
        lambert_aspect_ratio_value=None):
    #set the default projection to 'data'
    if crnr is None:
        crnr = 'data'

    #set a label to use when saving the plot
    if crnr == 'data':
        mapstr = ''
    else:
        mapstr = '_' + crnr

    def circular_mean(lon_coordinate):
        sinlons = []
        coslons = []
        for value in lon_coordinate:
            sinlons.append(math.sin(math.radians(value)))
            coslons.append(math.cos(math.radians(value)))
        atan2lons = math.atan2(np.nansum(sinlons), np.nansum(coslons))
        circular_mean_lons = math.degrees(atan2lons)

        return circular_mean_lons

    if crnr == 'ciopsw':
        minlons = -142.28317260742188 
        minlats = 44.3333854675293 
        maxlons = -120.56839752197266 
        maxlats = 59.62149429321289 
        crnr = 'data'
    else:
        if map_extremes is None:
            milon = np.nanmin(lon_coordinate)
            malon = np.nanmax(lon_coordinate)
            milat = np.nanmin(lat_coordinate)
            malat = np.nanmax(lat_coordinate)
            '''
            print(
                'circmean', circular_mean(lon_coordinate), '\n',
                'nanmean', np.nanmean(lon_coordinate), '\n',
                'nanmedian', np.nanmedian(lon_coordinate), '\n',
                'exmean', (milon+malon)/2
                )
            '''

            if (crnr == 'data') or (crnr == 'nps') or (crnr == 'lambert'):
                latbuffer = (malat-milat)*0.05
                minlats = milat - latbuffer
                maxlats = malat + latbuffer
                lonbuffer = (malon-milon)*0.05
                minlons = milon - lonbuffer
                maxlons = malon + lonbuffer
            else:
                minlons = milon
                maxlons = malon
                minlats = milat
                maxlats = malat
        else:
            minlons = map_extremes[0]
            maxlons = map_extremes[1]
            minlats = map_extremes[2]
            maxlats = map_extremes[3]


    ###########################################################
    # various projection options
    # Robinson (robinson), North Polar Stereographic (nps),
    # Lambert (lambert), Albers (albers), Mercator (data)
    ###########################################################

    #robinson
    if crnr == 'robinson':
        bmap = Basemap(projection='robin', lon_0=-90, resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 0]
        parallel_label = [0, 0, 0, 0]
        bmap.drawparallels(np.arange(-90.,120.,30.), dashes=[1,1], linewidth=0.2, labels=parallel_label)
        bmap.drawmeridians(np.arange(0.,360.,60.), dashes=[1,1], linewidth=0.2, labels=parallel_label)

    #north polar stereographic projection
    elif crnr == 'nps':
        if np.nanmax(lon_coordinate) - np.nanmin(lon_coordinate) > 90:
            midlon = circular_mean(lon_coordinate)
        else:
            midlon = (maxlons+minlons)/2
        #midlon = circular_mean(lon_coordinate) # (maxlons + minlons) / 2
        bmap = Basemap(projection='npstere',boundinglat=minlats,lon_0=midlon,resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]
        bmap.drawparallels(np.arange(-80.,81.,20.), dashes=[1,1], linewidth=0.2, labels=parallel_label)
        bmap.drawmeridians(np.arange(-180.,181.,20.), dashes=[1,1], linewidth=0.2, labels=meridian_label)

    #lambert projection
    elif crnr == 'lambert':

        if np.nanmax(lon_coordinate)-np.nanmin(lon_coordinate) > 90:
            midlon = circular_mean(lon_coordinate)
        else:
            midlon = (maxlons+minlons)/2
        midlat = (maxlats+minlats)/2
        import geopy.distance
        data_width = geopy.distance.distance((midlat, minlons),(midlat, midlon)).m * 2
        data_height = geopy.distance.distance((minlats, midlon), (maxlats, midlon)).m
        data_height = data_height * 1.00
        data_width = data_width * 1.00
        #if the data is too wide, we need to set the maxlat to 90 for plotting.
        if data_width > 999999:
            plotmaxlat = 89
        else:
            plotmaxlat = maxlats

        #force_aspect = True
        #if force_aspect:
        if lambert_aspect_ratio_value is not None:
            force_aspect_value = max(data_width, data_height)
            data_width = force_aspect_value + lambert_aspect_ratio_value*force_aspect_value
            data_height = force_aspect_value

        bmap = Basemap(
            width=data_width,
            height=data_height,
            resolution=resolution,
            area_thresh=0.1,
            projection='lcc',
            lat_1=minlats,
            lat_2=plotmaxlat,
            lat_0=midlat,
            lon_0=midlon,
            ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    #Albers projection
    elif crnr == 'albers':
        if np.nanmax(lon_coordinate) - np.nanmin(lon_coordinate) > 90:
            midlon = circular_mean(lon_coordinate)
        else:
            midlon = (maxlons+minlons)/2
        #midlon = circular_mean(lon_coordinate) # np.nanmean(lon_coordinate)
        midlat = np.nanmean(lat_coordinate)
        import geopy.distance
        data_width = (geopy.distance.distance((midlat, minlons),(midlat, midlon)).m)*2
        data_height = geopy.distance.distance((minlats, midlon), (maxlats, midlon)).m
        data_height = data_height * 1.1
        data_width = data_width * 1.1
        bmap = Basemap(width=data_width,height=data_height,
            resolution=resolution,projection='aea',
            lat_1=minlats,lat_2=maxlats,
            lat_0=midlat, lon_0=midlon, 
            ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 1]

    elif (crnr == 'ciopsw') or (crnr == 'riops'):
        pass

    #determine boundaries from the data
    elif crnr == 'data':
        bmap = Basemap(projection='merc',
            lat_ts = 57,
            area_thresh = 0.1,
            llcrnrlon=minlons, llcrnrlat=minlats,
            urcrnrlon=maxlons, urcrnrlat=maxlats,
            resolution=resolution, ax=ax)
        meridian_label = [0, 0, 0, 1]
        parallel_label = [1, 0, 0, 0]

    else:
        print('this is an unknown projection')

    if (crnr == 'robinson') or (crnr == 'nps'):
        pass
    else:
        if mer_labels is None:
            meridian_label =  meridian_label
        else:
            meridian_label = mer_labels
            
        if par_labels is None:
            parallel_label = parallel_label
        else:
            parallel_label = par_labels

        if maxlons-minlons < 6:
            merfmt = '%.1f'
            merrotamt = 0 #45
            rangestep = (maxlons-minlons)/4
            shiftstep = rangestep/2
            merrange = np.arange(minlons+shiftstep, maxlons+shiftstep, rangestep)
        else:
            merfmt = '%.0f'
            merrotamt = 0
            rangestep = (np.floor(maxlons)-np.ceil(minlons))/6
            shiftstep = rangestep/2 
            merrange = np.append(np.arange(np.ceil(minlons-shiftstep), np.floor(maxlons-shiftstep), rangestep), [np.floor(maxlons-shiftstep)])
            
        if maxlats-minlats < 6:
            parfmt = '%.1f'
            parrotamt = 90 
            rangestep = (maxlats-minlats)/4
            shiftstep = rangestep/2
            parrange = np.arange(minlats+shiftstep, maxlats+shiftstep, rangestep)
        else:
            parfmt = '%.0f'
            parrotamt = 90
            rangestep = (np.floor(maxlats)-np.ceil(minlats))/6
            shiftstep = rangestep/2
            parrange = np.append(np.arange(np.ceil(minlats-shiftstep), np.floor(maxlats-shiftstep), rangestep), [np.floor(maxlats-shiftstep)])

        bmap.drawmeridians(merrange, dashes=[1,1], linewidth=0.1, fontsize=8, fmt=merfmt, labels=meridian_label, rotation=merrotamt)
        bmap.drawparallels(parrange, dashes=[2,2], linewidth=0.1, fontsize=8, fmt=parfmt, labels=parallel_label, rotation=parrotamt)

    return bmap, mapstr


def plot_land_mask(bmap, landmask_type=None):

    landmask_directory = "C:/Users/Holdenje/DRIFT/sample_drift-tool_output/"
    land_color="gray" #or maybe "silver"?
    water_color="white"

    if landmask_type is None:
        print('..drawing land from default basemap landmask')
        # draw coastlines.
        bmap.drawcoastlines(linewidth=0.5)
        bmap.drawmapboundary(linewidth=0.5, fill_color=water_color)
        bmap.fillcontinents(color="silver",lake_color=water_color)
    else:
        print('..drawing land from ',landmask_type, ' landmask')
        if landmask_type=='riops':
            #ocean_mesh_file=("/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/"
            #                    "requests/RIOPS_CREG12_ext_data/mesh_mask.nc")
            ocean_mesh_file=(os.path.join(landmask_directory, 'RIOPS_landmask.nc'))
            lon_var='nav_lon'
            lat_var='nav_lat'
            tmask_var='tmask'
        elif landmask_type=='ciopsw':
            #ocean_mesh_file=("/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/"
            #    "requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/"
            #    "mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc")
            ocean_mesh_file = (os.path.join(landmask_directory, 'CIOPSW_landmask.nc'))
            lon_var='nav_lon'
            lat_var='nav_lat'
            tmask_var='tmask'
        elif landmask_type=='salishsea':
            #ocean_mesh_file=("C:/Users/Holdenje/DRIFT/sample_drift-tool_output/SalishSea_mesh_mask.nc")
            ocean_mesh_file = (os.path.join(landmask_directory, 'SalishSea_landmask.nc'))
            lon_var='nav_lon'
            lat_var='nav_lat'
            tmask_var='tmask'
        else:
            print("Unrecognized landmask_type specified (should be riops, ciopsw). "
                    "Using default basemap to draw land instead.")
            bmap.drawcoastlines(linewidth=0.5)
            bmap.drawmapboundary(linewidth=0.5, fill_color=water_color)
            bmap.fillcontinents(color="silver",lake_color=water_color)

        #assuming a landmask was given:
        ds = xr.open_dataset(ocean_mesh_file)
        lons = ds[lon_var].values
        lats = ds[lat_var].values
        mask = np.squeeze(ds[tmask_var].values)
        while mask.ndim > 2:
            mask = mask[0, ...]
        mask_masked = np.ma.masked_values(mask, 1)
        x, y = bmap(lons, lats)
        cmap = colors.LinearSegmentedColormap.from_list("", [land_color,water_color])
        # turn on/off for filled land
        # WARNING: CIOPS land isn't filled entirely because of domain boundaries
        bmap.contourf(x,y,mask_masked, levels=[0,1],cmap=cmap)
        # turn on/off contour below to show landmask edges
        # WARNING: RIOPS mask is strange in Pacific Ocean so edges appear below 46N
        bmap.contour(x,y,mask,levels=[0,],colors='k',linewidths=0.5)
        #print('land drawn')


def plot_all_tracks(
    df_all,
    savedir=None,
    startdate=None,
    enddate=None,
    obs_tracks=False,
    mod_tracks=False, 
    start_dot=False, 
    mod_start_dots=False, 
    drifter_prefix=None,
    landmask_type=None,
    etopo_file=None,
    ax=None, 
    bmap=None, 
    par_labels=None, 
    mer_labels=None, 
    map_extremes=None,
    obs_color=None,
    mod_color=None,
    dot_color=None,
    mod_dot_color=None,
    show_legend=True,
    lambert_aspect_ratio_value=None,
    map_projection=None):

    
    if drifter_prefix is not None: 
        buoyids = [drifter_prefix]
    else:
        buoyids = np.unique(df_all.buoyid.values)

    trackcounter = 0

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if drifter_prefix is None:
        lat_coor = df_all['obs_lat'].tolist() + df_all['mod_lat'].tolist()
        lon_coor = df_all['obs_lon'].tolist() + df_all['mod_lon'].tolist()
    else:
        buoydf = df_all.loc[df_all['buoyid'] == drifter_prefix]
        lat_coor = buoydf['obs_lat'].tolist() + buoydf['mod_lat'].tolist()
        lon_coor = buoydf['obs_lon'].tolist() + buoydf['mod_lon'].tolist()

    if bmap is None:
        bmap, mapstr = setup_map(
            crnr=map_projection, 
            resolution='h', 
            lat_coordinate=lat_coor, 
            lon_coordinate=lon_coor, 
            ax=ax, 
            par_labels=par_labels, 
            mer_labels=mer_labels,
            map_extremes=map_extremes,
            lambert_aspect_ratio_value=lambert_aspect_ratio_value)
    else:
        mapstr = ''

    #add bathymetery if specified
    if etopo_file is not None:
        LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox', ('lat_min', 'lat_max', 'lon_min', 'lon_max'))
        initial_bbox = LatLonBoundingBox(
            lon_min=np.nanmin(lon_coor),
            lat_min=np.nanmin(lat_coor),
            lon_max=np.nanmax(lon_coor),
            lat_max=np.nanmax(lat_coor)
            )
        mpl_util.plot_bathymetry(etopo_file, bmap, initial_bbox, colorbar_visible=True)

    #set up the land
    plot_land_mask(bmap, landmask_type=landmask_type)

    savestr = ''
    if obs_tracks is True:
        savestr = savestr + '_obs'
    if mod_tracks is True:
        savestr = savestr + '_mod'
    if start_dot is True:
        savestr = savestr + '_stdot'
    if mod_start_dots is True:
        savestr = savestr + '_modstdot'
    savestr = savestr.replace('_','-')
    savestr = savestr.replace('-','_',1)

    if obs_color is None:
        obs_color = 'black'
    if mod_color is None:
        mod_color = 'dimgray'
    if dot_color is None:
        dot_color = 'green'
    if mod_dot_color is None:
        mod_dot_color = 'blue'

    #to split the tracks if the lon values wrap.
    #from https://stackoverflow.com/questions/2652368
    #   /how-to-detect-a-sign-change-for-elements-in-a-numpy-array
    def get_split_indices(list_to_split):
        asign = np.sign(list_to_split)
        sz = asign == 0
        while sz.any():
            asign[sz] = np.roll(asign, 1)[sz]
            sz = asign == 0
        signchange = ((np.roll(asign, 1) - asign) != 0).astype(int)
        idx = [i for i, x in enumerate(signchange) if x == 1]
        if len(idx) == 0:
            idx = [0]
        return idx

    #from https://stackoverflow.com/questions/1198512
    #   /split-a-list-into-parts-based-on-a-set-of-indexes-in-python
    def partition(alist, indices):
        return [alist[i:j] for i, j in zip([0]+indices, indices+[None])]

    #for each obs drifter
    for buoyid in buoyids:
        #if buoyid == 'P10D-db4600778D20160208':
            #continue

        print(buoyid)
        trackcounter += 1

        if trackcounter == 1:
            obs_label = 'observed tracks'
            mod_label = 'modelled tracks'
            st_label = 'obs deployment points'
            mod_st_label = 'mod deployment points'
        else:
            obs_label = '_nolabel_'
            mod_label = '_nolabel_'
            st_label = '_nolabel_'
            mod_st_label = '_nolabel_'

        drifterdf = df_all.loc[df_all['buoyid'] == buoyid]

        if obs_tracks == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    obs_label = '_nolabel_'
                tracksplotdf = drifterdf[['obs_lat','obs_lon']].loc[drifterdf.index.get_level_values('model_run') == m].copy()
                tracksplotdf = tracksplotdf.dropna()

                full_lons = tracksplotdf['obs_lon'].values
                full_lats = tracksplotdf['obs_lat'].values
                
                #split the track into parts if necessary.
                idx = get_split_indices(full_lons)
                lon_list = partition(full_lons, idx)
                lat_list = partition(full_lats, idx)

                #plot the individual segments
                for l in range(1,(len(idx)+1)):
                    lats = lat_list[l]
                    lons = lon_list[l]
                    x,y = bmap(lons, lats)
                    bmap.plot(x, y, color=obs_color, linewidth=0.5, zorder=2, label=obs_label)

        if mod_tracks == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    mod_label = '_nolabel_'
                modtracksplotdf = drifterdf[['mod_lat','mod_lon']].loc[drifterdf.index.get_level_values('model_run') == m].copy()
                modtracksplotdf = modtracksplotdf.dropna()

                full_mod_lons = modtracksplotdf['mod_lon'].values
                full_mod_lats = modtracksplotdf['mod_lat'].values

                #split the track into parts if necessary.
                mod_idx = get_split_indices(full_mod_lons)
                mod_lon_list = partition(full_mod_lons, mod_idx)
                mod_lat_list = partition(full_mod_lats, mod_idx)

                #plot the individual segments
                for l in range(1,(len(mod_idx)+1)):
                    mlats = mod_lat_list[l]
                    mlons = mod_lon_list[l]
                    if np.std(mlons) > 10:
                        print('..strange values found in model track', m,'segment number', l)
                        pass
                    else:
                        mx,my = bmap(mlons, mlats)
                        bmap.plot(mx, my, color=mod_color, linewidth=0.3, zorder=1, label=mod_label)

        if mod_start_dots == True:
            for m in np.unique(drifterdf.index.get_level_values('model_run')):
                if m > 0:
                    mod_st_label = '_nolabel_'
                mlats = drifterdf['mod_lat'].loc[drifterdf.index.get_level_values('model_run') == m].values
                mlons = drifterdf['mod_lon'].loc[drifterdf.index.get_level_values('model_run') == m].values
                msdlat = mlats[0]
                msdlon = mlons[0]
                msdx,msdy = bmap(msdlon, msdlat)
                bmap.scatter(msdx, msdy, 4, marker='o', color=mod_dot_color, zorder=10, label=mod_st_label)

        if start_dot == True:
            origlat = drifterdf['obs_lat'][0,0]
            origlon = drifterdf['obs_lon'][0,0]
            orx,ory = bmap(origlon, origlat)
            bmap.scatter(orx, ory, 4, marker='o', color=dot_color, zorder=100, label=st_label)

    if show_legend:
        #ax.legend(loc='best', fontsize=10)
        # Shrink current axis by 20%
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5)) #, fontsize=10)

    if drifter_prefix is not None:
        savestr = drifter_prefix + '_drifter-tracks' + savestr + mapstr
        titleextrastr = drifter_prefix
    else:
        savestr = 'aggregated-plots_drifter-tracks' + savestr + mapstr
        titleextrastr= 'all drifter tracks'

    if savefig:
        if (startdate is None) or (enddate is None):
            title_date_str = ''
        else:
            if startdate != enddate:
                title_date_str = '\n' + str(startdate) + ' - ' + str(enddate)
            else:
                title_date_str = '\n' + str(startdate) #str(pd.to_datetime(ndate[0]).strftime('%Y-%m-%d'))
        ax.set_title(titleextrastr + title_date_str, fontsize=12)
        savestr = savestr + '.png'
        outfile=os.path.join(savedir,savestr)
        print('.... saving map', outfile)
        plt.savefig(outfile,bbox_inches='tight',format='png', dpi=300)
        plt.close()
    else:
        pass

    return savestr

def compute_score_per_leadtime(df_all, skill, leadt):

    #print(
    #    df_all[['obs_lat','obs_lon','mod_lat','mod_lon','sep','obs_dist','obs_disp']]
    #    [(df_all['buoyid'] == 'P10D-db4600778D20160208') &
    #     (df_all.index.get_level_values('model_run') == 19)]
    #    )

    '''
    dflim1 = df_all.loc[df_all['sep'] < 1000]
    dflim2 = df_all.loc[(df_all['sep'] >= 1000) & (df_all['sep'] < 10000)]
    dflim3 = df_all.loc[(df_all['sep'] >= 10000) & (df_all['sep'] < 20000)]
    dflim6 = df_all.loc[(df_all['sep'] >= 20000) & (df_all['sep'] < 30000)]
    dflim7 = df_all.loc[(df_all['sep'] >= 30000) & (df_all['sep'] < 100000)]
    dflim4 = df_all.loc[(df_all['sep'] >= 100000) & (df_all['sep'] < 1000000)]
    dflim5 = df_all.loc[(df_all['sep'] >= 1000000)]

    print(
        '< 1km', len(dflim1['sep'].values), '\n',
        '1 km < sep < 10 km', len(dflim2['sep'].values), '\n',
        '10 km < sep < 100 km', len(dflim3['sep'].values), '\n',
        '> 100 km < sep < 200 km', len(dflim4['sep'].values), '\n',
        '> 200 km < sep < 300 km', len(dflim7['sep'].values), '\n',
        '> 300 km < sep < 1000 km', len(dflim6['sep'].values), '\n',
        '> 1000 km', len(dflim5['sep'].values))
    '''

    #df_all_limited = df_all.loc[df_all['sep'] < 1000000]
    #df_lead = df_all_limited.loc[df_all_limited['hours_since_start'] == leadt]

    df_lead = df_all.loc[df_all['hours_since_start'] == leadt]
    print('..gathering scores for leadt ',leadt, 'h')
    lat = df_lead.groupby(['buoyid','mod_start_date'])['initial_lat'].mean().values
    lon = df_lead.groupby(['buoyid','mod_start_date'])['initial_lon'].mean().values
    score = df_lead.groupby(['buoyid','mod_start_date'])[skill].mean().values

    # Pad out the score when there aren't enough data points
    score_pad = np.zeros_like(lat)
    score_pad[:] = np.nan
    score_pad[:score.shape[0]] = score
    score = score_pad

    return (df_lead, lat, lon, score)


def plot_hexbins(
    skill, 
    leadt, 
    df_all, 
    savedir,
    grid_spacing, 
    edge_color, 
    opacity_level, 
    startdate=None,
    enddate=None,
    landmask_type=None, 
    ax=None, 
    bmap=None, 
    par_labels=None,
    mer_labels=None,
    map_extremes=None, 
    map_projection=None):

    df_lead, lat, lon, score = compute_score_per_leadtime(df_all, skill, leadt)

    print('..plotting score hexbins')
    do_hexbin_plot(
        df_lead, 
        lat, lon, score, 
        savedir,
        skill, leadt, 
        grid_spacing, 
        edge_color, 
        opacity_level,
        startdate=startdate,
        enddate=enddate,
        landmask_type=landmask_type, 
        ax=ax, 
        bmap=bmap, 
        par_labels=par_labels, 
        mer_labels=mer_labels,
        map_extremes=map_extremes, 
        map_projection=map_projection)


def do_hexbin_plot(
    df_lead, 
    lat, lon, score, 
    savedir,
    skill, leadt, 
    grid_spacing, 
    edge_color,
    opacity_level, 
    startdate=None,
    enddate=None,
    landmask_type=None, 
    ax=None, 
    bmap=None, 
    par_labels=None, 
    mer_labels=None,
    map_extremes=None, 
    map_projection=None):

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    lat_coor = lat 
    lon_coor = lon 

    if bmap is None:
        bmap, mapstr = setup_map(
            crnr=map_projection, 
            resolution='h', 
            lat_coordinate=lat_coor, 
            lon_coordinate=lon_coor, 
            ax=ax, 
            par_labels=par_labels, 
            mer_labels=mer_labels, 
            map_extremes=map_extremes)
    else:
        mapstr = ''

    lon_x, lat_y = bmap(lon, lat)

    plot_land_mask(bmap, landmask_type=landmask_type)

    cmap_value  = cmap.get_cmap('cmo.dense', 5)

    if skill == 'sep':
        vmin_value = np.nanmin(score)*1e-3 #0
        vmax_value = np.nanmax(score)*1e-3
        #colorbarlabel = 'Average separation distance (km)'
        skilllabel = 'Average separation distance'
        scorevalues = score*1e-3
        cbarval_list = np.linspace(vmin_value, vmax_value, 6, endpoint=True)
        colorbarstr = [f"{np.round(x):0.0f} km" for x in cbarval_list]
        #if not using the colorbar label, can add 'km'
        #colorbarstr = [f"{x:0.1f} km" for x in cbarval_list]
    else:
        vmin_value = 0
        vmax_value = 1.0
        if (skill == 'molcard') or (skill == 'liu'):
            skillcap = skill.capitalize()   
        else:
            skillcap = skill
        colorbarlabel = ' '.join([skillcap,'skill'])
        cbarval_list = np.linspace(vmin_value, vmax_value, 6, endpoint=True)
        colorbarstr = ['0','0.2','0.4','0.6','0.8','1.0']
        skilllabel = skill
        scorevalues=score

    sd_bin = bmap.hexbin(
        lon_x, lat_y,
        C=scorevalues,
        vmin=vmin_value,
        vmax=vmax_value,
        reduce_C_function=np.nanmean,
        linewidths=0.2,
        edgecolor=edge_color,
        gridsize=grid_spacing,
        zorder=100,
        alpha=opacity_level,
        cmap=cmap_value)
    cbar = bmap.colorbar(sd_bin, location='right', pad='2%') #, label=colorbarlabel)
    cbar.set_ticks(cbarval_list)
    cbar.ax.set_yticklabels(colorbarstr, fontsize=8)
    
    if savefig:
        if (startdate is None) or (enddate is None):
            title_date_str = ''
        else:
            if startdate != enddate:
                title_date_str = '\n' + str(startdate) + ' - ' + str(enddate)
            else:
                title_date_str = '\n' + str(startdate) #str(pd.to_datetime(ndate[0]).strftime('%Y-%m-%d'))
        ax.set_title(skilllabel + ' at ' + str(leadt).zfill(3)+' hr' + '\n' + title_date_str, fontsize=12)
        hexsavefilename = 'skillscoremap_'+skill+'_'+str(leadt).zfill(3)+'hr'+ mapstr +'.png'
        outfile=os.path.join(savedir, hexsavefilename)
        plt.savefig(outfile,bbox_inches='tight',format='png', dpi=200)
        print('saving figure ', outfile)
        plt.close()
    else:
        #plt.show()
        pass

############################################################################################

def compute_subtracted_score_per_leadtime(df_all, skill, leadt):

    df_all['buoyid_modcoords'] = df_all['buoyid'] + 'LON' + df_all['mod_start_lon'].astype(str) + 'LAT' + df_all['mod_start_lat'].astype(str)
    df_all.dropna(subset=[skill],inplace=True)
    df_all_grouped_tracks = df_all.groupby(['buoyid_modcoords','hours_since_start']).mean()
    df_all_grouped_tracks.reset_index(inplace=True)
    df_lead = df_all_grouped_tracks.loc[df_all_grouped_tracks['hours_since_start'] == leadt]

    return df_lead


def plot_subtracted_hexbins(
        skill, leadt, df_all, df_all2, dataset_list,savedir, grid_spacing,
        edge_color, opacity_level, startdate, enddate, landmask_type=None,
        ax=None, logplot=None, bmap=None, par_labels=None, mer_labels=None,
        map_extremes=None, map_projection=None):

    if skill != 'sep':
        print('Comparison hexbin plot for subtracted values not realistic for molcard and liu skill scores. Skipping plot.')
        return None

    print('..making subtracted hexbin plot for',skill, 'at', leadt, 'h')

    #the dataframes at this point are full concatonated dataframes with data binned by drifter and by leadt
    #This gives a dataframe that still has multiple values for each buoyid, to account for all the times
    #that fall into the hour before leadt

    #bin by mod start coordinates/buoyid and hours since start, then subtract
    df_lead = compute_subtracted_score_per_leadtime(df_all, skill, leadt)
    df_lead2 = compute_subtracted_score_per_leadtime(df_all2, skill, leadt)

    unique_buoymodruns1 = np.unique(df_lead['buoyid_modcoords'].values)
    unique_buoymodruns2 = np.unique(df_lead2['buoyid_modcoords'].values)

    commonids = [value for value in unique_buoymodruns1 if value in unique_buoymodruns2]

    uncommonids1 = [value for value in unique_buoymodruns1 if value not in unique_buoymodruns2]
    uncommonids2 = [value for value in unique_buoymodruns2 if value not in unique_buoymodruns1]
    uncommon_check1 = df_lead.loc[df_lead['buoyid_modcoords'].isin(uncommonids1)]
    uncommon_check2 = df_lead.loc[df_lead['buoyid_modcoords'].isin(uncommonids2)]
    if uncommon_check1.empty == False:
        print('Why are all the model runs not in the common set? They should be!')
    if uncommon_check2.empty == False:
        print('Why are all the model runs not in the common set? They should be!')

    df_all_common = df_lead.loc[df_lead['buoyid_modcoords'].isin(commonids)]
    df_all_common.reset_index(inplace=True)
    df_all2_common = df_lead2.loc[df_lead2['buoyid_modcoords'].isin(commonids)]
    df_all2_common.reset_index(inplace=True)

    ulat_check1 = ''.join(df_all_common['mod_start_lat'].astype(str).values)
    ulat_check2 = ''.join(df_all2_common['mod_start_lat'].astype(str).values)
    ulon_check1 = ''.join(df_all_common['mod_start_lon'].astype(str).values)
    ulon_check2 = ''.join(df_all2_common['mod_start_lon'].astype(str).values)
    buoymodcoords_check1 = ''.join(df_all_common['buoyid_modcoords'].astype(str).values)
    buoymodcoords_check2 = ''.join(df_all2_common['buoyid_modcoords'].astype(str).values)
    if (ulat_check1 not in ulat_check2):
        print('common lat columns are not the same length? why?')
    if (ulon_check1 not in ulon_check2):
        print('common lon columns are not the same length? why?')
    if (buoymodcoords_check1 not in buoymodcoords_check2):
        print('common lon columns are not the same length? why?')

    subdf = pd.DataFrame()
    subdf['lat'] = df_all_common['mod_start_lat'].values
    subdf['lon'] = df_all_common['mod_start_lon'].values
    subdf['score1'] = (df_all_common[skill]).astype(float)
    subdf['score2'] = (df_all2_common[skill]).astype(float)
    subdf.dropna(subset=['score1','score2'], inplace=True)
    subdf['score'] = subdf['score1'] - subdf['score2']

    sub_lat = subdf['lat'].values
    sub_lon = subdf['lon'].values
    sub_score = subdf['score'].values

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    do_subtracted_hexbin_plot(
        sub_lat,
        sub_lon,
        sub_score,
        skill, leadt, 
        grid_spacing,
        edge_color,opacity_level,
        dataset_list,
        savedir,
        landmask_type=landmask_type,
        ax=ax,
        logplot=logplot,
        bmap=bmap,
        subtracted=True,
        par_labels=par_labels,
        mer_labels=mer_labels,
        map_extremes=map_extremes,
        map_projection=map_projection)

    if savefig:
        ax.set_title(''.join(['Average Separation Distance at ',str(leadt),'h \n','-'.join(dataset_list)]), fontsize=14)
        if logplot:
            extralogstr = 'log_'
        else:
            extralogstr = 'linear_'

        subtractedplotfilename = 'comparison_mapped-skillscore_' + extralogstr + '-'.join(dataset_list) + '_' + str(skill)+ '.png' # + map_projection
        subtractedplotpath = os.path.join(savedir,subtractedplotfilename)
        plt.savefig(subtractedplotpath,bbox_inches='tight',format='png', dpi=300)
        plt.close()
    else:
        pass
    return True


def do_subtracted_hexbin_plot(
    lat, lon, score,
    skill, leadt,
    grid_spacing,
    edge_color,
    opacity_level,
    dataset_list,
    savedir,
    landmask_type=None,
    ax=None,
    logplot=None,
    bmap=None,
    subtracted=None,
    par_labels=None,
    mer_labels=None,
    map_extremes=None,
    map_projection=None):

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    lat_coor = lat 
    lon_coor = lon 

    if bmap is None:
        bmap, mapstr = setup_map(
            crnr=map_projection,
            resolution='h',
            lat_coordinate=lat_coor,
            lon_coordinate=lon_coor,
            ax=ax,
            par_labels=par_labels,
            mer_labels=mer_labels,
            map_extremes=map_extremes)

    lon_x, lat_y = bmap(lon, lat)

    plot_land_mask(bmap, landmask_type=landmask_type)

    cmap_value = cmocean.cm.balance
    cbar_extreme = max(max(score),abs(min(score)))
    import math 
    exp = math.ceil(math.log10(cbar_extreme*1e-3))
    vmin_value = cbar_extreme*-1e-3
    vmax_value = cbar_extreme*1e-3
    colorbarlabel = ''.join(['Average separation distance difference (km) \n',' - '.join(dataset_list)])
    scorevalues = score*1e-3
    norm_value=colors.SymLogNorm(linthresh=1, linscale=1,vmin=vmin_value, vmax=vmax_value)

    #if making a log plot version
    if logplot:
        sd_bin = bmap.hexbin(
            lon_x, lat_y,
            C=scorevalues,
            vmin=vmin_value,
            vmax=vmax_value,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            #mincnt=1,
            cmap=cmap_value,
            norm=norm_value)

    else:
        #the non-log version
        sd_bin = bmap.hexbin(
            lon_x, lat_y,
            C=scorevalues,
            vmin=vmin_value,
            vmax=vmax_value,
            reduce_C_function=np.nanmean,
            linewidths=0.2,
            edgecolor=edge_color,
            gridsize=grid_spacing,
            zorder=100,
            alpha=opacity_level,
            #mincnt=1,
            cmap=cmap_value)

    cbar = bmap.colorbar(sd_bin, location='right', pad='2%', label=colorbarlabel)
    ticklabs = cbar.ax.get_yticklabels()
    print(ticklabs)
    #cbar.ax.set_yticklabels(ticklabs, fontsize=8)

    if savefig:
        ax.set_title(''.join(['Average separation distance difference at ',str(leadt),'h \n',' - '.join(dataset_list)]), fontsize=14)

    if logplot == True:
        extralogstr = 'log_'
    else:
        extralogstr = 'linear_'

    subtractedplotfilename = 'comparison_mapped-skillscore_' + extralogstr + '-'.join(dataset_list) + '_' + str(skill) + mapstr + '.png'
    subtractedplotpath = os.path.join(savedir,subtractedplotfilename)
    if savefig:
        plt.savefig(subtractedplotpath,bbox_inches='tight',format='png', dpi=300)
        plt.close()
    else:
        pass


######################################################################################
# for later
######################################################################################

#to add ciopsw domain boundary:
'''
#import the ciopse domain pickle
import pickle
load_from_pickle='/gpfs/fs4/dfo/dpnm/dfo_odis/jeh326/boundary-handling/domains/ciopsw_domain.pickle'
with open(load_from_pickle, 'rb') as f:
    domainspecs = pickle.load(f)

#add the boundary
x_left,y_left = map(domainspecs['left_lon'], domainspecs['left_lat'])
map.plot(x_left, y_left, linewidth=0.5, color='r')
x_right,y_right = map(domainspecs['right_lon'], domainspecs['right_lat'])
#map.plot(x_right, y_right, 'o', markersize=1, color='r')
map.plot(x_right, y_right, linewidth=0.5,  color='r')
x_top,y_top = map(domainspecs['top_lon'], domainspecs['top_lat'])
map.plot(x_top, y_top, linewidth=0.5,  color='r')
x_bottom,y_bottom = map(domainspecs['bottom_lon'], domainspecs['bottom_lat'])
map.plot(x_bottom, y_bottom, linewidth=0.5,  color='r')
'''

# to plot the polygon areas for ciopsw
'''
#define the polygon zones
Juan_de_Fuca = np.array([[48.4,-123.6],[48.62,-124.7],[48.37,-124.7],[48.14,-124.],[48.1,-123.5],[48.4,-123.6]])
Puget_Sound = np.array([[48.4,-123.6],[48.1,-123.5],[48.,-122.8],[48.25,-122.72],[48.38,-122.63],[48.47,-123.],[48.42,-123.32],[48.48,-123.46],[48.4,-123.6]])
Gulf_Islands = np.array([[48.47,-123.],[48.38,-122.63],[48.5,-122.25],[48.85,-122.6],[48.7,-122.9],[48.965,-123.53],[49.18,-123.8],[49.2,-124.],[48.4,-123.6],[48.48,-123.46],[48.67,-123.42],[48.67,-123.15],[48.47,-123.]])
Haro_Strait = np.array([[48.47,-123.],[48.67,-123.15],[48.67,-123.42],[48.48,-123.46],[48.42,-123.32],[48.47,-123.]])
SOG_north = np.array([[49.34,-123.38],[49.41,-123.55],[50.,-124.],[50.2,-125.],[50.,-125.4],[49.4,-124.8],[49.2,-124.],[49.18,-123.8],[49.34,-123.38]])
SOG_south = np.array([[48.7,-122.9],[48.85,-122.6],[49.1,-122.85],[49.3,-123.],[49.25,-123.3],[49.34,-123.38],[49.18,-123.8],[48.965,-123.53],[48.7,-122.9]])
Van_Harbour = np.array([[49.25,-123.3],[49.34,-123.38],[49.35,-123.25],[49.3,-123.],[49.25,-123.3]])
Howe_Sound = np.array([[49.34,-123.38],[49.41,-123.55],[49.53,-123.55],[49.71,-123.15],[49.5,-123.1],[49.35,-123.25],[49.34,-123.38]])
WCVI_Shelf = np.array([[48.37,-124.7],[48.62,-124.7],[49.,-125.],[49.3,-126.],[50.9,-128.75],[50.9,-129.5],[50.5,-129.5],[48.,-125.7],[46.2,-125.],[46.2,-124.],[48.37,-124.7]])

#plot the polygon zones
for polyname in [Juan_de_Fuca, Puget_Sound, Gulf_Islands, Haro_Strait, SOG_north, SOG_south, Van_Harbour, Howe_Sound, WCVI_Shelf]:
    map.plot(polyname[:,1], polyname[:,0], latlon=True, color='k', linewidth=0.35)
'''