"""
crop common netcdfs
==========================
:Author: Jennifer Holden
:Created: 2020-06-15

This module gathers the drift-predict outputs from experiments run with multiple datasets.
It crops the output to a common timeframe and common observed drifter set, then writes the
data to a single common class 4 like file per drifter.
"""

import os
import glob
import sys
import xarray as xr
import numpy as np
import pandas as pd
import yaml
#import argparse
#import json

#from . import utils
import utils
logger = utils.logger

'''
## Example input with three datasets:
## CIOPSE vs RIOPS and GIOPS ##
datadirs = {
'CIOPS-W': 'path/to/aggregated/ciops/datafiles/',
'RIOPS': 'path/to/aggregated/riops/datafiles/',
'GIOPS': 'path/to/aggregated/giops/datafiles/'
}
'''
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

#datadirs = {
#    'CIOPS-W': 'C:/Users/Holdenje/DRIFT/sample_drift-tool_output/20200520/West_Coast/CIOPSW-daily/',
#    'RIOPS': 'C:/Users/Holdenje/DRIFT/sample_drift-tool_output/20200520/West_Coast/RIOPS/'
#}
#savedir = "C:/Users/Holdenje/DRIFT/sample_drift-tool_output/cropped_sets_20200521/daily/"


def main(args=sys.argv[1:]):

    #parser = argparse.ArgumentParser()
    #parser.add_argument('--savedir', type=str, help='savedir for output')
    #parser.add_argument('--datadirs', type=str, help='dictionary of datasets at least two datasets (key:value = dataset_name:path_to_files) ')

    #args = (parser.parse_args())
    #savedir = args.savedir
    #datadirs = args.datadirs
    #datadirs = json.loads(args.datadirs)

    user_config_filename = "user_comparison_config.yaml"

    def load_yaml_config(config_file):
        with open(config_file, "r") as ymlfile:
            try:
                return yaml.safe_load(ymlfile)
            except yaml.YAMLError as exc:
                logger.info(exc)

    # load the user defined configuation file
    usercfg = load_yaml_config(user_config_filename)
    datadirs = usercfg['raw_datadirs']
    savedir = usercfg['comparison_output_savedir']

    #############################################################################
    # Find the common buoyids between folders. Only keep buoyids that show up in both
    #############################################################################

    #for each dictionary, make a list of the buoyids, then find the common ids
    def grab_buoyids(datadir):
        """ make a list of unique ids  """
        iname = []
        for fpath in glob.glob(os.path.join(datadir,'*.nc')): #'*P2D-wp3802519929D20160526*.nc')):
            fname = os.path.basename(fpath)
            iname.append(fname.split('_')[-2])
        return np.unique(iname)

    buoyids = {}
    for run, dir in datadirs.items():
        buoyids[run] = grab_buoyids(dir)

    commonids = None
    for modelrun in buoyids.keys():
       if commonids is None:
           commonids = set(buoyids[modelrun])
       commonids.intersection_update(buoyids[modelrun])

    #############################################################################
    # set up input and output folders
    #############################################################################

    for dir in datadirs.values():
        if os.path.isdir(dir) is False:
            logger.info('the input folder does not exist: ', dir)
        else:
            if len(glob.glob(os.path.join(dir, '*.nc'))) == 0:
                logger.info('There are no files in the input folder: ', dir)

    #make a new directory in a shared subfolder to store the data for each dataset
    dirs = list(datadirs.values())
    dataset_list = [l.replace('-','') for l in list(datadirs.keys())] # ex: ['CIOPSW','RIOPS']
    if os.path.isdir(savedir) == False:
        os.makedirs(savedir)

    subsavedir = os.path.join(savedir,'-'.join(dataset_list))
    if os.path.isdir(subsavedir) == False:
        os.makedirs(subsavedir)

    #############################################################################
    # open all the netcdf files and loop though each common drifter
    # drifters with non-common ids are not opened and so no file is written to 
    # the cropped subdirectories for them
    #############################################################################

    #for each drifter
    for drifterid in commonids:
        logger.info('')
        logger.info(drifterid)
        #open the two (or more) datasets belonging to the drifter id
        ds = [xr.open_dataset(os.path.join(d, drifterid + "_aggregated.nc")) for d in dirs]

        #find the min and max values that contain times for all of the datasets.
        #what does this do if one observed track is missing all the data in the middle?
        mintime = max([d.time.values.min() for d in ds])
        maxtime = min([d.time.values.max() for d in ds])

        #find the common model start coordinates
        all_common_starts = [
            list(zip(d.mod_start_lat.values.tolist(),d.mod_start_lon.values.tolist())) for d in ds]
        common_coordinates = []
        for valpair in all_common_starts[0]:
            match = True
            for l in all_common_starts[1:]:
                match = match and valpair in l

            if match:
                common_coordinates.append(valpair)

        # If no matching model tracks (ie, common_coordinates is an empty list)
        # set all the tracks for that buoyid to NaNs
        if not common_coordinates:
            logger.info('.... no matching model tracks - excluding drifter')
            continue

        #make a list of variables that do not use timestep as a dimension (ex, mod_start_date)
        model_run_only_vars = []
        for d in ds[0].data_vars:
            if len(ds[0][d].dims) == 1:
                model_run_only_vars.append(d)

        # for each of the files for each buoy, crop the dataset to the
        # common time and common model run ids.
        for i in range(0, len(dirs)):

            #crop to the common set of model runs
            for m in ds[i].model_run.values: 
                mod_lats = [ds[i].mod_start_lat[m].values.tolist()]
                mod_lons = [ds[i].mod_start_lon[m].values.tolist()]
                mod_start_pair = list(zip(mod_lats,mod_lons))[0]

                if mod_start_pair not in common_coordinates:
                    #print('.... excluding some modruns with non-matching start positions')
                    for v in ds[i].data_vars:
                        if v in ['time','mod_start_lat','mod_start_lon','mod_start_date','original_filename']:
                            continue
                        var = ds[i][v]
                        if len(var.dims) == 2:
                            var[m, :] = np.nan

                '''
                # This is WAY TOO SLOW, but can work in a pinch if the future warning turns into an actual error. 
                # I think it will filter values outside the time range, but should be fixed as soon as it needs to be used. 
                print(mintime,maxtime) 
                for v in ds[i].data_vars:
                    if v in ['time','mod_start_lat','mod_start_lon','mod_start_date','original_filename']:
                        continue
                    var = ds[i][v]
                    if len(var.dims) == 2:
                        for t in range(0,len(var[m,:].values)):
                            #if time is not NaT
                            if np.isnat(ds[i]['time'][m,t]) == False:
                                if (ds[i]['time'][m,t] >= mintime) & (ds[i]['time'][m,t] <= maxtime):
                                    continue
                                else:  
                                    var[m,t] = np.nan                 
                            else:
                                #if the time is a NaT, everything should already be NaNs
                                continue
                '''
                
            #crop to the common timeset. Currently gives future warning about 
            #'NAT <= x' and 'x <= NAT' will always be False. should be fixed ASAP!
            ds[i] = ds[i].where((ds[i].time >= mintime) & (ds[i].time <= maxtime))
            ds[i] = ds[i].reset_coords('time')

            #the following covers the case where a model track started before the common 
            #time and so it now begins with NaNs but ends with valid data.
            #The entire modelled track is tossed in this case and all values 
            #except for time are set to NaN.
            for m in ds[i].model_run.values:
                if np.isnan(ds[i].obs_lat[m, 0].values):
                    #NaN the whole model run except for time
                    for v in ds[i].data_vars:
                        if v in ['time','mod_start_lat','mod_start_lon','mod_start_date','original_filename']:
                            continue
                        var = ds[i][v]
                        if len(var.dims) == 2:
                            var[m, :] = np.nan

            #cleanup take timestep 0 from each of the model_run_only_vars and
            #recreate the original one dim format for those variables.
            for v in model_run_only_vars:
                ds[i][v] = ds[i][v].sel(timestep=0).drop('timestep')

            ds[i] = ds[i].set_coords('time')

            datasetsstr = [''.join([str(list(datadirs.keys())[d]), ':', 
                str(ds[d].attrs['ocean_model']), '_', 
                str(ds[d].attrs['drift_model'])]) 
                for d in range(0,len(datadirs.keys()))]

            #add attributes to identify cropped dataset
            ds[i].attrs['cropped_to_datasets_short'] = '-'.join([l.replace('-','') for l in list(datadirs.keys())])
            ds[i].attrs['cropped_to_datasets_specific'] = ', '.join(datasetsstr) 
            ds[i].attrs['cropped_to_time_range_min'] = str(mintime)
            ds[i].attrs['cropped_to_time_range_max'] = str(maxtime)

        # Convert datasets to pandas dataframes
        dfs = []
        prct = 0
        for d in ds:
            if 'DEPTH' in list(d.dims):
                df_all_depths = d.to_dataframe()
                df = df_all_depths[df_all_depths.index.get_level_values('DEPTH') == d.DEPTH[0].values].copy()
                df.reset_index(level='DEPTH', inplace=True)
                df.drop(labels='DEPTH', axis=1, inplace=True)
            else:
                df = d.to_dataframe()

            # add some extra columns for global attributes that will need to become variables
            df.insert(len(df.keys()), "drift_model", d.drift_model, True)
            df.insert(len(df.keys()), "ocean_model", d.ocean_model, True)
            df.insert(len(df.keys()), "mod_run_name", d.mod_run_name, True)
            df.insert(len(df.keys()), "setname", list(datadirs.keys())[prct].replace('-','').replace(' ',''), True)
            prct += 1 #increment the counter

            # if obs_lat is nan, set all the other variables to nan. This makes it easier to use df.dropna() later
            df.loc[df.obs_lat.isnull(), :] = np.nan
            #maybe not necessary, but for completeness
            df.loc[df.obs_lon.isnull(), :] = np.nan
            df.loc[df.mod_lat.isnull(), :] = np.nan
            df.loc[df.mod_lon.isnull(), :] = np.nan

            #drop the rows with NaNs and reset the model_run index to ignore the runs that were dropped
            df.dropna(inplace=True, how='all')
            df.reset_index(inplace=True)
            model_run = df.model_run.copy()
            for idx, mr in enumerate(sorted(np.unique(df.model_run))):
                model_run.loc[df.model_run == mr] = idx
            df.model_run = model_run
            df.set_index(['setname', 'model_run', 'timestep'], inplace=True)
            dfs.append(df)

        # Since I dropped tracks above that were entirely NaNs, it's possible here that the model tracks
        # do not line up again. I need to drop model tracks that don't exist in all comparison sets again
        # at this point, before the dataframes are combined into a concatenated set.
        # #make a list of lists of all the model runs in each set
        modrunlist = []
        for d in range(0,len(dfs)):
            modrunlist.append(list(np.unique(dfs[d].index.get_level_values('model_run').values.tolist())))

        # compare each set against the first set to find model runs that do not exist in both sets. The range starts
        # at 1, goes to the length of dfs and compares back to the first set. Add the model runs to exclude to a list
        # of lists.
        for c in range(1,len(dfs)):
            #compare each list to the first list
            comp = np.array_equal(modrunlist[0],modrunlist[c])
            nanset = []
            if not comp:
                nanset.append(list(set(modrunlist[0]).symmetric_difference(set(modrunlist[c]))))

        #flatten the list of lists, then set the rows for those model runs to NaNs to make them easier to drop.
        nanset = [item for sublist in nanset for item in sublist]
        for d in range(0, len(dfs)):
            dfs[d].loc[(dfs[d].index.get_level_values('model_run').isin(nanset))] = np.nan
            dfs[d].dropna(inplace=True, how='all')

        # Add all the dataframes in the list to one large dataframe
        megadf = pd.concat(dfs)

        #cycle though the model runs to see if the ends need to be cropped off to make the sets match
        for i in np.unique(megadf.index.get_level_values('model_run')):
            modelrundfs = []
            for d in np.unique(megadf.index.get_level_values('setname')):
                df = megadf[
                    (megadf.index.get_level_values('model_run') == i) &
                    (megadf.index.get_level_values('setname') == d)]
                modelrundfs.append(df)

            tsteplen = []
            for d in range(0,len(np.unique(megadf.index.get_level_values('setname')))):
                tsteplen.append(len(modelrundfs[d].index.get_level_values('timestep').values.tolist()))

            if len(np.unique(tsteplen)) != 1:
                # find rows with timesteps greater than min(np.unique(tsteplen)) and set all values to NaNs
                tstepdrops = (megadf.index.get_level_values('timestep')[
                          (megadf.index.get_level_values('timestep') >= min(np.unique(tsteplen)))
                          & (megadf.index.get_level_values('model_run') == i)
                          ].values.tolist())

                megadf.loc[(megadf.index.get_level_values('timestep').isin(tstepdrops)) & (
                            megadf.index.get_level_values('model_run') == i)] = np.nan

                # drop the rows with NaNs and reset the model_run index to ignore the runs that were dropped
                megadf.dropna(inplace=True, how='all')

        #do some final checks here to be sure that I haven't missed an edge case:
        #check to be sure that there are the same number of model tracks in all the comparison sets at this point
        modrunlen = []
        for d in np.unique(megadf.index.get_level_values('setname')):
            persetdf = megadf[megadf.index.get_level_values('setname') == d]
            modrunlen.append(len(np.unique(persetdf.index.get_level_values('model_run').values.tolist())))
        if len(np.unique(modrunlen)) != 1:
            logger.info('The number of model tracks in each comparison set are not equal:',
                             modrunlen,'. this is unexpected behavior, so exiting')
            raise ValueError('The number of model tracks in each comparison set are not equal:',
                             modrunlen,'. this is unexpected behavior, so exiting')



        #check that the timesteps match
        for m in np.unique(megadf.index.get_level_values('model_run')):
            timechecklist = []
            perrundf = megadf[megadf.index.get_level_values('model_run') == m]

            for d in np.unique(perrundf.index.get_level_values('setname')):
                timechecklist.append(
                    perrundf['time'][perrundf.index.get_level_values('setname') == d].values.tolist())
            for ll in range(0,len(timechecklist[0])):
                comparelist = []
                for t in range(0,len(timechecklist)):
                    comparelist.append(timechecklist[t][ll])
                if len(np.unique(comparelist)) != 1:
                    logger.info('The timestamps in modelrun', m,
                        'do not match accross the comparison sets. This is unexpected behavior, so exiting')
                    raise ValueError('The timestamps in modelrun', m,
                        'do not match accross the comparison sets. This is unexpected behavior, so exiting')

        #convert back to one large dataset here, instead of one large dataframe
        megads = megadf.to_xarray()

        #add the attributes. First add the comparison set specific attribute:
        singledimvar = ['ocean_model', 'drift_model', 'mod_run_name']
        for sdvar in singledimvar:
            megads[sdvar] = megads[sdvar].sel(timestep=0, model_run=0, drop=True)
        doubledimvar = ['mod_start_date','mod_start_lat','mod_start_lon','original_filename']
        for ddvar in doubledimvar:
            megads[ddvar] = megads[ddvar].sel(timestep=0, drop=True)
        setspecificvar = singledimvar + doubledimvar + ['cropped_to_time_range_min', 'cropped_to_time_range_max']

        #create a list of the non-comparison set specific attributes from the original datasets and add them to megads
        attlist = []
        for d in ds:
            z = d.attrs
            for item in z.items():
                if item[0] not in setspecificvar:
                    attlist.append((item[0],item[1]))
        # I really should find the unique pairs in attlist here.
        # Right now this probably loops though attributes and overwrites them
        for item in attlist:
            megads.attrs[item[0]] = str(item[1])

        #add an attribute for the min and max times in the mega file
        newmintime = max([megads.time.values.min()])
        newmaxtime = min([megads.time.values.max()])
        megads.attrs['cropped_to_time_range_min'] = str(newmintime)
        megads.attrs['cropped_to_time_range_max'] = str(newmaxtime)
        megads.attrs['comparison_file_description'] = str(
            'The data in this set has been cropped to represent a '
            'comparison between multiple user defined data sets. '
            'The data represents a common time period and only '
            'contains model tracks that are common to all comparison sets. '
            'In the case where one track ends early due to grounding, '
            'corresponding tracks in all comparison sets have been '
            'truncated as well.')

        #add attributes to the data variables from the original datasets
        for varname, da in megads.data_vars.items():
            if varname in ds[0].data_vars:
                da.attrs = ds[0].data_vars[varname].attrs

        #add any remaining attributes that aren't covered in the original sets
        megads.data_vars['mod_start_date'].attrs = [
            ('long_name', 'Initial start date of modelled trajectory')]
        megads.data_vars['mod_start_lat'].attrs = [
            ('long_name', 'Latitude of modelled trajectory starting position'), ('units', 'degrees_north')]
        megads.data_vars['mod_start_lon'].attrs = [
            ('long_name', 'Longitude of modelled trajectory starting position'), ('units', 'degrees_east')]
        megads.data_vars['original_filename'].attrs = [
            ('long_name', 'Filename of original drift tool output')]
        megads.data_vars['drift_model'].attrs = [
            ('long_name','Drift model used to create modelled trajectory prediction')]
        megads.data_vars['ocean_model'].attrs = [
            ('long_name', 'Ocean model used to predict ocean currents')]
        megads.data_vars['mod_run_name'].attrs = [
            ('long_name', 'Original model run identifier')]
        megads.data_vars['time'].encoding['units'] = 'seconds since ' + \
                                                     str(megads.time.values[0][0][0])

        #write out the finished netcdf file containing the comparison dataset.
        write_to_file = True
        if write_to_file:
            savenamestr = str(drifterid) + '_comparison_' + \
                          str(megads.attrs['cropped_to_datasets_short']) + '.nc'
            savename = os.path.join(subsavedir, savenamestr)
            megads.to_netcdf(savename)
            logger.info("writing ", savename)


if __name__ == '__main__':
    main()


