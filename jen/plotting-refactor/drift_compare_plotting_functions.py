#!/usr/bin/env python

import os
#from os.path import join as joinpath
#import datetime as dt
import glob

import xarray as xr
import numpy as np
import pandas as pd
#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import matplotlib.cm as cmap

from dateutil.parser import parse
import collections
#from driftutils import mpl_util
import mpl_util

import aggregated_plotting_functions as aggplots

######################################################################################################################################
# Make plots
######################################################################################################################################
def setup_map(resolution='c', minlat=None, minlon=None, maxlat=None, maxlon=None, etopo_file=None, ax=None):

    latbuffer = (maxlat-minlat)*0.05
    lonbuffer = (maxlon-minlon)*0.05
    minlat = minlat-latbuffer
    maxlat = maxlat+latbuffer
    minlon = minlon-lonbuffer
    maxlon = maxlon+lonbuffer

    #set up the basemap and plot
    map = Basemap(projection='merc',
            lat_0 = 57, lon_0 = -135, lat_ts = 57,
            resolution = 'h', area_thresh = 0.1,
            llcrnrlon=minlon, llcrnrlat=minlat,
            urcrnrlon=maxlon, urcrnrlat=maxlat,
            ax=ax)
    if etopo_file is not None:
        basemap_plot=map
        LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',('lat_min', 'lat_max', 'lon_min', 'lon_max'))
        initial_bbox = LatLonBoundingBox(lon_min = minlon, lat_min = minlat, lon_max = maxlon, lat_max = maxlat)
        mpl_util.plot_bathymetry(etopo_file,basemap_plot, initial_bbox)

    meridian_label = [0, 0, 0, 1]
    parallel_label = [1, 0, 0, 1]
    map.drawparallels(np.linspace(minlat, maxlat, 7),dashes=[2,2],linewidth=0.1,fontsize=8,fmt='%.2f',labels=parallel_label)
    map.drawmeridians(np.linspace(minlon, maxlon, 5),dashes=[2,2],linewidth=0.1,fontsize=8,fmt='%.2f',labels=meridian_label, rotation=45)

    map.drawcoastlines(linewidth=0.1)
    map.drawmapboundary(linewidth=0.2, fill_color='white')
    map.fillcontinents(color='silver',lake_color='white')

    return map

def plot_model_tracks(drifterid, datadir, savedir=None, etopo_file=None, minlon=None, maxlon=None, minlat=None, maxlat=None, obscolor="k", modcolor="g", domainname="data", leg=False, dataset_label=None, ax=None, map=None):
    """ Plot observed and model tracks by drifter """

    fname = os.path.join(datadir,drifterid + "_aggregated.nc")
    #fname = os.path.join(datadir,drifterid + "_aggregated_cropped.nc")

    with xr.open_dataset(fname) as ds:

        #decide whether to save the figure or add it to a subplot with other figures
        savefig = False
        if ax is None:
            fig, ax = plt.subplots(figsize=(11,8.5))
            savefig = True

        #if no plotting domain is specified, determine one from the data
        if minlon is None or maxlon is None:
            minlon = min(float(ds.obs_lon.min()), float(ds.mod_lon.min()))
            maxlon = max(float(ds.obs_lon.max()), float(ds.mod_lon.max()))
        if minlat is None or maxlat is None:
            minlat = min(float(ds.obs_lat.min()), float(ds.mod_lat.min()))
            maxlat = max(float(ds.obs_lat.max()), float(ds.mod_lat.max()))

        if np.isnan(ds.obs_lon.values).all():
            print("all observed is nan, skipping...")
            return

        if map is None:
            map = setup_map(resolution='h', minlat=minlat, minlon=minlon, maxlat=maxlat, maxlon=maxlon, etopo_file=etopo_file, ax=ax)

        #for each modelled drifter run, add the track
        for md in range(0,len(ds.model_run)):
           
            #Convert the lat and lon to projection co-ordinates and plot
            lats = ds.obs_lat[md].values
            lons = ds.obs_lon[md].values
            mlats = ds.mod_lat[md].values
            mlons = ds.mod_lon[md].values
            if md is 0:
                origlat = lats[0] #ds.obs_lat[0,0]
                origlon = lons[0] #ds.obs_lon[0,0]

            modlabel='_nolabel_'
            obslabel='_nolabel_'
            if md == 0:
                if dataset_label is not None:
                    modlabel=dataset_label

            x,y = map(lons, lats)
            map.plot(x, y, color=obscolor, linewidth=0.85, zorder=2,label=obslabel) 

            mx,my = map(mlons, mlats)
            map.plot(mx, my, color=modcolor,linewidth=0.65, zorder=1,label=modlabel) 
            
            orx,ory = map(origlon, origlat)
            map.scatter(orx, ory, 10, marker='o', color='g', zorder=10, label='_nolabel_')

        '''
        ax.set_title(str(ds.ocean_model) + " (" + str(ds.drift_model) + 
            ", " + drifterid.split('-')[0] + ')' + "\n" + 
            str(drifterid.split('-')[1]), fontsize=14) 
        '''
        ax.set_title(str(drifterid.split('-')[1]), fontsize=14)

        if leg == True:
            ax.legend(fontsize=12)
 
        #save the plot or show it
        trafignamepng="{}_{}_{}_{}_DrifterTracks.png".format(str(drifterid), str(ds.ocean_model), str(ds.drift_model), domainname)
        if savefig:
            #savedir = os.path.join(datadir,'plots/tracks_per_drifter/')
            plt.savefig(os.path.join(savedir,trafignamepng),bbox_inches='tight',dpi=300)  #papertype='letter',orientation='portrait')
            plt.close()
        else:
            pass


###################################################################################
###################################################################################
############################### Deprecated ########################################
###################################################################################
'''
def bin_skills(ds, skill, sampling_freq):
    """ bin skill scores by drifter """

    #This part makes a binned average of the skill. 
    ds['time_since_start'] = ds['time'].astype(np.timedelta64)
    for m in ds.model_run.values:
        ds['time_since_start'][m, :] = ds['time'][m, :] - ds['mod_start_date'][m].astype(str).astype(np.datetime64) #ds['time'][m, 0]
    df = ds.to_dataframe()
    df = df.set_index('time_since_start')
    df_res = df[skill].dropna().sort_index().resample(sampling_freq, label='right').mean().interpolate()
    df_res = df_res.reset_index()
    df_res['hours since start'] = df_res.time_since_start.astype('timedelta64[h]')

    return df_res
'''
###################################################################################
############################### Deprecated ########################################
###################################################################################
def plot_skills(drifterid, ds, skill, color, skill_label_params, opacity, sampling_freq, labs=True, leg=False, ax=None):
    """ Plot skill scores by drifter (currently works for molcard, liu, sep, sutherland)"""

    #Note:
    #currently works for liu, molcard, sep, sutherland, obs_dist, obs_disp, mod_dist, mod_disp
    #In the future, can leverage ds attributes for labels: skill:long_name, skill:units (ex, liu:units)

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    ###fname = os.path.join(datadir,drifterid +"_aggregated.nc")
    #fname = os.path.join(datadir,drifterid +"_aggregated_cropped.nc")
    ###with xr.open_dataset(fname) as ds:

    #call the binning function
    df_res = aggplots.bin_skills(ds, skill, sampling_freq)

    #if using sep, dists or disps, convert the units to km
    if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
        df_res[skill]=df_res[skill]/1000.

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    #add the plot
    ax.plot(df_res['hours since start'][1:-1], df_res[skill][1:-1], color, alpha=opacity, label=str(ds.ocean_model))
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)

    if labs == True:
        #skilltitlestr = 'Average ' + str(skill) + '\n' + drifterid
        #ax.set_title(skilltitlestr)
        ax.set_ylabel(ylab, fontsize=12)
        ax.set_xlabel('hours since start',fontsize=12)

    if leg == True:
        ax.legend(fontsize=12)

    #save the plot or show it
    skfigname="{}_{}_{}_{}.png".format(str(drifterid), str(ds.ocean_model), str(ds.drift_model), skill)
    skfignamepdf="{}_{}_{}_{}.pdf".format(str(drifterid), str(ds.ocean_model), str(ds.drift_model), skill)
    if savefig:
        savedir = os.path.join(datadir,'plots/skills_per_drifter/')
        plt.savefig(os.path.join(savedir,skfigname),bbox_inches='tight',dpi=300) #,papertype='letter',orientation='portrait')
        #plt.savefig(os.path.join(savedir,skfignamepdf),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        #plt.show()
        pass

################################################################################################################
def plot_combined_skill(commonids, datadir, savedir, skill, lcolors, skill_label_params, lwidth, sampling_freq, lstyle=None, leg=False, ax=None):

    #for each id in commonids, run bin_skill
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    ########################################
    for drifterid in commonids:
        drifterds = aggplots.drifter2datasets(datadir, drifterid)
    ########################################

    for modelrun in datadir.keys():
        print("...... plotting average skill for ",str(modelrun))

        newmodel = True
        for drifterid in commonids:
            #print("............",str(drifterid))

            fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated.nc")
            with xr.open_dataset(fname) as ds:

                df_res = aggplots.bin_skills(ds, skill, sampling_freq)
                if newmodel is True:
                    df_res_all = df_res
                    newmodel=False
                else:
                    df_res_all = pd.concat((df_res_all, df_res), axis=0)

        df_res_all = df_res_all.set_index('time_since_start')
        df_res_combined = df_res_all[skill].dropna().sort_index().resample('1H', label='right').mean().interpolate()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined['hours since start'] = df_res_combined.time_since_start.astype('timedelta64[h]')

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            df_res_combined[skill]=df_res_combined[skill]/1000.

        #add the plot
        if modelrun == 'CIOPSW_hourly':
            modelrunstr = 'CIOPS-W (hourly)'
        elif modelrun == 'CIOPSW_daily':
            modelrunstr = 'CIOPS-W'
        elif modelrun == 'RIOPS_daily':
            modelrunstr = 'RIOPS'
        else:
            modelrunstr = str(modelrun)

        ax.plot(df_res_combined['hours since start'][1:-2], df_res_combined[skill][1:-2], lcolors[modelrun], linewidth=lwidth, label=modelrunstr)
        ax.minorticks_on()
        ax.grid(which='major', alpha=0.4)
        ax.grid(which='minor', alpha=0.2)

        ylab = str(skill_label_params[skill]['ylab'])
        yleg = str(skill_label_params[skill]['yleg'])

        ax.set_xlabel('hours since start', fontsize=10)
        ax.set_ylabel(ylab, fontsize=10)
        #ax.set_title('Average of mean for all drifters', fontsize=14)

        if leg == True:
            ax.legend(fontsize=10)

    #save the plot or show it
    if savefig:
        combinedskfignamepng = "comparison_{}_{}.png".format(str('-'.join(datadirs.keys())), skill)
        plt.savefig(os.path.join(savedir,combinedskfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        pass

################################################################################################################
def plot_subtracted_skill(commonids, datadirs, savedir, skill, modelruns, lcolors, skill_label_params, sampling_freq, leg=False, ax=None):

    #for each id in commonids, run bin_skill
    print("Plotting subtracted skill scores ", skill)

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    plotlims = []
    phigh = []
    plow = []
    pmax = []
    for drifterid in commonids:
        
        mod1name = os.path.join(datadirs[modelruns[0]],drifterid +"_aggregated.nc")
        mod2name = os.path.join(datadirs[modelruns[1]],drifterid +"_aggregated.nc")
        #mod1name = os.path.join(datadirs[modelruns[0]],drifterid +"_aggregated_cropped.nc")
        #mod2name = os.path.join(datadirs[modelruns[1]],drifterid +"_aggregated_cropped.nc")
        
        ds1 = xr.open_dataset(mod1name)
        ds2 = xr.open_dataset(mod2name)

        dfres1 = aggplots.bin_skills(ds1, skill, sampling_freq)
        dfres2 = aggplots.bin_skills(ds2, skill, sampling_freq)

        subdf = dfres1[['time_since_start', 'hours since start']]

        subdf[skill] = dfres1[skill].sub(dfres2[skill])

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            subdf[skill]=subdf[skill]/1000.

        #add the plot
        ax.plot(subdf['hours since start'][1:], subdf[skill][1:], label=(drifterid), zorder=2)  #label=str('-'.join(modelruns)))

        phigh = float(np.nanmax(subdf[skill][1:].values))
        plow = float(abs(np.nanmin(subdf[skill][1:].values)))
        pmax = max(phigh, plow)
        plotlims.append(pmax)       

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    #ax.grid(which='minor', alpha=0.2)

    ylab = str(skill_label_params[skill]['ylab']) + '\n' + str('-'.join(modelruns))
    yleg = str(skill_label_params[skill]['yleg'])

    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    #ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=12)

    plt.axhline(y=0,color='k',linewidth=1,linestyle=':', zorder=1)
    plt.xlim([0, len(subdf['hours since start'])])
    plt.ylim(max(plotlims)*-1,max(plotlims))

    #save the plot or show it
    subtractedskfignamepng="comparison_{}_skills-diff_{}.png".format(str('-'.join(modelruns)), skill)
    if savefig:
        plt.savefig(os.path.join(savedir,subtractedskfignamepng),bbox_inches='tight',dpi=300) 
        plt.close()
    else:
        pass

################################################################################################################
'''
def plot_combined_filled_skill(commonids, datadirs, savedir, skill, lcolors, skill_label_params, sampling_freq, filledrangetype, leg=False, ax=None):

    #for each id in commonids, run bin_skill
    print("...... calculating filled zones")

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if skill is 'logobsratio':
        plotlog=True
        skill = 'obsratio'
    elif skill is 'logmodratio':
        plotlog=True
        skill = 'modratio'
    else:
        plotlog=False

    ### For each set of data
    for modelrun in datadirs.keys(): 
        #print("...... plotting filled range for ",str(modelrun))

        ### for each drifter in the set of common drifter ids
        newdrifter = True 
        for drifterid in commonids:
            #print("............",str(drifterid))
            fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated.nc")
            #fname = os.path.join(datadirs[modelrun],drifterid +"_aggregated_cropped.nc")
            with xr.open_dataset(fname) as ds:
                df_res = aggplots.bin_skills(ds, skill, sampling_freq)

                if newdrifter is True:                    
                    df_res_all = df_res
                    newdrifter=False
                else:
                    df_res_all = pd.concat((df_res_all, df_res), axis=0)

        df_res_all = df_res_all.set_index('time_since_start')
        df_res_combined = df_res_all[skill].dropna().sort_index().resample('1H', label='right').mean().interpolate()
        df_res_combined = df_res_combined.reset_index()
        df_res_combined['hours since start'] = df_res_combined.time_since_start.astype('timedelta64[h]')

        print('here is a test. my filled range is ', filledrangetype)

        if filledrangetype == 'IQR':
            df_q1_quan = df_res_all[skill].dropna().sort_index().resample('1H', label='right').quantile(0.25).interpolate()
            df_q3_quan = df_res_all[skill].dropna().sort_index().resample('1H', label='right').quantile(0.75).interpolate()
            df_res_combined['upper'] = df_q3_quan.values
            df_res_combined['lower'] = df_q1_quan.values
            leg_str = 'Interquartile Range'
        elif filledrangetype == '1std':
            df_std = df_res_all[skill].dropna().sort_index().resample('1H', label='right').std().interpolate()
            df_std = df_std.reset_index()
            df_res_combined['std'] = df_std[skill]
            df_res_combined['upper'] = df_res_combined[skill]+df_res_combined['std']
            df_res_combined['lower'] = df_res_combined[skill]-df_res_combined['std']
            if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
                df_res_combined['std'] = df_res_combined['std']/1000.
            leg_str = '1std'
        elif filledrangetype == 'extremes':
            df_min_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').min().interpolate()
            df_max_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').max().interpolate()
            df_res_combined['upper'] = df_max_values.values
            df_res_combined['lower'] = df_min_values.values
            leg_str = 'extremes'
        else:
            print('you need to pass in a filledrangetype')

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            df_res_combined[skill]=df_res_combined[skill]/1000.
            df_res_combined['upper'] = df_res_combined['upper']/1000.
            df_res_combined['lower'] = df_res_combined['lower']/1000.

        #make the plot
        if plotlog is True:
            #ylab = 'log(' +  skill_label_params[skill['ylab']]+ ')'
            #ax.semilogy(df_res_combined['hours since start'][1:-2], df_res_combined[skill][1:-2], lcolors, label=yleg)
            print('I can not do this yet :(')
            exit()
        else:
            ax.fill_between(
                df_res_combined['hours since start'][1:-2],
                df_res_combined['lower'][1:-2],
                df_res_combined['upper'][1:-2],
                alpha=0.2,
                facecolor=lcolors[modelrun],
                label=leg_str)
            ax.plot(df_res_combined['hours since start'][1:-2],df_res_combined['upper'][1:-2],'k',linewidth=0.5, label='_nolegend_')
            ax.plot(df_res_combined['hours since start'][1:-2],df_res_combined['lower'][1:-2],'k',linewidth=0.5, label='_nolegend_')

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])
 
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('hours since start', fontsize=10)
    ax.set_ylabel(ylab, fontsize=10)
    #ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=10)

    #save the plot or show it
    filledavgskillfignamepng = "comparison_{}_{}_{}.png".format(str('-'.join(datadirs.keys())),skill,filledrangetype)

    if savefig:
        print('saving', filledavgskillfignamepng)
        #print('saving ', os.path.join(savedir,filledavgskillfignamepng))
        plt.savefig(os.path.join(savedir,filledavgskillfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        pass

'''
