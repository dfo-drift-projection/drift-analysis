#!/usr/bin/env python

import os
import glob

import xarray as xr
import numpy as np
import pandas as pd
import matplotlib

#matplotlib.use('agg')
matplotlib.use('TkAgg')
import matplotlib.pyplot as plt
import datetime
import sys
import yaml

# some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')

import drift_compare_plotting_functions as deplots
import aggregated_plotting_functions as aggplots #temp as aggplots #
import mapped_skillscores_functions as mss

user_config_filename = "user_comparison_config.yaml"
#user_config_filename = "salish-ciopsw-riops-20200521_config.yaml"
#user_config_filename = "CIOPSW-RIOPS_comparison_config.yaml"


def load_yaml_config(config_file):
    with open(config_file, "r") as ymlfile:
        try:
            return yaml.safe_load(ymlfile)
        except yaml.YAMLError as exc:
            print(exc)


def main():
    # load any default parameters for plotting
    cfg = load_yaml_config("aggregated_plotting_configuration.yaml")
    skill_label_params = cfg["skill_label_params"]
    lcolors = cfg["lcolors"]

    # load the user defined configuation file
    usercfg = load_yaml_config(user_config_filename)
    datadir = usercfg['comparison_output_savedir'] + '/' + \
              '*'.join(list(usercfg['raw_datadirs'].keys())).replace('-','').replace('*','-')
    rawsavedir = usercfg['plot_savedir']
    etopo_file = usercfg['etopo_file']

    # including a check that will exit if the drift-tool output folder is not present
    # this could be due to an error or inadequate ocean/drifter data to run the tool.
    if os.path.isdir(datadir) is False:
        sys.exit('The input folder ' + str(datadir) + ' does not exist!')

    # read which plots to create from the user defined yaml file
    plot_selection = usercfg['plot_selection']

    # add default False for any plot options that aren't chosen
    plot_options_list = [
        'plot_track_indiv', 'plot_tracks_side_by_side',
        'plot_drifter_skillscores', 'plot_average_of_mean_skills', 'plot_skills_subplots_withindv',
        'plot_skills_subplots_with_filled', 'plot_subtracted_skillscores', 'plot_indv_subtracted_skillscores',
        'plot_ratio_combined', 'plot_subtracted_hexbins']
    for val in plot_options_list:
        if val not in plot_selection.keys():
            plot_selection[val] = False

    if plot_selection['plot_subtracted_skillscores'] or plot_selection['plot_indv_subtracted_skillscores'] or plot_selection['plot_subtracted_hexbins']:
        use_reference_set = usercfg['use_reference_set']
        if use_reference_set:
            reference_set = usercfg['reference_set']
        else:
            reference_set = None

    skills_list = usercfg['skills_list']
    duration = usercfg['duration']
    #sampling_freq = usercfg['sampling_freq']

    filled_calculation_method = usercfg['filled_calculation_method']
    if filled_calculation_method is None:
        filled_range_type = 'IQR'
    else:
        filled_range_type = filled_calculation_method  # filled_calculation_method.split(',')

    label_list = aggplots.get_label_list(datadir)

    #############################################################################
    # make sure the savedir exists. Prep for making subfolders below if
    # user chooses to make plots for individual drifters
    #############################################################################

    savedir = rawsavedir
    if os.path.isdir(savedir) is False:
        os.makedirs(savedir)

    '''
    def setup_savedir(fname):
        subdir = os.path.join(savedir, str(fname))
        if os.path.isdir(subdir) is False:
            os.makedirs(subdir)
        return subdir
    '''

    #############################################################################
    # make a list of the common ids between comparison sets by making a list of
    # the filenames in the datadir. Comparison output files are not created
    # unless data exists for all the sets
    #############################################################################

    '''
    # for each dictionary, make a list of the common ids
    def grab_buoyids(datadir):
        """ make a list of unique ids  """
        iname = []
        for fpath in glob.glob(os.path.join(datadir, '*.nc')):
            fname = os.path.basename(fpath)
            iname.append(fname.split('_')[0])
        return np.unique(iname)
    '''

    commonids = aggplots.grab_buoyids(datadir)
    dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir)


    # dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(
    #    datadir, buoyids=['wp4332522315D20160617', 'wp4342522942D20160617', 'wp4352522490D20160617'])

    #############################################################################
    # common functions
    #############################################################################
    def return_map_extremes(drifterid, datadir):
        for fpath in glob.glob(os.path.join(datadir, drifterid + "*.nc")):
            fname = os.path.basename(fpath)
            with xr.open_dataset(fname) as ds:
                minlon = min([float(ds.obs_lon.min()), float(ds.mod_lon.min())])
                maxlon = max([float(ds.obs_lon.max()), float(ds.mod_lon.max())])
                minlat = min([float(ds.obs_lat.min()), float(ds.mod_lat.min())])
                maxlat = max([float(ds.obs_lat.max()), float(ds.mod_lat.max())])
                return (minlon, maxlon, minlat, maxlat)

    #############################################################################
    # Plot the individual drifter tracks (one obs track, modelled tracks from one dataset)
    #############################################################################
    if plot_selection['plot_track_indiv'] is True:
        # plot the tracks individually for each drifter for each model.
        # This will plot all, regardless of if the ids are common
        print('assembling drifter data set')
        datasetsdf = mss.create_comparison_tracks_dataframe(datadir)
        for modelrun in datasetsdf.keys():
            indv_tracks_savedir = aggplots.setup_savedir(savedir,('tracks_per_drifter/' + str(modelrun)))
            print('......plotting drifter tracks for', modelrun)

            for drifterid in np.unique(datasetsdf[modelrun]['buoyid'].values):  # ['P2D-wp3772515323D20160526']:

                mss.plot_all_tracks(
                    datasetsdf[modelrun],
                    savedir=indv_tracks_savedir,
                    startdate=None, enddate=None,
                    obs_tracks=True, mod_tracks=True,
                    start_dot=True, mod_start_dots=True,
                    landmask_type=modelrun.replace('-', '').lower(),  # None,
                    etopo_file=etopo_file,
                    ax=None,
                    par_labels=None, mer_labels=None,
                    map_extremes=None,
                    obs_color=None, mod_color=None,
                    dot_color=None, mod_dot_color='orange',
                    map_projection='lambert',
                    drifter_prefix=drifterid
                )

    #############################################################################
    # Plot the comparison drifter tracks (one obs track, modelled tracks from all sets)
    #############################################################################
    if plot_selection['plot_track_comparison'] is True:
        # plot the tracks individually for each drifter for each model.
        # This will plot all, regardless of if the ids are common
        comparison_tracks_subdir = aggplots.setup_savedir(savedir,'comparison_tracks_per_drifter')
        for drifterid in commonids:
            print('assembling drifter data sets for ', drifterid)
            datasetsdf = mss.create_comparison_tracks_dataframe(datadir, drifterid)

            # find the plot extremes
            all_lats = []
            all_lons = []
            for k in datasetsdf.keys():
                all_lats.extend(datasetsdf[k]['obs_lat'].values.tolist())
                all_lats.extend(datasetsdf[k]['mod_lat'].values.tolist())
                all_lons.extend(datasetsdf[k]['obs_lon'].values.tolist())
                all_lons.extend(datasetsdf[k]['mod_lon'].values.tolist())
            compare_min_lats = np.nanmin(all_lats)
            compare_max_lats = np.nanmax(all_lats)
            compare_min_lons = np.nanmin(all_lons)
            compare_max_lons = np.nanmax(all_lons)
            latbuff = (compare_max_lats - compare_min_lats) * 0.1
            lonbuff = (compare_max_lons - compare_min_lons) * 0.1
            fig, ax = plt.subplots(1, 1, squeeze=False, figsize=(11, 8.5))
            tracks_crnr = 'lambert'
            map_extremes = [compare_min_lons - lonbuff, compare_max_lons + lonbuff, compare_min_lats - latbuff,
                            compare_max_lats + lonbuff]

            # set up a basemap where all the tracks will be plotted
            bmap, mapstr = mss.setup_map(
                crnr=tracks_crnr, resolution='h',
                lat_coordinate=all_lats, lon_coordinate=all_lons,
                ax=ax[0][0], par_labels=None, mer_labels=None,
                map_extremes=map_extremes)

            # plot the mod_tracks
            for modelrun in datasetsdf.keys():
                print('......plotting modelled drifter tracks for', modelrun)
                mss.plot_all_tracks(
                    datasetsdf[modelrun],
                    savedir=comparison_tracks_subdir,
                    startdate=None, enddate=None,
                    obs_tracks=False, mod_tracks=True,
                    start_dot=False, mod_start_dots=False,
                    landmask_type=None,
                    etopo_file=etopo_file,
                    ax=ax[0][0],
                    par_labels=None, mer_labels=None,
                    map_extremes=map_extremes,
                    obs_color=None, mod_color=lcolors[modelrun],
                    dot_color=None, mod_dot_color=None,
                    map_projection=tracks_crnr,
                    bmap=bmap,
                    drifter_prefix=drifterid
                )

            # plot the observed track (arbitrarily choosing to do this using whatever is the first dataset)
            print('......plotting observed drifter tracks')
            mss.plot_all_tracks(
                datasetsdf[list(datasetsdf.keys())[0]],
                savedir=comparison_tracks_subdir,
                startdate=None, enddate=None,
                obs_tracks=True, mod_tracks=False,
                start_dot=True, mod_start_dots=False,
                landmask_type=None,
                etopo_file=etopo_file,
                ax=ax[0][0],
                par_labels=None, mer_labels=None,
                map_extremes=map_extremes,
                obs_color=None, mod_color=lcolors[modelrun],
                dot_color=None, mod_dot_color=None,
                map_projection=tracks_crnr,
                bmap=bmap,
                drifter_prefix=drifterid)

            fig.tight_layout()
            # fix the legend so that it shows the dataset name instead of "modelled tracks"
            handles, labels = ax[0][0].get_legend_handles_labels()
            newlabels = list(datasetsdf.keys()) + ['observed tracks', 'obs deployment point']

            # Shrink current axis by 20%
            box = ax[0][0].get_position()
            ax[0][0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
            # Put a legend to the right of the current axis
            ax[0][0].legend(handles, newlabels, loc='center left', bbox_to_anchor=(1, 0.5), fontsize=10)
            # ax[0][0].legend(handles,newlabels, loc='best')

            cfignamepng = "{}_comparison_tracks.png".format(str(drifterid))
            plt.savefig(os.path.join(comparison_tracks_subdir, cfignamepng), bbox_inches='tight', dpi=300)
            plt.close()
            print('saving', os.path.join(comparison_tracks_subdir, cfignamepng))

    #############################################################################
    # plots side by side versions of each dataset for each drifter
    #############################################################################

    if plot_selection['plot_tracks_side_by_side'] is True:

        sidebyside_tracks_subdir = aggplots.setup_savedir(savedir,'side-by-side_comparison_tracks')
        for drifterid in commonids:
            print('assembling drifter data sets for ', drifterid)
            datasetsdf = mss.create_comparison_tracks_dataframe(datadir, drifterid)
            # find the plot extremes
            all_lats = []
            all_lons = []

            for k in datasetsdf.keys():
                all_lats.extend(datasetsdf[k]['obs_lat'].values.tolist())
                all_lats.extend(datasetsdf[k]['mod_lat'].values.tolist())
                all_lons.extend(datasetsdf[k]['obs_lon'].values.tolist())
                all_lons.extend(datasetsdf[k]['mod_lon'].values.tolist())
            compare_min_lats = np.nanmin(all_lats)
            compare_max_lats = np.nanmax(all_lats)
            compare_min_lons = np.nanmin(all_lons)
            compare_max_lons = np.nanmax(all_lons)
            latbuff = (compare_max_lats - compare_min_lats) * 0.1
            lonbuff = (compare_max_lons - compare_min_lons) * 0.1

            if (latbuff / lonbuff) > 0.8:
                fig, ax = plt.subplots(1, len(list(datasetsdf.keys())), squeeze=False, figsize=(11, 8.5))
            else:
                fig, ax = plt.subplots(len(list(datasetsdf.keys())), 1, squeeze=False, figsize=(11, 8.5))

            tracks_crnr = 'lambert'
            map_extremes = [compare_min_lons - lonbuff, compare_max_lons + lonbuff, compare_min_lats - latbuff, compare_max_lats + lonbuff]

            for p in range(0, len(list(datasetsdf.keys()))):
                if (latbuff / lonbuff) > 0.8:
                    ax_handle = ax[0][p]
                else:
                    ax_handle = ax[p][0]

                # plot the mod_tracks
                print('......plotting modelled drifter tracks for', p)
                mss.plot_all_tracks(
                    datasetsdf[list(datasetsdf.keys())[p]],
                    savedir=sidebyside_tracks_subdir,
                    startdate=None, enddate=None,
                    obs_tracks=True, mod_tracks=True,
                    start_dot=False, mod_start_dots=False,
                    landmask_type=None,
                    etopo_file=etopo_file,
                    ax=ax_handle,
                    par_labels=None, mer_labels=None,
                    map_extremes=map_extremes,
                    obs_color=None, mod_color=lcolors[list(datasetsdf.keys())[p]],
                    dot_color=None, mod_dot_color=None,
                    map_projection=tracks_crnr,
                    bmap=None,
                    drifter_prefix=drifterid,
                    show_legend=False)

                ax_handle.set_title(list(datasetsdf.keys())[p])

            fig.tight_layout()
            sbsfignamepng = "{}_side-by-side.png".format(str(drifterid))
            plt.savefig(os.path.join(sidebyside_tracks_subdir, sbsfignamepng), bbox_inches='tight', dpi=300)
            plt.close()
            print('saving', sbsfignamepng)

    #############################################################################
    # plot skill scores on subplots
    #############################################################################
    if plot_selection['plot_drifter_skillscores'] is True:
        skills_subdir = aggplots.setup_savedir(savedir,'skills_subplots')
        # for each common id, plot the skill scores together
        print("Plotting multi skillscores on subplots")
        for drifterid in commonids: # ['P2D-wp3952528532D20161116']: #commonids:
            print(drifterid)
            dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids=[drifterid])
            plotcolors = aggplots.assign_colors(dataset_labels, lcolors)
            drifterds = datasets_dict
            fig, ax = plt.subplots(3, 1, squeeze=False, figsize=(8.5, 11), sharex='all')
            for setname in dataset_labels:
                subdrifterds = drifterds[setname][0]
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'sep', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[0][0])
                handles, labels = ax[0][0].get_legend_handles_labels()
                newlabels = list(label_list)
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'molcard', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[1][0])
                aggplots.plot_skills(drifterid, subdrifterds, setname, 'liu', plotcolors[setname], skill_label_params, 1, True, False, ax=ax[2][0])
            #ax[0][0].legend(handles, newlabels, loc='center left', bbox_to_anchor=(1, 0.5), fontsize=10)
            ax[0][0].legend(handles, newlabels, loc='best', fontsize=10)
            fig.tight_layout(rect=[0, 0.03, 1, 0.95])
            fig.suptitle(str(drifterid), fontsize=14)
            ax[0][0].set_xlabel('')
            ax[1][0].set_xlabel('')
            plt.subplots_adjust(wspace=0, hspace=0.05)
            multiskillfignamepng = "{}_skills.png".format(str(drifterid))
            plt.savefig(os.path.join(skills_subdir, multiskillfignamepng), bbox_inches='tight', dpi=300)
            plt.close()
            #plt.close(os.path.join(skills_subdir, multiskillfignamepng))
            print()

        print('drifter_skillscores', str(datetime.datetime.now()))
        print()

    #############################################################################
    # plot average of means for each model
    #############################################################################
    if plot_selection['plot_average_of_mean_skills'] is True:

        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir)
        plotcolors = aggplots.assign_colors(dataset_labels, lcolors)
        print('plotting average means for ', str('-'.join(dataset_labels)))

        for skill in skills_list:
            fig, ax = plt.subplots(figsize=(11, 8.5))
            for setname in datasets_dict.keys():
                all_mean, all_drifters_all_tracks = aggplots.bin_skills_all_drifters(datasets_dict[setname], skill)
                aggplots.plot_average_skill(all_mean, setname, savedir, skill, plotcolors[setname], skill_label_params, 1, leg=True, ax=ax)
            combinedskfignamepng = "aggregated_mean_{}.png".format(skill)
            plt.savefig(os.path.join(savedir, combinedskfignamepng), bbox_inches='tight', dpi=300, papertype='letter', orientation='portrait')
            plt.close()
        print('average_of_mean_skills', str(datetime.datetime.now()))
        print()

    #############################################################################
    # plot subtracted hexbins
    #############################################################################
    if plot_selection['plot_subtracted_hexbins'] is True:

        grid_spacing = 80  # I think this can also be ['100','100'], etc
        edge_color = 'black'  # can be a color like 'black' or 'face' for no color
        opacity_level = 1  # 0.8
        leadt_list = [duration]
        print('plotting subtracted hexbins')

        for skill in skills_list:
            for leadt in leadt_list:
                df_res_combined_dict = mss.create_master_dataframe(datadir, commonids)
                dataset_list = list(df_res_combined_dict.keys())

                # determine if there is a reference set chosen, then create a zipped list of pairs:
                plot_pairs = []
                if use_reference_set:
                    print('use reference set', reference_set)
                    for l in list(df_res_combined_dict.keys()):
                        if l != reference_set:
                            plot_pairs.extend(zip([reference_set], [l]))
                else:
                    print('no reference set')
                    for first, second in zip(dataset_list, dataset_list[1:]):
                        if first != second:
                            plot_pairs.extend(zip([first],[second]))

                for plot_pair in plot_pairs:
                    print(plot_pair)
                    fig, ax = plt.subplots(figsize=(11, 8.5))
                    df1 = df_res_combined_dict[plot_pair[0]]
                    df2 = df_res_combined_dict[plot_pair[1]]

                    hbplot = mss.plot_subtracted_hexbins(skill, leadt, df1, df2, plot_pair, savedir, grid_spacing,
                                                         edge_color, opacity_level, startdate=None, enddate=None,
                                                         landmask_type=None, ax=ax, logplot=True, bmap=None,
                                                         par_labels=None, mer_labels=None, map_extremes=None,
                                                         map_projection=None)

                    if hbplot:
                        subtractedhexfignamepng = "comparison_{}_hexbin-diff_{}.png".format(str('-'.join(plot_pair)), skill)
                        plt.savefig(os.path.join(savedir, subtractedhexfignamepng), bbox_inches='tight', dpi=300)
                        print(os.path.join(savedir, subtractedhexfignamepng))
                        plt.close()


    #############################################################################
    # plot subtracted of means for each drifter
    #############################################################################
    if plot_selection['plot_subtracted_skillscores'] is True:

        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir)
        plot_pairs = aggplots.find_plot_pairs(dataset_labels, use_reference_set=use_reference_set, reference_set=reference_set)

        for skill in skills_list:
            print(skill)
            for plot_pair in plot_pairs:
                print(plot_pair)
                plotlabel = plot_pair[0] + '-' + plot_pair[1]
                fig, ax = plt.subplots(figsize=(11, 8.5))
                #plot the subtracted mean for each drifter
                plims = []
                tlims = []
                for drifter in commonids:
                    for ds in datasets_list:
                        if ds.obs_buoyid == drifter:
                            df1, all_tracks1 = aggplots.bin_skills_individual_drifter(ds.sel(setname=plot_pair[0]), skill)
                            df2, all_tracks2 = aggplots.bin_skills_individual_drifter(ds.sel(setname=plot_pair[1]), skill)
                            plim, tlim = aggplots.plot_subtracted_skill(df1, df2, skill, '_nolegend_', skill_label_params, 1, 1, ax=ax)
                            plims.append(plim)
                            tlims.append(tlim)

                #plot the subtracted mean for the entire set
                alldf1, all_drifters_all_tracks1 = aggplots.bin_skills_all_drifters(datasets_dict[plot_pair[0]], skill)
                alldf2, all_drifters_all_tracks2 = aggplots.bin_skills_all_drifters(datasets_dict[plot_pair[1]], skill)
                plim, tlim = aggplots.plot_subtracted_skill(alldf1, alldf2, skill, plotlabel, skill_label_params, 3, 3, lcolor='k', ax=ax)

                '''
                allsubdf = alldf1[['time_since_start', 'hours since start']].copy()
                allsubdf[skill] = alldf1[skill].sub(alldf2[skill])
                allsubdf = allsubdf[allsubdf['hours since start'] > 0].copy()

                # if using sep, dists or disps, convert the units to km
                if (skill == 'sep') or (skill == 'obs_disp') or (skill == 'obs_dist') or (skill == 'mod_disp') or (skill == 'mod_dist'):
                    allsubdf[skill] = allsubdf[skill] / 1000.

                ax.plot(allsubdf['hours since start'].values, allsubdf[skill].values, label=(str(plotlabel)), color='k', linewidth=3, zorder=2)
                

                #plot parameters
                ax.minorticks_on()
                ax.grid(which='major', alpha=0.4)
                # ax.grid(which='minor', alpha=0.2)

                ylab = str(skill_label_params[skill]['ylab']) + '\n' + plotlabel

                ax.set_xlabel('hours since start', fontsize=12)
                ax.set_ylabel(ylab, fontsize=12)
                # ax.set_title('Average of mean for all drifters', fontsize=14)

                #if leg == True:
                ax.legend(fontsize=12)

                plt.axhline(y=0, color='k', linewidth=1, linestyle=':', zorder=1)
                '''

                print(np.max(plims))
                plt.xlim([0, np.max(tlims)])
                plt.ylim(np.max(plims) * -1, np.max(plims))
                subtractedskfignamepng = "comparison_{}_skills-diff_{}.png".format(str('-'.join(plot_pair)), skill)
                plt.savefig(os.path.join(savedir, subtractedskfignamepng), bbox_inches='tight', dpi=300)
                print(os.path.join(savedir, subtractedskfignamepng))
                plt.close()
                print()

    #############################################################################
    # plot subtracted of means for each drifter
    #############################################################################
    #deprecated version
    '''
    if plot_selection['plot_subtracted_skillscores'] is True:

        #decide whether to use a reference set or plot everything, then call the plotting function.
        sampling_freq = '1H'
        for skill in skills_list:
            for drifterid in commonids:
                print(drifterid)
                dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids=[drifterid])
                ds_list = datasets_dict
                print(ds_list['SalishSea'])
                #ds_list = aggplots.drifter2datasets(datadir, drifterid)
                test = aggplots.bin_skills_all_drifters(ds_list, skill)
            exit()

            df_res_combined_dict = aggplots.create_master_binned_dataframe(datadir, commonids, skill, duration, '1H')
            dataset_list = list(df_res_combined_dict.keys())

            # Check if the user has given the option for a reference set in the config file.
            # If there is a reference set chosen, then create a zipped list of pairs:
            plot_pairs = []
            if use_reference_set:
                print('use reference set', reference_set)
                for l in list(df_res_combined_dict.keys()):
                    if l != reference_set:
                        plot_pairs.extend(zip([reference_set], [l]))
            else:
                print('no reference set')
                for first, second in zip(dataset_list, dataset_list[1:]):
                    if first != second:
                        plot_pairs.extend(zip([first], [second]))

            for plot_pair in plot_pairs:
                print(plot_pair)
                fig, ax = plt.subplots(figsize=(11, 8.5))
                plims = []
                tlims = []
                for drifterid in commonids:
                    drifterds = aggplots.drifter2datasets(datadir, drifterid)
                    df1, all_tracks = aggplots.bin_skills_individual_drifter(drifterds[plot_pair[0]], skill)
                    df2, all_tracks2 = aggplots.bin_skills_individual_drifter(drifterds[plot_pair[1]], skill)
                    pmax, tmax = aggplots.plot_subtracted_skill(df1, df2, drifterid, savedir, skill, lcolors,
                                                                skill_label_params, 1, leg=False, ax=ax)
                    plims.append(pmax)
                    tlims.append(tmax)

                # then the average
                dfavg1 = df_res_combined_dict[plot_pair[0]]
                dfavg2 = df_res_combined_dict[plot_pair[1]]
                aggplots.plot_subtracted_skill(dfavg1, dfavg2, '-'.join(plot_pair), savedir, skill, lcolors, skill_label_params, 4, leg=False, ax=ax)
                plt.xlim([0, max(tlims)])
                plt.ylim(max(plims) * -1, max(plims))

                subtractedskfignamepng = "comparison_{}_skills-diff_{}.png".format(str('-'.join(plot_pair)), skill)
                plt.savefig(os.path.join(savedir, subtractedskfignamepng), bbox_inches='tight', dpi=300)
                print(os.path.join(savedir, subtractedskfignamepng))
                plt.close()
    '''

    #############################################################################
    # plot subtracted of means for each drifter
    #############################################################################
    if plot_selection['plot_indv_subtracted_skillscores'] is True:

        print('plotting indv subtracted skillscores')
        subtracted_subdir = aggplots.setup_savedir(savedir,'indv_subtracted_plots')

        sampling_freq = '1H'
        for skill in skills_list:
            df_res_combined_dict = aggplots.create_master_binned_dataframe(datadir, commonids, skill, duration, sampling_freq)
            dataset_list = list(df_res_combined_dict.keys())

            # Check if the user has given the option for a reference set in the config file.
            # If there is a reference set chosen, then create a zipped list of pairs:
            plot_pairs = []
            if use_reference_set:
                print('use reference set', reference_set)
                for l in list(df_res_combined_dict.keys()):
                    if l != reference_set:
                        plot_pairs.extend(zip([reference_set], [l]))
            else:
                print('no reference set')
                for first, second in zip(dataset_list, dataset_list[1:]):
                    if first != second:
                        plot_pairs.extend(zip([first], [second]))

            for plot_pair in plot_pairs:
                print(plot_pair)
                for drifterid in commonids:
                    fig, ax = plt.subplots(figsize=(11, 8.5))
                    drifterds = aggplots.drifter2datasets(datadir, drifterid)
                    df1, all_tracks = aggplots.bin_skills_individual_drifter(drifterds[plot_pair[0]], skill)
                    df2, all_tracks2 = aggplots.bin_skills_individual_drifter(drifterds[plot_pair[1]], skill)
                    pmax, tmax = aggplots.plot_subtracted_skill(df1, df2, drifterid, savedir, skill, lcolors, skill_label_params, 1, leg=False, ax=ax)
                    plt.xlim([0, tmax])
                    plt.ylim(pmax * -1, pmax)

                    subtractedskfignamepng = "comparison_{}_skills-diff_{}_{}.png".format(str('-'.join(plot_pair)), skill, drifterid)
                    plt.savefig(os.path.join(subtracted_subdir, subtractedskfignamepng), bbox_inches='tight', dpi=300)
                    print(os.path.join(subtracted_subdir, subtractedskfignamepng))
                    plt.close()

    #############################################################################
    # plot average of means for each model WITH filled
    #############################################################################
    if plot_selection['plot_skills_subplots_with_filled'] is True:

        dataset_labels, datasets_list, datasets_dict, buoyids = aggplots.prep_files(datadir, buoyids)
        plotcolors = aggplots.assign_colors(dataset_labels, lcolors)
        for skill in ['sep','molcard']: #'liu', 'sutherland', 'obsratio', 'modratio', 'logobsratio', 'logmodratio'
            print(skill, str(datetime.datetime.now()))
            for fill_range_type in ['IQR']: #,'extremes','1std']:
                print(fill_range_type)
                aggplots.plot_skill_with_filled(datasets_dict, dataset_labels, savedir, skill, plotcolors,
                                                skill_label_params, 2, fill_range_type, lab=True, leg=True, ax=None)


if __name__ == '__main__':
    main()
