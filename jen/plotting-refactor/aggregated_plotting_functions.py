#!/usr/bin/env python

import os
#from os.path import join as joinpath
#import datetime as dt
import glob
import xarray as xr
import numpy as np
import pandas as pd
from matplotlib.pyplot import cm
#some pandas parameters
pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 1000)
pd.set_option('display.colheader_justify', 'left')
import matplotlib.pyplot as plt
import matplotlib
from mpl_toolkits.basemap import Basemap
#import matplotlib.cm as cmap
#from dateutil.parser import parse
import collections
#from driftutils import mpl_util
import mpl_util
import datetime
import dateutil
import yaml

from datetime import timedelta

######################################################################################################################################
# common functions
######################################################################################################################################


############################################################
# I don't think this is being used at the moment
############################################################
def drifter2dataframes(datadir, drifterid):
    for fpath in glob.glob(os.path.join(datadir, drifterid + "*.nc")):
        fname = os.path.basename(fpath)
        drifterdict = {}
        with xr.open_dataset(fpath) as ds:
            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            for setnameplace in range(0, len(ds.setname.values)):
                s = ds.setname.values[setnameplace]
                df = ds[vars_to_keep].sel(setname=s).to_dataframe()
                df.insert(0, 'buoyid', drifterid, True)
                # add the data frame to a list of dataframes
                drifterdict[s] = df
    # return: a dictionary with setnames as keys and associated dataframes as values
    return drifterdict

############################################################
def get_label_list(datadir, buoyids=None):
    files = create_file_list(datadir, buoyids=buoyids)
    label_list = []
    for file in files:
        with xr.open_dataset(file) as ds:
            if 'setname' in ds.dims:
                label_list.extend(ds.setname.values)
            else:
                label_list.extend([ds.ocean_model])

            return np.unique(label_list)

##################################################################################################################
# I can add an extra function that splits the df into per area data.
# should pull the area boundaries from a pickled file, then run something to find which data is in each area.
# this function could be run after the one that makes the dataframes, then cycle though the areas?
# This commented code is also in the plotting functions script at the moment
# the polygon areas should be moved to a pickled file. should probably have pickled files for polygon areas, and model
# domain boundaries.
###################################################################################################################

'''
#define the polygon zones
Juan_de_Fuca = np.array([[48.4,-123.6],[48.62,-124.7],[48.37,-124.7],[48.14,-124.],[48.1,-123.5],[48.4,-123.6]])
Puget_Sound = np.array([[48.4,-123.6],[48.1,-123.5],[48.,-122.8],[48.25,-122.72],[48.38,-122.63],[48.47,-123.],[48.42,-123.32],[48.48,-123.46],[48.4,-123.6]])
Gulf_Islands = np.array([[48.47,-123.],[48.38,-122.63],[48.5,-122.25],[48.85,-122.6],[48.7,-122.9],[48.965,-123.53],[49.18,-123.8],[49.2,-124.],[48.4,-123.6],[48.48,-123.46],[48.67,-123.42],[48.67,-123.15],[48.47,-123.]])
Haro_Strait = np.array([[48.47,-123.],[48.67,-123.15],[48.67,-123.42],[48.48,-123.46],[48.42,-123.32],[48.47,-123.]])
SOG_north = np.array([[49.34,-123.38],[49.41,-123.55],[50.,-124.],[50.2,-125.],[50.,-125.4],[49.4,-124.8],[49.2,-124.],[49.18,-123.8],[49.34,-123.38]])
SOG_south = np.array([[48.7,-122.9],[48.85,-122.6],[49.1,-122.85],[49.3,-123.],[49.25,-123.3],[49.34,-123.38],[49.18,-123.8],[48.965,-123.53],[48.7,-122.9]])
Van_Harbour = np.array([[49.25,-123.3],[49.34,-123.38],[49.35,-123.25],[49.3,-123.],[49.25,-123.3]])
Howe_Sound = np.array([[49.34,-123.38],[49.41,-123.55],[49.53,-123.55],[49.71,-123.15],[49.5,-123.1],[49.35,-123.25],[49.34,-123.38]])
WCVI_Shelf = np.array([[48.37,-124.7],[48.62,-124.7],[49.,-125.],[49.3,-126.],[50.9,-128.75],[50.9,-129.5],[50.5,-129.5],[48.,-125.7],[46.2,-125.],[46.2,-124.],[48.37,-124.7]])

#plot the polygon zones
for polyname in [Juan_de_Fuca, Puget_Sound, Gulf_Islands, Haro_Strait, SOG_north, SOG_south, Van_Harbour, Howe_Sound, WCVI_Shelf]:
    map.plot(polyname[:,1], polyname[:,0], latlon=True, color='k', linewidth=0.35)
'''
############################################################
# convert the new format files into a list of datasets
############################################################
def drifter2datasets(datadir, drifterid):
    for fpath in glob.glob(os.path.join(datadir, '*' + drifterid + "*.nc")):
        drifterdict = {}
        with xr.open_dataset(fpath) as ds:
            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            if 'setname' in ds.dims:
                for setnameplace in range(0, len(ds.setname.values)):
                    s = ds.setname.values[setnameplace]
                    dsnew = ds[vars_to_keep].sel(setname=s)
                    # add the data frame to a list of dataframes
                    drifterdict[s] = dsnew
            else:
                s = ds.ocean_model
                drifterdict[s] = ds[vars_to_keep]

    # return: a dictionary with setnames as keys and associated datasets as values
    return drifterdict


def create_file_list(datadir, buoyids=None):
    if buoyids is None:
        files = glob.glob(os.path.join(datadir, '*.nc'))
    else:
        potential_files = ["{}{}{}".format('*', i, '*.nc') for i in buoyids]
        files = []
        for potential_file in potential_files:
            file = glob.glob(os.path.join(datadir, potential_file))
            files.extend(file)
    files.sort()
    return files

def create_datasets_list(files):
    datasets=[]
    for f in files:
        with xr.open_dataset(f) as ds:
            vars_to_keep = [var for var in ds.data_vars if 'DEPTH' not in ds[var].dims]
            datasets.append(ds[vars_to_keep])
    #return a list of datasets
    return datasets

def create_datasets_dict(datasets_list, dataset_labels):
    drifterdict = {k: [] for k in dataset_labels}
    for d in datasets_list:
        if 'setname' in d.dims:
            for s in d.setname.values:
                dssel = d.sel(setname=s)
                if s in drifterdict.keys():
                    drifterdict[s].append(dssel)
        else:
            s = d.ocean_model
            if s in drifterdict.keys():
                drifterdict[s].append(d)
    # return: a dictionary with setnames as keys and associated datasets as values. ie {CIOPSW: [drifter1, drifter2, etc]}
    return drifterdict

def load_yaml_config(config_file):
    with open(config_file, "r") as ymlfile:
        try:
            return yaml.safe_load(ymlfile)
        except yaml.YAMLError as exc:
            print(exc)

def grab_buoyids(datadir):
    """ make a list of unique ids  """
    iname = []
    for fpath in glob.glob(os.path.join(datadir, '*.nc')):
        fname = os.path.basename(fpath)
        iname.append(fname.split('_')[0].split('-')[1])
    return np.unique(iname)

def setup_savedir(savedir,fname):
    subdir = os.path.join(savedir, str(fname))
    if os.path.isdir(subdir) is False:
        os.makedirs(subdir)
    return subdir

def prep_files(datadir, buoyids=None):
    #if no buoy ids are given, find all the file names in the datadir
    if buoyids is None:
        buoyids = grab_buoyids(datadir)
    #get a list of labels for the setnames
    dataset_labels = get_label_list(datadir, buoyids=buoyids)
    #create a list of the files to use
    files = create_file_list(datadir, buoyids=buoyids)
    #create a list of datasets
    datasets_list = create_datasets_list(files)
    #create a dictionary with setnames as keys and lists of datasets as values
    datasets_dict = create_datasets_dict(datasets_list, dataset_labels)
    #dataset_labels is a list of the data set names
    #datasets_lists is a list of datasets, one element for each of the drifters (ie, one per file). ex: [drifter1, drifter2, etc]
    #datasets_dict: a dictionary with setnames as keys and associated datasets as values. ie {CIOPSW: [drifter1, drifter2, etc]}
    return dataset_labels, datasets_list, datasets_dict, buoyids

def assign_colors(dataset_labels, lcolors=None):

    #define a set of auto colors for each of the lines
    num_of_sets = len(dataset_labels)
    pclrs = cm.viridis(np.linspace(0, 1, num_of_sets))
    autoplotcolors = list(zip(dataset_labels, pclrs))
    pclrs_rounded = []
    for p in pclrs:
        pclrs_rounded.append([round(num, 1) for num in p.tolist()])

    #if you haven't been given a dictionary of colors
    if lcolors is None:
        plotcolors = autoplotcolors
    else:
        plotcolors = []
        temp_setname_list = []
        #if there is a dictionary of colors
        if isinstance(lcolors,dict):
            for setname in dataset_labels:
                if setname in lcolors.keys():
                    pclr = matplotlib.colors.to_rgba_array(lcolors[setname], alpha=None)
                    plotcolors.append((setname, pclr[0]))
                else:
                    #if the setname isn't in the list, put it aside for now
                    temp_setname_list.append(setname)

            plotcolors_rounded = []
            for p in plotcolors:
                plotcolors_rounded.append([round(num, 0) for num in p[1]])

            if len(temp_setname_list) != 0:
                #if there are setnames not in the color list, get them from the auto colors
                colorcount = 0
                for setname in temp_setname_list:
                    #newcolor = [item for item in autoplotcolors if setname in item[0]]
                    newcolor_rounded = pclrs_rounded[colorcount]
                    colorcount += 1
                    #newcolor_rounded = [round(num, 0) for num in list(newcolor)]
                    for color in plotcolors_rounded:
                        if len(color) == len(newcolor_rounded) and len(color) == sum(
                                [1 for i, j in zip(color, newcolor_rounded) if i == j]):
                            print("The color for ", setname,
                                  "is identical to the color used by another setname. Please add a color value for ",
                                  setname, "to aggregated_plotting_configuration.yaml. Setting the current plot color to black.")
                            #newcolor = ['k']
                            print('trying again')
                            continue
                        else:
                            print('colors do not match')
                            newcolor = newcolor_rounded
                            plotcolors.append((setname,np.array(newcolor)))
                            break
        else:
            #if there's a single color passed in
            for setname in dataset_labels:
                plotcolors.append((setname, lcolors))

    #convert the list of tuples to a dataframe with the setnames as keys
    plotcolorsdict = dict()
    for key, value in plotcolors:
        #plotcolorsdict.setdefault(key,[]).append(value)
        plotcolorsdict[key] = value

    return plotcolorsdict


def find_plot_pairs(dataset_labels, use_reference_set=None, reference_set=None):
    # Check if the user has given the option for a reference set in the config file.
    # If there is a reference set chosen, then create a zipped list of pairs:
    plot_pairs = []
    if use_reference_set:
        print('use reference set', reference_set)
        for l in dataset_labels:
            if l != reference_set:
                plot_pairs.extend(zip([reference_set], [l]))
    else:
        print('no reference set')
        for first, second in zip(dataset_labels, dataset_labels[1:]):
            if first != second:
                plot_pairs.extend(zip([first], [second]))

    return plot_pairs
############################################################
# bin the data for an individual track
############################################################

def weighted_average(data, weights):
    try:
        avg = np.average(data[data.notnull()], weights=weights[data.notnull()])
    except ZeroDivisionError:
        avg = np.nan
    return avg

def grouped_mean(df, skill):
    df_grouped = df.groupby('time_since_start')
    df_mean = df_grouped.apply(
        lambda x: pd.Series({skill: weighted_average(x[skill], x['count']),
                             'count': x[x[skill].notnull()]['count'].sum()}))
    df_mean = df_mean.reset_index()
    df_mean['hours since start'] = df_mean.time_since_start.astype('timedelta64[h]')
    return df_mean

def bin_skills_individual_drifter(ds, skill):
    all_tracks = pd.DataFrame()

    for m in ds.model_run.values:
        start = datetime.datetime.strptime(ds['mod_start_date'].values[m], '%Y-%m-%d %H:%M:%S')
        df_track = ds.sel(model_run=m).to_dataframe()
        df_track = df_track.set_index('time')
        # Need to keep track of the counts in order to do the drifter average properly
        # Use base in case model does not start right on the hour.
        df_count = df_track[skill].dropna().sort_index().resample('1H', closed='right', label='right', base=start.minute / 60.).count()
        df_track = df_track[skill].dropna().sort_index().resample('1H', closed='right', label='right', base=start.minute / 60.).mean()
        df_track = df_track.reset_index()
        df_track['time_since_start'] = df_track['time'] - start
        df_track['model_run'] = m
        df_track['count'] = df_count.values
        all_tracks = pd.concat([all_tracks, df_track])
    all_tracks = all_tracks.reset_index()
    all_tracks['hours since start'] = all_tracks.time_since_start.astype('timedelta64[h]')
    # Now find mean of all tracks
    drifter_mean = grouped_mean(all_tracks, skill)
    return drifter_mean, all_tracks

def grouped_IQR(df, skill):
    df_grouped = df.groupby('time_since_start')
    df_iqr = df_grouped.apply(
        lambda x: pd.Series({'25th percentile': x[skill].quantile(0.25),
                             '75th percentile': x[skill].quantile(0.75)}))
    df_iqr = df_iqr.reset_index()
    df_iqr['hours since start'] = df_iqr.time_since_start.astype('timedelta64[h]')
    return df_iqr


def bin_skills_all_drifters(ds_list, skill):
    all_drifters_all_tracks = pd.DataFrame()
    for ds in ds_list:
        drifter_mean, drifter_tracks = bin_skills_individual_drifter(ds, skill)
        drifter_tracks['buoyid'] = ds.obs_buoyid
        all_drifters_all_tracks = pd.concat([all_drifters_all_tracks, drifter_tracks])
    all_drifter_all_tracks = all_drifters_all_tracks.reset_index()
    all_drifter_all_tracks['hours since start'] = all_drifter_all_tracks.time_since_start.astype('timedelta64[h]')
    # Now find mean of all drifters and all tracks
    all_mean = grouped_mean(all_drifters_all_tracks, skill)

    return all_mean, all_drifters_all_tracks

'''
def bin_skills_all_drifters(ds_list, skill):
    all_drifters_all_tracks = pd.DataFrame()
    for ds in ds_list:
        drifter_mean, drifter_tracks = bin_skills_individual_drifter(ds, skill)
        drifter_tracks['obs_buoyid'] = ds.obs_buoyid
        all_drifters_all_tracks = pd.concat([all_drifters_all_tracks, drifter_tracks])
    all_drifter_all_tracks = all_drifters_all_tracks.reset_index()
    all_drifter_all_tracks['hours since start'] = all_drifter_all_tracks.time_since_start.astype('timedelta64[h]')
    # Now find mean of all drifters and all tracks
    all_mean = grouped_mean(all_drifters_all_tracks, skill)

    return all_mean, all_drifters_all_tracks
'''

############################################################
# bin the data for all tracks in multiple sets and return
# a list of binned dataframes (each df contains all tracks
# for a given data set)
############################################################
def create_master_binned_dataframe(datadir, buoyids, skill, duration, sampling_freq, extra_bin=None):

    if extra_bin is not False:
        extra_bin = True

    masterdict = {}
    for drifterid in buoyids:

        drifterdict = drifter2datasets(datadir, drifterid)
        # return: a dictionary with data labels as keys and associated datasets as values per drifter
        for setnameplace in range(0,len(drifterdict.keys())):
            s = list(drifterdict.keys())[setnameplace]
            #for each dataset for each model, feed into bin skill, df comes out. add to a list of dfs, then concat

            ds = drifterdict[s]
            df_res, all_tracks = bin_skills_individual_drifter(ds, skill)
            df_res.insert(0, 'buoyid', drifterid, True)

            if s in masterdict.keys():
                masterdict[s].append(df_res)
            else:
                masterdict[s] = [df_res]

    #need result to be per model. combine all the lists of dataframes into one dataframe
    for key, value in masterdict.items():
        masterdict[key] = pd.concat(value)

    #masterdict is now a dict with labels for keys and concatonated binned dfs for values
    binneddict = {}
    prebindict = {}
    for s in masterdict.keys():
        df_res_all = masterdict[s]
        df_res_all = df_res_all.set_index('time_since_start')
        prebindict[s] = df_res_all

        if extra_bin:
            df_res_combined = df_res_all[skill].dropna().sort_index().resample(sampling_freq, closed='right', label='right').mean() #.interpolate()
            df_res_combined = df_res_combined.reset_index()
            df_res_combined['hours since start'] = df_res_combined.time_since_start.astype('timedelta64[h]')
            binneddict[s] = df_res_combined

    if extra_bin:
        return binneddict
    else:
        return prebindict

######################################################################################################################################
# plotting functions
######################################################################################################################################

############################################################
# Plot skill scores by drifter
############################################################
def plot_skills(drifterid, ds, setname, skill, lcolors, skill_label_params, opacity, labs=True, leg=False, add_markers=True, ax=None):
    """ Plot skill scores by drifter (currently works for molcard, liu, sep, sutherland)"""
    #Note: currently works for liu, molcard, sep, sutherland, obs_dist, obs_disp, mod_dist, mod_disp
    #In the future, can leverage ds attributes for labels: skill:long_name, skill:units (ex, liu:units)

    #decide whether to save the figure or add it to a subplot with other figures
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if skill is 'logobsratio':
        plotlog=True
        skill = 'obsratio'
    elif skill is 'logmodratio':
        plotlog=True
        skill = 'modratio'
    else:
        plotlog=False

    #call the binning function
    df_res, all_tracks = bin_skills_individual_drifter(ds, skill)
    df_res = df_res[df_res['hours since start'] > 0]

    #if using sep, dists or disps, convert the units to km
    if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
        df_res[skill]=df_res[skill]/1000.

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    if isinstance(lcolors,dict):
        if setname in lcolors.keys():
            plotcolor = lcolors[setname]
        else:
            plotcolor = 'b'
    else:
        plotcolor = lcolors

    #add the plot
    if plotlog is True:
        ylab = 'log(' + ylab + ')'
        ax.semilogy(df_res['hours since start'], df_res[skill], color=plotcolor, alpha=opacity, label=yleg)
    else:
        ax.plot(df_res['hours since start'], df_res[skill], color=plotcolor, alpha=opacity, label=yleg)  # str(ds.ocean_model))
        if add_markers is True:
            ax.plot(df_res['hours since start'], df_res[skill], 'o', color=plotcolor, markersize=3,  alpha=opacity)  # str(ds.ocean_model))
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)

    if labs == True:
        ax.set_ylabel(ylab, fontsize=12)
        ax.set_xlabel('hours since start', fontsize=12)

    if leg == True:
        ax.legend(fontsize=12)

    #save the plot or show it
    skfignamestr="{}_{}_{}_{}".format(str(drifterid), str(ds.ocean_model), str(ds.drift_model), skill)
    if savefig:
        savedir = os.path.join(datadir,'aggregated_plots/skills_per_drifter')
        plt.savefig(os.path.join(savedir,[skfignamestr,'.png']),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        #plt.show()
        pass

############################################################
# Plot binned skill score for entire analysis set
############################################################
#def plot_average_skill(buoyids, datadir, savedir, skill, duration, lcolors, skill_label_params, lwidth, sampling_freq, leg=False, ax=None):
def plot_average_skill(df_res_combined, setname, savedir, skill, lcolors, skill_label_params, lwidth, leg=False, ax=None):

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11, 8.5))
        savefig = True

    if skill is 'logobsratio':
        plotlog = True
        skill = 'obsratio'
    elif skill is 'logmodratio':
        plotlog = True
        skill = 'modratio'
    else:
        plotlog = False

    # if using sep, dists or disps, convert the units to km
    if (skill == 'sep') or (skill == 'obs_disp') or (skill == 'obs_dist') or (skill == 'mod_disp') or (
            skill == 'mod_dist'):
        df_res_combined[skill] = df_res_combined[skill] / 1000.
    df_res_combined = df_res_combined[df_res_combined['hours since start'] > 0]

    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    if isinstance(lcolors,dict):
        if setname in lcolors.keys():
            plotcolor = lcolors[setname]
        else:
            plotcolor = 'b'
    else:
        plotcolor = lcolors

    # make the plot
    if plotlog is True:
        ylab = 'log(' + ylab + ')'
        #ax.semilogy(df_res_combined['hours since start'][1:-1], df_res_combined[skill][1:-1], plotcolor, linewidth=lwidth, label=setname)
        ax.semilogy(df_res_combined['hours since start'], df_res_combined[skill], color=plotcolor,
                    linewidth=lwidth, label=setname)
    else:
        #ax.plot(df_res_combined['hours since start'][1:-1], df_res_combined[skill][1:-1], plotcolor, linewidth=lwidth, label=setname)
        ax.plot(df_res_combined['hours since start'], df_res_combined[skill], color=plotcolor,
                linewidth=lwidth, label=setname)

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=12)

    '''
    # save the plot or show it
    combinedskfignamepng = "aggregated_mean_{}.png".format(skill)
    if savefig:
        plt.savefig(os.path.join(savedir, combinedskfignamepng), bbox_inches='tight', dpi=300, papertype='letter',
                    orientation='portrait')
        plt.close()
    else:
        pass
    '''

############################################################
# Plot the average skill for the entire analysis set with
# the individual skills for each drifter (individual
# scores are un-binned)
############################################################
def plot_skill_with_indv(buoyids, datadir, savedir, skill, duration, lcolors, skill_label_params, lwidth, sampling_freq,lab=True, leg=False, ax=None):
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    for drifterid in buoyids:
        print(drifterid)
        dataset_labels, datasets_list, datasets_dict, buoyids = prep_files(datadir, buoyids=[drifterid])
        drifterds = datasets_dict
        #fig, ax = plt.subplots(figsize=(8.5, 11))
        for setname in dataset_labels:
            subdrifterds = drifterds[setname][0]
            print(lcolors)
            plot_skills(drifterid, subdrifterds, setname, skill, lcolors, skill_label_params, 0.8, True, False, add_markers=False, ax=ax)
            handles, labels = ax.get_legend_handles_labels()
            newlabels = list(dataset_labels)

    #plot average
    dataset_labels, datasets_list, datasets_dict, buoyids = prep_files(datadir)
    for setname in datasets_dict.keys():
        all_mean, all_drifters_all_tracks = bin_skills_all_drifters(datasets_dict[setname], skill)
        plot_average_skill(all_mean, setname, savedir, skill, lcolors, skill_label_params, 3, leg=True, ax=ax)

    ax.legend(handles, newlabels, loc='best', fontsize=10)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    if savefig:
        avgskillfignamepng = "mean_{}_withindv.png".format(skill)
        plt.savefig(os.path.join(savedir,avgskillfignamepng),bbox_inches='tight',dpi=300)
        plt.close()


'''
def plot_skill_with_indv(buoyids, datadir, savedir, skill, duration, lcolors, skill_label_params,  lwidth, sampling_freq,lab=True, leg=False, ax=None):
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True
    for drifterid in buoyids:
        drifterds = drifter2datasets(datadir, drifterid)
        for modelrun in drifterds.keys():
            plot_skills(drifterid, drifterds[modelrun], skill, 'gray', skill_label_params, 0.4, True, False, ax=ax)
    plot_average_skill(buoyids, datadir, savedir, skill, duration, lcolors, skill_label_params, lwidth, sampling_freq, leg, ax=ax)
    fig.tight_layout(rect=[0, 0.03, 1, 0.95])

    if savefig:
        avgskillfignamepng = "mean_{}_withindv.png".format(skill)
        plt.savefig(os.path.join(savedir,avgskillfignamepng),bbox_inches='tight',dpi=300)
        plt.close()
'''
############################################################
# Plot the average skill plus filled range
############################################################
def plot_skill_with_filled(datasets_dict, dataset_labels, savedir, skill, plotcolors, skill_label_params, lwidth, filledrangetype, lab=True, leg=False, ax=None):
    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11, 8.5))
        savefig = True

    for setname in datasets_dict.keys():
        print(setname)
        all_mean, all_drifters_all_tracks = bin_skills_all_drifters(datasets_dict[setname], skill)
        print('done binning', str(datetime.datetime.now()))
        plot_filled_skill(all_drifters_all_tracks, setname, savedir, skill, plotcolors[setname], skill_label_params, 0.5, filledrangetype, leg=True, ax=ax)
        print('done plotting filled', str(datetime.datetime.now()))
        plot_average_skill(all_mean, setname, savedir, skill, plotcolors[setname], skill_label_params, lwidth, leg=leg, ax=ax)
        print('done plotting average', str(datetime.datetime.now()))

    if savefig is True:
        combinedskfignamepng = "mean_{}_{}.png".format(skill,filledrangetype)
        plt.savefig(os.path.join(savedir, combinedskfignamepng), bbox_inches='tight', dpi=300, papertype='letter',
                orientation='portrait')
        plt.close()

############################################################
# plot only a filled range calculated from the average skill
############################################################
def plot_filled_skill(all_drifters_all_tracks, setname, savedir, skill, lcolors, skill_label_params, lwidth, filledrangetype, leg=False, ax=None):

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if filledrangetype == 'IQR':
        df_grouped = all_drifters_all_tracks.groupby('time_since_start')
        df_filledrange = df_grouped.apply(lambda x: pd.Series({'25th percentile': x[skill].quantile(0.25),'75th percentile': x[skill].quantile(0.75)}))
        df_filledrange = df_filledrange.reset_index()
        df_filledrange.rename(columns={'25th percentile': 'lower', '75th percentile': 'upper'}, inplace=True)
        leg_str = 'Interquartile Range'

    elif filledrangetype == '1std':
        df_grouped = all_drifters_all_tracks.groupby('time_since_start')
        df_filledrange = df_grouped.apply(lambda x: pd.Series({'std': x[skill].std(),'1std_upper': (x[skill]+x[skill].std()),'1std_lower': (x[skill]-x[skill].std())}))
        df_filledrange = df_filledrange.reset_index()
        df_filledrange['upper'] = df_filledrange['1std_upper']
        df_filledrange['lower'] = df_filledrange['1std_lower']
        leg_str = '1std'

    elif filledrangetype == 'extremes':
        df_grouped = all_drifters_all_tracks.dropna().groupby('time_since_start')
        df_filledrange = df_grouped.apply(lambda x: pd.Series({'min_values': x[skill].min(),'max_values': x[skill].max()}))
        df_filledrange = df_filledrange.reset_index()
        df_filledrange['upper'] = df_filledrange['max_values'] #df_max_values.values
        df_filledrange['lower'] = df_filledrange['min_values'] #df_min_values.values
        leg_str = 'extremes'
    else:
        print('you need to pass in a filledrangetype')
    

    #if using sep, dists or disps, convert the units to km
    if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
        #df_filledrange[skill]=df_filledrange[skill]/1000.
        df_filledrange['upper'] = df_filledrange['upper']/1000.
        df_filledrange['lower'] = df_filledrange['lower']/1000.


    ylab = str(skill_label_params[skill]['ylab'])
    yleg = str(skill_label_params[skill]['yleg'])

    if isinstance(lcolors,dict):
        if setname in lcolors.keys():
            plotcolor = lcolors[setname]
        else:
            plotcolor = 'b'
    else:
        plotcolor = lcolors

    #to remove a spike in the skill scores at zero
    df_filledrange['hours_since_start'] = df_filledrange['time_since_start'].apply(lambda x: np.ceil(x.total_seconds() / 3600))
    df_filledrange = df_filledrange[df_filledrange['hours_since_start'] > 0].copy()

    #make the plot
    ax.fill_between(df_filledrange['hours_since_start'].values,df_filledrange['lower'].values,df_filledrange['upper'].values,alpha=0.2,facecolor=plotcolor,label=leg_str)
    ax.plot(df_filledrange['hours_since_start'].values,df_filledrange['upper'].values,'k',linewidth=lwidth, label='_nolegend_')
    ax.plot(df_filledrange['hours_since_start'].values,df_filledrange['lower'].values,'k',linewidth=lwidth, label='_nolegend_')

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('time since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=12)

    #save the plot or show it
    combinedskfignamepng="mean_{}_{}.png".format(skill,filledrangetype)
    if savefig:
        print('saving ', os.path.join(savedir,combinedskfignamepng))
        plt.savefig(os.path.join(savedir,combinedskfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        #plt.show()
        pass

'''
#old version
def plot_filled_skill(buoyids, datadir, savedir, skill, duration, lcolors, skill_label_params, lwidth, sampling_freq, filledrangetype, leg=False, ax=None):

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11,8.5))
        savefig = True

    if skill is 'logobsratio':
        plotlog=True
        skill = 'obsratio'
    elif skill is 'logmodratio':
        plotlog=True
        skill = 'modratio'
    else:
        plotlog=False

    #created dictionary of binned dataframes of the data
    df_res_combined_dict = create_master_binned_dataframe(datadir, buoyids, skill, duration, sampling_freq)
    df_res_all_dict = create_master_binned_dataframe(datadir, buoyids, skill, duration, sampling_freq, extra_bin=False)
    print('finished binning data')

    #for each set of data
    for setname in df_res_combined_dict.keys():
        df_res_combined = df_res_combined_dict[setname]
        df_res_all = df_res_all_dict[setname]

        if filledrangetype == 'IQR':
            df_grouped = df_res_all.groupby('time_since_start')
            df_iqr = df_grouped.apply(lambda x: pd.Series({'25th percentile': x[skill].quantile(0.25),'75th percentile': x[skill].quantile(0.75)}))
            df_iqr = df_iqr.reset_index()
            df_res_combined['upper'] = df_iqr['25th percentile']
            df_res_combined['lower'] = df_iqr['75th percentile']
            leg_str = 'Interquartile Range'

        elif filledrangetype == '1std':
            df_grouped = df_res_all.groupby('time_since_start')
            df_std = df_grouped.apply(lambda x: pd.Series({'std': x[skill].std(),'1std_upper': (x[skill]+x[skill].std()),'1std_lower': (x[skill]-x[skill].std())}))
            df_std = df_std.reset_index()
            df_res_combined['upper'] = df_std['1std_upper']
            df_res_combined['lower'] = df_std['1std_lower']
            leg_str = '1std'

        elif filledrangetype == 'extremes':
            df_grouped = df_res_all.dropna().groupby('time_since_start')
            df_extremes = df_grouped.apply(lambda x: pd.Series({'min_values': x[skill].min(),'max_values': x[skill].max()}))
            df_extremes = df_extremes.reset_index()
            ##df_min_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').min().interpolate()
            #df_min_values = df_min_values.reset_index()
            ##df_max_values = df_res_all[skill].dropna().sort_index().resample('1H', label='right').max().interpolate()
            #df_max_values = df_max_values.reset_index()
            df_res_combined['upper'] = df_extremes['max_values'] #df_max_values.values
            df_res_combined['lower'] = df_extremes['min_values'] #df_min_values.values
            leg_str = 'extremes'
        else:
            print('you need to pass in a filledrangetype')

        #if using sep, dists or disps, convert the units to km
        if (skill =='sep') or (skill=='obs_disp') or (skill=='obs_dist') or (skill=='mod_disp') or (skill=='mod_dist'):
            df_res_combined[skill]=df_res_combined[skill]/1000.
            df_res_combined['upper'] = df_res_combined['upper']/1000.
            df_res_combined['lower'] = df_res_combined['lower']/1000.

        ylab = str(skill_label_params[skill]['ylab'])
        yleg = str(skill_label_params[skill]['yleg'])

        if isinstance(lcolors,dict):
            if setname in lcolors.keys():
                plotcolor = lcolors[setname]
            else:
                plotcolor = 'b'
        else:
            plotcolor = lcolors

        #to remove a spike in the skill scores at zero
        df_res_combined = df_res_combined[df_res_combined['hours since start'] > 0].copy()

        #make the plot
        if plotlog is True:
            ylab = 'log(' + ylab + ')'
            ax.semilogy(df_res_combined['hours since start'], df_res_combined[skill], plotcolor, linewidth=lwidth, label=yleg)
        else:
            ax.fill_between(df_res_combined['hours since start'],df_res_combined['lower'],df_res_combined['upper'],alpha=0.2,facecolor=plotcolor,label=leg_str)
            ax.plot(df_res_combined['hours since start'],df_res_combined['upper'],'k',linewidth=0.5, label='_nolegend_')
            ax.plot(df_res_combined['hours since start'],df_res_combined['lower'],'k',linewidth=0.5, label='_nolegend_')

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    ax.grid(which='minor', alpha=0.2)
    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=12)

    #save the plot or show it
    combinedskfignamepng="mean_{}_{}.png".format(skill,filledrangetype)
    if savefig:
        print('saving ', os.path.join(savedir,combinedskfignamepng))
        plt.savefig(os.path.join(savedir,combinedskfignamepng),bbox_inches='tight',dpi=300,papertype='letter',orientation='portrait')
        plt.close()
    else:
        #plt.show()
        pass
'''

############################################################
############################################################
'''
def plot_subtracted_skill(df1, df2, plotlabel, savedir, skill, lcolors, skill_label_params, lwidth=None, leg=False, ax=None):

    savefig = False
    if ax is None:
        fig, ax = plt.subplots(figsize=(11, 8.5))
        savefig = True

    subdf = df1[['time_since_start', 'hours since start']].copy()
    subdf[skill] = df1[skill].sub(df2[skill])
    subdf = subdf[subdf['hours since start'] > 0].copy()

    # if using sep, dists or disps, convert the units to km
    if (skill == 'sep') or (skill == 'obs_disp') or (skill == 'obs_dist') or (skill == 'mod_disp') or (
            skill == 'mod_dist'):
        subdf[skill] = subdf[skill] / 1000.

    # add the plot
    ax.plot(subdf['hours since start'], subdf[skill], label=(str(plotlabel)), linewidth=lwidth, zorder=2)  # label=str('-'.join(modelruns)))

    plotlims = []
    timemax = len(subdf['hours since start'])
    phigh = float(np.nanmax(subdf[skill].values))
    plow = float(abs(np.nanmin(subdf[skill].values)))
    pmax = max(phigh, plow)
    plotlims.append(pmax)

    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    # ax.grid(which='minor', alpha=0.2)

    ylab = str(skill_label_params[skill]['ylab']) + '\n' + str(plotlabel)
    yleg = str(skill_label_params[skill]['yleg'])

    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    # ax.set_title('Average of mean for all drifters', fontsize=14)

    if leg == True:
        ax.legend(fontsize=12)

    plt.axhline(y=0, color='k', linewidth=1, linestyle=':', zorder=1)
    plt.xlim([0, len(subdf['hours since start'])])
    plt.ylim(max(plotlims) * -1, max(plotlims))

    # save the plot or show it
    subtractedskfignamepng = "comparison_{}_skills-diff_{}.png".format(str(plotlabel), skill)
    if savefig:
        plt.savefig(os.path.join(savedir, subtractedskfignamepng), bbox_inches='tight', dpi=300)
        print(os.path.join(savedir, subtractedskfignamepng))
        plt.close()
    else:
        pass

    return pmax, timemax
'''

def plot_subtracted_skill(df1, df2, skill, plotlabel, skill_label_params, lwidth, zorder, lcolor=None, ax=None):
    subdf = df1[['time_since_start', 'hours since start']].copy()
    subdf[skill] = df1[skill].sub(df2[skill])
    subdf = subdf[subdf['hours since start'] > 0].copy()

    # if using sep, dists or disps, convert the units to km
    if (skill == 'sep') or (skill == 'obs_disp') or (skill == 'obs_dist') or (skill == 'mod_disp') or (
            skill == 'mod_dist'):
        subdf[skill] = subdf[skill] / 1000.

    diff = subdf[skill].values

    if ax is None:
        fig, ax = plt.subplots(figsize=(11, 8.5))

    print(plotlabel)

    ax.plot(subdf['hours since start'].values, diff, label=str(plotlabel), color=lcolor, linewidth=lwidth, zorder=zorder)
    plim = max(abs(diff))
    tlim = max(subdf['hours since start'].values)

    # plot parameters
    ax.minorticks_on()
    ax.grid(which='major', alpha=0.4)
    # ax.grid(which='minor', alpha=0.2)

    ylab = str(skill_label_params[skill]['ylab']) + '\n' + plotlabel

    ax.set_xlabel('hours since start', fontsize=12)
    ax.set_ylabel(ylab, fontsize=12)
    # ax.set_title('Average of mean for all drifters', fontsize=14)

    ax.legend(fontsize=12)
    plt.axhline(y=0, color='k', linewidth=1, linestyle=':', zorder=1)

    return plim, tlim