"""
Run Drift Patterns Workflow
=============================
:Author: Samuel T. Babalola, Clyde Clements
:Created: 2018-07-17
"""
import collections
import datetime
import os
from os.path import abspath, isabs, join as joinpath
from os import path
import shutil
import sys

import dateutil.parser
import dateutil.rrule
import xarray as xr
import numpy as np

from . import configargparse
from .add_traj_data_date import (add_traj_data_date_to_datafile, 
                                 add_attributes_to_dataset)
from .assemble_ocean_data import assemble_ocean_data
from .assemble_ocean_metadata import assemble_ocean_metadata
from .make_ariane_namelist import make_ariane_namelist
from .run_ariane import run_ariane
from .verify_ocean_data import verify_ocean_data
from .ioutils import dump_yaml
from .utils import (defaults, logger, set_run_name)
from driftutils.set_mapped_initial_position import generate_particle_grid
from . import utils


LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max', 'lon_min', 'lon_max'))
def drift_map(
        *,
        experiment_dir,
        ocean_data_dir,
        ocean_mesh_file,
        ocean_model_name,
        first_start_date,
        last_start_date,
        drift_duration,
        num_particles_x,
        num_particles_y,
        initial_bbox = None,
        drifter_depth='1c',
        drift_model_name='Ariane',
        ariane_config_file='ariane.yaml',
        ariane_exec='ariane',
        lon_var='nav_lon',
        lat_var='nav_lat',
        ulon_var='glamu',
        ulat_var='gphiu',
        vlon_var='glamv',
        vlat_var='gphiv',
        wdep_var='gdepw_1d',
        tmask_var='tmask',
        interp_method='nearest',            
        start_date_frequency='daily', start_date_interval=1,
        xwatervel=defaults['xwatervel']['nc_name'],
        ywatervel=defaults['ywatervel']['nc_name'],
        zwatervel=None,
        temperature=None,
        salinity=None,
        density=None):
    """Run drift map workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    ocean_mesh_file : str
        Path to mesh file for ocean model.
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    first_start_date : datetime.datetime
        Start date for first drift simulation.
    last_start_date : datetime.datetime
        Start date for last drift simulation.
    drift_duration : datetime.timedelta
        Duration of drift.
    num_particles_x : float
        number of x particles
    num_particles_y : float
        number of y  particles
    initial_bbox : str
        Specify coordinates for ocean model data
    drifter_depth : str
        Specifies the depth for drift trajectory simulations. If the string
        begins with 'L', it indicates the corresponding depth layer for the
        W (vertical) velocity gridpoints; otherwise, it indicates the depth in
        meters. If the string ends with 'c', it indicates a constant-layer
        (or fixed depth) trajectory calculation; otherwise, the particle is
        free to move in the vertical direction.
    drift_model_name : str
        Name of drift model (one of "Ariane", "MLDP" or "OpenDrift").
    ariane_config_file : str
        Path to configuration file specifying further parameters for Ariane.
    ariane_exec : str
        Path to Ariane executable.
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    ulon_var : str
        Name of U longitude variable in mesh file.
    ulat_var : str
        Name of U latitude variable in mesh file.
    vlon_var : str
        Name of V longitude variable in mesh file.
    vlat_var : str
        Name of V latitude variable in mesh file.
    wdep_var : str
        Name of W depth variable in mesh file.
    tmask_var : str
        Name of variable containing land/ocean mask for T grid points in mesh
        file.
    interp_method : str
        Interpolation method to use to find grid point closest to drifter
        location.
    start_date_frequency : str
        Identifies the type of recurrence rule for start dates. The default
        value is "daily", to specify repeating events based on an interval of
        a day or more.
    start_date_interval : int
        A positive integer representing how often the recurrence rule for start
        dates repeats. The default value is 1, meaning every day for a daily
        rule. A value of 2 would mean every two days for a daily rule and so
        on.
    xwatervel : str
        Name of variable containing X water velocity
    ywatervel : str
        Name of variable containing Y water velocity
    zwatervel : str
        Name of variable containing Z water velocity
    temperature : str
        Name of variable containing temperature
    salinity : str
        Name of variable containing salinity
    density : str
        Name of variable containing density
    """

    # Track details about each run such as run directory and the workflow start
    # and finish times.
    
    run_details = {
        'ocean_model': ocean_model_name,
        'runs': {},
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }
    run_details_file = abspath(joinpath(experiment_dir, 'runs.yaml'))
        
    ocean_metadata_file = abspath(joinpath(experiment_dir, 'ocean_data.yaml'))
    if not isabs(ariane_config_file):
        ariane_config_file = abspath(ariane_config_file)

    logger.info('Creating workflow directory...')
    os.makedirs(experiment_dir)
    
    # Create an output file
    os.chdir(experiment_dir)        
    os.makedirs('output')
    output_file_dir = os.getcwd()
    output_file_dir = '{}/output'.format(output_file_dir)
   
    assemble_ocean_metadata(
        ocean_data_dir, ocean_mesh_file, ocean_metadata_file,
        xwatervel=xwatervel, ywatervel=ywatervel, zwatervel=zwatervel,
        temperature=temperature, salinity=salinity, density=density)

    verify_ocean_data(
        ocean_metadata_file, first_start_date, last_start_date, drift_duration)

    rrule_freq_dict = {
        'yearly': dateutil.rrule.YEARLY, 'monthly': dateutil.rrule.MONTHLY,
        'weekly':dateutil.rrule.WEEKLY, 'daily': dateutil.rrule.DAILY,
        'hourly': dateutil.rrule.HOURLY, 'minutely': dateutil.rrule.MINUTELY,
        'secondly': dateutil.rrule.SECONDLY
    }
    
    rrule = dateutil.rrule.rrule(
        freq=rrule_freq_dict[start_date_frequency],
        interval=start_date_interval, dtstart=first_start_date,
        until=last_start_date)

    if rrule.count() > 1:
        rrule_increment = rrule[1] - rrule[0]
    else:
        rrule_increment = datetime.timedelta(0, 0)
    logger.info(
        ('Trajectories will be calculated with starting dates running from %s '
         'to %s of duration %s with an increment of %s between dates'),
        first_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        last_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        str(drift_duration), str(rrule_increment))
   
   # Open mesh file to get alternative coordinates
    ds = xr.open_dataset(ocean_mesh_file)
    mesh_lon_min = np.min(ds[lon_var].values[np.nonzero(ds[lon_var].values)])
    mesh_lon_max = np.max(ds[lon_var].values[np.nonzero(ds[lon_var].values)])
    mesh_lat_min = np.min(ds[lat_var].values[np.nonzero(ds[lat_var].values)])
    mesh_lat_max = np.max(ds[lat_var].values[np.nonzero(ds[lat_var].values)])
   
   # Set coordinates for particle grid
    if initial_bbox is not None:
        initial_bbox = initial_bbox.split(" ")
        bbox_lon_min=float(initial_bbox[0]) 
        bbox_lat_min=float(initial_bbox[1]) 
        bbox_lon_max=float(initial_bbox[2]) 
        bbox_lat_max=float(initial_bbox[3])
        
        if bbox_lon_min < mesh_lon_min:
            logger.info('Minimum longitude below allowed range. ' 
                        'Minimum longitude set to {}'.format(mesh_lon_min))
            bbox_lon_min = mesh_lon_min
        if bbox_lon_max > mesh_lon_max:
            logger.info('Maximum longitude exceeds allowed range. ' 
                        'Maximum longitude set to {}'.format(mesh_lon_max))
            bbox_lon_max = mesh_lon_max 
        if bbox_lat_min < mesh_lat_min:
            logger.info('Minimum latitude below allowed range. ' 
                        'Minimum latitude set to {}'.format(mesh_lat_min))
            bbox_lat_min = mesh_lat_min
        if bbox_lat_max > mesh_lat_max:
            logger.info('Maximum latitude exceeds allowed range. ' 
                        'Maximum latitude set to {}'.format(mesh_lat_max))
            bbox_lat_max = mesh_lat_max
        initial_bbox = LatLonBoundingBox(lon_min = bbox_lon_min, lat_min = bbox_lat_min, 
                                         lon_max = bbox_lon_max, lat_max = bbox_lat_max)
    else:
        logger.info("No inputs recorded in initial_bbox. Initializing constant particle grid ")
        # Use coordinates in the mesh file if initial_bbox is none
        initial_bbox = LatLonBoundingBox(lon_min = mesh_lon_min, lat_min = mesh_lat_min, 
                                         lon_max = mesh_lon_max, lat_max = mesh_lat_max)
    
    for start_date in rrule:
        logger.info(
            '\nBeginning trajectory calculations for start date %s...',
            start_date.strftime('%Y-%m-%d %H:%M:%S')
        )
        end_date = start_date + drift_duration
        run_name = set_run_name(ocean_model_name, drift_model_name, start_date, drift_duration)
        run_base_dir = joinpath('runs', run_name)
        ariane_run_dir = joinpath(experiment_dir, run_base_dir)
        os.makedirs(ariane_run_dir)
        cwd = os.getcwd()
        os.chdir(ariane_run_dir)
        data_assembly_dir = 'data'
        ocean_data = assemble_ocean_data(
            ocean_metadata_file, start_date, drift_duration, data_assembly_dir)

        
        drifter_positions, total_time  = generate_particle_grid(start_date=start_date,
            mesh_file=ocean_data['ocean_mesh_file'],
            run_ocean_data_dates=ocean_data['run_ocean_data_dates'],
            drifter_depth=drifter_depth,
            lon_var=lon_var, lat_var=lat_var,
            ulon_var=ulon_var, ulat_var=ulat_var,
            vlon_var=vlon_var, vlat_var=vlat_var,
            wdep_var=wdep_var, tmask_var=tmask_var,
            interp_method=interp_method,
            initial_bbox = initial_bbox,
            num_particles_x = num_particles_x,
            num_particles_y = num_particles_y,
            output_file='drifter_positions.yaml'  
            )

        num_drifters = len(drifter_positions.keys())
        ariane_namelist_file = 'namelist'
        make_ariane_namelist(
            ariane_config_file, os.getcwd(), ocean_data['ocean_data_dir'],
            ocean_data['run_ocean_data_dates'],
            ocean_data['ocean_data_variables'],
            ocean_data['ocean_mesh_file'],num_drifters,
            drift_duration, ariane_namelist_file)

        # Sort positions by buoy ID (which is the key in the positions
        # dictionary).
        initial_positions = []
        for key in sorted(drifter_positions.keys()):
            initial_positions.append(drifter_positions[key])

        # Information about this run.
        run_info = {
            'drift_start_date': start_date.isoformat(),
            'drift_end_date': end_date.isoformat(),
            'ocean_mesh_file': ocean_data['ocean_mesh_file'],
            'ocean_data_files': ocean_data['ocean_data_files'],
            'initial_drifter_grid_positions': drifter_positions,
            'drift_calculation_method': 'ariane',
            'drift_calculation_status': 'in_progress',
            'run_dir': run_base_dir,
            'updated': datetime.datetime.utcnow().isoformat()
        }
        run_details['runs'][run_name] = run_info

        # Write out initial run information *before* calling Ariane. Since the
        # Ariane process may be long running, this allows the user to see some
        # details pertaining to the run while waiting for Ariane to finish.
        # Once Ariane is done, the run information will be updated and written
        # again.
        # NOTE: If this script is ever parallelized, then the next step will
        # need to be adapted to ensure only a single process writes to the
        # output file at any given time.
        dump_yaml(run_details, run_details_file)

        start_time = datetime.datetime.utcnow()
        status = run_ariane(os.getcwd(), ariane_namelist_file,
                            initial_positions, ariane_exec,
                            overwrite_run_dir=True)
        finish_time = datetime.datetime.utcnow()
        run_info.update({
            'drift_calculation_status': 'finished' if status == 0 else 'error',
            'drift_calculation_start_time': start_time.isoformat(),
            'drift_calculation_finish_time': finish_time.isoformat(),
            'ariane_status': status,
            'updated': datetime.datetime.utcnow().isoformat()
        })
        run_details['runs'][run_name] = run_info
        ariane_traj_file = 'ariane_trajectories_qualitative.nc'
        output_file = '{}.nc'.format(run_name)
        run_details['updated'] = datetime.datetime.utcnow().isoformat()
        shutil.copy(ariane_traj_file,output_file_dir)
        # Move Ocean files to the output directory
        os.chdir(output_file_dir)        
        os.rename(ariane_traj_file,output_file)
        add_traj_data_date_to_datafile(output_file, start_date)
        # Add new attributes to dataset
        add_attributes_to_dataset(output_file, run_name, 
                                  drift_duration, total_time)
        dump_yaml(run_details, run_details_file)
        os.chdir(cwd)
        
    now = datetime.datetime.utcnow().isoformat()
    run_details['workflow_finish_time'] = now
    run_details['updated'] = now
    dump_yaml(run_details, run_details_file)


def main():
    from . import cli
    cli.run(drift_map)

if __name__ == '__main__':
    main()

