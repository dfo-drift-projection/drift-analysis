"""
Create MLDP namelist
=============================
:Author: Samuel T. Babalola
:Created: 2018-09-19

This module write MLDP namelist into an input file.
"""
import collections
import xarray as xr
import numpy as np
import glob
import os
import datetime
import time
import sys

from driftutils import configargparse
from driftutils import utils

logger = utils.logger

LatLonBoundingBox = collections.namedtuple('LatLonBoundingBox',
                                          ('lat_min', 'lat_max', 'lon_min', 'lon_max'))


def make_mldp_namelist(data_dir, output_dir, initial_bbox, num_particles):

    mask_file = glob.glob(os.path.join(data_dir, 'mask.in'))
    grid_file = glob.glob(os.path.join(data_dir, 'grid.in'))
    meterological_file = glob.glob(os.path.join(data_dir, '2*'))

    if initial_bbox is not None:
        initial_bbox = initial_bbox.split(" ")
        bbox_lon_min=float(initial_bbox[0])
        bbox_lat_min=float(initial_bbox[1])
        bbox_lon_max=float(initial_bbox[2])
        bbox_lat_max=float(initial_bbox[3])
        initial_bbox = LatLonBoundingBox(lon_min = bbox_lon_min, lat_min = bbox_lat_min,
                                         lon_max = bbox_lon_max, lat_max = bbox_lat_max)

    lon_interval = np.linspace(initial_bbox.lon_min,initial_bbox.lon_max,num_particles)
    lat_interval = np.linspace(initial_bbox.lat_min,initial_bbox.lat_max,num_particles)

    print(lat_interval)
    print(lon_interval)

    file_name = 'MLDP.in'
    filepath = output_dir
    temp_path = filepath
    print(temp_path)
    with open(filepath + file_name, mode="w", encoding="utf-8") as f:
        f.write("\n \
    #----- Model parameters\n \
    \n \
    MDL_DT_INT             = 5        # Internal model time step \[s\]. \n \
    MDL_DT_SUR_TL          = 1.0      # Ratio of diffusion time step over Lagrangian time scale \[dimensionless$ \n \
    MDL_DT_BAS             = 1.0      # Diffusion time step minimum value \[s\]. \n \
    MDL_SIG2_V             = 2.00     # Horizontal wind velocity variance for mesoscale fluctuations \[m2/s2\]. \n \
    MDL_TL_V               = 10800.0  # Lagrangian time scale \[s\]. \n \
    MDL_INCLUDEHORIZDIFF   = 0        # isIncludeHorizDiff Flag indicating if including horizontal diffusion in$ \n \
    MDL_INCLUDESUV         = 0        # Flag indicating if including horizontal wind speed variances in diffusi$ \n \
    MDL_KERNEL             = DRIFTER    # Diffusion kernel selection method (VARIABLE or kernel name) \n \
    MDL_RETRO              = FALSE    # Backward simulation \n \
    MDL_FM_LIM             = 500.0    # FM limit calculations \n \
    MDL_RMS_UV             = 1.0 \n \
    MDL_ADVECT             = RK4 \n \
    \n \
    #----- Oil parameters \n \
    OIL_EMI          = Diesel_2002_1 \n \
    OIL_FATE         = FALSE \n \
    OIL_WIND_VAR     = FALSE \n \
    ")     
        drifter_id = 25710010001
        for lon, lat in zip (lon_interval, lat_interval):
            f.write("\n \
    SRC_NAME             = %s             # Source name ( Drifter ID??)\n \
    SRC_COORD            = %s %s\n \
    SRC_TIME             = 201804030600     # Emission date-time [UTC]: YearMonthDayHourMinute\n \
    SRC_EMI_GAUSS        = FALSE              # Source distribution type\n \
    SRC_EMI_INTERVAL     = 1 0 0.0 0.0 1.0 1  # Nb Parcel, Duration, Bottom of source m [AGL], Top\n \
    SRC_WIND_COEFF       = 0.03               # Source wind coefficient(A.K.A. wind drift factor)\n \
    SRC_CURR_COEFF       = 1.0                # Source current coefficient \n \
        "%(drifter_id, lat, lon))     
            drifter_id = drifter_id + 1

        for mask in mask_file:
            f.write("\n \
    #----- Output parameters\n \
    \n \
    OUT_DT         = 3600       # Output time step [s]\n \
    OUT_DELTA      = 0         # Output file interval [h]\n \
    OUT_GRID       = %s # Output gr$\n \
    "%(mask))
  
        for grid in grid_file:
            f.write("\n \
    #----- Meteorological parameters\n \
    MET_BOTTOM = 0.9999     # Bottom reflection level of particles in the atmosphere [hybrid|eta|sigma]\n \
    MET_MASK   = %s\n \
    MET_FILES  =           # Meteorological input files\n \
    "%(grid))     
    
        for meter in meterological_file:
            f.write("%s\n \
    "%(meter))
            
    print ('Execution completed.')

def main(args=sys.argv[1:]):
    for i, arg in enumerate(args):
        if (arg[0] == '-') and arg[1].isdigit():
            args[i] = ' ' + arg
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add(
        '-c', '--config', is_config_file=True,
        help='Name of configuration file')
    arg_parser.add(
        '--log_level', default='info', choices=utils.log_level.keys(),
        help='Set level for log messages')
    arg_parser.add(
        '--data_dir', type=str, default = '/home/samuel/meteo/',
        help='Path to directory containing meteo gile')
    arg_parser.add(
        '--output_dir', type=str, default = '/home/samuel/dmp/',
        help='Path to directory containing output file')
    arg_parser.add(
        '--initial_bbox', type=str, default = '-127 45 -123 49',
        help='Bounding box for generating particles')
    arg_parser.add(
        '--num_particles', type=float, default = 15,
        help='Number of particles for bounding box')


    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    make_mldp_namelist(config.data_dir, config.output_dir, config.initial_bbox, config.num_particles)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()
