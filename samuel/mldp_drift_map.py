"""
Run Drift Patterns Workflow
=============================
:Author: Samuel T. Babalola, Clyde Clements
:Created: 2018-07-17
"""
import collections
import datetime
import os
from os.path import abspath, isabs, join as joinpath
from os import path
import shutil
import sys

import dateutil.parser
import dateutil.rrule
import xarray as xr
import numpy as np

from driftutils import configargparse
from driftutils.assemble_atm_metadata import assemble_atm_metadata
from driftutils.assemble_ocean_metadata import assemble_ocean_metadata
from driftutils.make_mldp_namelist import make_mldp_namelist
from driftutils.run_mldp import run_mldp
from driftutils.ioutils import dump_yaml
from driftutils.utils import (defaults, logger, set_run_name)
from driftutils import utils

def drift_map(
        *,
        experiment_dir,
        ocean_data_dir,
        atm_data_dir,
        ocean_model_name,
        first_start_date,
        last_start_date,
        drift_duration,
        num_particles_x,
        num_particles_y,
        initial_bbox,
        drifter_depth=0.0 [m],
        drift_model_name ='MLDP',
        mldp_exec='MLDP',
        mldp_config_file='mldp.yaml',
        drifter_id_attr='buoyid',
        drifter_meta_variables=None,
        start_date_frequency='daily',
        start_date_interval=1,
        fcst_hrs=[00, 06, 12, 18]):
    
    """Run drift map workflow.

    Parameters
    ----------
    experiment_dir : str
        Name of directory in which to run the experiment. This is where the
        output files will be generated.
    ocean_data_dir : str
        Path to directory containing ocean-model data files.
    atm_data_dir : str
        Path to atmospheric file for ocean model.
    ocean_model_name : str
        Name of ocean model (e.g. GIOPS or Salish Sea).
    first_start_date : datetime.datetime
        Start date for first drift simulation.
    last_start_date : datetime.datetime
        Start date for last drift simulation.
    drift_duration : datetime.timedelta
        Duration of drift.
    num_particles_x : float
        number of x particles
    num_particles_y : float
        number of y  particles
    initial_bbox : str
        Specify coordinates for ocean model data
    drifter_depth : str
        Specifies the depth for drift trajectory simulations. If the string
        begins with 'L', it indicates the corresponding depth layer for the
        W (vertical) velocity gridpoints; otherwise, it indicates the depth in
        meters. If the string ends with 'c', it indicates a constant-layer
        (or fixed depth) trajectory calculation; otherwise, the particle is
        free to move in the vertical direction.
    drift_model_name : str
        Name of drift model (one of "Ariane", "MLDP" or "OpenDrift").
    mldp_exec : str
        Path to MLDP executable.
    mldp_config_file : str
        Path to configuration file specifying further parameters for MLDP.
    drifter_id_attr : str
        Drifter identification
    drifter_meta_variables : str
        Drifter metadata variables
    start_date_frequency : str
        Identifies the type of recurrence rule for start dates. The default
        value is "daily", to specify repeating events based on an interval of
        a day or more.
    start_date_interval : int
        A positive integer representing how often the recurrence rule for start
        dates repeats. The default value is 1, meaning every day for a daily
        rule. A value of 2 would mean every two days for a daily rule and so
        on.
    fcst_hrs : list
        List of hours for metadata
    """
    
     # Track details about each run such as run directory and the workflow start
    # and finish times.
    
    run_details = {
        'ocean_model': ocean_model_name,
        'runs': {},
        'workflow_start_time': datetime.datetime.utcnow().isoformat()
    }
    run_details_file = abspath(joinpath(experiment_dir, 'runs.yaml'))
        
    ocean_metadata_file = abspath(joinpath(experiment_dir, 'ocean_data.yaml'))
    if not isabs(mldp_config_file):
        mldp_config_file = abspath(mldp_config_file)

    logger.info('Creating workflow directory...')
    os.makedirs(experiment_dir)
    
    # Create an output file
    os.chdir(experiment_dir)        
    os.makedirs('output')
    output_file_dir = os.getcwd()
    output_file_dir = '{}/output'.format(output_file_dir)
        
    assemble_ocean_metadata(
        data_dir, output_file, start_hrs=defaults['start_hrs'])
    
    rrule_freq_dict = {
        'yearly': dateutil.rrule.YEARLY, 'monthly': dateutil.rrule.MONTHLY,
        'weekly':dateutil.rrule.WEEKLY, 'daily': dateutil.rrule.DAILY,
        'hourly': dateutil.rrule.HOURLY, 'minutely': dateutil.rrule.MINUTELY,
        'secondly': dateutil.rrule.SECONDLY
    }
    
    rrule = dateutil.rrule.rrule(
        freq=rrule_freq_dict[start_date_frequency],
        interval=start_date_interval, dtstart=first_start_date,
        until=last_start_date)

    if rrule.count() > 1:
        rrule_increment = rrule[1] - rrule[0]
    else:
        rrule_increment = datetime.timedelta(0, 0)
    logger.info(
        ('Trajectories will be calculated with starting dates running from %s '
         'to %s of duration %s with an increment of %s between dates'),
        first_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        last_start_date.strftime('%Y-%m-%d %H:%M:%S'),
        str(drift_duration), str(rrule_increment))
        
    # Set coordinates for particle grid
    if initial_bbox is not None:
        initial_bbox = initial_bbox.split(" ")
        bbox_lon_min=float(initial_bbox[0]) 
        bbox_lat_min=float(initial_bbox[1]) 
        bbox_lon_max=float(initial_bbox[2]) 
        bbox_lat_max=float(initial_bbox[3])
        
        initial_bbox = LatLonBoundingBox(lon_min = bbox_lon_min, lat_min = bbox_lat_min, 
                                         lon_max = bbox_lon_max, lat_max = bbox_lat_max)
    for start_date in rrule:
        logger.info(
            '\nBeginning trajectory calculations for start date %s...',
            start_date.strftime('%Y-%m-%d %H:%M:%S')
        )
        end_date = start_date + drift_duration
        run_name = set_run_name(ocean_model_name, drift_model_name, start_date, drift_duration)
        run_base_dir = joinpath('runs', run_name)
        mldp_run_dir = joinpath(experiment_dir, run_base_dir)
        os.makedirs(mldp_run_dir)
        cwd = os.getcwd()
        os.chdir(mldp_run_dir)
        #assemble_ocean_atm_data()
        
        make_mldp_namelist(experiment_dir, output_dir, initial_bbox, num_particles)
        
        run_mldp(mldp_exec, namelist, experiment_dir)
        
        covert_files(experiment_dir, output_dir)

def main():
    from driftutils import cli
    cli.run(drift_map)

if __name__ == '__main__':
    main()
