from collections import namedtuple
import hashlib
import os
from os.path import join as joinpath, exists as pathexists
from os import path
import pickle
import sys
import warnings


from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
import glob
import os
import pandas as pd
import datetime
import dateutil
import math

from . import configargparse
from driftutils import ioutils as ioutils
from . import mpl_util
from . import utils

logger = utils.logger


results_dir='/home/samuel/data/drift_tool_test/salish_test_20170415/output/'
# Use glob to find all netcdf files in results_dir
files = glob.glob(os.path.join(results_dir, '*.nc'))



def plot_model_obs_unique(file_name,basemap,bbox):
    """Plot the modelled and observed trajector in a file fname 
     Parameters
     ----------
     file_name : File whose varibles and attributes will be used to plot the trajectories
     basemap : Basemap
             Basemap on which to plot the modelled and observed trajector
     bbox : LatLonBoundingBox
             Specifies the bounding box for the plot. It determines the coordinates for each drifter for accurate plotting 
    
  
    """
    
    # open dataset for driftes 
    logger.info('Plotting trajectories...')
    ds = xr.open_dataset(file_name)
    times = pd.to_datetime(ds.time.values)
    start = times[0]
    end = start +datetime.timedelta(hours=8)
    ds_new = ds.sel(time=slice(start,end))
    basemap.drawcoastlines(linewidth=1.5)
    
    # assign dataset variables for plotting
    x, y =basemap(ds.obs_lon.values, ds.obs_lat.values)
    j,k = basemap(ds_new.mod_lon.values, ds_new.mod_lat.values)
    basemap.plot(x, y, c='k', label='observed',linewidth=2.0)
    basemap.plot(x[0], y[0], 'ko', markersize=2.0, linewidth=1.5)
    basemap.plot(x[-1], y[-1], 'ro', markersize=2.0, linewidth=1.5)
    
    # model trajectory plotted with scatter 
    cm = plt.cm.get_cmap('summer')
    mesh =basemap.scatter(j, k,c=ds_new.sep.values,marker='o', s=15,vmin=0, vmax=10000,cmap=cm,linewidth=2.5)
    c_bar =basemap.colorbar(mesh,location='bottom',pad="5%")
    c_bar.set_label('Separation distance [m]', fontsize = 15)
    basemap.plot(j[0], k[0], 'ko', markersize=2.0, linewidth=1.5)
    basemap.plot(j[-1], k[-1], 'ro', markersize=2.0, linewidth=1.5)
    
    # fill continents, set lake color same as ocean color. 
    basemap.fillcontinents(color='white',lake_color='aqua')
    basemap.drawmapboundary(fill_color='aqua')
    plt.title('Drifter {}'.format(ds.obs_buoyid), fontsize = 25)
    
    parallels and meriands for the maps
    dlat = (bbox.lat_max - bbox.lat_min)/3
    dlon = (bbox.lon_max - bbox.lon_min)/3
    parallels = np.arange(np.floor(bbox.lat_min), np.ceil(bbox.lat_max), dlat)
    basemap.drawparallels(parallels, labels=[1, 0, 0, 0], fontsize=16,
                          linewidth=0.5, color='0.5')
    meridians = np.arange(np.floor(bbox.lon_min), np.ceil(bbox.lon_max), dlon)
    basemap.drawmeridians(meridians, labels=[0, 0, 0, 1], fontsize=16,
                          linewidth=0.5, color='0.5')
    
    ds.close()

    

#def map_features(length=12,breadth=15):
    # ax.set_xlabel('Longitude', fontsize=labelsize)
    # ax.set_ylabel('Latitude', fontsize=labelsize)
    #plt.rcParams['figure.figsize'] = [length,breadth]
    #ax.tick_params(axis='x', labelsize=label_size)
    #ax.tick_params(axis='y', labelsize=label_size)
    

def handle_duplicate_legends(axi):
    """Create legend for each plots and handle duplicated legends due to multipe unique observed id for all maps"""

    # handle duplicated legends 
    handles, labels = axi.get_legend_handles_labels()
    handle_list, label_list = [], []
    for handle, label in zip(handles, labels):
        if label not in label_list:
            handle_list.append(handle)
            label_list.append(label)
    plt.legend(handle_list, label_list, fontsize=18,loc='upper right')


LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

def determine_latlon_bbox_for_drifters(drifters, ratio = 4):
    """Determine the latitude and longitude bounding box for drifter tracks.

    Parameters
    ----------
    drifters : list of xarray.Dataset
        List of drifter tracks. Each drifter must be ``an xarray.Dataset`` with
        variables ``lat`` and ``lon``.

    Returns
    -------
    bbox : LatLonBoundingBox
    """
    # These initial values are outside the valid ranges for latitudes and
    # longitudes, and are guaranteed to change on the first loop below.
    lat_min = 1000.
    lon_min = 1000.
    lat_max = -1000.
    lon_max = -1000.
    for drifter in drifters:
        drifter_lat_min = drifter['lat'].values.min()
        drifter_lat_max = drifter['lat'].values.max()
        drifter_lon_min = drifter['lon'].values.min()
        drifter_lon_max = drifter['lon'].values.max()
        lat_min = min(drifter_lat_min, lat_min)
        lat_max = max(drifter_lat_max, lat_max)
        lon_min = min(drifter_lon_min, lon_min)
        lon_max = max(drifter_lon_max, lon_max)

    # lat_min = np.floor(lat_min) - 1
    # lat_max = np.ceil(lat_max) + 1
    # lon_min = np.floor(lon_min) - 1
    # lon_max = np.ceil(lon_max) + 1

    lat_diff = lat_max - lat_min
    lon_diff = lon_max - lon_min
        
    buffer_lon = lon_diff * (math.sqrt(ratio)-1)/2
    buffer_lat = lat_diff * (math.sqrt(ratio)-1)/2
    
    print(math.sqrt(ratio))
    print(buffer_lat)
    print(buffer_lon)
    
    lon_min=lon_min - buffer_lon
    lat_min=lat_min - buffer_lat
    lon_max=lon_max + buffer_lon
    lat_max=lat_max + buffer_lat
    

    #return LatLonBoundingBox(lon_min=lon_min, lat_min=lat_min,
    #                         lon_max=lon_max, lat_max=lat_max)
    return LatLonBoundingBox(lon_min=np.around(lon_min,decimals =2), lat_min=np.around(lat_min,decimals=2),
                             lon_max=np.around(lon_max,decimals=2), lat_max=np.around(lat_max,decimals=2))

def main():
    """ Obtain a unique list from files that contains drifters with same observed unique id 
        
        Run and display maps with the observed and modelled trajectory
     
        filelist : list of drifters with unique observed ids
        newFile : Get content of files to avoid overriding contents of files 
        map_features : function to plot map, set size and label of the map
    
    """
    unique_list =[]
    for file in files:
    # Open dataset
        ds = xr.open_dataset(file)
        drift_id = ds.obs_unique_id
        if drift_id not in unique_list:
            unique_list.append(drift_id)
        ds.close()
    print(unique_list)

    filelist = []
    newFiles = files 
    Savedir = '/home/samuel/gory/Commit/k/'

    for num in unique_list:
        filename = '*{}.nc'.format(num)
        newFiles = glob.glob(os.path.join(results_dir, filename))
        filelist.append(newFiles)
        fig, ax = plt.subplots(1,1,figsize=(15,12))

        for file in newFiles:
            ds = xr.open_dataset(file)
            times = pd.to_datetime(ds.time.values)
            start = times[0]
            end = start + datetime.timedelta(hours=8)
            ds_new = ds.sel(time=slice(start,end))
            bbox = determine_latlon_bbox_for_drifters(ds_new)
        maplot=Basemap(projection='merc',resolution='l',llcrnrlon=bbox.lon_min,llcrnrlat=bbox.lat_min,urcrnrlon=bbox.lon_max,urcrnrlat=bbox.lat_max)    
        for file in newFiles:
            plot_model_obs_unique(file,maplot,bbox)
            legends(ax)
        basename= '{}.png'.format(ds_new.obs_buoyid)
        fname=os.path.join(Savedir, basename)
        fig.savefig(fname)
    
if __name__ == '__main__':
   main()
    
