
import os
import os.path
import shutil
import subprocess
import sys

from . import configargparse
from . import utils
from .utils import logger

def run_mldp(
        mldp_exec, 
        namelist, 
        run_dir, 
        overwrite_run_dir=False):
    
    run_dir = os.path.abspath(run_dir)
    logger.info('Preparing to run MLDP in directory %s...', run_dir)

    if not os.path.exists(run_dir):
        logger.info('Creating run directory...')
        os.makedirs(run_dir)
    else:
        if not overwrite_run_dir:
            logger.error(('Run directory already exists. Please delete it or '
                          'specify a different directory.'))
            sys.exit(1)
            
    logger.info('Running MLDP...')
    process = subprocess.Popen(mldp_exec, cwd=run_dir,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.STDOUT)
    with open(os.path.join(run_dir, 'MLDPn.out'), 'w') as f:
        while True:
            out = process.stdout.read()
            out = out if isinstance(out, str) else out.decode('utf-8')
            if out == '' and process.poll() is not None:
                break
            f.write(out)
    return_status = process.poll()
    
    if return_status != 0:
        logger.warn('MLDP finished with an error status of %s',
                    return_status)
    logger.info('Finished running Ariane.')
    return return_status

def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')
    arg_parser.add('--mldp_exec', type=str,
                   default='/space/group/dfo_odis/project/sab002/code/drifters/MLDPn/libeerModel/bin/MLDPn',
                   help='Path to MLDP executable')
    arg_parser.add('--namelist', type=str, default='/space/group/dfo_odis/project/sab002/code/drifters/MLDPn/MLDPn.in',
                   help='Name of namelist configuration file')
    arg_parser.add('--run_dir', type=str, default='/space/group/dfo_odis/project/sab002/code/drifters/MLDPn/results',
                   help=('Name of directory in which to run MLDP. This is '
                         'where its output files will be generated.'))
    arg_parser.add('--overwrite_run_dir', type=bool, default=True,
                   action='store',
                   help='Overwrite contents of run directory if it exists')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    run_mldp(config.mldp_exec, config.namelist, 
             config.run_dir, overwrite_run_dir=config.overwrite_run_dir)

    utils.shutdown_logging()


if __name__ == '__main__':
    main()




