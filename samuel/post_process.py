"""
Covert csv files to netcdf
=============================
:Author: Nancy Soontiens
:Modified: Samuel Babalola
:Created: 2018-09-19

"""
import datetime
import glob
import os

import numpy as np
import pandas as pd
import xarray as xr

# Source and save directories
# Note: data in MOD_SRC_DIR is saved in csv.
# This codes expects the csv files to be in a certain format.
# Reading the csv is handled by one of three functions:
# read_model_csv, read_SCT_csv or read_other_csv (other is for OSKER or Roby)

def covert_files(MOD_SRC_DIR, SAVE_DIR):
    """ 
    Main function to compile both obs/model fo a given drifter into one netcdf file
    The purpose is to create netcdf files understandable by drift_evaluate"""
    mod_files = glob.glob(os.path.join(MOD_SRC_DIR, '*.csv'))
    # Loop through all obs and model
    for modf in mod_files:
        mod = read_model_csv(modf)
        grouped = mod.groupby('drifter ID')
        for modelrun in grouped.groups:
            modelid = '{}_{}'.format(modelrun.split('_')[0],
                                       modelrun.split('_')[1])
            windage = str_to_float(modelrun.split('_')[-1])
            start_date = modelrun.split('_')[0]
              # Prepare model data array
            g = grouped.get_group(modelrun)
            
            g = g.set_index(['time'])
            print(g)
            mod_array = g.to_xarray() 
            # save in netcdf
            mod_array['lon'] = wrap_to_180(mod_array.lon)
            # remove nans
            mod_lon = mod_array['lon']
            mod_lat = mod_array['lat']
            mod_time = mod_array['time']
            # Create dataset to save in netcdf
            ds = xr.Dataset(
                coords={'time': mod_time},
                data_vars={'mod_lat': ('time', mod_lat),
                           'mod_lon': ('time', mod_lon),})
            ds.mod_lat.attrs['long_name'] =\
                 'Latitude of modelled trajectory'
            ds.mod_lat.attrs['units'] = 'degrees_north'
            ds.mod_lat.attrs['_FillValue'] =\
                ds.mod_lat.dtype.type(np.nan)
            ds.mod_lon.attrs['long_name'] =\
                'Longitude of modelled trajectory'
            ds.mod_lon.attrs['units'] = 'degrees_east'
            ds.mod_lon.attrs['_FillValue'] =\
                ds.mod_lon.dtype.type(np.nan)                       
            ds.attrs['mod_run_name']=modelrun
            ds.attrs['mod_windage']=windage
            ds.attrs['drift_model']='MLPDn'
            ds.attrs['ocean_model']='RIOPS'
            ds.attrs['start_date']=start_date
            output_file = 'RIOPS_{}-{}.nc'.format(
                start_date,
                modelrun)
            outdir=os.path.join(SAVE_DIR,
                               'windage_{}'.format(str(windage)))
            if not os.path.exists(outdir):
                os.makedirs(outdir)
            output_file=os.path.join(outdir, output_file)
            print(output_file)
            ds.to_netcdf(output_file)

                    
def read_SCT_csv(fname):
    """Read drifter data from an SCT drifter. fname is filename (str)"""
    basename=os.path.basename(fname)
    drifterid='{}_{}'.format(basename.split('_')[0],
                             basename.split('_')[1])
    df = pd.read_csv(fname, header=None,
                     names=['time', 'lat', 'lon'],
                     usecols=[0,3,4], parse_dates=[0,])
    df['DrifterID'] = drifterid
    return df


def read_other_csv(fname):
    """Read drifter data from ROBY or OSKERS. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[1,],
                     usecols=[2,3,4,5], header=0,
                     names=['DrifterID','time','lat', 'lon'])
    
    return df


def read_model_csv(fname):
    """Read model drifter data. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[0,],
                     header=0,
                     names=['time','drifter ID','lat', 'lon'])
    return df

def interpolate_obs_to_model(obs, mod):
    """Interpolate observed drifter positions to model output times
    obs and mod are xrrayr.DataArray with variables time, lon, lat
    """
    mTime = mod['time'][:]
    lat = np.interp(
        mTime.astype('float64').values,
        obs['time'].astype('float64').values,
        obs['lat'].values,
        left=np.nan, right=np.nan)
    lon = np.interp(
        mTime.astype('float64').values,
        obs['time'].astype('float64').values,
        obs['lon'].values,
        left=np.nan, right=np.nan)
    interp_track = xr.Dataset(
        data_vars={'lon': ('time', lon), 'lat': ('time', lat)},
        coords={'time': mTime})
    return interp_track


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap

def main(args=sys.argv[1:]):
    arg_parser = configargparse.ArgParser(
        config_file_parser_class=configargparse.YAMLConfigFileParser
    )
    arg_parser.add('-c', '--config', is_config_file=True,
                   help='Name of configuration file')
    arg_parser.add('--log_level', default='info',
                   choices=utils.log_level.keys(),
                   help='Set level for log messages')
    arg_parser.add('--MOD_SRC_DIR', type=str, required=True,
                   help='Location of the csv files to be converted')
    arg_parser.add('--SAVE_DIR', type=str, required=True,
                   help='Location to save coverted files (netcdf)')
    config = arg_parser.parse(args)

    utils.initialize_logging(level=utils.log_level[config.log_level])

    convert_files(config.MOD_SRC_DIR, config.SAVE_DIR)

    utils.shutdown_logging()

if __name__=='__main__':
    main()
