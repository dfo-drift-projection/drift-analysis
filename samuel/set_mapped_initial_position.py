
"""
Set Initial Trajectory Positions
================================
:Author: Clyde Clements
:Created: 2017-08-28

This module determines the initial positions to use for trajectory simulations.
For a user-specificed date and time, the (interpolated) location of each
drifter buoy is found and the initial position for a corresponding trajectory
calculation is determined based on that drifter location and the given ocean
mesh.
"""

import datetime
from functools import wraps
import inspect
import logging
import os
import os.path

import numpy as np
from scipy import interpolate
from scipy import optimize
import xarray as xr
import yaml

from .find_nearest_grid_point import find_nearest_grid_point


class NoDrifterDataInModelDomain(Exception):
    pass


logger = logging.getLogger('drifter')




def set_ariane_interpolated_positions(
        lat, lon, mesh_dataset, ulon_var='glamu', ulat_var='gphiu',
        vlon_var='glamv', vlat_var='gphiv',
):
    ix_u, iy_u, lon_u, lat_u = find_containing_grid_cell(
        lon, lat, mesh_dataset, ulon_var, ulat_var
    )
    ix_v, iy_v, lon_v, lat_v = find_containing_grid_cell(
        lon, lat, mesh_dataset, vlon_var, vlat_var
    )

    lon_u_box = np.zeros((2, 2), dtype=np.float64)
    for i, j, u in zip(ix_u - ix_u.min(), iy_u - iy_u.min(), lon_u):
        lon_u_box[i][j] = u

    lat_v_box = np.zeros((2, 2), dtype=np.float64)
    for i, j, v in zip(ix_v - ix_v.min(), iy_v - iy_v.min(), lat_v):
        lat_v_box[i][j] = v

    # Determine initial guess for grid point indices.
    ix_interp = ix_u.mean()
    iy_interp = iy_v.mean()

    def residual(g, ulon, vlat, lon, lat):
        gi = g[0]
        gj = g[1]
        a = gi - int(gi)
        b = (gj + 0.5) - int(gj + 0.5)
        residual1 = (1 - a) * (1 - b) * ulon[0][0] \
            + (1 - a) * b * ulon[0][1] \
            + a * (1 - b) * ulon[1][0] \
            + a * b * ulon[1][1] - lon
        a = (gi + 0.5) - int(gi + 0.5)
        b = gj - int(gj)
        residual2 = (1 - a) * (1 - b) * vlat[0][0] \
            + (1 - a) * b * vlat[0][1] \
            + a * (1 - b) * vlat[1][0] \
            + a * b * vlat[1][1] - lat
        return [residual1, residual2]

    # Note that solutions to the above residual are not unique; if [gi, gj] is
    # a solution, then [gi, gj] + n * [1, 1] where n is an integer will also be
    # a solution. A root-finding method such as optimize.root() will find a
    # solution, but is not guaranteed to find the right solution. Therefore, we
    # resort to a least squares procedure with bound constraints.
    bounds = ((ix_u.min(), iy_v.min()), (ix_u.max(), iy_v.max()))
    sol = optimize.least_squares(residual, [ix_interp, iy_interp],
                                 args=(lon_u_box, lat_v_box, lon, lat),
                                 bounds=bounds)
    if sol.success:
        ix_interp = sol.x[0]
        iy_interp = sol.x[1]
        # Compute interpolated lat/lon positions.
        lon_interp, lat_interp = residual([ix_interp, iy_interp],
                                          lon_u_box, lat_v_box, 0, 0)
    else:
        # Fallback for when root method fails to converge.
        ix_interp = ix_u[0]
        iy_interp = iy_v[0]
        lon_interp = lon_u_box[0]
        lat_interp = lat_v_box[0]

    return ix_interp, iy_interp, lon_interp, lat_interp



def find_containing_grid_cell(
        lon,           # type: float
        lat,           # type: float
        mesh_dataset,  # type: xr.Dataset
        lon_var,       # type: str
        lat_var        # type: str
):  # type: (...) -> (nd.array, nd.array, nd.array, nd.array)
    """Find the grid cell containing a location.

    Parameters
    ----------
    lon : float
        Longitude of location in question.
    lat : float
        Latitude of location in question.
    mesh_dataset : xarray.Dataset
        Mesh dataset in which to find the grid cell.
    lon_var : str
        Name of longitude variable in mesh dataset.
    lat_var : str
        Name of latitude variable in mesh dataset.

    Returns
    -------
    tuple
        A four-element tuple containing ``ix``, ``iy``, ``lon`` and ``lat``.
        Each element is a *numpy.ndarray* containing four values corresponding
        to the x indices, y indices, longitudes, and latitudes, respectively,
        of the containing grid cell.
    """
    # First find the closest grid point.
    dist_sq, j, i, clat, clon = find_nearest_grid_point(
        lat, lon, mesh_dataset, lat_var, lon_var, n=1
    )
    # Values i and j are arrays with only a single value; change them to
    # scalars.
    i = i[0]
    j = j[0]

    # The longitude and latitude variables should be two-dimensional arrays.
    # In some mesh files, these are four-dimensional arrays but the extra
    # dimensions are of size 1. The squeeze() removes the single-dimensional
    # entries.
    lons = mesh_dataset[lon_var].squeeze()
    lats = mesh_dataset[lat_var].squeeze()

    # Now check each box that the closest grid point is a member of. We have
    # four boxes to check:
    #
    # * one with lower-left corner point (i, j);
    # * one with lower-left corner point (i - 1, j);
    # * one with lower-left corner point (i, j - 1); and
    # * one with lower-left corner point (i - 1, j - 1).
    #
    # As soon as we find a containing box, we immediately return the details of
    # that box.
    for jshift in [0, -1]:
        for ishift in [0, -1]:
            ix = [i + ishift, i + ishift + 1, i + ishift + 1, i + ishift]
            iy = [j + jshift, j + jshift, j + jshift + 1, j + jshift + 1]
            try:
                lon_box = lons.values[[iy, ix]].squeeze()
                lat_box = lats.values[[iy, ix]].squeeze()
            except IndexError:
                continue
            if inquad(lon, lat, lon_box, lat_box):
                ix = np.array(ix)
                iy = np.array(iy)
                return ix, iy, lon_box, lat_box

    msg = 'Could not determine box containing lat/lon point ({}, {})'.format(
        lat, lon
    )
    raise NoDrifterDataInModelDomain(msg)


def generate_particle_grid(*, start_date = "2017-04-10", mesh_file = '$HOME/data/salish/grid/mesh_mask201702.nc',
                              drifter_depth='1c', lon_var='nav_lon', lat_var='nav_lat',
                              ulon_var='glamu', ulat_var='gphiu',vlon_var='glamv', vlat_var='gphiv', wdep_var='gdepw_1d',
                              tmask_var='tmask', interp_method='ariane', output_file=None, 
                              lon_min = -124, lat_min = 48.25, lon_max = -122.5, 
                              lat_max = 49.5, num_particles = 20):
    

    start_date = "2017-04-10"
    mesh_file = '/home/samuel/data/salish/grid/mesh_mask201702.nc'

    
    """Set initial trajectory positions.

    The format for specifying the drifter depth is detailed below; some
    examples of possible values are:

    - '15.0': Starting depth of 15.0 m

    - '15.0c': Constant depth of 15.0 m

    - 'L15': Starting depth corresponding to 15th depth layer

    - 'L15c': Constant depth corresponding to 15th depth layer

    - 'L15.2c': Constant depth corresponding to the "15.2"th depth layer.
      Note non-integer values are permitted to introduce shift with respect
      to the exact position of the depth level. This value corresponds to
      a depth partway between the 15th and 16th depth level.

    If specified, an output file will be in created in YAML format and will
    contain the following entries:

    - ``start_date`` (*str*): Date/time in ISO format for which positions
      of drifters was determined.

    - ``mesh_file`` (*str*): Name of ocean mesh file used.

    - ``drifter_grid_positions`` (*dict*): Dictionary containing initial
      drifter positions, the same as the returned result; see below.

    - ``drifter_data_files`` (*dict*): Dictionary containing drifter data
      files for this run. The key is the drifter buoy id and the value is
      the name of the data file. Note the file paths will be relative to
      the output file.

    - ``updated`` (*str*): Date/time in ISO format when this information
      was constructed.

    If ``output_file`` is set to ``None``, no file will be created.

    Parameters
    ----------
    start_date : datetime.datetime
        Date for which to determine drifter positions.
    drifter_data_dir : str
        Name of directory containing drifter data files.
        All NetCDF files in this directory and any subdirectories will be
        processed.
    mesh_file : str
        Name of NetCDF file containing ocean mesh.
    run_ocean_data_dates: np.ndarray
        Dates, in ascending order, corresponding to ocean current data for
        all ocean data linked in run dir.
    drifter_depth : str
        Drifter depth in the format "[L]n[c]". An optional prefix of "L" means
        the number "n" indicates the depth layer; otherwise, the number "n"
        indicates the actual depth in meters. An optional suffix of "c"
        indicates a constant-layer (or fixed depth) trajectory calculation.
        Specifies the depth for drift trajectory simulations; otherwise, the
        particle is free to move in the vertical direction.
    lon_var : str
        Name of longitude variable in mesh file.
    lat_var : str
        Name of latitude variable in mesh file.
    ulon_var : str
        Name of variable in mesh file defining longitude for U velocity
        points.
    ulat_var : str
        Name of variable in mesh file defining latitude for U velocity
        points.
    vlon_var : str
        Name of variable in mesh file defining longitude for V velocity
        points.
    vlat_var : str
        Name of variable in mesh file defining latitude for V velocity
        points.
    wdep_var : str
        Name of variable in mesh file defining depth for W velocity points.
        This must be a one dimensional array.
    tmask_var : str
        Name of variable in mesh file containing land/ocean mask for T grid
        points.
    interp_method : str
        Name of interpolation method to use for determining drifter horizontal
        position. Must be one of 'linear', 'nearest', 'ariane', 'cubic' or
        'quintic'.
    output_file : str
        Name of file to create containing details of determined drifter
        positions.
        If not specified, no file is created.

    Returns
    -------
    grid_positions : dict
        A dictionary containing initial drifter positions. The key is the
        drifter buoy id and the value is a two-element list containing the x
        and y indices in terms of the given mesh.
    """
    logger.info('Determining initial positions for drifters...')
    if interp_method not in ['nearest', 'ariane', 'linear', 'cubic',
                             'quintic']:
        msg = ('Unknown interpolation method "{}"; valid options are nearest, '
               'ariane, linear, cubic and quintic').format(interp_method)
        raise ValueError(msg)

    # Parse drifter depth option.
    if drifter_depth.startswith('L'):
        depth_in_layers = True
        depth_option = drifter_depth[1:]
    else:
        depth_in_layers = False
        depth_option = drifter_depth
    if depth_option.endswith('c'):
        constant_depth = True
        depth_option = depth_option[:-1]
    else:
        constant_depth = False
    depth = float(depth_option)

    mesh_dataset = xr.open_dataset(mesh_file)
    
        
    lon_interval = np.linspace(lon_min,lon_max,num_particles)
    lat_interval = np.linspace(lat_min,lat_max,num_particles)
    print(lon_interval)
    print(lat_interval)
    
    lon_grid, lat_grid = np.meshgrid(lon_interval,lat_interval)
    
    for lon, lat in zip(lon_grid,lat_grid):
        ix_interp, iy_interp, lon_interp, lat_interp \
                        = set_ariane_interpolated_positions(
                            lat, lon, mesh_dataset, ulon_var=ulon_var,
                            ulat_var=ulat_var, vlon_var=vlon_var, vlat_var=vlat_var
                        )
        
        print(ix_interp)

generate_particle_grid()

