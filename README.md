A repository for shared analysis related to drift projection.

The repository contains individual user analysis scripts and notebooks and also scripts that were used to pre-process drifter data.
