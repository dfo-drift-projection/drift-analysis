#!/bin/bash

# User Defined Parameters
# -----------------------
# MINICONDA_PATH: path to drift-tool python environment
MINICONDA_PATH=/home/sdfo000/sitestore4/opp_drift_fa3/software/drift-tool-miniconda
# CONFIG_FILE: experiment specific configuation file
CONFIG_FILE=ciopsev2.yaml
# EXPERIMENT_DIR: path to user defined output directory
EXPERIMENT_DIR=/home/nso001/data/work2/OPP/IC3-ciopse/ciopsev2

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


startdate='2020-02-01'
enddate='2020-02-23'
#end-date: '2020-02-23'
EXPERIMENT_DIR=/home/nso001/data/work2/OPP/IC3-ciopse/ciopsev2/${startdate}_${enddate}


# Run drift tool
# --------------
drift_correction_factor \
    -c $CONFIG_FILE \
    --experiment-dir $EXPERIMENT_DIR \
    --start-date $startdate \
    --end-date $enddate
