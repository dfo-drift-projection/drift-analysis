# Script to combine correction calculations
# Nancy Soontiens

import glob
import os

import xarray as xr

data_dir='/home/nso001/data/work2/OPP/IC3-ciopse/ciopsev2/'

def main():
    combine_drifter_calculations(data_dir)

def list_drifters(data_dir):
    files = glob.glob(os.path.join(data_dir,'*/output/*.nc'))
    files.sort()
    drifters_list = []
    for f in files:
        basename = os.path.basename(f)
        if basename not in drifters_list:
            drifters_list.append(basename)
    return drifters_list

def combine_drifter_calculations(data_dir):
    drifters_list = list_drifters(data_dir)
    print(drifters_list)
    for drifter in drifters_list:
        drifter_files = glob.glob(os.path.join(data_dir,
                                               '*/output/{}'.format(drifter)))
        print(drifter)
        print(drifter_files)
        drifter_files.sort()
        drifter_datasets = []
        for f in drifter_files:
            d = xr.open_dataset(f)
            drifter_datasets.append(d)
        dnew = xr.concat(drifter_datasets, dim='time')
        dnew.to_netcdf(os.path.join(data_dir, 'combined', drifter))

if __name__ == '__main__':
    main()
