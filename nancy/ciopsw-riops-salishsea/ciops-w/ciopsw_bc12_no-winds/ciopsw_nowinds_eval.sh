#!/bin/bash

# Script for ciopsw evaluations
# Nancy Soontiens


# usage: bash ciopsw_winds_eval.sh first_start_date last_start_date
# first_start_date and last_start_date are in format
# yyyy-mm-ddTHH:MM:SS
# Set environment                                                                
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

config=/home/nso001/code/drifters/drift-analysis/nancy/ciops-w/ciopsw_bc12_no-winds/ciopsw_bc12_2D.yaml

# Atmosphere parameters
atmos_data_dir='None'
atmos_model_name='None'
alpha_wind=0
first_start_date="$1"
last_start_date="$2"
experiment_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_nowinds/${first_start_date}_${last_start_date}

LOG=/home/nso001/data/work2/OPP/ciopsw/logs/ciopsw_bc12_nowinds/ciopsw_nowinds_${first_start_date}_${last_start_date}


drift_predict \
    -c $config \
    --experiment-dir $experiment_dir \
    --atmos-data-dir $atmos_data_dir \
    --atmos-model-name $atmos_model_name \
    --alpha-wind $alpha_wind \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date"  >> $LOG 2>&1

drift_evaluate \
    --data_dir $experiment_dir/output >> $LOG 2>&1

combine_track_segments \
    --data_dir $experiment_dir/output >>$LOG 2>&1

