Scripts for submitting runs used in the CIOPSW-RIOPS-SalishSea comparisons. Analysis yaml templates also provided.

WARNING: The files have been moved around so some of the paths are broken. However, these files still record how the runs were submitted and the parameters used

A note on the CIOPSW paper
--------------------------
November 2020
The runs used for the drift part of the CIOPSW paper were CIOPSWBC12nowinds and RIOPSBC12nowinds
The cropped results are stored here: /home/nso001/data/work2/OPP/salish-ciopsw-comparisons/cropped_datasets/CIOPSWBC12nowinds-RIOPSBC12nowinds
The individual runs are stored here:
CIOPSWBC12nowinds: /home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_nowinds
RIOPSBC12nowinds: /home/nso001/data/work2/OPP/ciopsw/riops_bc12_nowinds
