# Script to redo the post-prcessing
# assemble_drift_predictions, drift_evaluate, combine_tracks, removing bad drifters

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


ocean_mesh_file=/home/sdfo000/sitestore4/opp_drift_fa3/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc
lat_var=nav_lat
lon_var=nav_lon

#ciopsw-hourly
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_winds
#ciopsw-daily
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw3D_winds
#riops-daily
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_daily_winds
#ciopsw-nowinds
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_nowinds
# ciops hourly windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_hourly_windage2
#ciops daily windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciops_daily_windage2
#riops dailt windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_daily_windage2
# ciopsw_bc12_hourly_windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_hourly_windage2
# riops_bc12_hourly_windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_hourly_windage2
# riops_bc12_hourly_windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_nowinds
# ciopsw_bc12_hourly_nowinds
data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_nowinds
# ciopsw_bc12_hourly_windage1
#data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_hourly_windage1
# riops_bc12_hourly_windage1
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_hourly_windage1




#for experiment_dir in $data_dir/*; do
#    assemble_drift_predictions \
#	--ocean-data-file $ocean_mesh_file \
#	--lat-var $lat_var \
#	--lon-var $lon_var \
#	--experiment-dir $experiment_dir
#    drift_evaluate --data_dir $experiment_dir/output
#done
mkdir -p $data_dir/all_output
cp $data_dir/*/output/* $data_dir/all_output/.

#remove exluded drifters
#mkdir -p $data_dir/excluded-drifters
#while read p; do echo "$p"; ls -l $data_dir/all_output/*"$p"*; mv $data_dir/all_output/*"$p"* $data_dir/excluded-drifters/.; done < /home/nso001/code/drifters/drift-analysis/ciopsw-riops-salishsea/bad_drifters_remove_all

# final aggregation
combine_track_segments --data_dir $data_dir/all_output
