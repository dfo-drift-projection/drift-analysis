##############################################################################
# user config file for plotting
##############################################################################

# Directory containing the dataset(s) that will be plotted

#sample comparison input
datadir: "/home/nso001/data/work2/OPP/salish-ciopsw-comparisons/cropped_datasets/SalishSeaBC12onowinds-SalishSeaBC12owindage2-SalishSeaBC12owindage1-SalishSeaBC12owindage0.5/"

# Main output directory for the plots. 
# Plotting scripts create subfolders for organization.
savedir: "/home/nso001/data/work2/OPP/salish-ciopsw-comparisons/cropped_datasets/plots"

# Path to etopo file. If missing or None, no bathymetry is plotted. 
#etopo_file: "/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests\
#            /share_drift/sample_visualization_input\
#            /ETOPO1_Bed_g_gmt4.grd"

# Optionally included analysis zones. If none are defined, the default 
# behavior is to use all the areas listed as keys in the polygons 
# definitions file below. Plots analyzed over the entire dataset
# are also included by default, regardless of whether a polygon area is 
# provided for the full domain.
analysis_zones:
    - SoGS 
    - SoGN 
    - JdFE 
    - JdFW

# Path to a yaml file containing analysis zone polygons definitions
polygons_file: "/home/nso001/data/work2/OPP/CTD_analysis_domain_config_template-SalishSea.yml"


# Dictionary containing paths to landmask files if required. If None or 
# missing, default basemap landmask will be plotted. Landmask files 
# should be in NetCDF format and contain lon, lat and tmask variables 
# (ex, NEMO mesh mask files). If using default NEMO variable names 
# (nav_lon, nav_lat, tmask), each dictionary value can simply be a 
# string representing the full path to the file.  If using non-default 
# variable names, each dictionary value should contain a nested 
# dictionary with keys 'path', 'lon_var', 'lat_var', and 'tmask_var'.
# If providing multiple landmask files, it is acceptable to include
# variable names for some files but not for others. 
# The dictionary keys must match the setname variables if using a 
# comparison set or the ocean_model attribute if using normal 
# drift-tool output.
# Sample directory on the GPSC currently contains files for CIOPS-W,
# RIOPS, and SalishSeaCast: "/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x\
# /SeDOO/requests/share_drift/sample_visualization_input/land_mask


# Dictionary of colors to use for plotting. if lcolors: None or missing, 
# colors will be assigned randomly during plotting. Dictionary keys must 
# match the setname variables if using a comparison set or the 
# ocean_model attribute if using normal drift-tool output.

#lcolors:
    #RIOPSBC12nowinds: lightsteelblue
    #RIOPSBC12windage1: cornflowerblue
    #RIOPSBC12windage2: blue
    #CIOPSWBC12: red 
#    SalishSeaBC12onowinds: lightcoral
#    SalishSeaBC12owindage0.5: indianred
#    SalishSeaBC12owindage1: red
#    SalishSeaBC12owindage2: darkred
    #CIOPSWBC12nowinds: lightsteelblue
    #CIOPSWBC12windage1: cornflowerblue
    #CIOPSWBC12windage2: blue


# skills_list can include sep, molcard, liu, sutherland, obsratio, 
# modratio, logobsratio, logmodratio, obs_dist, obs_disp, mod_dist, 
# and mod_disp. If none or missing, defaults of sep and molcard are used.
skills_list:
    - sep
    - molcard

# filled_calculation_method can include IQR, 1std, and extremes. If
# None or missing, default is set to IQR.
filled_calculation_method:
    - IQR

# Plot durations to use with hexbin plots. Value can be a single integer 
# or a list of integers. For example, a value of 48 will produce a plot 
# representing skill score at 48h.
plot_durations:
    - 24
    - 48

# Reference set to use with the subtracted plots. Value must match the 
# setname in comparison set output files.
reference_set: RIOPSBC12nowinds

########################################################################
# Choose which plots to create. Options set to False or missing
# will not execute.
########################################################################

# plot_track_per_drifter: 
#   individual track plots for each drifter for each dataset

# plot_comparison_track_per_drifter: 
#   track per drifter with modelled tracks for all datasets on each plot

# plot_track_side_by_side: 
#   side by side subplots of the drifter tracks for all datasets

# plot_all_drifter_tracks: 
#   plot all observed tracks in the comparison set

# plot_all_drifter_tracks_with_mod:
#   plot all observed tracks in the comparison set as well as all
#   modelled tracks on the same axis.

# plot_drifter_skillscores:
#   per-drifter plot of skillscore averaged across all modeled tracks 

# plot_average_of_mean_skills: 
#   per-experiment (or per-area) plot of average of skillscores 
#   across all included drifters and modeled tracks

# plot_skills_subplots_with_filled: 
#   per-experiment (or per-area) plot of average of skillscores
#   across all included drifters including all modeled tracks. Plot 
#   includes a filled range defined by choosing filled_range_type above.

# plot_skills_subplots_with_indv: 
#   per-experiment (or per-area) plot of average of skillscores
#   across all included drifters including all modeled tracks. Plot also
#   includes individual lines showing average skill per drifter. 

# plot_hexbin_skillscores: 
#   hexbin plot of the average skillscore at a time chosen by the
#   plot_durations argument above.

# plot_subtracted_skillscores:
#   lineplot showing the average skill for one experiment subtracted from
#   the average skill for a second experiment. Plot shows an overall
#   average for the experiments as well as per drifter lines. This plot 
#   will not be created if two experiments are not included in the 
#   input files in datadir

# plot_subtracted_hexbins: 
#   hexbin plot of the average skill for one experiment subtracted from
#   the average skill for a second experiment at a time chosen by the 
#   plot_durations argument above.

# plot_skills_plus_map: 
#   Plot that include subplots of average skill score for 'sep', 
#   'molcard' and 'liu', as well as the observed and modelled drifter 
#   tracks. This option is quite slow to run, but produces useful plots
#   for quality controlling erroneous output data.


plot_selection:
    #drifter track related plots
    plot_track_per_drifter: False 
    plot_track_comparison: True
    plot_track_side_by_side: False
    plot_all_drifter_tracks: True
    plot_all_drifter_tracks_with_mod: False
    #skillscore related lineplots
    plot_drifter_skillscores: False
    plot_average_of_mean_skills: True 
    plot_skills_subplots_with_filled: True
    plot_skills_subplots_with_indv: False
    #hexbin plots
    plot_hexbin_skillscores: False
    #subtracted plots
    plot_subtracted_skillscores: False
    plot_subtracted_hexbins: False
