#!/bin/bash

# Script for ciopsw evaluations
# Nancy Soontiens


# usage: bash ciopsw_winds_eval.sh first_start_date last_start_date
# first_start_date and last_start_date are in format
# yyyy-mm-ddTHH:MM:SS
# Set environment                                                              
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

config=/home/nso001/code/drifters/drift-analysis/nancy/SalishSea-BC12/salishsea.yaml

# Atmosphere parameters
atmos_data_dir=/home/nso001/data/work2/models/hrdps-west-mshydro
atmos_model_name='hdrps'
lat_var_atmos='nav_lat'
lon_var_atmos='nav_lon'
model_time_atmos='time_counter'
alpha_wind=0.5
ocean_model_name='SalisheSea-BC12o-windage0.5'

first_start_date="$1"
last_start_date="$2"
experiment_dir=/home/nso001/data/work2/OPP/SalishSea-BC12o/hourly-windage0.5/${first_start_date}_${last_start_date}

LOG=/home/nso001/data/work2/OPP/SalishSea-BC12o/logs/hourly-windage0.5/${first_start_date}_${last_start_date}


drift_predict \
    -c $config \
    --experiment-dir $experiment_dir \
    --atmos-data-dir $atmos_data_dir \
    --atmos-model-name $atmos_model_name \
    --ocean-model-name $ocean_model_name \
    --alpha-wind $alpha_wind \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --lon-var-atmos $lon_var_atmos \
    --lat-var-atmos $lat_var_atmos \
    --model-time-atmos $model_time_atmos >> $LOG 2>&1

drift_evaluate \
    --data_dir $experiment_dir/output >> $LOG 2>&1

combine_track_segments \
    --data_dir $experiment_dir/output >>$LOG 2>&1

