# Script to copy the submission scripts and modifyu for riops and ciops_daily
# Nancy Soontiens

SRCDIR=/home/nso001/code/drifters/drift-analysis/nancy/ciops-w/submission_scripts

#windage2 - ciops daily
SAVDIR=no-winds
SAVPRE=salishsea_no-winds

for f in $SRCDIR/*; do
    echo $f
    newfile=$(echo $f | sed "s+ciops-w/submission_scripts+SalishSea-N08/${SAVDIR}/submission_scripts+")
    newfile=$(echo $newfile | sed "s+ciopsw_+${SAVPRE}_+")
    echo $newfile

    sed "s+ciopsw/logs+SalishSea-N08/logs/${SAVDIR}+" $f > $newfile

    sed -i "s+nancy/ciops-w+nancy/SalishSea-N08/${SAVDIR}+" $newfile

    sed -i "s+ciopsw_+${SAVPRE}_+" $newfile
done
