# Script to the post-prcessing
# assemble_drift_predictions, drift_evaluate, combine_tracks, removing bad drifters

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

data_dir=/home/nso001/data/work2/OPP/SalishSea-P01/no-winds


mkdir -p $data_dir/all_output
cp $data_dir/*/output/* $data_dir/all_output/.

#remove exluded drifters
mkdir -p $data_dir/excluded-drifters
while read p; do echo "$p"; ls -l $data_dir/all_output/*"$p"*; mv $data_dir/all_output/*"$p"* $data_dir/excluded-drifters/.; done < ../ciops-w/bad_drifters_remove_all

# final aggregation
combine_track_segments --data_dir $data_dir/all_output
