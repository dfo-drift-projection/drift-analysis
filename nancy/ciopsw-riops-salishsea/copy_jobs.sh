# Script to copy the submission scripts and modifyu for riops and ciops_daily
# Nancy Soontiens

SRCDIR=/home/nso001/code/drifters/drift-analysis/nancy/ciops-w/submission_scripts

# RIOPS settings
SAVDIR=riops_daily
SAVPRE=riops3D

#CIOPS settins
SAVDIR=ciops_daily
SAVPRE=ciopsw3D

#no winds
SAVDIR=ciops_nowinds
SAVPRE=ciops_nowinds

#windage2 - ciops hourly
SAVDIR=ciops_hourly_windage2
SAVPRE=ciops_hourly_windage2

#windage2 - riops daily
SAVDIR=riops_daily_windage2
SAVPRE=riops_daily_windage2

#windage2 - ciops daily
SAVDIR=ciops_daily_windage2
SAVPRE=ciops_daily_windage2

#windage2 = ciopsw_bc12
SAVDIR=ciopsw_bc12_hourly_windage2
SAVPRE=ciopsw_bc12_hourly_windage2

#windage2 = riops_bc12 
SAVDIR=riops_bc12_hourly_windage2
SAVPRE=riops_bc12_hourly_windage2


for f in $SRCDIR/*; do
    echo $f
    newfile=$(echo $f | sed "s+submission_scripts+${SAVDIR}/submission_scripts+")
    #newfile=$(echo $newfile | sed "s+ciopsw_+${SAVPRE}_+")
    echo $newfile

    sed "s+ciopsw/logs+ciopsw/logs/${SAVDIR}+" $f > $newfile

    sed -i "s+nancy/ciops-w+nancy/ciops-w/${SAVDIR}+" $newfile

    #sed -i "s+ciopsw_+${SAVPRE}_+" $newfile
done
