# Script to the post-prcessing
# assemble_drift_predictions, drift_evaluate, combine_tracks, removing bad drifters

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


#BC12 hourly-windage1
data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_hourly_windage1
#BC12 hourly-windage2
data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_hourly_windage2
#BC12 no-winds
data_dir=/home/nso001/data/work2/OPP/ciopsw/ciopsw_bc12_nowinds
#RIOPS BC12 no-winds
data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_nowinds
#RIOPS BC12 houlry-windage1
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_hourly_windage1
#RIOPS BC12 houlry-windage2
#data_dir=/home/nso001/data/work2/OPP/ciopsw/riops_bc12_hourly_windage2



# Remove old output_per_drifter files
rm -rf $data_dir/*/output/output_per_drifter

mkdir -p $data_dir/old
mv $data_dir/all_output/* $data_dir/old/.
mv $data_dir/output_per_drifter $data_dir/old/.
# Redo drift evaluate:
for d in $data_dir/2016*; do
    drift_evaluate --data_dir $d/output
done	 


mkdir -p $data_dir/all_output
cp $data_dir/*/output/* $data_dir/all_output/.

#remove exluded drifters
#mkdir -p $data_dir/excluded-drifters
#while read p; do echo "$p"; ls -l $data_dir/all_output/*"$p"*; mv $data_dir/all_output/*"$p"* $data_dir/excluded-drifters/.; done < /home/nso001/code/drifters/drift-analysis/ciopsw-riops-salishsea/bad_drifters_remove_all

# final aggregation
combine_track_segments --data_dir $data_dir/all_output
