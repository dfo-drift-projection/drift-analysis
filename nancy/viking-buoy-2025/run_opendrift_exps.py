import datetime
import glob
import os

import numpy as np

from opendrift.models import oceandrift
from opendrift.readers import (reader_NEMO_native,
                               reader_netCDF_CF_irregular2D,
                               reader_netCDF_CF_generic)

OUTDIR = '/home/nso001/data/work7/OPP/viking-buoy/ciopse-era5-waves_diff5-recirculate/'


# Supporting functions
def make_circle_of_points(lon, lat, diff_deg, num=20):
    lons = np.linspace(lon - diff_deg, lon + diff_deg, num=num)
    lats = np.linspace(lat - diff_deg, lat + diff_deg, num=num)
    lons, lats = np.meshgrid(lons, lats)
    # Remove points outside of radisu
    inds = np.where(np.sqrt((lons-lon)**2 + (lats-lat)**2) > diff_deg)
    lons[inds] = np.nan
    lats[inds] = np.nan
    lons = lons.flatten()[~np.isnan(lons.flatten())]
    lats = lats.flatten()[~np.isnan(lats.flatten())]
    return lons, lats


def make_perimeter_of_points(lon, lat, diff_deg, num=20):
    thetas = np.linspace(0, 2*np.pi, num=num)
    lons = [lon + diff_deg*np.cos(theta) for theta in thetas]
    lats = [lat + diff_deg*np.sin(theta) for theta in thetas]
    return lons, lats


def run_opendrift(lons, lats, stime, duration,
                  winds, reader_ocean, reader_atmos, reader_atmos_ens, reader_waves, outfile,
                  logfile,
                  time_step=900, diffusivity=0):
    urms = np.sqrt(2*diffusivity/np.abs(time_step))
    o = oceandrift.OceanDrift(loglevel=20, seed=None, logfile=logfile)
    o.max_speed = 5
    o.add_reader([reader_ocean, reader_atmos, reader_atmos_ens, reader_waves])
    o.set_config('drift:advection_scheme', 'runge-kutta4')
    o.set_config('general:coastline_action', 'previous')
    o.set_config('seed:ocean_only', False)
    o.set_config('general:use_auto_landmask', False)
    o.set_config('drift:current_uncertainty', urms)
    o.seed_elements(lon=lons, lat=lats, z=0,
                    time=stime, wind_drift_factor=winds)
    o.run(time_step=time_step, time_step_output=3600,
           steps=int(duration.total_seconds()/np.abs(time_step)),
           outfile=outfile, export_buffer_length=400)

    del o


def main():
    # Constants such as locations and timing
    # Where strike occured
    start_lon = -52.59168
    start_lat = 47.502228
    start_date = datetime.datetime(2025, 1, 3, 6, 45)
    # Where sighting occured
    #sight_lon = -65.05138889
    #sight_lat = 42.19694444
    end_date = datetime.datetime(2025, 1, 28) 

    # Seeding particles
    diff_deg = 0.01  # radius around centre for seeeding (degrees)
    num = 20  # number of particles in x/y directions
    start_lons, start_lats = make_circle_of_points(start_lon,
                                                   start_lat,
                                                   diff_deg,
                                                   num=num)
    start_perim_lons, start_perim_lats = make_perimeter_of_points(start_lon,
                                                                  start_lat,
                                                                  diff_deg,
                                                                  num=num)
    #start_lons, start_lats = make_circle_of_points(start_lon,
    #                                               start_lat,
    #                                               diff_deg,
    #                                               num=num)
    #sight_perim_lons, sight_perim_lats = make_perimeter_of_points(sight_lon,
    #                                                              sight_lat,
    #                                                              diff_deg,
    #                                                              num=num)

    # Diffusivyt
    Kdiff=5 # m^2/s

    # Prepare readers and model data
    # Ocean
    ocean = '/home/nso001/data/work7/OPP/viking-buoy/data/ciopse/*.nc'
    mesh = '/home/sdfo600/gpfs7/CONSTANTS/CIOPS-E_v2.0.0/mesh_mask.nc'
    rotation_pickle = '/home/nso001/data/work7/rotation_pickles/ciopsev2/ciopsev2.rotation.pickle'
    ocean_files = glob.glob(ocean)
    ocean_files.sort()
    mapping = {'vos': 'y_sea_water_velocity',
               'uos': 'x_sea_water_velocity',
               'time_counter': 'time',
               'nav_lon': 'longitude',
               'nav_lat': 'latitude'}
    reader_ocean = reader_NEMO_native.Reader(
        ocean_files, meshfile=mesh,
        variable_mapping=mapping,
        rotation_pickle_file=rotation_pickle)
    # Atmos
    #hrdps = '/home/nso001/data/work7/OPP/viking-buoy/data/hrdps/*.nc'
    #hrdps_files = glob.glob(hrdps)
    #hrdps_files.sort()
    #atmos_variable_mapping = {'u_wind': 'x_wind',
    #                          'v_wind': 'y_wind',
    #                          'time_counter': 'time',
    #                          'nav_lon': 'longitude',
    #                          'nav_lat': 'latitude'}
    #reader_atmos = reader_netCDF_CF_irregular2D.Reader(
    #    hrdps_files,
    #    variable_mapping=atmos_variable_mapping)
    # Atmos
    era5 = '/home/nso001/data/work7/OPP/viking-buoy/data/era5/*subset.nc'
    era5_files = glob.glob(era5)
    mapping = {
        'u10': 'x_wind',
        'v10': 'y_wind',
        'valid_time': 'time'
    }
    reader_atmos = reader_netCDF_CF_generic.Reader(era5_files, standard_name_mapping=mapping)
    era5_ens = '/home/nso001/data/work7/OPP/viking-buoy/data/era5/*ensmean.nc'
    era5_ens_files = glob.glob(era5_ens)
    reader_atmos_ens = reader_netCDF_CF_generic.Reader(era5_ens_files, standard_name_mapping=mapping)

    smoc = '/home/nso001/data/work7/OPP/viking-buoy/data/SMOC/*.nc'
    smoc_files = glob.glob(smoc)
    mapping = {
        'vsdx': 'sea_surface_wave_stokes_drift_x_velocity',
        'vsdy': 'sea_surface_wave_stokes_drift_y_velocity',
        }
    reader_waves= reader_netCDF_CF_generic.Reader(smoc_files, standard_name_mapping=mapping)

    # Windage
    wdfs = np.arange(0.0, 0.06, 0.01)
    # forward first
    starts = [start_date,]
    for start in starts:
        print(start)
        duration = end_date - start
        os.makedirs(OUTDIR, exist_ok=True)
        for wdf in wdfs:
            print(wdf)
            outfile = os.path.join(
                OUTDIR,
                f'opendrift-ciopse-hrdps-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}_diff{Kdiff}.nc'
            )
            logfile = os.path.join(
                OUTDIR,
                f'opendrift-ciopse-hrdps-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}_diff{Kdiff}.log'
            )
            run_opendrift(start_lons,
                          start_lats, start, duration, wdf,
                          reader_ocean, reader_atmos, reader_atmos_ens, reader_waves, outfile, logfile,
                          time_step=900, diffusivity=Kdiff)
    # Backwards
    #starts = [end_date - datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
    #for start in starts:
    #    print(start)
    #    duration = start - start_date
    #    os.makedirs(OUTDIR, exist_ok=True)
    #    for wdf in wdfs:
    #        print(wdf)
    #        outfile = os.path.join(
    #            OUTDIR,
    #            f'opendrift-ciopse-hrdps-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}_diff{Kdiff}.nc'
    #        )
    #        logfile = os.path.join(
    #            OUTDIR,
    #            f'opendrift-ciopse-hrdps-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}_diff{Kdiff}.log'
    #        )
    #        run_opendrift(sight_lons,
    #                      sight_lats, start, duration, wdf,
    #                      reader_ocean, reader_atmos, outfile, logfile,
    #                      time_step=-900, diffusivity=Kdiff)

if __name__=='__main__':
    main()
