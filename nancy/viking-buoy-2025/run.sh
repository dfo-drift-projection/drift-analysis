#! /bin/bash -l
#
#SBATCH --job-name=run_whales.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/viking-buoy/opendrift.out
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

export PATH=~/data/work7/miniconda-gpsc7-new/envs/update-opendrift-1.12.0/bin:$PATH

cd /home/nso001/code/drifters/drift-analysis/nancy/viking-buoy-2025/
python run_opendrift_exps.py
