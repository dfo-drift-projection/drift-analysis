# Script to download GLORYS12-data in a given year and month
# Daily 3D temp and salinity in a region that covers NL Shelf
# Usage: bash glorys12-update-download.sh year month
# Needs to be run with the cmt_1.0 environment

year=$1
month=$2

lonmin=-65
latmin=41
lonmax=-39
latmax=62

outdir=/data/ocean/SMOC


month_start=$(date -u --date $year-$month-01 +%F)
month_end=$(date --date "$month_start+1month-1day" +%F)

mkdir -p $outdir/$year/
echo $month_start
echo $month_end


fname=SMOC_${year}${month}_${var}_PT1H.nc

copernicusmarine subset \
   --dataset-id cmems_mod_glo_phy_anfc_merged-uv_PT1H-i \
   --minimum-longitude $lonmin \
   --maximum-longitude $lonmax \
   --minimum-latitude $latmin \
   --maximum-latitude $latmax \
   --start-datetime $month_start \
   --end-datetime $month_end \
   --minimum-depth 0.49402499198913574 \
   --maximum-depth 0.49402499198913574 \
   --variable uo \
   --variable utide \
   --variable utotal \
   --variable vo \
   --variable vsdx \
   --variable vsdy \
   --variable vtide \
   --variable vtotal \
   --output-directory $outdir/$year \
   --output-filename $fname \
   --force-download \
