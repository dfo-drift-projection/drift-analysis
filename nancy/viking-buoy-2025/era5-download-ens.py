# A script to download ERA5 data in the region of the grand banks, one year at a time
# Only the 10m u and v winds are downloaded.
# Usage: python era5-download.py year out_dir

# Requirements:
# 1. A Python environment with cdsapi installed. cdsapi can be installed with: pip install cdsapi
# 2. A CDS account and environment set up as described here: https://cds.climate.copernicus.eu/api-how-to

# References
# https://cds.climate.copernicus.eu/cdsapp#!/dataset/reanalysis-era5-single-levels?tab=overview

import os
import sys

import cdsapi

# Sub area
LAT_MAX = 51
LAT_MIN = 42
LON_MAX = -45
LON_MIN = -57


def download_era5(year, out_dir):
    """
    Download era5 interim 10m_u_component_of_wind,
    and 10m_v_component_of_wind for a given year.
    Save the results in out_dir

    Parameters
    ----------
    year : int
        The year to download (>=1959)
    out_dir : str
        The directory in which to save the results.
    """

    out_file = os.path.join(out_dir, f'ERA5-{year}-subset_ensmean.nc')

    c = cdsapi.Client()

    c.retrieve(
        'reanalysis-era5-single-levels',
        {
            'product_type': 'ensemble_mean',
            'format': 'netcdf',
            'variable': [
                '10m_u_component_of_wind', '10m_v_component_of_wind',
            ],
            'year': f'{year}',
            'month': [
                '01', '02', '03',
                '04', '05', '06',
                '07', '08', '09',
                '10', '11', '12',
            ],
            'day': [
                '01', '02', '03',
                '04', '05', '06',
                '07', '08', '09',
                '10', '11', '12',
                '13', '14', '15',
                '16', '17', '18',
                '19', '20', '21',
                '22', '23', '24',
                '25', '26', '27',
                '28', '29', '30',
                '31',
            ],
            'time': [
                '00:00', '01:00', '02:00',
                '03:00', '04:00', '05:00',
                '06:00', '07:00', '08:00',
                '09:00', '10:00', '11:00',
                '12:00', '13:00', '14:00',
                '15:00', '16:00', '17:00',
                '18:00', '19:00', '20:00',
                '21:00', '22:00', '23:00',
            ],
            'area': [
                LAT_MAX, LON_MIN, LAT_MIN,
                LON_MAX,
            ],
        },
        out_file
		)


if __name__ == '__main__':
    year, out_dir = sys.argv[1:]
    download_era5(year, out_dir)
