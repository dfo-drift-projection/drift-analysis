import datetime
import glob
import os

import numpy as np
import xarray as xr

from opendrift.models import oceandrift
from opendrift.readers import reader_netCDF_CF_generic

OUTDIR = '/data/viking-buoy-2025/smoc-era5-recirculate'


# Supporting functions
def make_circle_of_points(lon, lat, diff_deg, num=20):
    lons = np.linspace(lon - diff_deg, lon + diff_deg, num=num)
    lats = np.linspace(lat - diff_deg, lat + diff_deg, num=num)
    lons, lats = np.meshgrid(lons, lats)
    # Remove points outside of radisu
    inds = np.where(np.sqrt((lons-lon)**2 + (lats-lat)**2) > diff_deg)
    lons[inds] = np.nan
    lats[inds] = np.nan
    lons = lons.flatten()[~np.isnan(lons.flatten())]
    lats = lats.flatten()[~np.isnan(lats.flatten())]
    return lons, lats


def make_perimeter_of_points(lon, lat, diff_deg, num=20):
    thetas = np.linspace(0, 2*np.pi, num=num)
    lons = [lon + diff_deg*np.cos(theta) for theta in thetas]
    lats = [lat + diff_deg*np.sin(theta) for theta in thetas]
    return lons, lats


def run_opendrift(lons, lats, stime, duration,
                  winds, reader_ocean, reader_atmos, reader_atmos_ens, reader_land, outfile,
                  logfile,
                  time_step=900, diffusivity=0):
    urms = np.sqrt(2*diffusivity/np.abs(time_step))
    o = oceandrift.OceanDrift(loglevel=20, seed=None, logfile=logfile)
    max_speed = 5 # m/s
    default_reader_dt = 3600
    drift_dur_secs = duration.total_seconds()
    mask_maxspeed = max_speed * drift_dur_secs / default_reader_dt
    o.max_speed = mask_maxspeed
    o.add_reader(reader_land)
    o.max_speed = max_speed
    o.add_reader([reader_ocean, reader_atmos, reader_atmos_ens])
    o.set_config('drift:advection_scheme', 'runge-kutta4')
    o.set_config('general:coastline_action', 'previous')
    o.set_config('seed:ocean_only', False)
    o.set_config('general:use_auto_landmask', False)
    o.set_config('drift:current_uncertainty', urms)
    o.seed_elements(lon=lons, lat=lats, z=0,
                    time=stime, wind_drift_factor=winds)
    o.run(time_step=time_step, time_step_output=3600,
           steps=int(duration.total_seconds()/np.abs(time_step)),
           outfile=outfile, export_buffer_length=500)


def main():
    # Constants such as locations and timing
    # Where strike occured
    start_lon = -52.59168
    start_lat = 47.502228
    start_date = datetime.datetime(2025, 1, 3, 6, 45)  # 11:20am - TZ?
    # Where sighting occured
    #sight_lon = -65.05138889
    #sight_lat = 42.19694444
    end_date = datetime.datetime(2025, 2, 1)  # sighting on May 12 unsure time - end May 13

    # Seeding particles
    diff_deg = 0.01  # radius around centre for seeeding (degrees)
    num = 20  # number of particles in x/y directions
    start_lons, start_lats = make_circle_of_points(start_lon,
                                                   start_lat,
                                                   diff_deg,
                                                   num=num)
    start_perim_lons, start_perim_lats = make_perimeter_of_points(start_lon,
                                                                  start_lat,
                                                                  diff_deg,
                                                                  num=num)
    #sight_lons, sight_lats = make_circle_of_points(sight_lon,
    #                                               sight_lat,
    #                                               diff_deg,
    #                                               num=num)
    #sight_perim_lons, sight_perim_lats = make_perimeter_of_points(sight_lon,
    #                                                              sight_lat,
    #                                                              diff_deg,
    #                                                              num=num)

    # Diffusivity
    Kdiff = 5 #m^2/s

    # Prepare readers and model data
    # Ocean
    ocean = '/data/ocean/SMOC/2025/*.nc'
    ocean_files = glob.glob(ocean)
    ocean_files.sort()
    mapping = {
        'utotal': 'x_sea_water_velocity',
        'vtotal': 'y_sea_water_velocity'
        }
    reader_ocean = reader_netCDF_CF_generic.Reader(ocean_files, standard_name_mapping=mapping)
    # Land mask
    d = xr.open_dataset(ocean_files[0])
    landmask = (np.isnan(d['uo'].values[0,0,:,:]))*1.0
    ds = xr.Dataset(data_vars=dict(land_binary_mask=(['latitude','longitude'], landmask)),
                    coords=dict(latitude=d.latitude, longitude= d.longitude))
    ds.land_binary_mask.attrs = {'standard_name': 'land_binary_mask'}
    ds.to_netcdf('land_mask.nc')
    reader_land = reader_netCDF_CF_generic.Reader('land_mask.nc')

    # Atmos
    era5 = '/data/ocean/ERA5/viking-buoy-2025/*subset.nc'
    era5_files = glob.glob(era5)
    mapping = {
        'u10': 'x_wind',
        'v10': 'y_wind',
        'valid_time': 'time'
    }
    reader_atmos = reader_netCDF_CF_generic.Reader(era5_files, standard_name_mapping=mapping)
    era5_ens = '/data/ocean/ERA5/viking-buoy-2025/*ensmean.nc'
    era5_ens_files = glob.glob(era5_ens)
    reader_atmos_ens = reader_netCDF_CF_generic.Reader(era5_ens_files, standard_name_mapping=mapping)

    # Windage
    wdfs = np.arange(0.0, 0.06, 0.01)
    # forward first
    starts = [start_date, ]
    for start in starts:
        print(start)
        duration = end_date - start
        os.makedirs(OUTDIR, exist_ok=True)
        for wdf in wdfs:
            print(wdf)
            outfile = os.path.join(
                OUTDIR,
                f'opendrift-smoc-era5-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}-diff{Kdiff}.nc'
            )
            logfile = os.path.join(
                OUTDIR,
                f'opendrift-smoc-era5-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}-diff{Kdiff}.log'
            )
            run_opendrift(start_lons,
                          start_lats, start, duration, wdf,
                          reader_ocean, reader_atmos, reader_atmos_ens, reader_land, outfile, logfile,
                          time_step=900, diffusivity=Kdiff)
    # Backwards
    #starts = [end_date - datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
    #for start in starts:
    #    print(start)
    #    duration = start - start_date
    #    os.makedirs(OUTDIR, exist_ok=True)
    #    for wdf in wdfs:
    #        print(wdf)
    #        outfile = os.path.join(
    #            OUTDIR,
    #            f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}-diff{Kdiff}.nc'
    #        )
    #        logfile = os.path.join(
    #            OUTDIR,
    #            f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}-diff{Kdiff}.log'
    #        )
    #        run_opendrift(sight_lons,
    #                      sight_lats, start, duration, wdf,
    #                      reader_ocean, reader_atmos, reader_land, outfile, logfile,
    #                      time_step=-900, diffusivity=Kdiff)

if __name__=='__main__':
    main()
