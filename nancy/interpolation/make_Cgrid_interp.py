# Script to interpolate from Agrid to Cgrid
# Also links T/W/bio files to Cgrid_interp dir
# Usage: python make_Cgrid_interp.pu YYYYMMDD 
# Author: Nancy Soontiens May 2018


import netCDF4 as nc
import numpy as np
import os
import glob
import sys
import subprocess
import scipy.interpolate as interp

ORIG_PATH = '/home/nso001/data/work2/models/ciops-e/orig-cgrid'
AGRID_PATH='/home/nso001/data/work2/models/ciops-e/agrid/'
SAVEDIR = '/home/nso001/data/work2/models/ciops-e/interp-cgrid'

MESH='/home/nso001/data/work2/models/ciops-e/CIOPSE_InitialTest_Surface_mesh.nc'

def interp_to_Cgrid(lonU, latU, lonV, latV, lonT, latT,
                    umask, vmask, datestr):
    # Load
    ufiles = glob.glob(os.path.join(AGRID_PATH,	'*U*{}*.nc'.format(datestr)))
    vfiles = glob.glob(os.path.join(AGRID_PATH, '*V*{}*.nc'.format(datestr)))
    ufiles.sort()
    vfiles.sort()
    
    for uf, vf in zip(ufiles, vfiles):
        AfU = nc.Dataset(uf)
        AfV = nc.Dataset(vf)
        Uagrid = AfU.variables['uos'][:]
        Vagrid = AfV.variables['vos'][:]
        Ustressagrid = AfU.variables['tauuo'][:]
        Vstressagrid = AfV.variables['tauvo'][:]
        
        # Construct save files names
        basename = os.path.basename(uf)
        usave = os.path.join(SAVEDIR,'{}.nc'.format(basename[:-9]))
        uorig = os.path.join(ORIG_PATH, '{}.nc'.format(basename[:-9]))
        subprocess.call(['cp', uorig, usave])

        # V
        basename = os.path.basename(vf)
        vsave = os.path.join(SAVEDIR, '{}.nc'.format(basename[:-9]))
        vorig = os.path.join(ORIG_PATH, '{}.nc'.format(basename[:-9]))
        subprocess.call(['cp', vorig, vsave])

        print('Beginning interpolation')
        # interpolate
        U_cgrid = np.zeros_like(Uagrid)
        V_cgrid = np.zeros_like(Vagrid)
        Ustress_cgrid = np.zeros_like(Ustressagrid)
        Vstress_cgrid = np.zeros_like(Vstressagrid)
        points = (lonT.flatten(), latT.flatten())
        for t in range(U_cgrid.shape[0]):
            print('{}/{}'.format(t, U_cgrid.shape[0]))
            for k in range(U_cgrid.shape[1]):
                U_cgrid[t,k,:,:] = interp.griddata(points,
                                                   Uagrid[t,k,:,:].flatten(),
                                                   (lonU, latU))
                V_cgrid[t,k,:,:] = interp.griddata(points,
                                                   Vagrid[t,k,:,:].flatten(),
                                                   (lonV, latV))
            Ustress_cgrid[t,:,:] = interp.griddata(points,
                                                   Ustressagrid[t,:,:].flatten(),
                                                   (lonU, latU))
            Vstress_cgrid[t,:,:] = interp.griddata(points,
                                                   Vstressagrid[t,:,:].flatten(),
                                                   (lonV, latV))

        print('Finished interpolation')
        AfU.close()
        AfV.close()
        # Saving
        CfU = nc.Dataset(usave, 'r+')
        U = CfU.variables['uos']
        U[:] = np.ma.masked_array(
            U_cgrid[:],
            mask=np.logical_not(umask + np.zeros_like(U_cgrid)))
        U.comment = 'Interpolated from A grid to C grid'
        U.missing_value=float(1.0e20)
        Ustress = CfU.variables['tauuo']
        Ustress[:] = np.ma.masked_array(
            Ustress_cgrid[:],
            mask=np.logical_not(umask[:,0,:,:] + np.zeros_like(Ustress_cgrid)))
        Ustress.comment = 'Interpolated from A grid to C grid'
        CfU.comment1 = 'U fields interpolated from A to C grid'

        CfU.comment2 = 'Created with script {}'.format(os.path.join(os.getcwd(),
                                                               sys.argv[0]))
        CfU.close()

        CfV = nc.Dataset(vsave,'r+')
        V = CfV.variables['vos']
        V[:] = np.ma.masked_array(
            V_cgrid[:],
            mask=np.logical_not(vmask + np.zeros_like(V_cgrid)))
        V.comment = 'Interpolated from A grid to C grid'
        V.missing_value = float(1.0e20)
        Vstress = CfV.variables['tauvo']
        Vstress[:] = np.ma.masked_array(
            Vstress_cgrid[:],
            mask=np.logical_not(vmask[:,0,:,:] + np.zeros_like(Vstress_cgrid)))
        Vstress.comment = 'Interpolated from A grid to C grid'
        CfV.comment1 = 'V fields interpolated from A grid to C grid'
        CfV.comment2 = 'Created with script {}'.format(os.path.join(os.getcwd(),
                                                               sys.argv[0]))
        CfV.close()
        
if __name__ == '__main__':
    mesh = nc.Dataset(MESH)
    lonu = mesh.variables['glamu'][0,:]
    lonv = mesh.variables['glamv'][0,:]
    lont = mesh.variables['glamt'][0,:]
    latu = mesh.variables['gphiu'][0,:]
    latv = mesh.variables['gphiv'][0,:]
    latt = mesh.variables['gphit'][0,:]
    umask = mesh.variables['umask'][:]
    vmask = mesh.variables['vmask'][:]
    datestr=sys.argv[1]
    interp_to_Cgrid(lonu, latu, lonv, latv, lont, latt, umask, vmask, datestr)
