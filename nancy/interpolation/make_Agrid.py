# Script to 'unstagger' velocity fields and write to new netcdf files
# Will not operate on a file if it has already been unstaggered/linked
# Author: Nancy Soontiens Feb 2018


import netCDF4 as nc
import numpy as np
import os
import glob
import sys
import subprocess

MODEL_PATH = '/home/nso001/data/work2/models/ciops-e/orig-cgrid'
SAVEDIR = '/home/nso001/data/work2/models/ciops-e/agrid'
MESHFILE = '/home/nso001/data/work2/models/ciops-e/CIOPSE_InitialTest_Surface_mesh.nc'

def main(ufiles, vfiles, meshfile, savedir=SAVEDIR):
    """ Unstagger and save U velocity in ufiles nd V velocity in vfiles"""
    for fname in ufiles:
        print(fname)
        build_unstaggered_U_file(fname, meshfile, savedir=savedir)
    for fname in vfiles:
        print(fname)
        build_unstaggered_V_file(fname, meshfile, savedir=savedir)

def build_unstaggered_U_file(fname, meshfile, savedir=SAVEDIR):
    # Copy file
    basename = os.path.basename(fname)
    newname = os.path.join(savedir, '{}_Agrid.nc'.format(basename[:-3]))
    if not os.path.exists(newname):
        subprocess.call(['cp', fname, newname])
        # Open file and load U
        fU = nc.Dataset(newname, 'r+')
        uold = fU.variables['uos']
        # Unstagger
        unew = np.empty_like(uold[:])
        unew[...,1:] = np.add(uold[...,:-1], uold[...,1:]) / 2
        uold[:] = unew[:]
        uold.comment = 'unstaggered to A grid (T cells)'
        # wind stress
        ustressold = fU.variables['tauuo']
        # Unstagger 
        ustressnew = np.empty_like(ustressold[:])
        ustressnew[...,1:] = np.add(ustressold[...,:-1], ustressold[...,1:]) / 2
        ustressold[:] = ustressnew[:]
        ustressold.comment = 'unstaggered to A grid (T cells)'

        # Update other variables (nav_lon, nav_lat, depthu, area)
        mesh = nc.Dataset(meshfile)
        lonT = mesh.variables['nav_lon']
        latT = mesh.variables['nav_lat']
        depthT = mesh.variables['gdept_1d'][0,:]
        lon = fU.variables['nav_lon']
        lon[:] = lonT[:]
        lat = fU.variables['nav_lat']
        lat[:] = latT[:]
        depth = fU.variables['depth']
        depth[:] = depthT[:]
        depth.long_name = 'Vertical T levels'
        mesh.close()
        # Update global attributes
        fU.comment1 = 'U unstaggered to A-grid'
        fU.comment2 = 'Created with script {}'.format(os.path.join(
                                                      os.getcwd(),sys.argv[0]))
        # Save (Close file)
        fU.close()
        

def build_unstaggered_V_file(fname, meshfile, savedir=SAVEDIR):
    # Copy file
    basename = os.path.basename(fname)
    newname = os.path.join(savedir, '{}_Agrid.nc'.format(basename[:-3]))
    if not os.path.exists(newname):
        subprocess.call(['cp', fname, newname])
        # Open file and load V
        fV = nc.Dataset(newname, 'r+')
        vold = fV.variables['vos']
        # Unstagger
        vnew = np.empty_like(vold[:])
        vnew[...,1:, :] = np.add(vold[...,:-1, :], vold[...,1:, :]) / 2
        vold[:] = vnew[:]
        vold.comment = 'unstaggered to A grid (T cells)'
        # wind stress
        vstressold = fV.variables['tauvo']
        # Unstagger 
        vstressnew = np.empty_like(vstressold[:])
        vstressnew[...,1:] = np.add(vstressold[...,:-1], vstressold[...,1:]) / 2
        vstressold[:] = vstressnew[:]
        vstressold.comment = 'unstaggered to A grid (T cells)'

        # Update other variables (nav_lon, nav_lat, depth)
        mesh = nc.Dataset(meshfile)
        lonT = mesh.variables['nav_lon']
        latT = mesh.variables['nav_lat']
        depthT = mesh.variables['gdept_1d']
        lon = fV.variables['nav_lon']
        lon[:] = lonT[:]
        lat = fV.variables['nav_lat']
        lat[:] = latT[:]
        depth = fV.variables['depth']
        depth[:] = depthT[:]
        depth.long_name = 'Vertical T levels'
        mesh.close()
        # Update global attributes
        fV.comment1 = 'V unstaggered to A-grid'
        fV.comment2 = 'Created with script {}'.format(os.path.join(
                                                      os.getcwd(),sys.argv[0]))
        # Save (Close file)
        fV.close()

                                                
    
        
if __name__ == '__main__':
    ufiles = glob.glob(os.path.join(MODEL_PATH, '*_grid_U*.nc'))
    vfiles = glob.glob(os.path.join(MODEL_PATH, '*_grid_V*.nc'))
    main(ufiles, vfiles, MESHFILE)
