# Script to average over SVP drouge depths for CIOPS-E
# Usage: python average_SVP_depths.py YYYYMM 
# Author: Nancy Soontiens May 2020

import xarray as xr
import numpy as np
import os
import glob
import sys
import subprocess
import scipy.interpolate as interp
import sys

#CIOPS-E
#ORIG_PATH = '/home/nso001/data/work2/models/ciops-e/CIOPSE_SN1500_3D/'
#SAVEDIR = '/home/nso001/data/work2/models/ciops-e/depth_averaged'

#MESH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc'

# RIOPS
#ORIG_PATH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_rx_021/'
#SAVEDIR='/home/nso001/data/work2/models/ciops-e/riops_depth_averaged'

#MESH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
#year=sys.argv[1]

# GIOPS
ORIG_PATH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/GIOPS_gx_008/'
SAVEDIR='/home/nso001/data/work2/models/ciops-e/giops_depth_averaged'

MESH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/model_mesh_files/GIOPS/mask_3D_GIOPS_v300.nc'
year=sys.argv[1]

def main():
    filesU = glob.glob(os.path.join(ORIG_PATH, '{}*/*grid_U*'.format(year)))
    filesV = glob.glob(os.path.join(ORIG_PATH, '{}*/*grid_V*'.format(year)))
    filesU.sort()
    filesV.sort()

    mesh = xr.open_dataset(MESH)
    umask = mesh.umask.values[0,...]
    vmask = mesh.vmask.values[0,...]
    e3u = mesh.e3u_0.values[0,...]
    e3v = mesh.e3v_0.values[0,...]

    # find indices
    dep = mesh.gdept_1d.values[0,...]
    inds = np.argmin(np.abs(dep-10))
    print(inds, dep[inds])
    inde = np.argmin(np.abs(dep-20))
    print(inde, dep[inde])
    indm = np.argmin(np.abs(dep-15))
    print(indm, dep[indm])
    
    # U first
    for f in filesU:
        basename = os.path.basename(f)
        print(basename)
        new = os.path.join(SAVEDIR, basename)
        d = xr.open_dataset(f)
        u = d.uo.values
        uavg = depth_average(u, umask, e3u, inds, inde)
        d['uavg'] = (['time_counter', 'y','x'], uavg, {'_FillValue': 1.0e20, 'missing_value': 1.0e20})
        d['uavg'].attrs['comment'] = 'current averaged from approximately 10 to 20 m'
        d['uavg'].attrs['description'] = 'depth averaged current on U grid'
        d['uavg'].attrs['start_depth_index'] = float(inds)
        d['uavg'].attrs['start_depth_value'] = dep[inds]
        d['uavg'].attrs['end_depth_index'] = float(inde)
        d['uavg'].attrs['end_depth_value'] = dep[inde]

        d['avg_depth'] = (['avg_depth'], 15*np.ones(1))
        d['avg_depth'].attrs['units'] = 'metres'
        d['avg_depth'].attrs['comment'] = 'approximate depth of currents'
        d.to_netcdf(new)

    # V next
    for f in filesV:
        basename = os.path.basename(f)
        print(basename)
        new = os.path.join(SAVEDIR, basename)
        d = xr.open_dataset(f)
        v = d.vo.values
        vavg = depth_average(v, vmask, e3v, inds, inde)
        d['vavg'] = (['time_counter', 'y','x'], vavg, {'_FillValue': 1.0e20, 'missing_value': 1.0e20})
        d['vavg'].attrs['comment'] = 'current averaged from 10 to 20 m'
        d['vavg'].attrs['description'] = 'depth averaged current on V grid'
        d['vavg'].attrs['start_depth_index'] = float(inds)
        d['vavg'].attrs['start_depth_value'] = dep[inds]
        d['vavg'].attrs['end_depth_index'] = float(inde)
        d['vavg'].attrs['end_depth_value'] = dep[inde]

        d['avg_depth'] = (['avg_depth'], 15*np.ones(1))
        d['avg_depth'].attrs['units'] = 'metres'
        d['avg_depth'].attrs['comment'] = 'approximate depth of currents'
        d.to_netcdf(new)

        
        
def depth_average(var, mask, e3, ist, ied):
    """Compute depth average of a variable between depth indices ist and ied
    var (t,z,y,x)
    mask (z,y,x)
    e3 (z,y,x) - vertical grid spacing""" 
    integrand = var*mask*e3
    integral = np.sum(integrand[:, ist:ied+1, ...], axis=1)
    height = np.sum(e3[ist:ied+1, ...]*mask[ist:ied+1, ...], axis=0)
    height = np.ma.masked_values(height,0)
    avg = integral/height
    return avg
    

if __name__ == '__main__':
    main()
