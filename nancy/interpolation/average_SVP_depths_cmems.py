# Script to average over SVP drouge depths for cmems
# Usage: python average_SVP_depths_cmems.py YYYY MM
# Author: Nancy Soontiens August

import xarray as xr
import numpy as np
import os
import glob
import sys
import subprocess
import scipy.interpolate as interp
import sys

# GIOPS
ORIG_PATH='/home/soontiensn/remote2/hank/cmems/GLOBAL_REANALYSIS_PHY_001_030/global-reanalysis-phy-001-030-daily/'
SAVEDIR='/home/soontiensn/data/cmems/daily_depth_averaged_10to20m'

MESH='/home/soontiensn/remote2/hank/cmems/GLOBAL_REANALYSIS_PHY_001_030/global-reanalysis-phy-001-030-statics/GLO-MFC_001_030_coordinates.nc'
MASK='/home/soontiensn/remote2/hank/cmems/GLOBAL_REANALYSIS_PHY_001_030/global-reanalysis-phy-001-030-statics/GLO-MFC_001_030_mask_bathy.nc'
year=sys.argv[1]
month=sys.argv[2]

VARS_KEEP=['longitude', 'latitude', 'depth', 'uavg', 'vavg', 'avg_depth',
           'time']

def main():
    files = glob.glob(os.path.join(ORIG_PATH,
                                   '{}/*/*_{}{}*'.format(year, year, month)))

    mesh = xr.open_dataset(MESH)
    e3t = mesh.e3t.values[:]

    # find indices
    dep = mesh.depth.values[:]
    inds = np.argmin(np.abs(dep-10))
    print(inds, dep[inds])
    inde = np.argmin(np.abs(dep-20))
    print(inde, dep[inde])
    indm = np.argmin(np.abs(dep-15))
    print(indm, dep[indm])

    maskf = xr.open_dataset(MASK)
    mask = maskf.mask.values[:]
    
    # U first
    for f in files:
        basename = os.path.basename(f)
        print(basename)
        new = os.path.join(SAVEDIR, basename)
        if os.path.exists(new):
            continue
        d = xr.open_dataset(f)
        u = d.uo.values
        uavg = depth_average(u, mask, e3t, inds, inde)
        d['uavg'] = (['time', 'latitude','longitude'], uavg, {'_FillValue': 1.0e20, 'missing_value': 1.0e20})
        d['uavg'].attrs['comment'] = 'current averaged from approximately 10 to 20 m'
        d['uavg'].attrs['description'] = 'depth averaged current on U grid'
        d['uavg'].attrs['start_depth_index'] = float(inds)
        d['uavg'].attrs['start_depth_value'] = dep[inds]
        d['uavg'].attrs['end_depth_index'] = float(inde)
        d['uavg'].attrs['end_depth_value'] = dep[inde]

        d['avg_depth'] = (['avg_depth'], 15*np.ones(1))
        d['avg_depth'].attrs['units'] = 'metres'
        d['avg_depth'].attrs['comment'] = 'approximate depth of currents'

        v = d.vo.values
        vavg = depth_average(v, mask, e3t, inds, inde)
        d['vavg'] = (['time', 'latitude','longitude'], vavg, {'_FillValue': 1.0e20, 'missing_value': 1.0e20})
        d['vavg'].attrs['comment'] = 'current averaged from 10 to 20 m'
        d['vavg'].attrs['description'] = 'depth averaged current on V grid'
        d['vavg'].attrs['start_depth_index'] = float(inds)
        d['vavg'].attrs['start_depth_value'] = dep[inds]
        d['vavg'].attrs['end_depth_index'] = float(inde)
        d['vavg'].attrs['end_depth_value'] = dep[inde]

        # vars to delete
        drop = []
        for var in d.variables:
            if var not in VARS_KEEP:
                drop.append(var)
        print(drop)
        d = d.drop(labels=drop)
        
        d.to_netcdf(new)

        
        
def depth_average(var, mask, e3, ist, ied):
    """Compute depth average of a variable between depth indices ist and ied
    var (t,z,y,x)
    mask (z,y,x)
    e3 (z,y,x) - vertical grid spacing""" 
    integrand = var*mask*e3
    integral = np.sum(integrand[:, ist:ied+1, ...], axis=1)
    height = np.sum(e3[ist:ied+1, ...]*mask[ist:ied+1, ...], axis=0)
    height = np.ma.masked_values(height,0)
    avg = integral/height
    return avg
    

if __name__ == '__main__':
    main()
