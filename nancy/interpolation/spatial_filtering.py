# Script to spatially filter/smooth model data
# Usage: python spatial_filtering.py YYYYMM 
# Author: Nancy Soontiens July 2020

import xarray as xr
import numpy as np
import os
import glob
import sys
import subprocess
from scipy import ndimage
import sys

#SIZE = 20
SIZE=10

#CIOPS-E
#ORIG_PATH = '/home/nso001/data/work2/models/ciops-e_bc12_plus/orig/'
SAVEDIR = '/home/nso001/data/work2/models/ciops-e_bc12_plus/filter_{}x{}'.format(SIZE,SIZE)

#RIOPS
ORIG_PATH='/home/nso001//data/work2/models/riops_bc12_plus/hourly/'
SAVEDIR = '/home/nso001/data/work2/models/riops_bc12_plus/filter_{}x{}'.format(SIZE,SIZE)


#MESH='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc'

uvar='u15'
vvar='v15'
year=sys.argv[1]

def main():
    filesU = glob.glob(os.path.join(ORIG_PATH, '*/*grid_U*'))
    filesV = glob.glob(os.path.join(ORIG_PATH, '*/*grid_V*'))
    filesU.sort()
    filesV.sort()
    
    # U first
    for f in filesU:
        basename = os.path.basename(f).split('.')[0]
        print(basename)
        new = os.path.join(SAVEDIR, '{}_filter{}.nc'.format(basename, SIZE))
        d = xr.open_dataset(f)
        dnew = filter_dataset(d, [uvar, ], size=SIZE)
        dnew.to_netcdf(new)

    # V next
    for f in filesV:
        basename = os.path.basename(f).split('.')[0]
        print(basename)
        new = os.path.join(SAVEDIR, '{}_filter{}.nc'.format(basename, SIZE))
        d = xr.open_dataset(f)
        dnew = filter_dataset(d, [vvar, ], size=SIZE)
        dnew.to_netcdf(new)
        

def filter_dataset(d, variables, size=10):
    for v in variables:
        data_var = d.variables[v]
        data = np.ma.masked_invalid(data_var.values)
        mask = data.mask
        filtered_data = ndimage.uniform_filter(np.ma.filled(data,0),
                                               size=size,
                                               mode='nearest')
        filtered_data = np.ma.masked_array(filtered_data,mask)
        new_var='{}_filtered'.format(v)
        d[new_var] = (data_var.dims,
                      filtered_data,
                      {'_FillValue': 1.0e20, 'missing_value': 1.0e20})
        d[new_var].attrs['units'] = 'm/s'
        d[new_var].attrs['comment'] =\
            'spatially filtered with uniform filter size {} '\
            'grid points'.format(size)
    return d
    

if __name__ == '__main__':
    main()
