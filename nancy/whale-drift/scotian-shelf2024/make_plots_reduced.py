import glob
import pickle

import xarray as xr
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
from matplotlib.lines import Line2D
import matplotlib.colors as mcolors

import datetime
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import pandas as pd

import warnings


def make_circle_of_points(lon, lat, diff_deg, num=20):
    lons = np.linspace(lon - diff_deg, lon + diff_deg,num=num)
    lats = np.linspace(lat - diff_deg, lat + diff_deg,num=num)
    lons, lats = np.meshgrid(lons, lats)
    # Remove points outside of radisu
    inds = np.where(np.sqrt((lons-lon)**2 + (lats-lat)**2) > diff_deg)
    lons[inds] = np.nan
    lats[inds] = np.nan
    lons = lons.flatten()[~np.isnan(lons.flatten())]
    lats = lats.flatten()[~np.isnan(lats.flatten())]
    return lons, lats


def make_perimeter_of_points(lon, lat, diff_deg, num=20):
    thetas = np.linspace(0, 2*np.pi, num=num)
    lons = [lon + diff_deg*np.cos(theta) for theta in thetas]
    lats = [lat + diff_deg*np.sin(theta) for theta in thetas]
    return lons, lats


def plot(ds, ax,  target_date, initial_lon, initial_lat, color, delta_t=-1):
    dp=ds.where((ds.lon<360) | (ds.lat<360), np.nan)
    start_lons = dp.lon.isel(time=0).values
    start_lats = dp.lat.isel(time=0).values
    inds=np.where((np.abs(start_lons-initial_lon)< 0.01) &\
                  (np.abs(start_lats-initial_lat)< 0.01))
    print(inds)
    dp = dp.isel(trajectory=inds[0])
    if delta_t == -1:
        dend=dp.where((pd.to_datetime(ds.time.values)>=target_date + delta_t*datetime.timedelta(days=1))
             & (pd.to_datetime(ds.time.values) <=target_date))
    else:
        dend=dp.where((pd.to_datetime(ds.time.values)>=target_date) &
                  (pd.to_datetime(ds.time.values) <=target_date + delta_t*datetime.timedelta(days=1)))
    # Tracking number of particles launched via status first time
    # (stauts=1 means stranded)
    num_particles_launched = np.sum(dp.status.isel(time=0).values==0)
    # cataloging lons and lats on end day
    lons_end = np.ma.masked_invalid(dend.lon.values)
    lons_end = lons_end[~lons_end.mask].data
    lats_end = np.ma.masked_invalid(dend.lat.values)
    lats_end = lats_end[~lats_end.mask].data

    for t in dp.trajectory.values:
        dp_t = dp.sel(trajectory=t)
        dend_t = dend.sel(trajectory=t)
        lons = np.ma.masked_invalid(dp_t.lon.values)
        lats = np.ma.masked_invalid(dp_t.lat.values)
        lons_sd = np.ma.masked_invalid(dend_t.lon.values)
        lats_sd = np.ma.masked_invalid(dend_t.lat.values)
        #print(lons[~lons.mask])
        ax.plot(lons[~lons.mask], lats[~lats.mask], color=color, lw=0.1,transform=ccrs.PlateCarree())
        #if np.any(~lons_sd.mask):
        #    ax.plot(lons_sd[~lons_sd.mask], lats_sd[~lats_sd.mask], '.', color='C1',
        #            transform=ccrs.PlateCarree(),zorder=10)
    return num_particles_launched, lons_end, lats_end


def main(run, drift):
    data_dir=f'/data/whale-drift/{run}'

    diff_deg = 0.1
    num=20

    # Where strike occured
    strike_lon = -63.0617
    strike_lat =  44.625
    strike_date = datetime.datetime(2024,4,18) # 11:20am - not sure time zone

    # Where sighting occured
    sight_lon = -65.05138889
    sight_lat = 42.19694444
    sight_date = datetime.datetime(2024, 5, 13) #sighting on May 2 but not sure what time so project to May 13

    strike_lons, strike_lats = make_circle_of_points(strike_lon, strike_lat, diff_deg,num=num)
    strike_perim_lons, strike_perim_lats = make_perimeter_of_points(strike_lon, strike_lat, diff_deg,num=num)

    sight_lons, sight_lats = make_circle_of_points(sight_lon, sight_lat, diff_deg,num=num)
    sight_perim_lons, sight_perim_lats = make_perimeter_of_points(sight_lon, sight_lat, diff_deg,num=num)


    n_lines = 6
    cmap = mpl.colormaps['plasma']

    windages = np.arange(0.00, 0.06, 0.01)
    colors = cmap(np.linspace(0, 1, n_lines))
    colour_mapping = {'{:.2f}'.format(w): c for w,c in zip(windages, colors)}
    windages_str = ['0.0', '0.01', '0.02', '0.03', '0.04', '0.05']

    # Do the plots
    if drift == 'forward':
        start_lons = strike_lons
        start_lats = strike_lats
        start_lon = strike_lon
        start_lat = strike_lat
        start_perim_lons = strike_perim_lons
        start_perim_lats = strike_perim_lats
        end_perim_lons = sight_perim_lons
        end_perim_lats = sight_perim_lats
        end_lon = sight_lon
        end_lat = sight_lat
        delta_t = -1
        starts = [strike_date + datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
        target_date = sight_date
    else:
        start_lons = sight_lons
        start_lats = sight_lats
        start_lon = sight_lon
        start_lat = sight_lat
        start_perim_lons = sight_perim_lons
        start_perim_lats = sight_perim_lats
        end_perim_lons = strike_perim_lons
        end_perim_lats = strike_perim_lats
        end_lon = strike_lon
        end_lat = strike_lat
        delta_t = 1
        starts = [sight_date - datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
        target_date = strike_date

    fig, axs = plt.subplots(2,3,subplot_kw={'projection': ccrs.PlateCarree()},
                            figsize=(10,5),
                            sharex=True, sharey=True, constrained_layout=True)
    num_launches ={windage: 0 for windage in windages_str}
    lons_end_all = {windage: np.empty((0,)) for windage in windages_str}
    lats_end_all = {windage: np.empty((0,)) for windage in windages_str}
    for s in starts:
        sd = s.strftime("%Y%m%d%H")
        for windage, ax in zip(windages_str, axs.flatten()):
            files = glob.glob(f'{data_dir}/*{sd}*{drift}*wind{windage}.nc')
            print(files)
            d = xr.open_dataset(files[0])
            with warnings.catch_warnings():
                warnings.simplefilter("ignore")
                num, lons_end, lats_end = plot(d, ax, target_date, start_lon, start_lat,
                                                colour_mapping['{:.2f}'.format(float(windage))],
                                                delta_t=delta_t)
                num_launches[windage]+=num
                lons_end_all[windage] = np.append(lons_end_all[windage], lons_end)
                lats_end_all[windage] = np.append(lats_end_all[windage], lats_end)
            d.close()
    # Pretty the plot and add histogram
    lon_min, lon_max = -67, -61
    lat_min, lat_max = 40.5, 45.5
    hist_bins_lons = np.arange(lon_min, lon_max, step=0.2)
    hist_bins_lats = np.arange(lat_min, lat_max, step=0.2)
    grid_lons, grid_lats = np.meshgrid(hist_bins_lons, hist_bins_lats)
    for windage, ax in zip(windages_str, axs.flatten()):
        counts, _, _ = np.histogram2d(lons_end_all[windage],
                                      lats_end_all[windage],
                                      bins=[hist_bins_lons, hist_bins_lats])
        counts = np.ma.masked_values(counts, 0)/(num_launches[windage]*25)
        print(counts.max())
        boundaries = np.arange(0, 0.55, 0.05)
        cmap = plt.cm.get_cmap('gist_heat_r',len(boundaries))
        mesh = ax.pcolormesh(
            grid_lons, grid_lats, counts.T, zorder=20,alpha=0.8,
            cmap=cmap,norm=mcolors.BoundaryNorm( boundaries, len(boundaries)+1),
            )

        ax.coastlines()
        ax.add_feature(cfeature.LAND)
        ax.add_feature(cfeature.OCEAN)
        #ax.plot(start_perim_lons,start_perim_lats, 'C2',lw=1.0, transform=ccrs.PlateCarree())
        ax.plot(start_lon, start_lat, 'k*', transform=ccrs.PlateCarree())
        #ax.plot(end_perim_lons, end_perim_lats, 'C3-',lw=1.0,
        #        transform=ccrs.PlateCarree(),zorder=20)
        ax.plot(end_lon, end_lat,
                'C3*', transform=ccrs.PlateCarree(),zorder=20)
        ax.set_xlim([lon_min, lon_max])
        ax.set_ylim([lat_min, lat_max])
        ax.set_title(f'windage: {float(windage)*100}%')
    axs[1,0].gridlines(draw_labels=['bottom', 'left'],linestyle='--',alpha=0.5)
    axs[0,0].gridlines(draw_labels=[ 'left'],linestyle='--',alpha=0.5)
    axs[1,1].gridlines(draw_labels=['bottom'],linestyle='--',alpha=0.5)
    axs[1,2].gridlines(draw_labels=['bottom'],linestyle='--',alpha=0.5)
    axs[0,1].gridlines(linestyle='--',alpha=0.5)
    axs[0,2].gridlines(linestyle='--',alpha=0.5)
    cbar = fig.colorbar(mesh, ax=axs)
    legend_date = target_date
    if drift == 'forward':
        legend_date = legend_date - datetime.timedelta(hours=1)
    cbar.set_label(f'Particle density \n on {legend_date.strftime("%b %d, %Y")}')
    # Add legend
    #sp = Line2D([0], [0], label='Start radius', color='C2', lw=1.0)
    #ep = Line2D([0], [0], label='Target radius', color='C3', lw=1.0)
    if drift == 'forward':
        star_s = Line2D([0], [0], label='Strike location', color='k',marker='*',
            linestyle='')
        star_e = Line2D([0], [0], label='Sight location', color='C3',marker='*',
            linestyle='')
    else:
        star_s = Line2D([0], [0], label='Sight location', color='k',marker='*',
            linestyle='')
        star_e = Line2D([0], [0], label='Strike location', color='C3',marker='*',
            linestyle='')
    handles = [star_s, star_e, ]
    leg2 = fig.legend(handles=handles, loc='upper right', bbox_to_anchor=(-.2,1.0),
                     bbox_transform=axs[0,0].transAxes)
    handles = [ Line2D([0], [0], label=f'{float(windage)*100}%',
        color=colour_mapping['{:.2f}'.format(float(windage))],) for windage in windages_str]
    leg1 = fig.legend(handles=handles, loc='upper right', bbox_to_anchor=(-0.2,1.0),
                      bbox_transform=axs[1,0].transAxes, title="Track lines\nfor each windage",
                      )
    plt.setp(leg1.get_title(), multialignment='center')
    axs[0,0].add_artist(leg2)
    # Saving
    fig.suptitle(f'{drift.capitalize()} tracking \n Hourly releases on {starts[1].strftime("%b %d, %Y")}\n{run}')
    fig.savefig(f'{drift}-{run}windages-hourlyreleases-{starts[1].strftime("%Y%m%d")}_reduced.png', bbox_inches='tight')
    #with open(f'{data_dir}/{drift}-fig.pickle', 'wb') as f:
    #    pickle.dump(fig, f)

if __name__=='__main__':
    for run in ['ciopse-hrdps', 'glorys12-era5']:
        for drift in ['forward', 'backward']:
            print(run, drift)
            main(run, drift)
