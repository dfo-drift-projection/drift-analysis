

# User Defined Parameters
# -----------------------
# MINICONDA_PATH: path to drift-tool python environment
#MINICONDA_PATH=/home/nso001/data/work7/miniconda-gpsc7-new/envs/develop/
# CONFIG_FILE: experiment specific configuation file
CONFIG_FILE=eval-glorys12.yaml
# EXPERIMENT_DIR: path to user defined output directory
EXPERIMENT_DIR=/data/whale-drift/glorys12-eval-windage0.00/

# Set environment
# ---------------
#old_PYTHONPATH=$PYTHONPATH
#unset PYTHONPATH
#export PATH=$MINICONDA_PATH/bin:$PATH
#export PYTHONPATH=$MINICONDA_PATH/bin


LOG=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/logs/eval-windage0.00.log

drift_predict -c $CONFIG_FILE --experiment-dir $EXPERIMENT_DIR
