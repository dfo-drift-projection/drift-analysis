# This configuration file contains run settings for a DriftEval simulation using
# OpenDrift, CIOPS-W and HRDPS mshydro atmospheric forcing. It serves as an
# example of how to create similar simulations with NEMO C-grid ocean output.
# Note: the ocean-mesh-file and rotation-data-file settings are required for
# NEMO C-grid ocean output when running with OpenDrift. If no rotation-data-file
# exists yet then please specify the path where it will be saved and the program
# will create it.

# At present, only atmospheric output with winds aligned with east/north can be
# used. To run without wind forcing, remove the 'Atmos data settings' section.

# Each entry below represents an argument for the main DriftEval program.
# For a description of each argument use "drift_predict --help"
# Hint: you will need to have the drift-tool installed in your python environment
# to execute the help command. Or run in the repos src/driftutils directory.

# In this example, virtual drifters are compared with observed SCT drifters
# from the waterproperties dataset. Virtual drifters are released at the surface
# every 6 hours between 2016-06-08 and 2016-06-10 for a duration of 48 hours. The
# observed drifter trajectory is used to determine the initial position of each
# virtual drifter. Virtual drifters are influenced by the ocean currents from
# CIOPS-W and 2% of the winds from the HRDPS mshydro database.

# Please see run_drift_eval.sh for an example of how to use a configuration file
# like this one.

# Ocean data settings
ocean-model-name: GLORYS12
ocean-data-dir: "/data/ocean/GLORYS12v1/whale-drift/2024/"
ocean-mesh-file: None
xwatervel: uo
ywatervel: vo
model-time-ocean: time
lon-var: longitude
lat-var: latitude
grid-type: user_grid

# Atmos data settings
atmos-model-name: ERA5
atmos-data-dir: "/data/ocean/ERA5/scotian-shelf/drift-tool"
alpha-wind: 0.0
xwindvel: u10
ywindvel: v10
model-time-atmos: valid_time
lon-var-atmos: longitude
lat-var-atmos: latitude

# Observed drifter settings
drifter-data-dir: "/data/whale-drift/observations/drifters-qcd"
drifter-id-attr: buoyid

# Virtual drifter settings
drifter-depth: 0c

# Run settings
drift-model-name: OpenDrift
first-start-date: '2024-04-23'
last-start-date: '2024-04-28'
drift-duration: 48
start-date-frequency: hourly
start-date-interval: 1
land-mask-type: ocean_model
opendrift-dt: 60.
overwrite-savedir: False
