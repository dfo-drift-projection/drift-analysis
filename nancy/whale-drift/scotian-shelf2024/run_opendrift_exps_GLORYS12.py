import datetime
import glob
import os

import numpy as np
import xarray as xr

from opendrift.models import oceandrift
from opendrift.readers import reader_netCDF_CF_generic

OUTDIR = '/data/whale-drift/glorys12-era5_diff5'


# Supporting functions
def make_circle_of_points(lon, lat, diff_deg, num=20):
    lons = np.linspace(lon - diff_deg, lon + diff_deg, num=num)
    lats = np.linspace(lat - diff_deg, lat + diff_deg, num=num)
    lons, lats = np.meshgrid(lons, lats)
    # Remove points outside of radisu
    inds = np.where(np.sqrt((lons-lon)**2 + (lats-lat)**2) > diff_deg)
    lons[inds] = np.nan
    lats[inds] = np.nan
    lons = lons.flatten()[~np.isnan(lons.flatten())]
    lats = lats.flatten()[~np.isnan(lats.flatten())]
    return lons, lats


def make_perimeter_of_points(lon, lat, diff_deg, num=20):
    thetas = np.linspace(0, 2*np.pi, num=num)
    lons = [lon + diff_deg*np.cos(theta) for theta in thetas]
    lats = [lat + diff_deg*np.sin(theta) for theta in thetas]
    return lons, lats


def run_opendrift(lons, lats, stime, duration,
                  winds, reader_ocean, reader_atmos, reader_land, outfile,
                  logfile,
                  time_step=900, diffusivity=0):
    urms = np.sqrt(2*diffusivity/np.abs(time_step))
    o = oceandrift.OceanDrift(loglevel=20, seed=None, logfile=logfile)
    o.max_speed = 5
    o.add_reader([reader_land])
    o.add_reader([reader_ocean, reader_atmos])
    o.set_config('drift:advection_scheme', 'runge-kutta4')
    o.set_config('general:coastline_action', 'stranding')
    o.set_config('seed:ocean_only', False)
    o.set_config('general:use_auto_landmask', False)
    o.set_config('drift:current_uncertainty', urms)
    o.seed_elements(lon=lons, lat=lats, z=0,
                    time=stime, wind_drift_factor=winds)
    o.run(time_step=time_step, time_step_output=3600,
           steps=int(duration.total_seconds()/np.abs(time_step)),
           outfile=outfile, export_buffer_length=500)


def main():
    # Constants such as locations and timing
    # Where strike occured
    strike_lon = -63.0617
    strike_lat = 44.625
    start_date = datetime.datetime(2024, 4, 18)  # 11:20am - TZ?
    # Where sighting occured
    sight_lon = -65.05138889
    sight_lat = 42.19694444
    end_date = datetime.datetime(2024, 5, 13)  # sighting on May 12 unsure time - end May 13

    # Seeding particles
    diff_deg = 0.1  # radius around centre for seeeding (degrees)
    num = 20  # number of particles in x/y directions
    strike_lons, strike_lats = make_circle_of_points(strike_lon,
                                                     strike_lat,
                                                     diff_deg,
                                                     num=num)
    strike_perim_lons, strike_perim_lats = make_perimeter_of_points(strike_lon,
                                                                    strike_lat,
                                                                    diff_deg,
                                                                    num=num)
    sight_lons, sight_lats = make_circle_of_points(sight_lon,
                                                   sight_lat,
                                                   diff_deg,
                                                   num=num)
    sight_perim_lons, sight_perim_lats = make_perimeter_of_points(sight_lon,
                                                                  sight_lat,
                                                                  diff_deg,
                                                                  num=num)

    # Diffusivity
    Kdiff = 5 #m^2/s

    # Prepare readers and model data
    # Ocean
    glorys = '/data/ocean/GLORYS12v1/whale-drift/2024/*.nc'
    glorys_files = glob.glob(glorys)
    glorys_files.sort()
    reader_ocean = reader_netCDF_CF_generic.Reader(glorys_files)
    # Land mask
    d = xr.open_dataset(glorys_files[0])
    landmask = (np.isnan(d['uo'].values[0,0,:,:]))*1.0
    ds = xr.Dataset(data_vars=dict(land_binary_mask=(['latitude','longitude'], landmask)), 
                    coords=dict(latitude=d.latitude, longitude= d.longitude))
    reader_land = reader_netCDF_CF_generic.Reader('land_mask.nc')

    # Atmos
    era5 = '/data/ocean/ERA5/scotian-shelf/*clean.nc'
    era5_files = glob.glob(era5)
    mapping = {
        'u10': 'x_wind',
        'v10': 'y_wind',
        'valid_time': 'time'
    }
    reader_atmos = reader_netCDF_CF_generic.Reader(era5_files, standard_name_mapping=mapping)

    # Windage
    wdfs = np.arange(0.0, 0.06, 0.01)
    # forward first
    starts = [start_date + datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
    for start in starts:
        print(start)
        duration = end_date - start
        os.makedirs(OUTDIR, exist_ok=True)
        for wdf in wdfs:
            print(wdf)
            outfile = os.path.join(
                OUTDIR,
                f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}-diff{Kdiff}.nc'
            )
            logfile = os.path.join(
                OUTDIR,
                f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-forward-wind{wdf}-diff{Kdiff}.log'
            )
            run_opendrift(strike_lons,
                          strike_lats, start, duration, wdf,
                          reader_ocean, reader_atmos, reader_land, outfile, logfile,
                          time_step=900, diffusivity=Kdiff)
    # Backwards
    starts = [end_date - datetime.timedelta(hours=h) for h in np.arange(0.0, 25.0)]
    for start in starts:
        print(start)
        duration = start - start_date
        os.makedirs(OUTDIR, exist_ok=True)
        for wdf in wdfs:
            print(wdf)
            outfile = os.path.join(
                OUTDIR,
                f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}-diff{Kdiff}.nc'
            )
            logfile = os.path.join(
                OUTDIR,
                f'opendrift-glorys12-era5-{start.strftime("%Y%m%d%H")}-backward-wind{wdf}-diff{Kdiff}.log'
            )
            run_opendrift(sight_lons,
                          sight_lats, start, duration, wdf,
                          reader_ocean, reader_atmos, reader_land, outfile, logfile,
                          time_step=-900, diffusivity=Kdiff)

if __name__=='__main__':
    main()
