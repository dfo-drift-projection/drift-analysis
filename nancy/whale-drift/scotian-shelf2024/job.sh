#! /bin/bash -l
#
#SBATCH --job-name=correction_nemo_cgrid.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/logs/jobs/correction_nemo_cgrid.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/logs/correction.log
cd /home/nso001/code/drifters/drift-analysis/nancy/whale-drift/scotian-shelf2024/
# Call your program
./run_drift_correction_factor.sh > $LOG 2>&1
