#! /bin/bash -l
#
#SBATCH --job-name=drift-eval.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/logs/jobs/eval.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

# User Defined Parameters
# -----------------------
# MINICONDA_PATH: path to drift-tool python environment
MINICONDA_PATH=/home/nso001/data/work7/miniconda-gpsc7-new/envs/develop/
# CONFIG_FILE: experiment specific configuation file
CONFIG_FILE=eval.yaml
# EXPERIMENT_DIR: path to user defined output directory
EXPERIMENT_DIR=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/eval-windage0.00/

# Set environment
# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin


LOG=/home/nso001/data/work7/OPP/whale-drift/scotian-shelf2024/logs/eval-windage0.00.log
cd /home/nso001/code/drifters/drift-analysis/nancy/whale-drift/scotian-shelf2024/

drift_predict -c $CONFIG_FILE --experiment-dir $EXPERIMENT_DIR > $LOG 2>&1
