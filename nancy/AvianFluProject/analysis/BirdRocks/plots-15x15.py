import datetime
import glob
import os

import cartopy.geodesic as cgeodesic
import cartopy.crs as ccrs
from geopy.distance import distance
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import shapely
import xarray as xr

import matplotlib
matplotlib.use('agg')


colonies = {'BirdRocks': [47.83, -61.15]}
LON_MIN=-67.5
LON_MAX=-56
LAT_MIN=44
LAT_MAX=52
lat, lon = colonies['BirdRocks']

plot_proj = ccrs.Miller()
data_proj = ccrs.PlateCarree()


# Parameters to define foraging distance circle
radius_in_meters=92500


def main():
    data_dir='/home/soontiensn/data/gpsc-work2/OPP/AvianFluProject/DriftMap/15x15/CIOPSE-HRDPS-0.03/output/'

    files = glob.glob(os.path.join(data_dir, '*.nc'))
    files.sort()

    for file in files:
        fig = generate_maps(file)
    

def plot_map(myd):
  
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(1,1,1, projection=plot_proj)
    ax.tissot(rad_km=radius_in_meters/1000,lats=lat,lons=lon,facecolor='none', edgecolor='gray')
    ax.set_extent([LON_MIN,LON_MAX,LAT_MIN,LAT_MAX])
    ax.coastlines()
    gl = ax.gridlines(draw_labels=['bottom', 'left'])

    #plot colony
    ax.plot(lon,lat, '*',ms=10, transform=data_proj)

    #plot starts
    #mesh = ax.scatter(myd.mod_lon.values[:,0], myd.mod_lat.values[:,0], marker='o', c=myd.distance_from_colony.values, 
    #                  transform=data_proj, cmap='copper')
    ax.plot(myd.mod_lon.values[:,0], myd.mod_lat.values[:,0],'oC2',ms=3, transform=data_proj)
    # plot stops
    #mesh = ax.scatter(myd.mod_lon.values[:,-1], myd.mod_lat.values[:,-1],marker='s', c=myd.distance_from_colony.values, 
    #                  transform=data_proj, cmap='copper')
    #cbar = plt.colorbar(mesh, ax=ax)
    ax.plot(myd.mod_lon.values[:,-1], myd.mod_lat.values[:,-1],'oC3',ms=3,transform=data_proj)


    #plot tracks
    num_runs = myd.model_run.size
    for i in range(num_runs):
        ax.plot(myd.mod_lon.values[i,:], myd.mod_lat.values[i,:], 'k-', lw=0.5,alpha=0.5, transform=data_proj)
    # Formatting for title/save string
    timedelta = myd.time.values[-1] - myd.time.values[0]
    timedelta = timedelta.astype(np.timedelta64(1,'D'))/np.timedelta64(1,'D')
    start =  pd.to_datetime(myd.time.values[0])
    now = pd.to_datetime(myd.time.values[-1])
    model_des = '{} with {}% winds from {}'.format(myd.mod_ocean_model,
                                                   myd.mod_alpha_wind, 
                                                   myd.mod_atmos_model)
    ax.set_title('{} \n {} + {} days \nDate: {}'.format(model_des, 
                                                        start.strftime('%Y-%m-%d %H:%M:%S'),
                                                        timedelta,
                                                        now.strftime('%Y-%m-%d %H:%M:%S')))
    return fig


def generate_maps(filename):
    # Open data
    d = xr.open_dataset(filename)
    
    # filter data outside of foraging range
    initial_lon = d.mod_lon.values[:,0]
    initial_lat = d.mod_lat.values[:,0]
    distance_from_colony = np.empty(initial_lon.shape) 
    for i in range(distance_from_colony.size):
        distance_from_colony[i] = distance((lat,lon), (initial_lat[i], initial_lon[i])).km
    distance_from_colony = xr.DataArray(data=distance_from_colony, dims='model_run')
    d['distance_from_colony'] = distance_from_colony
    dnew = d.where(distance_from_colony<radius_in_meters/1000,drop=True)
    print(dnew)
    
    # Set up save directory
    thisdir='15x15'
    run_save_dir='{}_{}_{}'.format(dnew.mod_ocean_model,
                                   dnew.mod_atmos_model,
                                   dnew.mod_alpha_wind)
    if not os.path.exists(f'{thisdir}/{run_save_dir}'):
        os.makedirs(f'{thisdir}/{run_save_dir}')
    times = dnew.time.values
    start = times[0]
    end = times[-1]
    timesave = pd.to_datetime(start).strftime('%Y%m%d%H')
    
    if not os.path.exists('{}/{}/{}'.format(thisdir,run_save_dir,timesave)):
        os.makedirs('{}/{}/{}'.format(thisdir,run_save_dir,timesave))
    
    # Loop over times
    dplot = start
    while dplot <= end:
        dtime = dnew.where(dnew.time<=dplot ,drop=True)
        timedelta = dtime.time.values[-1] - dtime.time.values[0]
        timedelta = timedelta.astype(np.timedelta64(1,'D'))/np.timedelta64(1,'D')
        fig = plot_map(dtime)
        fsave = '{}-{}_P{}D.png'.format(run_save_dir.lower(),
                                    timesave,
                                    str(int(timedelta)).zfill(3))
        savename = os.path.join(thisdir,
                                run_save_dir,
                                timesave,
                                fsave
                               )
        print(savename)
        fig.savefig(savename, bbox_inches='tight', transparent=False, dpi=300, facecolor='white')
        dplot = dplot + np.timedelta64(3, 'D')
    return fig


if __name__ == '__main__':
    main()
