import datetime
import glob
import os

import cartopy.geodesic as cgeodesic
import cartopy.crs as ccrs
from geopy.distance import distance
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import shapely
import xarray as xr

import matplotlib
matplotlib.use('agg')

import colonies

cols = colonies.colonies
COLNAME = 'Baccalieu'

lat =  cols[COLNAME]['lat']
lon =  cols[COLNAME]['lon']
radius_in_meters =  cols[COLNAME]['radius']

dlon=3
dlat=3
LON_MIN=lon - (dlon) 
LON_MAX=lon + (dlon)
LAT_MIN=lat - dlat
LAT_MAX=lat + dlat

MONTH='June2022'
run_details='CIOPSE-HRDPS-0.03'

plot_proj = ccrs.Miller()
data_proj = ccrs.PlateCarree()


def main():
    data_dir=f'/home/soontiensn/data/gpsc-work2/OPP/AvianFluProject/DriftCircle/{COLNAME}/{run_details}/{MONTH}/runs/*/'

    files = glob.glob(os.path.join(data_dir, '*.nc'))
    files.sort()
    stats_all = pd.DataFrame()
    for file in files:
        fig, stats = generate_maps(file)
        stats_all = pd.concat([stats_all, stats],ignore_index=True)
    stats_all = stats_all.reset_index(drop=True)
    # Save and plot stats
    run_save_dir=f'{MONTH}/{COLNAME}/{run_details}'                                                                                                   
    if not os.path.exists(run_save_dir):                                                                                                              
        os.makedirs(run_save_dir) 
    out_csv = f'{run_save_dir}/stats.csv'
    stats_all.to_csv(out_csv)
    fig = plot_stats(stats_all);
    fig.savefig(f'{run_save_dir}/stats.png', dpi=300, bbox_inches='tight')
    
    
def plot_map(myd):
  
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(1,1,1, projection=plot_proj)
    ax.tissot(rad_km=radius_in_meters/1000,lats=lat,lons=lon,facecolor='none', edgecolor='gray')
    ax.set_extent([LON_MIN,LON_MAX,LAT_MIN,LAT_MAX])
    ax.coastlines()
    gl = ax.gridlines(draw_labels=['bottom', 'left'])

    #plot colony
    ax.plot(lon,lat, '*',ms=10, transform=data_proj)

    #plot starts
    ax.plot(myd.lon.values[:,0], myd.lat.values[:,0],'oC2',ms=3, transform=data_proj)
    # plot stops
    ax.plot(myd.lon.values[:,-1], myd.lat.values[:,-1],'oC3',ms=3,transform=data_proj)


    #plot tracks
    num_runs = myd.ntraj.size
    for i in range(num_runs):
        ax.plot(myd.lon.values[i,:], myd.lat.values[i,:], 'k-',
                lw=0.5,alpha=0.5, transform=data_proj)
    # Formatting for title/save string
    timedelta = myd.time.values[-1] - myd.time.values[0]
    timedelta = timedelta.astype(np.timedelta64(1,'D'))/np.timedelta64(1,'D')
    start =  pd.to_datetime(myd.time.values[0])
    now = pd.to_datetime(myd.time.values[-1])
    ax.set_title('{} \n {} + {} days \nDate: {}'.format(run_details, 
                                                        start.strftime('%Y-%m-%d %H:%M:%S'),
                                                        timedelta,
                                                        now.strftime('%Y-%m-%d %H:%M:%S')))
    return fig


def prepare_datafile(filename):
    d = xr.open_dataset(filename)
    # filter on land
    d = d.where(d.moving==1, drop=True)
    #mask for land
    mask = np.ma.masked_invalid(d.land_binary_mask.values)
    d = d.where(~mask.mask)
    # Add variable for distance from colony
    lons = d.lon.values
    lats = d.lat.values
    distance_from_colony = np.empty(lons.shape) 
    for i in range(distance_from_colony.shape[0]):
        for j in range(distance_from_colony.shape[-1]):
            if ~mask.mask[i,j]:
                distance_from_colony[i, j] = distance((lat,lon), (lats[i, j], lons[i, j])).km
            else:
                distance_from_colony[i, j] = np.nan
    distance_from_colony = xr.DataArray(data=distance_from_colony, dims=('ntraj', 'time'))
    d['distance_from_colony'] = distance_from_colony
    initial_distance_from_colony=xr.DataArray(data=d.distance_from_colony.values[:,0], dims=('ntraj'))
    d['initial_distance_from_colony'] = initial_distance_from_colony
    d = d.where(initial_distance_from_colony<radius_in_meters/1000,drop=True)    
    return d


def generate_maps(filename):
    # Open data
    d = prepare_datafile(filename)
    
    # Set up save directory
    run_save_dir=f'{MONTH}/{COLNAME}/{run_details}'
    if not os.path.exists(run_save_dir):
        os.makedirs(run_save_dir)
    times = d.time.values
    start = times[0]
    end = times[-1]
    timesave = pd.to_datetime(start).strftime('%Y%m%d%H')
    
    if not os.path.exists('{}/{}'.format(run_save_dir,timesave)):
        os.makedirs('{}/{}'.format(run_save_dir,timesave))
    
    stats = pd.DataFrame()
    # Loop over times
    dplot = start
    while dplot <= end:
        dtime = d.where(d.time<=dplot ,drop=True)
        # grounding statistics
        df = grounding_statistics(dtime)
        timedelta = dtime.time.values[-1] - dtime.time.values[0]
        timedelta = timedelta.astype(np.timedelta64(1,'D'))/np.timedelta64(1,'D')
        df['num_days_since_release'] = timedelta
        fig = plot_map(dtime)
        fsave = '{}-{}_P{}D.png'.format(run_details.lower(),
                                    timesave,
                                    str(int(timedelta)).zfill(3))
        savename = os.path.join(run_save_dir,
                                timesave,
                                fsave
                               )
        fig.savefig(savename, bbox_inches='tight', transparent=False, dpi=300, facecolor='white')
        dplot = dplot + np.timedelta64(3, 'D')
        stats = pd.concat([stats, df], ignore_index=True)
    stats= stats.reset_index(drop=True)
    return fig, stats


def grounding_statistics(d):
    # count land points/points inside/outside foraging
    all_points = d.ntraj.shape[0]
    num_initial = all_points
    dsel=d.isel(time=-1)
    # num_points grounded
    mask = np.ma.masked_invalid(dsel.land_binary_mask.values)
    num_grounded = np.sum(mask.mask)
    # num_points inside foraging
    dnoground = dsel.where(~mask.mask, np.nan).dropna(dim='ntraj')
    outside = dnoground.where(dnoground.distance_from_colony.values > radius_in_meters/1000,np.nan).dropna(dim='ntraj')
    num_outside = outside.ntraj.shape[0]
    df = pd.DataFrame({'Initial_Time': d.time.values[0], 
                       'Date': d.time.values[-1],
                       'num_initial': num_initial,
                       'num_grounded': num_grounded,
                       'num_outside_foraging_radius': num_outside}, index=[0,])
    return df


def plot_stats(stats_all):
    fig,axs=plt.subplots(4,1,figsize=(8,15),sharex=True)
    axs=axs.flatten()
    count=0
    group = stats_all.groupby('Initial_Time')
    for name, g in group:
        ax=axs[count]
        ax.plot(g['num_days_since_release'], g['num_initial'],label='Released')
        ax.plot(g['num_days_since_release'], g['num_grounded'],label='Grounded')
        ax.plot(g['num_days_since_release'], g['num_outside_foraging_radius'],label='Outside foraging radius')
        num_active= g['num_initial'] - g['num_grounded']
        ax.plot(g['num_days_since_release'], num_active,label='Active')
        ax.set_title('Initial release date: {}'.format(name))
        ax.legend()
        count+=1
        ax.set_ylabel('Numer of particles')
        ax.grid()
    ax.set_xlabel('Days since release')
    return fig


if __name__ == '__main__':
    main()
