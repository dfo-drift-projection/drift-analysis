colonies = {
    'CapeStMarys': {
        'lon': -54.2,
        'lat': 46.83,
        'radius': 74400
        },
    'Witless': {
        'lon': -52.77,
        'lat': 47.26,
        'radius': 80000
        },
    'Funk': {
        'lon': -53.18,
        'lat': 49.77,
        'radius': 57000
        },
    'Baccalieu': {
        'lon': -52.78,
        'lat': 48.12,
        'radius': 92500
        },
    }
    
