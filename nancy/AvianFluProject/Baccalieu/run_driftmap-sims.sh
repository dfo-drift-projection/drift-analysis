#!/bin/bash

########################################################################
# This script provides an example of how to run a DriftMap simualtion
# using a run configuation file and then plot the results. The run
# configuration file contains all of the run parameters needed for the
# simulation. Several example configuration files are provided in
# examples/runs/DriftMap.

# To run a new experiment, users should provide the following variables
# via the command line:
# MINICONDA_PATH (optional)
# CONFIG_FILE
#
# Note: if running on the gpsc, it is recommended that this script is
# submitted to the job queues.
#
# Usage: run_driftmap.sh -c <yaml config file> -m <optional. Full path
# to drift-tool python environment if other than default provided below>
########################################################################

# MINICONDA_PATH: path to drift-tool python environment. For example, the path
# to the master branch environment of the drift-tool is:
MINICONDA_PATH='/home/nso001/data/work2/miniconda-new'

cd '/home/nso001/code/drifters/drift-analysis/nancy/AvianFluProject/'
CONFIG='/home/nso001/code/drifters/drift-analysis/nancy/AvianFluProject/Baccalieu/ciopse.yaml'
experiment_dir='/home/nso001/data/work2/OPP/AvianFluProject/DriftCircle/Baccalieu/CIOPSE-HRDPS-0.03/June2022'

# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Run drift tool
# --------------
python drift_map.py \
    -c $CONFIG \
    --experiment-dir $experiment_dir \
    --first-start-date '2022-06-15'\
    --last-start-date '2022-06-30' 

