#!/bin/bash

########################################################################
# This script provides an example of how to run a DriftMap simualtion
# using a run configuation file and then plot the results. The run
# configuration file contains all of the run parameters needed for the
# simulation. Several example configuration files are provided in
# examples/runs/DriftMap.

# To run a new experiment, users should provide the following variables
# via the command line:
# MINICONDA_PATH (optional)
# CONFIG_FILE
#
# Note: if running on the gpsc, it is recommended that this script is
# submitted to the job queues.
#
# Usage: run_driftmap.sh -c <yaml config file> -m <optional. Full path
# to drift-tool python environment if other than default provided below>
########################################################################

# MINICONDA_PATH: path to drift-tool python environment. For example, the path
# to the master branch environment of the drift-tool is:
MINICONDA_PATH='/home/sdfo000/sitestore4/opp_drift_fa3/software/drift-tool-miniconda-v4.1.1'

CONFIG='/home/nso001/code/drifters/drift-analysis/nancy/AvianFluProject/ciopse.yaml'
experiment_dir='/home/nso001/data/work2/OPP/AvianFluProject/DriftMap/CIOPSE-HRDPS-0.01'

# ---------------
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Run drift tool
# --------------
drift_map \
    -c $CONFIG \
    --experiment-dir $experiment_dir \
    --alpha-wind 1. \

# Plot the results. Plots will be available in $experiment_dir/plot
# ------------------------------------------------------------------
plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/home/sdfo000/sitestore4/opp_drift_fa3/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk' \
  --interval 72
