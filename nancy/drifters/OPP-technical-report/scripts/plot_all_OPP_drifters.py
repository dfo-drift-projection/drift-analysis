'''Plot all OPP drifters, each year a different colour.'''
# Nancy Soontiens, August 2023

import datetime
import json

import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
import numpy as np
import xarray as xr

from driftutils.assemble_drifter_metadata import assemble_drifter_metadata


def identify_drifters_in_launch_year(year, metadata):
    '''Return a list of drifter filenames launched in year'''
    drifters = []
    for d in metadata['drifters']:
        launch = datetime.datetime.strptime(d['launch_date'], '%Y-%m-%d')
        if launch.year == year:
            drifters.append(d['filename'])
    return drifters


def plot_drifters(drifter_files, ax, color='r', lw=0.75,
                  begin_kwargs={}, end_kwargs={}):
    '''Plot all drifters in list drifter_files'''
    for drifter_file in drifter_files:
        d = xr.open_dataset(drifter_file)
        ax.plot(d.LONGITUDE,
                d.LATITUDE,
                color=color,
                transform=ccrs.PlateCarree(),
                linewidth=lw)
        # plotstart and end points
        ax.plot(d.LONGITUDE[0], d.LATITUDE[0],
                transform=ccrs.PlateCarree(), zorder=10, **begin_kwargs)
        ax.plot(d.LONGITUDE[-1], d.LATITUDE[-1],
                transform=ccrs.PlateCarree(), zorder=10, **end_kwargs)


def plot_all_OPP_drifters(data_dir, linewidth=0.75):
    '''Plot all OPP drifters, coloured by year'''
    assemble_drifter_metadata(data_dir, 'all_drifters.json')
    with open('all_drifters.json', 'r') as f:
        metadata = json.load(f)

    years = np.arange(2018, 2024)
    year_colors = {year: f'C{i}' for i, year in enumerate(years)}

    begin_dot_kwargs = {
        'color': 'k',
        'marker': 'o',
        'linewidth': 0,
        'markerfacecolor': "None",
        'markersize': 5}
    end_dot_kwargs = {
        'color': 'C6',
        'marker': '.',
        'linewidth': 0,
        'markersize': 5,
        'markeredgecolor':'k',
        'markeredgewidth': 0.5}

    fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
    handles = []
    for year, color in year_colors.items():
        drifters = identify_drifters_in_launch_year(year, metadata)
        print(year,len(drifters))
        plot_drifters(drifters, ax, color=color, lw=linewidth,
                      begin_kwargs=begin_dot_kwargs,
                      end_kwargs=end_dot_kwargs)
        handles.append(Line2D([0],
                              [0],
                              label=year,
                              color=color,
                              linewidth=linewidth))
    handles.append(Line2D([0], [0], label='Begin track', **begin_dot_kwargs))
    handles.append(Line2D([0], [0], label='End track', **end_dot_kwargs))
    # Add legend, coastlines, gridlines
    ax.legend(handles=handles, bbox_to_anchor=(.9,.7))
    ax.add_feature(cfeature.LAND, edgecolor='black',
                   linewidth=0.5, facecolor='gray')
    ax.gridlines(draw_labels=["bottom", "left"], zorder=0,
                 linewidth=0.5,
                 linestyle='--')

    fig.savefig(f'OPPdrifters{years[0]}-{years[-1]}.png',
                bbox_inches='tight',
                dpi=500)


if __name__ == '__main__':
    data_dir = '/home/jmm000/work/Drifters/output/TechReport2023/netcdf/'
    plot_all_OPP_drifters(data_dir)
