'''Plot all drifters in a predefined deployment region'''

import json
import yaml

import cartopy
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import matplotlib.pyplot as plt
from matplotlib.lines import Line2D
from mpl_toolkits.axes_grid1.inset_locator import inset_axes
from shapely.geometry import Point, Polygon
import xarray as xr

from driftutils.assemble_drifter_metadata import assemble_drifter_metadata

# Colors for each drifter type
colors = {'CODE/Davis': 'C0',
          'iSVP': 'C1',
          'Stokes': 'C2',
          'Spotter': 'C3',
          'OSKER': 'C4',
          'iSphere': 'C5'}


def load_polygon_coordinates(filename):
    '''Load the poylgon coordinate file and return a dictionary
    with the shaplely Polygon for each region defined in the
    coordinate file'''

    with open(filename, 'r') as f:
        poly_coords = yaml.safe_load(f)
    regions = {}
    for region in poly_coords['polygon_coords']:
        coords = poly_coords['polygon_coords'][region]
        new_coords = [(lon, lat) for lat, lon in coords]
        regions[region] = Polygon(new_coords)
    return regions


def identify_drifters_region(region, metadata):
    '''Indentify which drifters start in a shapely Polygon
    defined by region'''
    drifters = []
    for d in metadata['drifters']:
        launch_lat = float(d['first_latitude_observation'])
        launch_lon = float(d['first_longitude_observation'])
        launch_coord = Point((launch_lon, launch_lat))
        if launch_coord.within(region):
            drifters.append(d['filename'])
    return drifters


def plot_drifters(drifter_files, ax, lw=0.5,
                  begin_kwargs={}, end_kwargs={}):
    '''Plot drifters in a list of files, coloured by drifter_type'''
    types = []
    for drifter_file in drifter_files:
        d = xr.open_dataset(drifter_file)
        drifter_type = d.type
        if drifter_type not in types:
            types.append(drifter_type)
        ax.plot(d.LONGITUDE, d.LATITUDE, color=colors[drifter_type],
                transform=ccrs.PlateCarree(), linewidth=lw)
        # plotstart and end points
        ax.plot(d.LONGITUDE[0], d.LATITUDE[0],
                transform=ccrs.PlateCarree(), zorder=10, **begin_kwargs)
        ax.plot(d.LONGITUDE[-1], d.LATITUDE[-1],
                transform=ccrs.PlateCarree(), zorder=10, **end_kwargs)
    return types


def add_inset(ax, drifters, width, height,
              lon_min, lon_max, lat_min, lat_max,
              anchor, begin_dot_kwargs={}, end_dot_kwargs={}):
    """add an inset with a zoom specificied by lon/lat coordinates"""
    axins = inset_axes(ax, width=width, height=height,
                       bbox_to_anchor=anchor, bbox_transform=ax.transAxes,
                       axes_class=cartopy.mpl.geoaxes.GeoAxes,
                       axes_kwargs={'map_projection': ccrs.PlateCarree()})
    plot_drifters(drifters, axins,
                  begin_kwargs=begin_dot_kwargs,
                  end_kwargs=end_dot_kwargs)
    axins.set_extent([lon_min, lon_max, lat_min, lat_max])
    axins.add_feature(cfeature.LAND, edgecolor='black', linewidth=0.5,facecolor='gray')
    ax.plot([lon_min, lon_min, lon_max, lon_max, lon_min,],
            [lat_min, lat_max, lat_max, lat_min, lat_min],
            'red', transform=ccrs.PlateCarree())


def plot_drifters_region(data_dir, region_file, linewidth=0.75):
    '''Plot all drifters that start in each region defined
    in region_file'''
    # Compile and load drifter metadata
    assemble_drifter_metadata(data_dir, 'all_drifters.json')
    with open('all_drifters.json', 'r') as f:
        metadata = json.load(f)
    # Load regions file
    regions = load_polygon_coordinates(region_file)

    begin_dot_kwargs = {
        'color': 'k',
        'marker': 'o',
        'linewidth': 0,
        'markerfacecolor': "None",
        'markersize': 5}
    end_dot_kwargs = {
        'color': 'C6',
        'marker': '.',
        'linewidth': 0,
        'markersize': 5,
        'markeredgecolor': 'k',
        'markeredgewidth': 0.5}

    for region in regions:
        print(region)
        fig, ax = plt.subplots(subplot_kw={'projection': ccrs.PlateCarree()})
        poly = regions[region]
        drifters = identify_drifters_region(poly, metadata)
        types = plot_drifters(drifters,
                              ax,
                              lw=linewidth,
                              begin_kwargs=begin_dot_kwargs,
                              end_kwargs=end_dot_kwargs)
        handles = [Line2D([0], [0],
                          label=drifter_type,
                          color=colors[drifter_type],
                          linewidth=linewidth)
                   for drifter_type in types]
        handles.append(Line2D([0], [0], label='Begin track',
                              **begin_dot_kwargs))
        handles.append(Line2D([0], [0], label='End track',
                              **end_dot_kwargs))
        ax.set_title(region)
        ax.add_feature(cfeature.LAND, edgecolor='black', linewidth=0.5,facecolor='gray')
        ax.gridlines(draw_labels=["bottom", "left"],
                     zorder=0,
                     linewidth=0.5,
                     linestyle='--')
        ax.legend(handles=handles, loc='lower right',
                  bbox_to_anchor=(1.3, 0.5))

        if region == 'Newfoundland Shelf':
            add_inset(ax, drifters, "100%", "50%",
                      -54, -51, 47.5, 49.5, (-.32, .1, 1, 1),
                      begin_dot_kwargs=begin_dot_kwargs,
                      end_dot_kwargs=end_dot_kwargs)

        elif region == "Scotian Shelf":
            add_inset(ax, drifters, "100%", "50%",
                      -64, -59, 44, 47, (-.2, -.415, 1, 1),
                      begin_dot_kwargs=begin_dot_kwargs,
                      end_dot_kwargs=end_dot_kwargs)
        fig.savefig(f'{region.replace(" ", "")}.png',
                    dpi=500,
                    bbox_inches='tight')


if __name__ == '__main__':
    regions_file = '../drifter-regions.yaml'
    data_dir = '/home/jmm000/work/Drifters/output/TechReport2023/netcdf/'
    plot_drifters_region(data_dir, regions_file)
