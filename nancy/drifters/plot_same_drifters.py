import matplotlib.pyplot as plt
import matplotlib.cm as cm
import matplotlib.colors as colors
import pandas as pd
import numpy as np
import xarray as xr
from mpl_toolkits.basemap import Basemap
import glob
import os
import datetime
from functools import reduce
import plot_ocean_mask as po

def special_resample(df, freq='1H'):
    dtime = df.set_index('time')
    dtime = dtime.groupby(['mod_start_lon', 'mod_start_lat', 'mod_start_time'])
    dtime = dtime.resample('1H',label='right', closed='right').mean()
    ddrop = dtime.drop(columns=['mod_start_lon', 'mod_start_lat'])
    ddrop = ddrop.reset_index()
    ddrop['time_since_start'] = ddrop['time'] - ddrop['mod_start_time']
    return ddrop

# meshes
RIOPS_MESH ='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
CIOPS_MESH = '/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
lat_var='nav_lat'
lon_var='nav_lon'
tmask_var='tmask'

# Step 1: Gather all the data needed
# Step 1a: Build comparison between RIOPS and CIOPS
run_dir1='/home/nso001/data/work2/OPP/ciopsw/ciops_daily_windage2/all_output/output_per_drifter'
run_dir2='/home/nso001/data/work2/OPP/ciopsw/riops_daily_windage2/all_output/output_per_drifter'


files1 = glob.glob(os.path.join(run_dir1, '*.nc'))
files2 = glob.glob(os.path.join(run_dir2, '*.nc'))

fcount=0
dcount=0
r1count=0
d = pd.DataFrame()
skill_score='sep'
for f1 in files1:
    basename=os.path.basename(f1)
    f2=os.path.join(run_dir2,basename)
    if f2 not in files2:
        print("{} no in {}".format(basename, run_dir2))
        continue
    fcount+=1
    d1 = xr.open_dataset(f1)
    d2 = xr.open_dataset(f2)
    mod_run=0
    for lat1, lon1, time in zip(d1.mod_start_lat.values,
                                d1.mod_start_lon.values,
                                d1.mod_start_date.values):
        r1count+=1
        ind_lat = np.where(d2.mod_start_lat.values==lat1)[0]
        ind_lon = np.where(d2.mod_start_lon.values==lon1)[0]
        ind_start = np.where(d2.mod_start_date.values==time)[0]
        inter = reduce(np.intersect1d, (ind_lat,ind_lon, ind_start))
        if inter.size > 0:
            dcount+=1
            skill_diff=[]
            time_since_start = []
            start_date = np.datetime64(time)
            # Removing nans
            # First run
            skill1 = d1[skill_score][mod_run,:].values
            inds1 = np.where(~np.isnan(skill1))
            time1 = d1.time[mod_run,:].values[inds1]
            #Second run
            skill2 = d2[skill_score][inter[0],:].values
            inds2 = np.where(~np.isnan(skill2))
            time2 = d2.time[inter[0],:].values[inds2]
            if (time1 == time2).all():
                skill_diff = skill1[inds1] - skill2[inds2]
                time_since_start = time1-start_date
                dnew=pd.DataFrame({'time': time1,
                                   'mod_start_lon': lon1,
                                   'mod_start_lat': lat1,
                                   'mod_start_time': start_date,
                                   'skill_diff': skill_diff})
                d = pd.concat([d, dnew])
        mod_run+= 1
print("duplicate files: ", fcount)
print("starting positions from run1", r1count)
print("duplicate start", dcount)

# Resample to hourly frequency with means
dcompare = special_resample(d)

# Step 1b:
# Gather data for ciops_hourly_windage2
run_dir3='/home/nso001/data/work2/OPP/ciopsw/ciopsw_hourly_windage2/all_output/output_per_drifter'
files3 = glob.glob(os.path.join(run_dir3, '*.nc'))
skill_score='molcard'
d = pd.DataFrame()
files3.sort()
for f in files3:
    print(f)
    ds = xr.open_dataset(f)
    mod_run=0
    for lat, lon, time in zip(ds.mod_start_lat.values,
                              ds.mod_start_lon.values,
                              ds.mod_start_date.values):
        dnew=pd.DataFrame({'time': ds.time[mod_run, :].values,
                           'mod_start_time': np.datetime64(time),
                           'mod_start_lat': lat,
                           'mod_start_lon': lon,
                           'mod_lon': ds.mod_lon[mod_run, :].values,
                           'mod_lat': ds.mod_lat[mod_run, :].values,
                           'obs_lat': ds.obs_lat[mod_run, :].values,
                           'obs_lon': ds.obs_lon[mod_run, :].values,
                            skill_score: ds[skill_score][mod_run, :].values})
        d = pd.concat([d, dnew])
        mod_run+=1
# Resample for the molcard skill score
d_skill = special_resample(d)
print(d_skill)

# Step 2 plots
fig, axs = plt.subplots(1,3,figsize=(8,4))
plt.subplots_adjust(wspace=0)
# Common variables
lon_min=-131.5
lon_max=-122.1
lat_min=46.5
lat_max=54
parallels = np.arange(47, lat_max, 2)
meridians = np.arange(-131, -122, 2)
fontsize=8

# Molcard plot
ax=axs[1]
bmap = Basemap(projection='merc',llcrnrlat=lat_min,
               urcrnrlat=lat_max,llcrnrlon=lon_min,urcrnrlon=lon_max,
               resolution='h',ax=ax)
ds_48 = d_skill[d_skill['time_since_start']==datetime.timedelta(hours=48)]
x,y = bmap(ds_48['mod_start_lon'].values, ds_48['mod_start_lat'].values)
cmap = cm.get_cmap('viridis',5)
mesh = bmap.hexbin(x,y,C=ds_48[skill_score].values,ax=ax,
                   vmin=0,vmax=1, edgecolors='k',cmap=cmap,
                   linewidths=0.2,zorder=10)
cbar = plt.colorbar(mesh, ax=ax,orientation='horizontal', pad=0.02,
                    shrink=0.9, ticks=[0,.2,.4,.6,.8,1])
cbar.ax.tick_params(labelsize=fontsize) 
cbar.set_label('Molcard skill', fontsize=fontsize)
po.plot_land_mask(bmap, lat_var, lon_var, tmask_var, CIOPS_MESH)
bmap.drawparallels(parallels, labels=[0,0,0,0], fontsize=fontsize)
bmap.drawmeridians(meridians, labels=[0,0,1,0], fontsize=fontsize,rotation=45)

# All tracks plot 
ax = axs[0]
bmap = Basemap(projection='merc',llcrnrlat=lat_min,
               urcrnrlat=lat_max,llcrnrlon=lon_min,urcrnrlon=lon_max,
               resolution='h',ax=ax)
dgroup = d.groupby(['mod_start_lon', 'mod_start_lat', 'mod_start_time'])
count=0
for n, d in dgroup:
    xmod, ymod = bmap(d['mod_lon'].values, d['mod_lat'].values)
    if count==0:
        bmap.plot(xmod, ymod, '-', c='gray',lw=0.4,label='modelled tracks',
                  zorder=10)
    else:
        bmap.plot(xmod, ymod,'-', c='gray',lw=0.4,label='_nolegend_',
                  zorder=10) 
    count+=1
count=0
for n, d in dgroup:
    xobs, yobs = bmap(d['obs_lon'].values, d['obs_lat'].values)
    if count==0:
        bmap.plot(xobs, yobs, '-', c='k',lw=0.3, label='observed tracks',
                  zorder=10)
    else:
        bmap.plot(xobs, yobs,'-', c='k',lw=0.1, label='_nolegend_',
                  zorder=10)
    count+=1
ax.legend(loc='upper right',fontsize=fontsize)
bmap.drawcoastlines(linewidth=0.3)
bmap.fillcontinents(color='lightgrey')
bmap.drawparallels(parallels, labels=[1,0,0,0], fontsize=fontsize)
bmap.drawmeridians(meridians, labels=[0,0,1,0],fontsize=fontsize,rotation=45)
cbar = plt.colorbar(mesh, ax=ax,orientation='horizontal',pad=0.02,
                    shrink=0.9)
cbar.remove()

# Difference plot
ax = axs[2]
bmap = Basemap(projection='merc',llcrnrlat=lat_min,
               urcrnrlat=lat_max,llcrnrlon=lon_min,urcrnrlon=lon_max,
               resolution='h',ax=ax)
norm = colors.SymLogNorm(1,vmin=-60,vmax=60)
dc_48=dcompare[dcompare['time_since_start']==datetime.timedelta(hours=48)]
x,y = bmap(dc_48['mod_start_lon'].values, dc_48['mod_start_lat'].values)
mesh = bmap.hexbin(x,y,C=dc_48['skill_diff'].values/1000, ax=ax,
                   cmap='bwr',vmin=-60,vmax=60, edgecolors='k',
                   linewidths=0.2,norm=norm,
                   zorder=10)
cbar = plt.colorbar(mesh, ax=ax,orientation='horizontal',pad=0.02,
                    shrink=0.9)
cbar.set_label('Separation Distance [km]', fontsize=fontsize)
cbar.ax.tick_params(labelsize=fontsize)
po.plot_land_mask(bmap, lat_var, lon_var, tmask_var, RIOPS_MESH)
bmap.drawparallels(parallels, labels=[0,0,0,0],fontsize=fontsize)
bmap.drawmeridians(meridians, labels=[0,0,1,0],fontsize=fontsize,rotation=45)

for ax, label in zip(axs, ['(a)', '(b)', '(c)']):
    ax.text(0.1,0.1,label,fontsize=fontsize, transform=ax.transAxes)

plt.draw()
plt.show()

fig.savefig('myplot.png', dpi=300,bbox_inches='tight')
