# Function to plot the land/ocean mask from an ocean model

import matplotlib.pyplot as plt
import matplotlib.colors as cm
import numpy as np
import xarray as xr
from mpl_toolkits.basemap import Basemap
import glob
import os
import datetime

def plot_land_mask(bmap, lat_var, lon_var, tmask_var, ocean_mesh_file):
    ds = xr.open_dataset(ocean_mesh_file)
    lons = ds[lon_var].values
    lats = ds[lat_var].values
    mask = np.squeeze(ds[tmask_var].values)
    while mask.ndim > 2:
        mask = mask[0, ...]
    mask_masked = np.ma.masked_values(mask, 1)
    x, y = bmap(lons, lats)
    land_color="gray"
    cmap = cm.LinearSegmentedColormap.from_list("", [land_color,"white"])
    # turn on/off for filled land
    # WARNING: CIOPS land isn't filled entirely because of domain boundaries
    bmap.contourf(x,y,mask_masked, levels=[0,1],cmap=cmap)
    # turn on/off contour below to show landmask edges
    # WARNING: RIOPS mask is strange in Pacific Ocean so edges appear below 46N
    bmap.contour(x,y,mask,levels=[0,],colors='k',linewidths=0.5)
    

        
if __name__ == '__main__':
    ocean_mesh_file='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
    ocean_mesh_file='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
    lon_var='nav_lon'
    lat_var='nav_lat'
    tmask_var='tmask'
    fig,ax = plt.subplots(1,1, figsize=(10,6))
    m = Basemap(projection='merc',llcrnrlat=46,urcrnrlat=55,
                llcrnrlon=-135,urcrnrlon=-122.1,resolution='l')
    plot_land_mask(m, lat_var, lon_var, tmask_var, ocean_mesh_file)
    #m.drawcoastlines()
    plt.show()
