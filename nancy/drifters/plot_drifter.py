import matplotlib.pyplot as plt
import numpy as np
import xarray as xr
from mpl_toolkits.basemap import Basemap
import glob
import os
import datetime

def plot_drifter(bmap, lat, lon):
    x, y = bmap(lon, lat)
    bmap.plot(x,y,'r-')

drift_dir='/home/nso001/data/work2/OPP/drifter_data/waterproperties'
drifter_files = glob.glob(os.path.join(drift_dir, '*.nc'))
drifter_files.sort()

fig,ax = plt.subplots(1,1, figsize=(10,6))
m = Basemap(projection='merc',llcrnrlat=45,urcrnrlat=55,llcrnrlon=-135,urcrnrlon=-120,resolution='l')
year=2016
d1=datetime.datetime(year,1,1)
d2=datetime.datetime(year,12,31)
count=0
count_good=0
large_gaps=[]
weird_lat=[]

for f in drifter_files:
    d = xr.open_dataset(f)
    dnew = d.sel(TIME=slice(d1,d2))
    lats = dnew.LATITUDE.values
    lons = dnew.LONGITUDE.values
    if lons.size:
        count+=1
        if lats.max() == 999:
            weird_lat.append(f)    
        time=dnew.TIME
        time_diff=time.diff('TIME')
        maxtime=np.max(time_diff.values).astype('timedelta64[h]')
        if maxtime < np.timedelta64(1,'D'):
            print(maxtime)
        if (time_diff > np.timedelta64(1,'D')).any():
           large_gaps.append(f)
        if (f in large_gaps) and (f not in weird_lat):
            plot_drifter(m, lats, lons)
m.drawcoastlines()
print(count)
print(len(weird_lat))
print(len(large_gaps))
print(count_good)
plt.show()
