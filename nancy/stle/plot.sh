#! /bin/bash -l
#
#SBATCH --job-name=plot.sh
#SBATCH --account=dfo_dpnm_fa3
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/stle-2023/logs/plot-stokes0.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

# Set up miniconda path
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/v6.1.1/

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Define working directoy
workdir=/home/nso001/data/work7/stle-2023
cd ${workdir}

# Specific run parameters
drifter='stokes0'
LOG=${workdir}/logs/${drifter}-plot
config=/home/nso001/code/drifters/drift-analysis/nancy/stle/${drifter}-plot.yaml

# Call your program
# Run drift prediction
# --------------------
plotting_workflow --user_config ${config} >> $LOG 2>&1



