#! /bin/bash -l
#
#SBATCH --job-name=stle.sh
#SBATCH --account=dfo_dpnm_fa3
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/stle-2023/logs/stle-stokes0.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

# Set up miniconda path
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/v6.1.1/

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Define working directoy
workdir=/home/nso001/data/work7/stle-2023
cd ${workdir}

# Specific run parameters
# windage values calcuulated with Hauke's code-incldues 1% for stokes
drifter='stokes'
#windage=4.457053588273564
windage=0
#drifter='osker'
#windage=4.457053588273564
#drifter='carthe'
#windage=1.7000000000000002
#drifter=davis
#windage=1.3625788391979687
LOG=${workdir}/logs/${drifter}-0wind
config=/home/nso001/code/drifters/drift-analysis/nancy/stle/stle.yaml
experiment_dir=${workdir}/runs/${drifter}-0wind
drifter_dir=${workdir}/drifters/${drifter}

# Call your program
# Run drift prediction
# --------------------
drift_predict \
    -c $config \
    --alpha-wind $windage \
    --drifter-data-dir $drifter_dir \
    --experiment-dir $experiment_dir >>$LOG 2>&1

# Compute skills scores
# ---------------------
drift_evaluate \
  --data-dir $experiment_dir/output >>$LOG 2>&1

# Combine the output into per drifter files
# -----------------------------------------
combine_track_segments \
  --data_dir=$experiment_dir/output >>$LOG 2>&1


