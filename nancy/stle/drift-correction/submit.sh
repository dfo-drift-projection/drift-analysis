#! /bin/bash -l
#
#SBATCH --job-name=osker-corection.sh
#SBATCH --account=dfo_dpnm_fa3
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/stle-2023/logs/correction-stle-osker.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

# Set up miniconda path
MINICONDA_PATH=/home/sdfo000/sitestore7/opp_drift_fa3/software/drift-tool-environment/envs/v6.1.1/

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin

# Define working directoy
workdir=/home/nso001/data/work7/stle-2023
cd ${workdir}

# Specific run parameters
#drifter='stokes'
drifter='osker'
#drifter='carthe'
#drifter='davis'
LOG=${workdir}/logs/correction-${drifter}
config=/home/nso001/code/drifters/drift-analysis/nancy/stle/drift-correction/stle.yaml
experiment_dir=${workdir}/correction/${drifter}
drifter_dir=${workdir}/drifters/${drifter}

# Call your program
# Run drift prediction
# --------------------
drift_correction_factor \
    -c $config \
    --drifter-data-dir $drifter_dir \
    --experiment-dir $experiment_dir >>$LOG 2>&1

# Compute skills scores
# ---------------------
correction_factors_evaluate \
    --experiment-dir $experiment_dir/output/

# Plot the output
# ----------------------------------
plot_correction_factors \
    --data-dir $experiment_dir/output/ \
    --style 'seaborn-v0_8-paper'


