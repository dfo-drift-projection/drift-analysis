# This is a script to analyze and save the RIOPS tidal constiuents using a package called pytdies
# Nancy Soontiens Oct 28, 2022
# Usage: python riops_tides variable js je
# where variable is 'VOZOCRTX' or 'VOMECRTY'
# Notes on the data
# Data
# * RIOPS forecasts June 1-30, 2022, hourly frequency, surface currents (30 days)
#* First 6 hours of every forecast is used (best esitmate)
# Notes on tidal analysis
# The big six resolvable constituents ( M2, N2, S2, K1, O1, Q1) were selected:
# followng the 30-day analysis presented here:  https://www.nature.com/articles/s41597-020-00578-z


import glob
import os
import sys

import numpy as np
import pandas as pd
import xarray as xr

import pytides.constituent as constituent
from pytides.tide import Tide

CONSTITUENTS = ['M2', 'S2', 'N2', 'K1', 'O1', 'Q1']

pytide_constituents = [constituent._M2,
                       constituent._S2,
                       constituent._N2,
                       constituent._K1,
                       constituent._O1,
                       constituent._Q1,]

DATA_DIR = '/home/soontiensn/data/DenisLefaivre/riops/data/'
OUT_DIR = '/home/soontiensn/data/DenisLefaivre/riops/tides/'


def calculate_tides(variable, js, je, data_dir, out_dir):

    files=glob.glob(os.path.join(data_dir, f'*{variable}*.nc'))
    files.sort()
    print(f'opening {variable} files in {data_dir}') 
    ds = xr.open_mfdataset(files)
    ps = ds['polar_stereographic']
    my_var = ds[variable.lower()]
    mask = np.ma.masked_invalid(my_var.isel(time=0).values).mask
    shape = my_var.isel(time=0).shape
    data_consts = {}
    for constituent in CONSTITUENTS:
        data_consts[f'{constituent}_amp'] = np.ma.masked_array(np.zeros(shape),mask=mask)
        data_consts[f'{constituent}_pha'] = np.ma.masked_array(np.zeros(shape),mask=mask)

    print('Beginning tidal analysis over file domain')
    NY = shape[0]
    NX = shape[1]
    for j in range(int(js), int(je)+1):
        print(f'Beginining row {j} out of {NY}')
        for i in range(NX):
            m = mask[j,i]
            if m:
                continue
            else:
                df = analyze_position(i, j, my_var)
                for constituent in CONSTITUENTS:
                    data_consts[f'{constituent}_amp'][j, i] = df['amplitude'][constituent]
                    data_consts[f'{constituent}_pha'][j, i] = df['phase'][constituent]
        print(f'Finished row {j} out of {NY}')
    
    # Saving analysis
    ds_out = xr.Dataset(coords=dict(xc=(['xc',], ds['xc'].values),
                                    yc=(['yc',], ds['yc'].values),
                                    longitude=(['yc','xc'], ds['longitude'].values),
                                    latitude=(['yc','xc'], ds['latitude'].values))
    )
    ds_out['longitude'].attrs = ds['longitude'].attrs
    ds_out['latitude'].attrs = ds['latitude'].attrs

    for constituent in CONSTITUENTS:
        ds_out[f'{constituent}_amp'] = xr.DataArray(
            data=data_consts[f'{constituent}_amp'],
            dims=['yc', 'xc'],
            attrs=dict(
                description=f'{constituent} amplitude for {variable}',
                units='m/s'
                )
            )
        ds_out[f'{constituent}_pha'] = xr.DataArray(
            data=data_consts[f'{constituent}_pha'],
            dims=['yc', 'xc'],
            attrs=dict(
                description=f'{constituent} phase for {variable}',
                units='degrees'
                )
            )
    ds_out['polar_stereographic'] = xr.DataArray(
        data=np.array(''),
        attrs=ps.attrs)
    ds_out['xc'].attrs = ds['xc'].attrs
    ds_out['yc'].attrs = ds['yc'].attrs

    print(ds_out)
    # outfile
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    st = np.datetime_as_string(ds.time.values[0], unit='D')
    ed = np.datetime_as_string(ds.time.values[-1], unit='D')
    outfile = os.path.join(out_dir, f'{variable}_tides_{st}-{ed}_{js}-{je}.nc')
    print(outfile)
    ds_out.to_netcdf(outfile)
                                                    

def tidal_analysis(speeds, times, constituents=pytide_constituents):
    demeaned = speeds - speeds.mean()
    dates=pd.to_datetime(times)
    if constituents:
        tide = Tide.decompose(demeaned,dates, constituents=constituents)
    else:
        tide = Tide.decompose(demeaned,dates,)
    
    constituent = [c.name for c in tide.model['constituent']]
    df = pd.DataFrame(tide.model, index=constituent).drop('constituent', axis=1)
    
    return df

        
def analyze_position(i,j, my_var):
    speeds = my_var.isel(xc=i,yc=j).values
    if np.isnan(speeds).any():
        df=None
    else:
        times = my_var.time.values
        df = tidal_analysis(speeds, times, constituents=pytide_constituents)
    return df


        
if __name__ == '__main__':
    variable, js, je = sys.argv[1:]
    calculate_tides(variable, js, je, DATA_DIR, OUT_DIR)
