import math

import numpy as np
from pykdtree.kdtree import KDTree

kd_cache = {}


def find_containing_grid_cell(
        lon,           # type: float
        lat,           # type: float
        ocean_dataset,  # type: xr.Dataset
        lon_var,       # type: str
        lat_var        # type: str
):  # type: (...) -> (nd.array, nd.array, nd.array, nd.array)
    """Find the grid cell containing a location.

    Parameters
    ----------
    lon : float
        Longitude of location in question.
    lat : float
        Latitude of location in question.
    ocean_dataset : xarray.Dataset
        Ocean model dataset in which to find the grid cell.
    lon_var : str
        Name of longitude variable in ocean dataset.
    lat_var : str
        Name of latitude variable ocean dataset.

    Returns
    -------
    tuple
        A four-element tuple containing ``ix``, ``iy``, ``lon`` and ``lat``.
        Each element is a *numpy.ndarray* containing four values corresponding
        to the x indices, y indices, longitudes, and latitudes, respectively,
        of the containing grid cell.
    """
    # First find the closest grid point.
    dist_sq, j, i, clat, clon = find_nearest_grid_point(
        lat, lon, ocean_dataset, lat_var, lon_var, n=1
    )
    # Values i and j are arrays with only a single value; change them to
    # scalars.
    i = i[0]
    j = j[0]
    # The longitude and latitude variables should be two-dimensional arrays.
    # In some mesh files, these are four-dimensional arrays but the extra
    # dimensions are of size 1. The squeeze() removes the single-dimensional
    # entries.
    lons = ocean_dataset[lon_var].values.squeeze()
    lats = ocean_dataset[lat_var].values.squeeze()

    # Most datasets have longitude defined over the range -180 to +180. The
    # GIOPS forecast data, however, currently uses a 0 to 360 range, so we
    # adjust those values where necessary.
    lons[lons > 180] -= 360

    if lats.ndim == 1:
        # If latitude and longitude are 1D arrays (as is the case with the
        # GIOPS forecast data currently pulled from datamart), then we need to
        # handle this situation in a special manner. If lats is shape m and
        # lons is shape n, we want to reshpe as (m, n) -> use meshgrid
        lons, lats = np.meshgrid(lons, lats)


    # Now check each box that the closest grid point is a member of. We have
    # four boxes to check:
    #
    # * one with lower-left corner point (i, j);
    # * one with lower-left corner point (i - 1, j);
    # * one with lower-left corner point (i, j - 1); and
    # * one with lower-left corner point (i - 1, j - 1).
    #
    # As soon as we find a containing box, we immediately return the details of
    # that box.
    for jshift in [0, -1]:
        for ishift in [0, -1]:
            ix = [i + ishift, i + ishift + 1, i + ishift + 1, i + ishift]
            iy = [j + jshift, j + jshift, j + jshift + 1, j + jshift + 1]
            try:
                lon_box = lons[tuple([iy, ix])].squeeze()
                lat_box = lats[tuple([iy, ix])].squeeze()
            except IndexError:
                continue
            if inquad(lon, lat, lon_box, lat_box):
                ix = np.array(ix)
                iy = np.array(iy)
                return ix, iy, lon_box, lat_box


def inquad(x, y, qx, qy):
    """Determine if a point lies within or on the boundary of a quadrilateral.

    Parameters
    ----------
    x : float
        X coordinate of point in question.
    y : float
        Y coordinate of point in question.
    qx : np.ndarray of size 4
        X coordinates of the four points of the quadrilateral.
    qy : np.ndarray of size 4
        Y coordinates of the four points of the quadrilateral.

    Returns
    -------
    inquad : bool
        True if a point lies within or on the boundary of a quadrilateral;
        false otherwise.

    Notes
    -----
    This function returns true if a point lies within or on the boundary of a
    quadrilateral of any shape on a plane. The coordinates ``qx``, ``qy`` of
    the points of the quadrilateral must be ordered; that is, it is assumed
    that point (``qx[0]``, ``qy[0]``) connects to point (``qx[1]``, ``qy[1]``),
    and (``qx[1]``, ``qy[1]``) connects to (``qx[2], ``qy[2]``), and so on.

    Method: For the quadrilateral ABCD (with the points ordered) and given
    point P, check if the cross (vector) products PA x PD, PB x PA, PD x PC,
    and PC x PB are all negative.
    """
    inquad = False
    zst1 = (x - qx[0]) * (y - qy[3]) - (y - qy[0]) * (x - qx[3])
    if zst1 <= 0.0:
        zst2 = (x - qx[3]) * (y - qy[2]) - (y - qy[3]) * (x - qx[2])
        if zst2 <= 0.0:
            zst3 = (x - qx[2]) * (y - qy[1]) - (y - qy[2]) * (x - qx[1])
            if zst3 <= 0.0:
                zst4 = (x - qx[1]) * (y - qy[0]) - (y - qy[1]) * (x - qx[0])
                if zst4 <= 0.0:
                    inquad = True
    return inquad


def find_nearest_grid_point(
        lat, lon, dataset, lat_var_name, lon_var_name, n=1
):
    """Find the nearest grid point to a given lat/lon pair.

    Parameters
    ----------
    lat : float
        Latitude value at which to find the nearest grid point.
    lon : float
        Longitude value at which to find the nearest grid point.
    dataset : xarray.Dataset
        An xarray Dataset containing the mesh variables.
    lat_var_name : str
        Name of the latitude variable in the dataset.
    lon_var_name : str
        Name of the longitude variable in the dataset.
    n : int, optional
        Number of nearest grid points to return. Default is to return the
        single closest grid point.

    Returns
    -------
    dist_sq, iy, ix, lat_near, lon_near
        A tuple of numpy arrays:

        - ``dist_sq``: the squared distance between the given lat/lon location
          and the nearest grid points
        - ``iy``: the y indices of the nearest grid points
        - ``ix``: the x indices of the nearest grid points
        - ``lat_near``: the latitude values of the nearest grid points
        - ``lon_near``: the longitude values of the nearest grid points
    """

    key = hash(frozenset(dataset.attrs))
    key = f'{key}{lon_var_name}{lat_var_name}'

    # Note the use of the squeeze method: it removes single-dimensional entries
    # from the shape of an array. For example, in the GIOPS mesh file the
    # longitude of the U velocity points is defined as an array with shape
    # (1, 1, 1021, 1442). The squeeze method converts this into the equivalent
    # array with shape (1021, 1442).
    latvar = dataset.variables[lat_var_name].squeeze()
    lonvar = dataset.variables[lon_var_name].squeeze()

    if key not in kd_cache:
        rad_factor = math.pi / 180.0
        latvals = latvar[:] * rad_factor
        lonvals = lonvar[:] * rad_factor
        clat, clon = np.cos(latvals), np.cos(lonvals)
        slat, slon = np.sin(latvals), np.sin(lonvals)
        if latvar.ndim == 1:
            # If latitude and longitude are 1D arrays (as is the case with the
            # GIOPS forecast data currently pulled from datamart), then we need to
            # handle this situation in a special manner. The clat array will be of
            # some size m, say, and the clon array will be of size n. By virtue of
            # being defined with different dimensions, the product of these two
            # arrays will be of size (m, n) because xarray will automatically
            # broadcast the arrays so that the multiplication makes sense to do.
            # Thus, the array calculated from
            #
            #   np.ravel(clat * clon)
            #
            # will be of size mn. However, the array
            #
            #   np.ravel(slat)
            #
            # will be of size m and this will cause the KDTree() call to fail. To
            # resolve this issue, we broadcast slat to the appropriate size and
            # shape.
            shape = (slat.size, slon.size)
            slat = np.broadcast_to(slat.values[:, np.newaxis], shape)
        else:
            shape = latvar.shape
        triples = np.array([np.ravel(clat * clon), np.ravel(clat * slon),
                            np.ravel(slat)]).transpose()
        kdt = KDTree(triples)
        kd_cache[key] = (kdt, shape)
    else:
        kdt, shape = kd_cache[key]

    dist_sq, iy, ix = _find_index(lat, lon, kdt, shape, n)
    # The results returned from _find_index are two-dimensional arrays (if
    # n > 1) because it can handle the case of finding indices closest to
    # multiple lat/lon locations (i.e., where lat and lon are arrays, not
    # scalars). Currently, this function is intended only for a single lat/lon,
    # so we redefine the results as one-dimensional arrays.
    if n > 1:
        dist_sq = dist_sq[0, :]
        iy = iy[0, :]
        ix = ix[0, :]

    if latvar.ndim == 1:
        lat_near = latvar.values[iy]
        lon_near = lonvar.values[ix]
    else:
        lat_near = latvar.values[iy, ix]
        lon_near = lonvar.values[iy, ix]

    # Most datasets have longitude defined over the range -180 to +180. The
    # GIOPS forecast data, however, currently uses a 0 to 360 range, so we
    # adjust those values where necessary.
    lon_near[lon_near > 180] -= 360

    return dist_sq, iy, ix, lat_near, lon_near



def _find_index(lat0, lon0, kdt, shape, n=1):
    """Finds the y, x indicies that are closest to a latitude, longitude pair.

    Arguments:
        lat0 -- the target latitude
        lon0 -- the target longitude
        n -- the number of indicies to return

    Returns:
        squared distance, y, x indicies
    """
    if hasattr(lat0, "__len__"):
        lat0 = np.array(lat0)
        lon0 = np.array(lon0)
        multiple = True
    else:
        multiple = False
    rad_factor = math.pi / 180.0
    lat0_rad = lat0 * rad_factor
    lon0_rad = lon0 * rad_factor
    clat0, clon0 = np.cos(lat0_rad), np.cos(lon0_rad)
    slat0, slon0 = np.sin(lat0_rad), np.sin(lon0_rad)
    q = [clat0 * clon0, clat0 * slon0, slat0]
    if multiple:
        q = np.array(q).transpose()
    else:
        q = np.array(q)
        q = q[np.newaxis, :]
    dist_sq_min, minindex_1d = kdt.query(np.float32(q), k=n)
    iy_min, ix_min = np.unravel_index(minindex_1d, shape)
    return dist_sq_min, iy_min, ix_min
