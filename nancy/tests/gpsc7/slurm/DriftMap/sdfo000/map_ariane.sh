#! /bin/bash -l
#
#SBATCH --job-name=map_ariane.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/map_ariane.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/sdfo000/map_ariane
cd /home/nso001/code/drifters/drift-tool/examples/DriftMap
# Call your program
bash run_driftmap-sims.sh -c config_dm-sims_nemo_cgrid_ariane.yaml  -o ~/data/work7/tests/sdfo000/map-ariane > $LOG 2>&1
