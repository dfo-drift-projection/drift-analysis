#! /bin/bash -l
#
#SBATCH --job-name=map_mldp_waves.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/map_mldp_waves.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/sdfo000/map_mldp_waves
cd /home/nso001/code/drifters/drift-tool/examples/DriftMap
# Call your program
bash run_driftmap-sims_mldp.sh -c config_dm-sims_mldp_waves.yaml  -o ~/data/work7/tests/sdfo000/map-mldp-waves > $LOG 2>&1
