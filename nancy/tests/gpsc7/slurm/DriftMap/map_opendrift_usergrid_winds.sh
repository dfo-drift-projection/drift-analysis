#! /bin/bash -l
#
#SBATCH --job-name=map_opendrift_usergrid_winds.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/map_opendrift_usergrid_winds.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/map_opendrift_usergrid_winds
cd /home/nso001/code/drifters/drift-tool/examples/DriftMap
# Call your program
bash run_driftmap-sims.sh -c config_dm-sims_user_grid_opendrift.yaml -m ~/data/work7/miniconda-gpsc7-new/envs/develop -o ~/data/work7/tests/map-opendrift-usergrid-winds > $LOG 2>&1
