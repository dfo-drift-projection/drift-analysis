#! /bin/bash -l
#
#SBATCH --job-name=eval_mldp_nopreprocess.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/eval_mldp_nopreprocess.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/eval_mldp_nopreprocess
cd /home/nso001/code/drifters/drift-tool/examples/DriftEval/simulations
# Call your program
bash run_drifteval-sims_mldp.sh -c config_de-sims_mldp_no_preprocess.yaml -m ~/data/work7/miniconda-gpsc7-new/envs/develop -o ~/data/work7/tests/eval-mldp-nopreprocess > $LOG 2>&1
