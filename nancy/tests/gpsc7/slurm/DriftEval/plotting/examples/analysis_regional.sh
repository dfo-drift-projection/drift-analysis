#! /bin/bash -l
#
#SBATCH --job-name=eval_regional_plot.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/eval_regional_plot.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu18.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/eval_regional_plot
cd /home/nso001/code/drifters/drift-analysis/nancy/tests/gpsc7/slurm/DriftEval/plotting/examples

export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH
# Call your program
plotting_workflow  --user_config config_de-ana_user_regional_analysis.yaml > $LOG 2>&1
