#! /bin/bash -l
#
#SBATCH --job-name=eval_crop.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/eval_crop.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu18.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/eval_crop
cd /home/nso001/code/drifters/drift-analysis/nancy/tests/gpsc7/slurm/DriftEval/plotting/examples

export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH
# Call your program
crop_to_common_comparison_set  --config config_de-ana_user_comparison_crop.yaml > $LOG 2>&1
