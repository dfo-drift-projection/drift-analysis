#! /bin/bash -l
#
#SBATCH --job-name=eval_opendrift-usergrid-winds_plot.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/v6.2/jobs/eval_opendrift-usergrid-winds_plot.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu18.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/v6.2/eval_opendrift-usergrid-winds_plot
cd /home/nso001/code/drifters/drift-analysis/nancy/tests/gpsc7/slurm/DriftEval/plotting/experiments

export PATH=/home/nso001/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH
# Call your program

# Crop
crop_to_common_comparison_set --config opendrift-user-crop.yaml >> $LOG 2>&1

# Plot
plotting_workflow --user_config opendrift-user-plot.yaml >> $LOG 2>&1
