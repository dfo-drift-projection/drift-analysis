#! /bin/bash -l
#
#SBATCH --job-name=eval_opendrift_cgrid_winds.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/eval_opendrift_cgrid_winds.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

LOG=/home/nso001/data/work7/tests/logs/sdfo000/eval_opendrift_cgrid_winds
cd /home/nso001/code/drifters/drift-tool/examples/DriftEval/simulations
# Call your program
bash run_drifteval-sims.sh -c config_de-sims_nemo_cgrid_opendrift.yaml  -o ~/data/work7/tests/sdfo000/eval-opendrift-cgrid-winds > $LOG 2>&1
