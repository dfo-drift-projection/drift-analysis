#! /bin/bash -l
#
#SBATCH --job-name=eval_ariane.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/tests/logs/jobs/eval_ariane.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00


LOG=/home/nso001/data/work7/tests/logs/sdfo000/eval_ariane
cd /home/nso001/code/drifters/drift-tool/examples/DriftEval/simulations
# Call your program
bash run_drifteval-sims.sh -c config_de-sims_nemo_cgrid_ariane.yaml -o ~/data/work7/tests/sdfo000/eval-ariane > $LOG 2>&1
