# Common settings for riops eval

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop
drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool

ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/RIOPS-PS-sample
drifter_data_dir=$HOME/data/work2/OPP/drifter_data/svp_082019/netcdf
ocean_mesh_file='None'
ocean_model_name=riopsPS
drifter_depth=15c
first_start_date="2019-09-06"
last_start_date="2019-09-06"
drift_duration=48
start_date_frequency='hourly'
start_date_interval=3
xwatervel='vozocrtx'
ywatervel='vomecrty'
drift_model_name='OpenDrift'
rotation_data_file='None'
alpha_wind=0.
opendrift_dt=180.
lon_var='longitude'
lat_var='latitude'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='depth'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time'
drifter_id_attr='buoyid'
