# Settigns for ciops eval tests

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

drifter_code_dir=$HOME/code/drifters/drift-tool
experiment=DIST

ariane_config_file=${drifter_code_dir}/examples/ariane-config-files/ciops_3D_ariane_config.yaml 
drifter_data_dir=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/DriftTool_Sample_Dataset/datasets/nafc
ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_3D_1d
ocean_mesh_file=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc
ocean_model_name=SN${experiment}
drifter_depth=15c 
interp_method=ariane
first_start_date="2016-06-11"
last_start_date="2016-06-12"
drift_duration=240
ariane_exec=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/bin/ariane
start_date_frequency='daily'
start_date_interval=1
xwatervel='uo'
ywatervel='vo'
rotation_data_file='/home/nso001/data/work2/rotation_pickles/ciopse/ciopse.rotation.pickle'
land_mask_type='ocean_model'
alpha_wind=3.
opendrift_dt=180.
initial_bbox='None'
lon_var='nav_lon'
lat_var='nav_lat'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='gdepw_1d'
tmask_var='tmask'
drifter_id_attr='buoyid'
drifter_meta_variables='None'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time_counter'
