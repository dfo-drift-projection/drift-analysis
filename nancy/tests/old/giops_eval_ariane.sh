#!/bin/bash
0;136;0c
# Set environment

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


drift_model_name='Ariane'
drifter_data_dir=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/drifters/DriftTool_Sample_Dataset/datasets/nafc
drifter_id_attr='buoyid'

config_file='/home/nso001/code/drifters/drift-analysis/nancy/tests/giops.yaml'
ariane_config_file='/home/nso001/code/drifters/drift-analysis/nancy/tests/giops_ariane.yaml'

experiment_dir=/home/nso001/data/work2/OPP/opendrift_test/DriftEval/giops_eval_ariane

echo "set arguments. running drift map..."
drift_predict \
    -c $config_file \
    --ariane-config-file $ariane_config_file \
    --experiment-dir $experiment_dir \
    --drifter-data-dir $drifter_data_dir \
    --drifter-id-attr $drifter_id_attr \
    --drift-model-name $drift_model_name 

drift_evaluate \
  --data_dir $experiment_dir/output

#combine the output into per drifter files
combine_track_segments \
  --data_dir=$experiment_dir/output


