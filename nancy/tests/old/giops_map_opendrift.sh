#!/bin/bash
0;136;0c
# Set environment

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


drift_model_name='OpenDrift'
num_particles_x=50
num_particles_y=50
initial_bbox='-170 60 -60 75'


config_file='/home/nso001/code/drifters/drift-analysis/nancy/tests/giops.yaml'
ariane_config_file='/home/nso001/code/drifters/drift-analysis/nancy/tests/giops_ariane.yaml'

experiment_dir=/home/nso001/data/work2/OPP/opendrift_test/DriftMap/giops_map_opendrift

echo "set arguments. running drift map..."

drift_map \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --initial-bbox "$initial_bbox" \
    --drift-model-name $drift_model_name 

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk' \
  --interval 24

