# Common setting for riops map

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool

experiment=PS

# Parameters
ariane_config_file=$HOME/code/drifters/drift-analysis/nancy/tests/nemo_2D_ariane_config.yaml 

#model
ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/RIOPS-PS-sample
ocean_mesh_file='None'
ocean_model_name=riops${experiment}
drifter_depth=0c
interp_method=ariane
num_particles_x=15
num_particles_y=15
first_start_date="2019-09-06"
last_start_date="2019-09-06"
drift_duration=48
ariane_exec=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/bin/ariane
start_date_frequency='daily'
start_date_interval=1
xwatervel='vozocrtx'
ywatervel='vomecrty'
drift_model_name='OpenDrift'
rotation_data_file='None'
alpha_wind=3.
opendrift_dt=180.
initial_bbox='-60 43 -50 47'
lon_var='longitude'
lat_var='latitude'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='depth'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time'
