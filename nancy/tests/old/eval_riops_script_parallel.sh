#!/bin/bash

# Script to run RIOPS drift evaluate
# Nancy Soontiens

source riops_eval_source.sh

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


# Run arameters
run_option='parallel'

experiment_dir=/home/nso001/data/work2/OPP/opendrfit_test/DriftEval/riops_parallel

echo "set arguments. running drift predict..."

drift_predict \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --ocean-mesh-file $ocean_mesh_file \
    --ocean-model-name $ocean_model_name\
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drift-duration $drift_duration \
    --run-option $run_option \
    --drifter-depth $drifter_depth \
    --drift-model-name $drift_model_name \
    --rotation-data-file $rotation_data_file \
    --utm-zone $utm_zone \
    --opendrift-map-reader $opendrift_map_reader \
    --alpha-wind $alpha_wind \
    --opendrift-dt $opendrift_dt \
    --lon-var $lon_var \
    --lat-var $lat_var \
    --ulon-var $ulon_var \
    --ulat-var $ulat_var \
    --vlon-var $vlon_var \
    --vlat-var $vlat_var \
    --dep-var $wdep_var \
    --tmask-var $tmask_var \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --xwatervel $xwatervel \
    --ywatervel $ywatervel \
    --zwatervel $zwatervel \
    --temperature $temperature \
    --salinity $salinity \
    --density $density \
    --model-time $model_time \
    --drifter-id-attr $drifter_id_attr\
    --drifter-data-dir $drifter_data_dir

drift_evaluate \
    --data_dir $experiment_dir/output

combine_track_segments \
    --data_dir $experiment_dir/output
