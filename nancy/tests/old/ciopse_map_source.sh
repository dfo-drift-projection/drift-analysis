# Common parameters for ciopse map tests

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool
ariane_config_file=$HOME/code/drifters/drift-analysis/nancy/tests/nemo_2D_ariane_config.yaml

ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_2D_1h
ocean_mesh_file=/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/CIOPSE_InitialTest_Surface_mesh.nc

experiment=DIST

ocean_model_name=SN${experiment}
drifter_depth=L1c
interp_method=ariane
num_particles_x=15
num_particles_y=15
first_start_date="2016-06-08"
last_start_date="2016-06-08"
drift_duration=240
ariane_exec=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/bin/ariane
start_date_frequency='daily'
start_date_interval=1
xwatervel='uos'
ywatervel='vos'
rotation_data_file='/home/nso001/data/work2/rotation_pickles/ciopse/ciopse.rotation.pickle'
land_mask_type='ocean_model'
alpha_wind=3.
opendrift_dt=180.
initial_bbox='-60 43 -50 47'
lon_var='nav_lon'
lat_var='nav_lat'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='gdepw_1d'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time_counter'
plot_interval=24
