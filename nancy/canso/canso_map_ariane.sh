#!/bin/bash

# Script for running with Port of Canso model
# Model provided by Adam Drozdowski
# Nancy Soontiens

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

drift_duration=48 # hours
start_date_frequency='daily'
start_date_interval=1
num_particles_x=20
num_particles_y=20
first_start_date='2016-01-01'
last_start_date='2016-12-31'
initial_bbox='None'

config=/fs/vnas_Hdfo/odis/nso001/code/drifters/drift-analysis/nancy/canso/canso.yaml

experiment_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/canso/2016_ariane_hourly

daily_drift_map \
    -c $config \
    --experiment-dir $experiment_dir \
    --drift-duration $drift_duration \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --initial-bbox $initial_bbox \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --first-start-date $first_start_date \
    --last-start-date $last_start_date

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk'
