#OUTDIR='/home/nso001/data/work7/OPP/salishsea-ensembles/drift_runs'
source PATHS.sh

# Submit all the drift runs
#ens=("E02" "E03" "E04" "E05" "E06" "E07" "E08" "E12" "E13" "E14" "E15" "E16" "E17" "E18" "E19" "E20" "ENS16")
ens=("${ENS_MEMBERS[@]}")
ens+=("${ENSMEAN}")

for idx in "${!ens[@]}"; do
    en=${ens[$idx]}
    jobfile=${RUNSET}/${PERIOD}/${en}_submit.sh
    cp run_jobs_template.sh ${jobfile}
    sed -i s"+MY_RUN+${en}+" ${jobfile}
    sed -i s"+run_jobs.sh+${jobfile}+" $jobfile
    sbatch --cluster=gpsc7 ${jobfile}
done

# Create the cropping config
NEWLINE=$'\n'
string='datadirs:'
for idx in "${!ens[@]}"; do
    en=${ens[$idx]}
    string+="${NEWLINE}    ${en}: '${OUTDIR}/${en}/output_per_drifter/'"
done
string+="${NEWLINE}savedir:  '${OUTDIR}/ensemble_group'"
string+="${NEWLINE}overwrite_savedir: False"
echo "$string" > ${RUNSET}/${PERIOD}/crop.yaml

# Create the plotting config
cp plotting_template.yaml ${RUNSET}/${PERIOD}/plotting.yaml
sed -i s"+MY_DATADIR+${OUTDIR}/ensemble_group+" ${RUNSET}/${PERIOD}/plotting.yaml
sed -i s"+MY_PLOTDIR+${OUTDIR}/ensemble_group_plots+" ${RUNSET}/${PERIOD}/plotting.yaml

# Submit the plotting and cropping separately with submit_plot.sh 
