#! /bin/bash -l
#
#SBATCH --job-name=post-process.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/salishsea-ensembles/logs/post-process.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00

source PATHS.sh

# A script to post-process the ensemble runs
export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH

#crop_to_common_comparison_set --config ${RUNSET}/${PERIOD}/crop.yaml
#plotting_workflow --user_config ${RUNSET}/${PERIOD}/plotting.yaml
crop_to_common_comparison_set --config crop_all.yaml
plotting_workflow --user_config plotting.yaml
