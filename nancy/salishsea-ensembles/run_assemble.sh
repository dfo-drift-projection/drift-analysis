#! /bin/bash -l
#
#SBATCH --job-name=run_assemble.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/salishsea-ensembles/logs/jobs/run_assemble.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00


#BASEDIR=/home/nso001/data/work7/OPP/salishsea-ensembles/
source PATHS.sh
LOG_DIR=${BASEDIR}/logs/
cd /home/nso001/code/drifters/drift-analysis/nancy/salishsea-ensembles

#runs=("E02" "E03" "E04" "E05" "E06" "E07" "E08" "E12" "E13" "E14" "E15" "E16" "E17" "E18" "E19" "E20")
runs=("${ENS_MEMBERS[@]}")
xvel='uo'
yvel='vo'

# Call your program
export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH
for idx in "${!runs[@]}"; do
    run=${runs[$idx]}
    exp_dir=${SAV_DIR}/${run}
    LOG=${LOG_DIR}/${run}_assemble.log
    ocean_meta=${BASEDIR}/${RUNSET}/${run}_ocean.json
    ocean_data_dir=${BASEDIR}/${RUNSET}/data/${run}/

    assemble_ocean_metadata --data_dir $ocean_data_dir --mesh_file ${MESH} --metadata_file $ocean_meta --xwatervel $xvel --ywatervel $yvel --depth 'gdepw_1d' > $LOG 2>&1
done

# Ensemble mean
yvel=vo_mean
xvel=uo_mean
ocean_data_dir=${BASEDIR}/${RUNSET}/data/${ENSMEAN}
ocean_meta=${BASEDIR}/${RUNSET}/${ENSMEAN}_ocean.json
LOG=${LOG_DIR}/${ENSMEAN}_assemble.log
assemble_ocean_metadata --data_dir $ocean_data_dir --mesh_file ${MESH} --metadata_file $ocean_meta --xwatervel $xvel --ywatervel $yvel --depth 'gdepw_1d' > $LOG 2>&1


# Atmos
assemble_atmos_wave_metadata --data_dir ${ATMOS_RUNS} -m ${ATMOS_METADATA_FILE}

# Drifters
assemble_drifter_metadata --data_dir ${DRIFTERS} -m ${DRIFTER_METADATA_FILE}
