#Source ensemble, atmos and drifter data
DATADIR=/home/mid002/WORK7/AUTORUN/SS500
BASEDIR=/home/nso001/data/work7/OPP/salishsea-ensembles
OUT=${BASEDIR}/data
#ENSMEAN=ENS16
#ENS_MEMBERS=("E02" "E03" "E04" "E05" "E06" "E07" "E08" "E12" "E13" "E14" "E15" "E16" "E17" "E18" "E19" "E20")
#RUNSET='ens16'


ENSMEAN=ENSK10
ENS_MEMBERS=("K01" "K02" "K03" "K04" "K05" "K06" "K07" "K08" "K09" "K10")
RUNSET="ensk10"

MESH='/home/sdfo600/CONSTANTS/sal500_20201211/mesh_mask201702us.nc'

#PERIOD='201710_201801'
PERIOD='201704_201709'
#PERIOD='201802_201805'

#Drift run output
OUTDIR=${BASEDIR}/${RUNSET}/${PERIOD}/drift_runs

mkdir -p ${RUNSET}/${PERIOD}

#Drifter dyata
DRIFTERS=/home/sdfo000/sitestore7/opp_drift_fa3/drifters/DriftTool_Sample_Dataset/datasets/waterproperties_drifters/netcdf/
DRIFTER_METADATA_FILE=${BASEDIR}/${RUNSET}/drifters.json

# ATMOS
ATMOSDIR=/home/sdfo600/gpfs7/MSHYDRO-OPPv2/
ATMOS_RUNS=${BASEDIR}/${RUNSET}/data/atmos
ATMOS_METADATA_FILE=${BASEDIR}/${RUNSET}/atmos.json
