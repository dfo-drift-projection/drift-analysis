#DATADIR=/home/mid002/WORK7/AUTORUN/SS500
#OUT=/home/nso001/data/work7/OPP/salishsea-ensembles/data
source PATHS.sh
OUT=${BASEDIR}/${RUNSET}/data

#ens=("E02" "E03" "E04" "E05" "E06" "E07" "E08" "E12" "E13" "E14" "E15" "E16" "E17" "E18" "E19" "E20")
ens=("${ENS_MEMBERS[@]}")

for idx in "${!ens[@]}"; do
    en=${ens[$idx]}
    echo ${en}
    mkdir -p ${OUT}/${en}
    ln -s ${DATADIR}/${en}/CDF/*/NEMO_RPN_1h_grid_[U,V]_20*.nc ${OUT}/${en}/.
done

# Atmos
mkdir -p ${ATMOS_RUNS}
ln -s ${ATMOSDIR}/201[6,7,8,9]/HRDPS_OPPwestV2_ps2.5km*.nc ${ATMOS_RUNS}/.
ln -s ${ATMOSDIR}/2020/HRDPS_OPPwestV2_ps2.5km*.nc ${ATMOS_RUNS}/.

# Ensemble mean
mkdir -p ${OUT}/${ENSMEAN}
ln -s ${DATADIR}/${ENSMEAN}/1h_grid_U/*.nc ${OUT}/${ENSMEAN}/.
ln -s ${DATADIR}/${ENSMEAN}/1h_grid_V/*.nc ${OUT}/${ENSMEAN}/.

