#! /bin/bash -l
#
#SBATCH --job-name=ensk10/201704_201709/K05_submit.sh
#SBATCH --account=dfo_dpnm
#SBATCH --partition=standard
#SBATCH --output=/home/nso001/data/work7/OPP/salishsea-ensembles/logs/jobs/ensk10/201704_201709/K05_submit.sh
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=1
#SBATCH --mem-per-cpu=50000M
#SBATCH --comment="image=registry.maze.science.gc.ca/ssc-hpcs/generic-job:ubuntu22.04"
#SBATCH --time=06:00:00


#BASEDIR=/home/nso001/data/work7/OPP/salishsea-ensembles/
source PATHS.sh

SAV_DIR=${OUTDIR}
LOG_DIR=${BASEDIR}/logs/
cd /home/nso001/code/drifters/drift-analysis/nancy/salishsea-ensembles

run=K05
config=/home/nso001/code/drifters/drift-analysis/nancy/salishsea-ensembles/${RUNSET}/${PERIOD}/config.yaml
if [[ "${run}" == "${ENSMEAN}" ]]; then
    config=/home/nso001/code/drifters/drift-analysis/nancy/salishsea-ensembles/${RUNSET}/${PERIOD}/config_mean.yaml
fi

# Call your program
export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH
exp_dir=${SAV_DIR}/${run}
LOG=${LOG_DIR}/${run}.log
ocean_meta=${BASEDIR}/${RUNSET}/${run}_ocean.json
ocean_data_dir=${BASEDIR}/${RUNSET}/data/${run}/
ocean_model_name=$run

command="drift_predict -c $config --experiment-dir $exp_dir --ocean-data-dir $ocean_data_dir --ocean-metadata-file $ocean_meta --ocean-model-name $run --atmos-data-dir ${ATMOS_RUNS} --atmos-metadata-file ${ATMOS_METADATA_FILE} --drifter-data-dir ${DRIFTERS} --drifter-metadata-file ${DRIFTER_METADATA_FILE} --ocean-mesh-file ${MESH} > $LOG 2>&1"
eval $command
echo "$command" > $exp_dir/run_command
cp $config $exp_dir/.
