# Script to combine outputs across periods from a runset
export PATH=~/data/work7/miniconda-gpsc7-new/envs/develop/bin:$PATH

source PATHS.sh

periods=("201704_201709" "201710_201801")

allout=${BASEDIR}/${RUNSET}/all_output
mkdir -p ${allout}

ens=("${ENS_MEMBERS[@]}")
ens+=("${ENSMEAN}")

for jdx in "${!periods[@]}"; do
    period=${periods[$jdx]}
    echo $period
    mydir=${BASEDIR}/${RUNSET}/${period}/drift_runs

    for idx in "${!ens[@]}"; do
        en=${ens[$idx]}
	echo $en
        mkdir -p ${allout}/${en}/output
        cp ${mydir}/${en}/output/*.nc ${allout}/${en}/output/.
    done
    
done

for idx in "${!ens[@]}"; do
    en=${ens[$idx]}
    combine_track_segments --data_dir ${allout}/${en}/output/
done


# Create the cropping config
NEWLINE=$'\n'
string='datadirs:'
for idx in "${!ens[@]}"; do
    en=${ens[$idx]}
    string+="${NEWLINE}    ${en}: '${allout}/${en}/output_per_drifter/'"
done
string+="${NEWLINE}savedir:  '${allout}/ensemble_group'"
string+="${NEWLINE}overwrite_savedir: False"
echo "$string" > crop_all.yaml

# Create the plotting config
cp plotting_template.yaml plotting.yaml
sed -i s"+MY_DATADIR+${allout}/ensemble_group+" plotting.yaml
sed -i s"+MY_PLOTDIR+${allout}/ensemble_group_plots+" plotting.yaml
