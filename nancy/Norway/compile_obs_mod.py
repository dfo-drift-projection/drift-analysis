# Script to compile all observations and model for a given drifter into
# a single netcdf file.
# Observations are interpolated to the model time.
# Output is formatted as what is expected from drift_evaluate
# Nancy Soontiens

import datetime
import glob
import os

import numpy as np
import pandas as pd
import xarray as xr

# Source sande save directories
MOD_SRC_DIR='/data/data1/OPP/Norway/model'
OBS_SRC_DIR='/data/data1/OPP/Norway/obs'
SAVE_DIR='/data/data1/OPP/Norway/netcdf'
# Note: data in MOD_SRC_DIR, and OBS_SRC_DIR are saved in csv.
# This codes expects the csv files to be in a certain format.
# Reading the csv is handled by one of three functions:
# read_model_csv, read_SCT_csv or read_other_csv (other is for OSKER or Roby)

def main():
    """ 
    Main function to compile both obs/model fo a given drifter into one netcdf file
    The purpose is to create netcdf files understandable by drift_evaluate"""
    obs_files = glob.glob(os.path.join(OBS_SRC_DIR, '*.csv'))
    mod_files = glob.glob(os.path.join(MOD_SRC_DIR, '*.csv'))
    # Loop through all obs and model
    for obsf in obs_files:
        basename=os.path.basename(obsf)
        if 'SCT' in basename:
            obs = read_SCT_csv(obsf)
        else:
            obs = read_other_csv(obsf)
        # Prepare obs data array
        driftid = obs['DrifterID'][0]
        obs = obs.sort_values(by=['time'])
        obs=obs.set_index(['time'])
        obs_array = obs.to_xarray()
        # Loop through model files
        for modf in mod_files:
            mod = read_model_csv(modf)
            grouped = mod.groupby('DrifterID')
            for modelrun in grouped.groups:
                modelid = '{}_{}'.format(modelrun.split('_')[0],
                                         modelrun.split('_')[1])
                windage = float(modelrun.split('_')[-1])
                start_date = modelrun.split('_')[2]
                if modelid == driftid:
                    # Prepare model data array
                    g = grouped.get_group(modelrun)
                    g = g.set_index(['time'])
                    mod_array = g.to_xarray()
                    # interpolate to model position
                    obs_interp = interpolate_obs_to_model(obs_array,
                                                          mod_array)   
                    # save in netcdf
                    obs_interp['lon'] = wrap_to_180(obs_interp.lon)
                    mod_array['lon'] = wrap_to_180(mod_array.lon)
                    # remove nans
                    obs_lon = obs_interp['lon'][~np.isnan(obs_interp['lon'])]
                    obs_lat = obs_interp['lat'][~np.isnan(obs_interp['lat'])]
                    mod_lon = mod_array['lon'][~np.isnan(obs_interp['lon'])]
                    mod_lat = mod_array['lat'][~np.isnan(obs_interp['lat'])]
                    mod_time = mod_array['time'][~np.isnan(obs_interp['lat'])]
                    # Create dataset to save in netcdf
                    ds = xr.Dataset(
                        coords={'time': mod_time},
                        data_vars={'obs_lat': ('time', obs_lat),
                                   'obs_lon': ('time', obs_lon),
                                   'mod_lat': ('time', mod_lat),
                                   'mod_lon': ('time', mod_lon),})
                    ds.obs_lat.attrs['long_name'] =\
                        'Latitude of observed trajectory'
                    ds.obs_lat.attrs['units'] = 'degrees_north'
                    ds.obs_lat.attrs['_FillValue'] =\
                        ds.obs_lat.dtype.type(np.nan)
                    ds.obs_lon.attrs['long_name'] =\
                        'Longitude of observed trajectory'
                    ds.obs_lon.attrs['units'] = 'degrees_east'
                    ds.obs_lon.attrs['_FillValue'] =\
                        ds.obs_lon.dtype.type(np.nan)
                    ds.mod_lat.attrs['long_name'] =\
                        'Latitude of modelled trajectory'
                    ds.mod_lat.attrs['units'] = 'degrees_north'
                    ds.mod_lat.attrs['_FillValue'] =\
                        ds.mod_lat.dtype.type(np.nan)
                    ds.mod_lon.attrs['long_name'] =\
                        'Longitude of modelled trajectory'
                    ds.mod_lon.attrs['units'] = 'degrees_east'
                    ds.mod_lon.attrs['_FillValue'] =\
                        ds.mod_lon.dtype.type(np.nan)                       
                    ds.attrs['obs_buoyid']=driftid
                    ds.attrs['mod_run_name']=modelrun
                    ds.attrs['mod_windage']=windage
                    ds.attrs['drift_model']='MLPDn'
                    ds.attrs['ocean_model']='RIOPS'
                    ds.attrs['start_date']=start_date
                    output_file = 'RIOPS_MLDPn_{}-{}.nc'.format(
                        start_date,
                        modelrun)
                    outdir=os.path.join(SAVE_DIR,
                                        'windage_{}'.format(str(windage)))
                    if not os.path.exists(outdir):
                        os.makedirs(outdir)
                    output_file=os.path.join(outdir, output_file)
                    ds.to_netcdf(output_file)

                    
def read_SCT_csv(fname):
    """Read drifter data from an SCT drifter. fname is filename (str)"""
    basename=os.path.basename(fname)
    drifterid='{}_{}'.format(basename.split('_')[0],
                             basename.split('_')[1])
    df = pd.read_csv(fname, header=None,
                     names=['time', 'lat', 'lon'],
                     usecols=[0,3,4], parse_dates=[0,])
    df['DrifterID'] = drifterid
    return df


def read_other_csv(fname):
    """Read drifter data from ROBY or OSKERS. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[1,],
                     usecols=[2,3,4,5], header=0,
                     names=['DrifterID','time','lat', 'lon'])
    
    return df


def read_model_csv(fname):
    """Read model drifter data. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[0,],
                     header=0,
                     names=['time','DrifterID','lat', 'lon'])
    return df


def interpolate_obs_to_model(obs, mod):
    """Interpolate observed drifter positions to model output times
    obs and mod are xrrayr.DataArray with variables time, lon, lat
    """
    mTime = mod['time'][:]
    lat = np.interp(
        mTime.astype('float64').values,
        obs['time'].astype('float64').values,
        obs['lat'].values,
        left=np.nan, right=np.nan)
    lon = np.interp(
        mTime.astype('float64').values,
        obs['time'].astype('float64').values,
        obs['lon'].values,
        left=np.nan, right=np.nan)
    interp_track = xr.Dataset(
        data_vars={'lon': ('time', lon), 'lat': ('time', lat)},
        coords={'time': mTime})
    return interp_track


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


if __name__=='__main__':
    main()
