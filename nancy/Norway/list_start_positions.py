# Script to generate a list of initial start times and positions
# for model runs. The resulting list will be shared with Guillaume
# for MLDP runs.
# Nancy Soontiens

import datetime
import glob
import os

import numpy as np
import pandas as pd
import xarray as xr

RESTART_FREQ=6 # restart frequency in hours - how often to restart model
DURATION=72 # duration in hours - how long to run model
OBS_SRC_DIR='/data/data1/OPP/Norway/obs' # location of observed drifters
SAVE_DIR='/data/data1/OPP/Norway/netcdf' # where to save

# initial release dates for each drfiter
RELEASE_DATES={'Osker_1': '20180606 11:40:07',
               'Osker_2': '20180606 11:40:25',
               'Osker_3': '20180606 11:38:59',
               'Roby_2':  '20180606 11:40:05',
               'SCT_868': '20180606 12:55:36',
               'SCT_869': '20180606 12:55:36',
               'Osker_4': '20180606 18:49:22',
               'Osker_5': '20180606 18:49:36',
               'Osker_6': '20180606 18:49:22',
               'Roby_3':  '20180606 18:41:36',
               'SCT_870': '20180606 20:06:42',
               'SCT_871': '20180606 20:04:05',
               'SCT_872': '20180606 20:06:31'}
# Drifters to exclude (bad data or retrieved by vessel)
exclude=['SCT_869', 'Roby_2']

def main():
    """Main function to generate list"""
    # Set up for identify extend of drifter data (needed by MLDP)
    lon_min=1000
    lon_max=-1000
    lat_min=1000
    lat_max=-1000
    # Identify all files
    obs_files = glob.glob(os.path.join(OBS_SRC_DIR, '*.csv'))
    # Initialize dataframe for initial positions and times.
    df_pos = pd.DataFrame(columns=['Drifter_ID', 'start_time',
                                   'end_time', 'start_lat', 'start_lon'])
    # Loop through all obs
    for obsf in obs_files:
        basename=os.path.basename(obsf)
        if 'SCT' in basename:
            obs = read_SCT_csv(obsf)
        else:
            obs = read_other_csv(obsf)
        # Prepare obs data array
        driftid = obs['DrifterID'][0]
        if driftid in exclude:
            continue
        obs = obs.sort_values(by=['time'])
        # Remove times less than release time
        release = datetime.datetime.strptime(RELEASE_DATES[driftid],
                                             '%Y%m%d %H:%M:%S')

        obs = obs[obs['time']>release]
        # Update min/max lat and lon
        lon_min = min(obs['lon'].min(), lon_min)
        lat_min = min(obs['lat'].min(), lat_min)
        lon_max = max(obs['lon'].max(), lon_max)
        lat_max = max(obs['lat'].max(), lat_max)
        # remove times greater than end - duration
        end_date = obs['time'].iloc[-1] - datetime.timedelta(hours=DURATION)
        obs = obs[obs['time']<end_date]
        obs = obs.reset_index()
        # Create start times
        num_restarts =\
            (obs['time'].iloc[-1] -\
             obs['time'].iloc[0]).total_seconds()/(3600.*RESTART_FREQ)
        start_times = [obs['time'].iloc[0] +\
                       datetime.timedelta(hours=RESTART_FREQ*n)
                       for n in range(int(num_restarts))]
        # isolate position closest to start_time, add info to dateframe
        for t in start_times:
            new = obs.copy()
            new['time_diff'] = abs(new['time'] - t)
            idx = new['time_diff'].idxmin()
            start = new['time'].iloc[idx].strftime('%m/%d/%Y %H:%M:%S')
            end =\
                (new['time'].iloc[idx] +\
                datetime.timedelta(hours=DURATION)).strftime('%m/%d/%Y %H:%M:%S')
            data_dict = {'Drifter_ID': driftid,
                         'start_time': start,
                         'end_time': end,
                         'start_lon': new['lon'].iloc[idx],
                         'start_lat': new['lat'].iloc[idx]}
            new_pd = pd.DataFrame(data=data_dict, index=[0])
            df_pos = pd.concat([df_pos, new_pd])
    outfile='/data/data1/OPP/Norway/start_positions.csv'
    df_pos.to_csv(outfile,index=False,
                  columns=['Drifter_ID','start_time', 'end_time',
                           'start_lon', 'start_lat'])
    print("Required domain {} {} {} {}".format(lon_min, lon_max,
                                               lat_min, lat_max))
                    
def read_SCT_csv(fname):
    """Read drifter data from an SCT drifter. fname is filename (str)"""
    basename=os.path.basename(fname)
    drifterid='{}_{}'.format(basename.split('_')[0],
                             basename.split('_')[1])
    df = pd.read_csv(fname, header=None,
                     names=['time', 'lat', 'lon'],
                     usecols=[0,3,4], parse_dates=[0,])
    df['DrifterID'] = drifterid
    return df


def read_other_csv(fname):
    """Read drifter data from ROBY or OSKERS. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[1,],
                     usecols=[2,3,4,5], header=0,
                     names=['DrifterID','time','lat', 'lon'])
    
    return df


if __name__=='__main__':
    main()
