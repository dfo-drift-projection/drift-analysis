# Run drift evaluate on mldp output
# Nancy Soontiens

export PATH=$HOME/miniconda/bin:$PATH

output=/data/data1/OPP/Norway/netcdf/
plots=/data/data1/OPP/Norway/plots/drift_output

drift_evaluate \
    --data_dir $output\
    --plot_dir $plots\
    --etopo /home/soontiensn/data/ocn-nav/ETOPO1_Bed_g_gmt4.grd
