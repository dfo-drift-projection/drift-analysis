# Script to plot skill and trajectories from Norway experiemment
# Color-code different windage values.+
# A new plot for each drift segment, each drifter and each skill
# Also a map for each segment and each drifter
# Run with driftutil installation
# export PATH=$HOME/miniconda/bin:$PATH
# Nancy Soontiens

import os
import glob

import matplotlib.cm as cmap
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import xarray as xr

from driftutils import ioutils as ioutils
from driftutils import plot_trajectories as plot_trajectories

DATA_DIR='/data/data1/OPP/Norway/netcdf/'
SAVE_DIR='/data/data1/OPP/Norway/plots/'
ETOPO='/home/soontiensn/data/ocn-nav/ETOPO1_Bed_g_gmt4.grd'



def main():
    """Main function for plotting skills"""
    # iterate through drifters - gather all ids and windages
    unique_ids=[]
    windages=[]
    start_dates=[]
    for dirpath, dirnames, filenames in os.walk(DATA_DIR):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            run_id = filename.split('-')[-1]
            drifter_id = '{}_{}'.format(run_id.split('_')[0],
                                        run_id.split('_')[1])
            start_date = run_id.split('_')[2]
            windage = float(run_id.split('_')[-1][:-3])
            if windage not in windages:
                windages.append(windage)
            if drifter_id not in unique_ids:
                unique_ids.append(drifter_id)
            if start_date not in start_dates:
                start_dates.append(start_date)
    # Plot for each drifter, each windage category and each startdate
    # Define colormaps
    windages.sort()
    num_windages=len(windages)
    cm=cmap.get_cmap('plasma_r', num_windages)
    colors=[cm(1.*i/num_windages) for i in range(num_windages)]
    # List skills and appropritate labels
    skills = ['sep', 'liu', 'molcard', 'mod_disp', 'mod_lat', 'mod_lon']
    labels = ['Separation Distance [km]',
              'Liu Skill Score',
              'Molcard Skill Score',
              'Displacement from origin [km]',
              'Latitude',
              'Longitude']
    # Iterate through drfters
    for drifter_id in unique_ids:
        for start_date in start_dates:
            # Plot map of trajectories
            plot_all_trajectories(drifter_id, start_date, windages, colors)
            for skill, label in zip(skills, labels):
                plot_skill_all(drifter_id, start_date, windages,
                               colors, skill, label)

                
def plot_all_trajectories(drifter_id, start_date, windages, colors):
    """Plot and save map of all trajectories associated with a 
       drifter_id(str) and start_date(str: YYYYMMDDhhmm)
       windages contains a list of windage values to be plotted.
       colors is a list of the colors associated with each windage. 
       """
    # Identify bbox
    mod_drifters=[]
    obs_drifters=[]
    fnames=glob.glob(os.path.join(DATA_DIR,
                                  '*',
                                  '*{}_{}*.nc'.format(drifter_id,
                                                      start_date)))
    print(fnames)
    if len(fnames) != 0:
        for fname in fnames:
            mod_track=ioutils.load_drifter_track(fname, name_prefix='mod_')
            obs_track=ioutils.load_drifter_track(fname, name_prefix='obs_')
            mod_drifters.append(mod_track)
            obs_drifters.append(obs_track)
            all_drifters=obs_drifters.copy()
            all_drifters.extend(mod_drifters)
        # Start plotting
        # Set up basemap and figure
        bbox = plot_trajectories.determine_latlon_bbox_for_drifters(
            all_drifters)
        basemap_settings = dict(
            llcrnrlon=bbox.lon_min, llcrnrlat=bbox.lat_min,
            urcrnrlon=bbox.lon_max, urcrnrlat=bbox.lat_max,
            ellps='WGS84', resolution='h', projection='merc', lat_ts=20.)
        fig_traj, ax_traj = plt.subplots(1,1)
        ax_traj.basemap = Basemap(**basemap_settings)

        plot_trajectories.plot_bathymetry(ETOPO,ax_traj.basemap, bbox)

        # Plot trajectories
        count=len(windages)-1
        for windage, color in zip(windages[::-1], colors[::-1]):
            print(count)
            fname = glob.glob(os.path.join(DATA_DIR,
                              'windage_{0:.1f}'.format(windage),
                              '*{0}_{1}_{2:.1f}.nc'.format(drifter_id,
                                                           start_date,
                                                           windage)))
            if len(fname)==0:
                continue
            if len(fname) > 1:
                print('More than one',fname)
            fname=fname[0]
            if count==1:
                obs_track = ioutils.load_drifter_track(fname,
                                                       name_prefix='obs_')
                plot_trajectories.plot_trajectory(obs_track, ax_traj.basemap,
                                                  'k', label='Observed')
            mod_track =ioutils.load_drifter_track(fname, name_prefix='mod_')
            plot_trajectories.plot_trajectory(
                mod_track, ax_traj.basemap,
                color,
                label='Windage {0:.1f}'.format(windage))
            count=count-1
        # Make plot pretty
        ax_traj.basemap.drawcoastlines(linewidth=0.15)
        ax_traj.basemap.fillcontinents()
        ax_traj.basemap.drawmapboundary()
        
        parallels = np.linspace(bbox.lat_min, bbox.lat_max, num=3)
        ax_traj.basemap.drawparallels(parallels, labels=[1, 0, 0, 0],
                                      fontsize=9,
                                      linewidth=0.5, color='0.5')
        meridians = np.linspace(bbox.lon_min, bbox.lon_max, num=3)
        ax_traj.basemap.drawmeridians(meridians, labels=[0, 0, 0, 1],
                                      fontsize=9,
                                      linewidth=0.5, color='0.5')
        handles, labels = ax_traj.get_legend_handles_labels()
        # sort both labels and handles by labels
        labels, handles = zip(*sorted(zip(labels, handles),
                                      key=lambda t: t[0]))
        ax_traj.legend(handles, labels,
                       loc='upper right',bbox_to_anchor=(-.25,1))
        ax_traj.set_title('{} {}'.format(drifter_id, start_date))
        plt.close()
        savedir = os.path.join(SAVE_DIR, 'maps')
        if not os.path.exisis(savedir):
            os.makedirs(savedir)
        outname = os.path.join(savedir,
                               '{}_{}_map.png'.format(drifter_id,
                                                      start_date))
        fig_traj.savefig(outname, bbox_inches='tight', dpi=300)
    else:
        print("No drifters for id {} and start {}".format(drifter_id,
                                                          start_date))

def plot_skill_all(drifter_id, start_date, windages, colors, skill, ylabel):
    """Plot and save time series of skill score for a given drifter_id (str) 
    and start_date(str: YYYYMMDDhhmm)
    windages contains a list of windage values to be plotted.
    colors is a list of the colors associated with each windage.
    skill (str) is the skill to be plotted
    ylabel (str) is the associated y-axis label for the skill
    """
    fig, ax = plt.subplots(1,1, figsize=(10,5))
    count=len(windages)-1
    for windage, color in zip(windages[::-1], colors[::-1]):
        fname = glob.glob(os.path.join(DATA_DIR,
                             'windage_{0:.1f}'.format(windage),
                             '*{0}_{1}_{2:.1f}.nc'.format(drifter_id,
                                                          start_date,
                                                          windage)))
        if not fname:
            continue
        print(len(fname), fname)
        fname=fname[0]
        with xr.open_dataset(fname) as ds:
            skill_values = ds[skill].values
            if (skill=='sep') or (skill=='mod_disp'):
                skill_values=skill_values/1000.
                if count==1:
                    ax.plot(ds.time,
                            ds.obs_disp.values/1000.,
                            'k',
                            label='Persistence')
            elif (skill =='mod_lon'):
                if count==1:
                    ax.plot(ds.time,
                            ds.obs_lon.values,
                            'k',
                            label='Observations')
            elif (skill =='mod_lat'):
                if count==1:
                    ax.plot(ds.time,
                            ds.obs_lat.values,
                            'k',
                            label='Observations')
            ax.plot(ds.time, skill_values, color=color,
                    label='Windage {0:.1f}'.format(windage))
            count=count-1
    if len(fname) != 0:
        ax.set_xlabel('Time')
        ax.set_ylabel(ylabel)
        ax.set_title('{} {}'.format(drifter_id, start_date))
        handles, labels = ax.get_legend_handles_labels()
        # sort both labels and handles by labels
        labels, handles = zip(*sorted(zip(labels, handles),
                                      key=lambda t: t[0]))
        ax.legend(handles, labels,
                  loc='upper left', bbox_to_anchor=(1,1))
        ax.grid()
        savedir=os.path.join(SAVE_DIR,'skills',skill)
        if not os.path.exists(savedir):
            os.makedirs(savedir)
        figname = os.path.join(savedir,
                               '{}_{}_{}'.format(drifter_id,
                                                 start_date,
                                                 skill))
        fig.savefig(figname, bbox_inches='tight', dpi=300)
    plt.close()

            
if __name__=="__main__":
    main()
