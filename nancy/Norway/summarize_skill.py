# Routine to summarize skill scores from Norway drift experiments.
# Can produce mean skill scores or histograms of skill and separation distance.
# Nancy Soontiens

import datetime
import glob
import os

import matplotlib.cm as cmap
import matplotlib.pyplot as plt
import pandas as pd
import xarray as xr

import numpy as np

NETCDF_DIR='/data/data1/OPP/Norway/netcdf'
SAVE_DIR='/data/data1/OPP/Norway/plots/summary'

# list of windages to consider
windages=[0,0.1,0.2,0.3,0.4,0.5,1,1.5,2,2.5,3,3.5,4,4.5,5]
# list of drifter ids to consider
drifter_ids=['Osker_1',
             'Osker_2',
             'Osker_3',
             'Osker_4',
             'Osker_5',
             'Osker_6',
             'Roby_3',
             'SCT_868',
             'SCT_870',
             'SCT_871',
             'SCT_872']

# Colors for windages
num_windages=len(windages)
cm=cmap.get_cmap('plasma_r', num_windages)
colors=[cm(1.*i/num_windages) for i in range(num_windages)]


def main2():
    """A function for plotting 2D histograms for a single drifter.
    molcard and liu skill scores are plotted.
    Skill scores are binned into numbers of hours since start time
    (x-axis) and 10 bins for the skill score (y-axis)
    Histogram is presented as percent occurence for each """
    skills = ['molcard', 'liu']
    labels = ['Mean Molcard skill score', 'Mean Liu skill score']
    ylabels = ['Molcard skill score', 'Liu skill score']
    for drifter_id in drifter_ids:
        #build data frames for eachn windage
        dfs = {}
        for windage in windages:
            print(windage, drifter_id)
            dfs[windage] = gather_drifter_skill(drifter_id, windage)
        # For one skill, plot one windage
        for windage in windages:
            fig,axs = plt.subplots(2,1,figsize=(10,10))
            for skill, label, ylabel, ax in zip(skills,labels,ylabels, axs):

                plot_drifter_histogram(ax, dfs[windage], skill, label)
                ax.set_xlim([1,71])
                ax.set_ylabel(ylabel)
                ax.set_title('{0} - Windage: {1:.1f} - {2}'.format(drifter_id,
                                                                   windage,
                                                                   ylabel))
            dirname = os.path.join(SAVE_DIR, 'histograms', drifter_id)
            if not os.path.exists(dirname):
                os.mkdir(dirname)
            figname=os.path.join(dirname,
                                 '{0}_{1:.1f}_hist.png'.format(drifter_id,
                                                               windage))
            fig.savefig(figname, dpi=300, bbox_inches='tight')

def main():
    """Plot mean skill for a drifter binned by number of hours since start"""
    skills = {'sep': 'Mean separation distance [km]',
              'molcard': 'Mean Molcard skill score',
              'liu': 'Mean Liu skill score'}
    for drifter_id in drifter_ids:
        #build data frames for eachn windage
        dfs = {}
        for windage in windages:
            print(windage, drifter_id)
            dfs[windage] = gather_drifter_skill(drifter_id, windage)
        # For one skill, plot all windages
        fig,axs = plt.subplots(len(skills), figsize=(10, 5*len(skills)),
                               sharex=True)
        for skill, ax in zip(skills,axs):
            count=0
            for windage, color in zip(windages, colors):
                if (skill == 'sep') and (count == 0):
                    plot_mean_drifter_skill(ax, dfs[windage], 'obs_disp', 'k', '1H',
                                            'Observations')    
                plot_mean_drifter_skill(ax, dfs[windage], skill, color, '1H',
                                        'Windage {0:.1f}'.format(windage))
                count+=1
            ax.set_xlim([1,71])
            ax.set_ylabel(skills[skill])
            ax.grid()
        axs[0].set_title(drifter_id)
        axs[0].legend(loc='upper left', bbox_to_anchor=(1,1))
        axs[0].set_ylim([0,100])
        axs[1].set_ylim([0,1])
        axs[2].set_ylim([0,1])
        #plt.show()
        figname=os.path.join(SAVE_DIR,
                             '{}'.format(drifter_id))
        fig.savefig(figname, dpi=300, bbox_inches='tight')
            

def gather_drifter_skill(drifter_id, windage,
                         skills=['sep', 'liu','molcard',
                                 'mod_dist', 'mod_disp',
                                 'obs_dist', 'obs_disp']):
    """Gather all skill scores for a drifter_id(str) and windage(float) combo 
    into one pandas dataframe"""
    fnames = glob.glob(os.path.join(NETCDF_DIR,
                                    'windage_{0:.1f}'.format(windage),
                                    '*{}*.nc'.format(drifter_id)))
    fnames.sort()
    for i, fname in enumerate(fnames):
        with xr.open_dataset(fname) as ds:
            startdate=datetime.datetime.strptime(ds.start_date,
                                                 '%Y%m%d%H%M%S')
            data_dict = {skill: ds[skill].values for skill in skills}
            data_dict['time'] = pd.to_datetime(ds.time.values)
            data_dict['time_since_start'] = data_dict['time'] - startdate
            df = pd.DataFrame(data_dict)
            df['startdate'] = startdate
            df['mod_run_name'] = ds.mod_run_name
            df['drifter_id'] = drifter_id
            df['windage'] = windage
            if i == 0:
                all_df = df.copy()
            else:
                all_df = pd.concat([all_df,df],ignore_index=True)
    return all_df


def plot_mean_drifter_skill(ax, df, skill, color, sampling_freq,
                            label):
    """Bin data into sampling_freq (str eg 1H) and plot mean skill on ax
    df is the pandas dataframe containing skill and time_since_start columns
    skill (str) is the skill to be plotted
    color is the line color
    label (str) is the legend label of the line"""
    df = df.set_index('time_since_start')
    df_res = df.resample(sampling_freq, label='right').mean()[skill]
    df_res = df_res.reset_index()
    df_res['hours since start'] = df_res.time_since_start.astype('timedelta64[h]')
    if (skill =='sep') or (skill=='obs_disp'):
        df_res[skill]=df_res[skill]/1000.
    df_res.plot(x='hours since start',
                y=skill,
                ax=ax, c=color,
                label = label,
                legend=False)
    
def plot_drifter_histogram(ax, df, skill, label):
    """Plot a histogram of skill score and its mean
       Divide into hourly bins and plot relative frequency 
       ax is the axis for the plot
       df is the pandas data frame continaing columns skill and hours_since_start
       skill(str) is the skill to plot
       label is legend label for the mean skill line."""
    df['hours since start'] = df.time_since_start.astype('timedelta64[h]')
    hist, xedges, yedges = np.histogram2d(df['hours since start'].values,
                                          df[skill].values,
                                          range=[[1,72],[0,1]],
                                          bins=[np.linspace(1,72,num=72), np.linspace(0,1,num=11)])
    hist=np.ma.masked_values(hist,0)
    X,Y = np.meshgrid(xedges, yedges)
    mesh_cm=cmap.get_cmap('RdYlGn_r', 10)
    mesh = ax.pcolormesh(X,Y,100*hist.T/np.sum(hist.T,axis=0),
                         vmin=0,vmax=100,cmap=mesh_cm)
    cbar = plt.colorbar(mesh, ax=ax)
    cbar.set_label('Percent occurence')
    # Add mean skill score
    df = df.set_index('time_since_start')
    df_res = df.resample('1H', label='right').mean()[skill]
    df_res = df_res.reset_index()
    df_res['hours since start'] = df_res.time_since_start.astype('timedelta64[h]')
    df_res.plot(x='hours since start',
                y=skill,
                ax=ax, c='k',
                label = label )
    
if __name__=='__main__':
    main()
    # main2() for making histograms
    # main() for mean skill scores binned by hour since start
    # I know this is bad... I was lazy
    
