#!/bin/bash

# Script to run CIOPS eval for MEOPAR TREX
# Nancy Soontiens

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

# Set environment          
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
experiment_dir=/home/nso001/data/work2/OPP/2021-CIOPSE-parallel-CPOP/drift-eval/ciopse-par
config_file=/home/nso001/code/drifters/drift-analysis/nancy/2021-CIOPSE-parallel-CPOP/drift-eval/ciopse-par.yaml

# Run arameters

drift_predict \
    -c $config_file \
    --experiment-dir $experiment_dir

drift_evaluate \
  --data_dir $experiment_dir/output

#combine the output into per drifter files
combine_track_segments \
  --data_dir=$experiment_dir/output
