#!/bin/bash

# Script to run correction factor for 2021 CIOPSE parallel pass
# Nancy Soontiens

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

# Set environment          
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
experiment_dir=/home/nso001/data/work2/OPP/2021-CIOPSE-parallel-CPOP/correction-factor/ciopse-par
config_file=/home/nso001/code/drifters/drift-analysis/nancy/2021-CIOPSE-parallel-CPOP/correction-factor/ciopse-par.yaml

drift_correction_factor \
    -c $config_file \
    --experiment-dir $experiment_dir 
