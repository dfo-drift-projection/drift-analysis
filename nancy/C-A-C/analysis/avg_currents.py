# Script to plot mean currents over domain

import os
import pickle

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import xarray as xr

data_dir = '/home/nso001/data/work2/models/ciops-e/jul2016avg/'
mfile='/home/sdfo000/sitestore4/opp_drift_fa3/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc'
rotation_file='/home/nso001/data/work2/rotation_pickles/ciopse/ciopse.rotation.pickle'
DLONL=-70
DLONU=-50
DLATL=43
DLATU=52

# open datasets u and v
du = xr.open_dataset(os.path.join(data_dir, 'NEMO_RPN_avg_grid_U_201607.nc'))
dv = xr.open_dataset(os.path.join(data_dir, 'NEMO_RPN_avg_grid_V_201607.nc'))
mesh = xr.open_dataset(mfile)

# extract u, v, coordinates
tmask = mesh['tmask'].values[0, 0, ...]
lons = mesh['nav_lon'].values[1:, 1:]
lats = mesh['nav_lat'].values[1:, 1:]
u = np.ma.masked_array(du['uo'].isel(time_counter=0, depthu=0).values,
                       mask = 1-tmask)
v = np.ma.masked_array(dv['vo'].isel(time_counter=0, depthv=0).values,
                       mask = 1-tmask)
# unstagger
u = 0.5*(u[1:, 1:] + u[1:, 0:-1])
v = 0.5*(v[1:, 1:] + v[0:-1, 1:])
# calculate speed
speed = np.sqrt(u**2 + v**2)
# rotate to east/north
with open(rotation_file, 'rb') as f:
    coeffs = pickle.load(f)
gsint = coeffs[0][1:, 1:]
gcost = coeffs[1][1:, 1:]
ur = u*gcost - v*gsint
vr = u*gsint + v*gcost

# set up plotting
fig, axs = plt.subplots(1, 2, figsize=(10, 3))
ax=axs[0]
m = Basemap(projection='merc',
            llcrnrlat=30,
            urcrnrlat=55,
            llcrnrlon=-80,
            urcrnrlon=-35,
            resolution='l',
            ax=ax)
um, vm,x, y = m.rotate_vector(ur, vr, lons, lats, returnxy=True)
mesh = ax.pcolormesh(x, y, speed, vmin=0, vmax=1, cmap='YlGnBu')
cbar = plt.colorbar(mesh, ax=ax)
cbar.set_label('speed [m/s]')
st=30
q = ax.quiver(x[::st, ::st], y[::st, ::st],
              um[::st, ::st], vm[::st, ::st],
              color='k', width=0.005)
ax.quiverkey(q, .1, .85, 1, '1 m/s', color= 'k')
ax.text(-.1, .9, '(a)', transform=ax.transAxes)
# plot initialxation box
XL, YL = m(DLONL, DLATL)
XU, YU = m(DLONU, DLATU)
m.plot([XL, XL], [YL, YU], 'r-')
m.plot([XL, XU], [YL, YL], 'r-')
m.plot([XL, XU], [YU, YU], 'r-')
m.plot([XU, XU], [YL, YU], 'r-')
# pretty map
m.drawcoastlines()
m.drawparallels(np.arange(30, 55, 5), labels=[1,0,0,0])
m.drawmeridians(np.arange(-80, -30, 10), labels=[0,0,0,1])
# Zoom in
ax=axs[1]
m = Basemap(projection='merc',
            llcrnrlat=42,
            urcrnrlat=53,
            llcrnrlon=-71,
            urcrnrlon=-49,
            resolution='l',
            ax=ax)
um, vm, x, y = m.rotate_vector(ur, vr, lons, lats, returnxy=True)
mesh = ax.pcolormesh(x, y, speed, vmin=0, vmax=.5, cmap='YlGnBu')
cbar = plt.colorbar(mesh, ax=ax)
cbar.set_label('speed [m/s]')
st=30
q = ax.quiver(x[::st, ::st], y[::st, ::st],
              um[::st, ::st], vm[::st, ::st],
              color='k', scale=3, width=0.005)
# particle initialation box
XL, YL = m(DLONL, DLATL)
XU, YU = m(DLONU, DLATU)
m.plot([XL, XL], [YL, YU], 'r-')
m.plot([XL, XU], [YL, YL], 'r-')
m.plot([XL, XU], [YU, YU], 'r-')
m.plot([XU, XU], [YL, YU], 'r-')

ax.quiverkey(q, .3, .77, 0.2, '0.2 m/s', color= 'k')
ax.text(-.1, .9, '(b)', transform=ax.transAxes)
# pretty map
m.drawcoastlines()
m.drawparallels(np.arange(30, 55, 5), labels=[1,0,0,0])
m.drawmeridians(np.arange(-80, -30, 5), labels=[0,0,0,1])
plt.show()
fig.savefig('figures/currents.png', bbox_inches='tight')


