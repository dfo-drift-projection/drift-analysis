#!/bin/bash

# Set environment

source ciopse_map_source.sh

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


run_option='linear'
drift_model_name='Ariane'
ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/models/ciops-e/interp-cgrid-old/
ariane_config_file=ciopse_hourly.yaml
experiment_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/opendrift_test/C-A-C/DriftMap/meta_tests/interp-cgrid-ariane-nometa

echo "set arguments. running drift map..."

daily_drift_map \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --ocean-mesh-file $ocean_mesh_file \
    --ocean-model-name $ocean_model_name \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drift-duration $drift_duration \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --run-option $run_option \
    --initial-bbox "$initial_bbox" \
    --drifter-depth $drifter_depth \
    --drift-model-name $drift_model_name \
    --ariane-config-file $ariane_config_file \
    --ariane-exec $ariane_exec \
    --rotation-data-file $rotation_data_file \
    --alpha-wind $alpha_wind \
    --opendrift-dt $opendrift_dt \
    --lon-var $lon_var \
    --lat-var $lat_var \
    --ulon-var $ulon_var \
    --ulat-var $ulat_var \
    --vlon-var $vlon_var \
    --vlat-var $vlat_var \
    --dep-var $wdep_var \
    --tmask-var $tmask_var \
    --interp-method $interp_method \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --xwatervel $xwatervel \
    --ywatervel $ywatervel \
    --zwatervel $zwatervel \
    --temperature $temperature \
    --salinity $salinity \
    --density $density \
    --model-time-ocean $model_time_ocean

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/space/group/dfo_odis/project/sab002/code/drifters/MLDPn/etopo_file/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk'

