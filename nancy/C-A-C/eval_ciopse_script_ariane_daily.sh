#!/bin/bash

# Set environment

source ciopse_eval_source_3D.sh

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


run_option='linear'
drift_model_name='Ariane'
ocean_data_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/models/ciops-e/CIOPSE_SN1500_3D/
ariane_config_file=ciopse_daily.yaml
experiment_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/opendrift_test/C-A-C/DriftEval/ciopse_ariane_daily

echo "set arguments. running drift map..."

drift_predict \
    --experiment-dir $experiment_dir \
    --drifter-data-dir $drifter_data_dir \
    --ocean-data-dir $ocean_data_dir \
    --ocean-mesh-file $ocean_mesh_file \
    --ocean-model-name $ocean_model_name \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drift-duration $drift_duration \
    --run-option $run_option \
    --drifter-id-attr $drifter_id_attr \
    --drifter-meta-variables $drifter_meta_variables\
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --drifter-depth $drifter_depth \
    --drift-model-name $drift_model_name \
    --ariane-config-file $ariane_config_file \
    --ariane-exec $ariane_exec \
    --rotation-data-file $rotation_data_file \
    --alpha-wind $alpha_wind \
    --opendrift-dt $opendrift_dt \
    --lon-var $lon_var \
    --lat-var $lat_var \
    --ulon-var $ulon_var \
    --ulat-var $ulat_var \
    --vlon-var $vlon_var \
    --vlat-var $vlat_var \
    --dep-var $wdep_var \
    --tmask-var $tmask_var \
    --interp-method $interp_method \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --xwatervel $xwatervel \
    --ywatervel $ywatervel \
    --zwatervel $zwatervel \
    --temperature $temperature \
    --salinity $salinity \
    --density $density \
    --model-time-ocean $model_time_ocean


drift_evaluate \
  --data_dir $experiment_dir/output

#combine the output into per drifter files
combine_track_segments \
  --data_dir=$experiment_dir/output

#plot the aggregated output
plot_aggregated_output \
  --data_dir=$experiment_dir/output/output_per_drifter \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --no-plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --no-plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_ratio_and_track

