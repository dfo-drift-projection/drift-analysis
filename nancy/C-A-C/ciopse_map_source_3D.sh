# Common parameters for ciopse map tests

MINICONDA_PATH=/home/nso001/data/sitestore/miniconda/envs/develop

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool

ocean_mesh_file=/fs/hnas1-evs1/Ddfo/dfo_odis/jpp001/ECCC/CIOPS-E/INITIALISATION/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc

experiment=DIST

ocean_model_name=SN${experiment}
drifter_depth=L1c
interp_method=ariane
num_particles_x=25
num_particles_y=25
first_start_date="2016-07-01"
last_start_date="2016-07-01"
drift_duration=744
ariane_exec=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/bin/ariane
start_date_frequency='daily'
start_date_interval=1
xwatervel='uo'
ywatervel='vo'
rotation_data_file='/home/nso001/data/sitestore/OceanModels/CIOPS-E-dup/CIOPSE_grid_rotation_coefficients.pickle'
land_mask_type='ocean_model'
alpha_wind=0.
opendrift_dt=180.
initial_bbox='-70 43 -50 52'
lon_var='nav_lon'
lat_var='nav_lat'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='gdepw_1d'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time_counter'
