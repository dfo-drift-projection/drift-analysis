# Common parameters for ciopse map tests

MINICONDA_PATH=/home/nso001/data/sitestore/miniconda/envs/develop

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool

ocean_mesh_file='None'

drifter_depth=0c
interp_method=ariane
num_particles_x=25
num_particles_y=25
first_start_date="2019-10-13"
last_start_date="2019-10-13"
drift_duration=167
ariane_exec=/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/bin/ariane
start_date_frequency='daily'
start_date_interval=1
xwatervel='vozocrtx'
ywatervel='vomecrty'
rotation_data_file='None'
alpha_wind=0.
land_mask_type='ocean_model'
opendrift_dt=180.
initial_bbox='-70 43 -50 52'
lon_var='longitude'
lat_var='latitude'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='gdepw_1d'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time_ocean='time'
