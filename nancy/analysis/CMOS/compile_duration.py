# Script to compile duration for which model is bettern than persistence
# Usage: python compile_duration.py output_dir save_dir
# output_dir is directory with drift prediction outputs
# save_dir is the directory to save duration
# Nancy Soontiens, June 2018

import datetime
import os
import sys

import pandas as pd
import xarray as xr
import numpy as np

def main(output_dir, save_dir):
    # Create pandas data frames for hourly bins
    skills = ['duration_better', 'total_duration']          
    column_names = ['obs_lat_min','obs_lat_max', 'obs_lon_min', 'obs_lon_max',
                    'buoy_id', 'mod_run_name']
    column_names.extend(skills)
    
    df = pd.DataFrame(columns=column_names)
    for dirpath, dirnames, filenames in os.walk(output_dir,
                                                followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            # Load drifter
            data_filename=os.path.join(dirpath, filename)
            print(data_filename)
            with xr.open_dataset(data_filename) as ds:
                # Resample the data set
                molcard = ds.molcard.values
                times = pd.to_datetime(ds.time.values)
                diffs = np.diff(times)
                seconds = [td/np.timedelta64(1, 's') for td in diffs]
                try:
                    ind = np.where(molcard[1:] > 0)[0][0]
                    print(ind)
                except IndexError:
                    duration_better=0
                    ind=None
                if ind is not None:
                    try:
                        ind2 = np.where(molcard[1:][ind:] ==0)[0][0]
                        ind2=ind2+ind
                    except IndexError:
                        ind2=-1
                    print(ind2)
                    duration_better = np.sum(seconds[ind:ind2])
                total_duration = np.sum(seconds)
                data_dict = {'obs_lat_min': ds.obs_lat.values.min(),
                             'obs_lat_max': ds.obs_lat.values.max(),
                             'obs_lon_min': ds.obs_lon.values.min(),
                             'obs_lon_max': ds.obs_lon.values.max(),
                             'buoy_id': ds.obs_buoyid,
                             'mod_run_name': ds.mod_run_name,
                             'duration_better': duration_better,
                             'total_duration': total_duration}
                new_df = pd.DataFrame(data=data_dict,index=[0])
                df = pd.concat([df,new_df], ignore_index=True)
    # Save
    fname = os.path.join(savedir, 'duration_skill2')
    df.index.name='index'
    df.to_csv(fname)
    print('Saved {}'.format(fname))
                

def resample_hourly(ds, how='mean'):
    """Resample the dataset to hourly by averaging

    Parameters
    ----------
    ds : xrarray.Dataset
        the dataset to be resample. Requires dimension time and arritrbute
        mod_run_name in format oceanmodel_drfitmodel_YYYYMMDDHH_depth
    how : str
       indicates how the resampling should be performed e.g mean, min, max

    Returns
    -------
    resampled : xarray.Dataset
    """
    run_name = ds.mod_run_name
    start_time = datetime.datetime.strptime(run_name.split('_')[2], '%Y%m%d%H')
    start_hour = start_time.hour
    res = ds.resample(freq='1H', how='mean', dim='time', base=start_hour,
                      keep_attrs=True)
    return res


if __name__=='__main__':
    outdir, savedir = sys.argv[1:]
    main(outdir, savedir)
