# Script to generate summary skill plots 
# Usage: python summarize_skill.py output_dir
# Nancy Soontiens, June 2018

from collections import namedtuple
import datetime
import os
import glob
import sys

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import pandas as pd


SAVEDIR='/data/data1/OPP/drifters/drift_tool_runs/CMOS/salish_2016_3hr/new_plots'

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

BBOXES={'Salish Sea': None,
        'Juan de Fuca': LatLonBoundingBox(lon_min=-124.7,
                                          lon_max=-123.6,
                                          lat_min=48.1,
                                          lat_max=48.65),
        'Strait of Georgia': LatLonBoundingBox(lon_min=-125.3,
                                               lon_max=-122.7,
                                               lat_min=48.9,
                                               lat_max=50.1),
        'Gulf': LatLonBoundingBox(lon_min=-123.6,
                                  lon_max=-122.2,
                                  lat_min=48.1,
                                  lat_max=48.9)
}

colors={'Salish Sea': 'firebrick',
        'Juan de Fuca': 'darkgreen',
        'Strait of Georgia': 'dodgerblue',
        'Gulf': 'darkorange'}


def summarize_skill(skill_dir, bboxes=None,
                    tick_fontsize=14, axis_fontsize=16,
                    title_fontsize=18, only_salish=True,
                    plot_quantiles=False):
    """Summarize skills metrics in a skill_dir"""
    files = glob.glob(os.path.join(skill_dir, '*runhour*'))
    files.sort()
    hours=[]
    mean_sep={key: [] for key in bboxes}
    mean_obs_disp={key: [] for key in bboxes}
    q10_sep={key: [] for key in bboxes}
    q90_sep={key: [] for key in bboxes}
    q10_obs_disp={key: [] for key in bboxes}
    q90_obs_disp={key: [] for key in bboxes}
    for f in files:
        h = int(f[-2:])
        hours.append(h)
        df = pd.read_csv(f)
        for key, bbox in bboxes.items():
            if bbox:
                dfnew = truncate_region(df, bbox)
            else:
                dfnew=df.copy()
            mean_sep[key].append(dfnew.sep.mean())
            mean_obs_disp[key].append(dfnew.obs_disp.mean())
            q10_sep[key].append(dfnew.sep.quantile(0.1))
            q90_sep[key].append(dfnew.sep.quantile(0.9))
            q10_obs_disp[key].append(dfnew.obs_disp.quantile(0.1))
            q90_obs_disp[key].append(dfnew.obs_disp.quantile(0.9))
    # Plot
    hours=np.array(hours)
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    if not only_salish:
        for key in bboxes:
            sep = np.array(mean_sep[key])/1000.
            q10 = np.array(q10_sep[key])/1000.
            q90 = np.array(q90_sep[key])/1000.
            if plot_quantiles:
                ax.errorbar(hours+1, sep, yerr=[sep-q10,q90-sep],
                            fmt='--o', label=key, c=colors[key],
                            capsize=10)
            else:
                ax.plot(hours+1, sep,
                        '--o', label=key, c=colors[key])
    else:
        key='Salish Sea'
        sep = np.array(mean_sep[key])/1000.
        q10 = np.array(q10_sep[key])/1000.
        q90 = np.array(q90_sep[key])/1000.
        obs_disp=np.array(mean_obs_disp[key])/1000.
        q10_disp=np.array(q10_obs_disp[key])/1000.
        q90_disp=np.array(q90_obs_disp[key])/1000.
        if plot_quantiles:
            ax.errorbar(hours+1, sep, yerr=[sep-q10,q90-sep],
                        fmt='--o', label='Separation distance', c=colors[key],
                        capsize=10)
            ax.errorbar(hours+1, obs_disp, yerr=[obs_disp-q10_disp,
                                                 q90_disp-obs_disp],
                        fmt='o--', label='Observed displacement from origin',
                        capsize=10, c='k')
        else:
            ax.plot(hours+1, sep,
                    '--o', label='Separation distance', c=colors[key])
            ax.plot(hours+1, obs_disp, '--o',
                    label='Observed displacement from origin', c='k')
    # Make plot pretty
    ax.set_xlim([0,25])
    ax.set_xlabel('Time since model start [hours]',
                  fontsize=axis_fontsize)
    ax.set_ylabel('Mean distance [km]',
                  fontsize=axis_fontsize)
    ax.legend(loc=0)
    ax.grid()
    plt.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    plt.show()
    if not only_salish:
        fname='mean_sep.png'
    else:
        fname='mean_sep_salish.png'
    if plot_quantiles:
        fname='{}_{}.png'.format(fname[:-4],'quantiles')
    print(fname)
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()
    # Plot boxes
    if not only_salish:
        fig,ax=plt.subplots(1,1,figsize=(10,10))
        bbox=LatLonBoundingBox(lon_min=-126.21, lon_max=-121.5,
                               lat_min=47.11, lat_max=51.01)
        basemap_settings = dict(llcrnrlon=bbox.lon_min,
                                llcrnrlat=bbox.lat_min,
                                urcrnrlon=bbox.lon_max,
		                urcrnrlat=bbox.lat_max,
                                ellps='WGS84', resolution='h',
                                projection='merc', lat_ts=20.)
        ax.basemap = Basemap(**basemap_settings)
        ax.basemap.drawmapboundary(fill_color="#DDEEFF")
        ax.basemap.fillcontinents()
        ax.basemap.drawcoastlines()
        draw_meridians_parallels(ax.basemap, bbox, fontsize=tick_fontsize)
        #Draw bboxes
        for key, bbox in bboxes.items():
            if bbox:
                ax.basemap.plot([bbox.lon_min,bbox.lon_min],
                                [bbox.lat_min,bbox.lat_max],
                                latlon=True,c=colors[key],lw=2)
                ax.basemap.plot([bbox.lon_min,bbox.lon_max],
                            [bbox.lat_max,bbox.lat_max],
                            latlon=True,c=colors[key],lw=2)
                ax.basemap.plot([bbox.lon_max,bbox.lon_max],
                                [bbox.lat_max,bbox.lat_min],
                                latlon=True,c=colors[key],lw=2)
                ax.basemap.plot([bbox.lon_max,bbox.lon_min],
                                [bbox.lat_min,bbox.lat_min],
                                latlon=True,c=colors[key], lw=2)
        fig.savefig(os.path.join(SAVEDIR,'regions.png'), bbox_inches='tight')
        plt.show()
    

def truncate_region(df, bbox):
    """Truncate data df based on region defined in bbox.
       That is, exclude data outside of bbox"""
    subset = df[(df.obs_lon_min >= bbox.lon_min) &
                (df.obs_lon_max <= bbox.lon_max) &
                (df.obs_lat_min >= bbox.lat_min) &
                (df.obs_lat_max <= bbox.lat_max)]
    return subset


def draw_meridians_parallels(bmap, bbox, num_meridians=3, num_parallels=3,
                             color='k', num_decimals=1, fontsize=14):
    # draw lats and lons                                                        
    pmin=round(bbox.lat_min, num_decimals)
    pmax=round(bbox.lat_max, num_decimals)
    step=round((pmax-pmin)/num_parallels, num_decimals)
    try:
        parallels=np.arange(pmin,
                            pmax+step,
                            step)
    except ValueError:
        parallels = np.linspace(bbox.lat_min, bbox.lat_max, num=num_parallels)
    bmap.drawparallels(parallels,
                       labels=[True, False, False, False],
                       color='k',
                       fontsize=fontsize)
    pmin=round(bbox.lon_min, num_decimals)
    pmax=round(bbox.lon_max, num_decimals)
    step=round((pmax-pmin)/num_meridians, num_decimals)
    try:
        merdians=np.arange(pmin,pmax+step,step)
    except ValueError:
        merdians = np.linspace(bbox.lon_min, bbox.lon_max, num=num_meridians)
    bmap.drawmeridians(merdians,
                       labels=[False, False, False, True],
                       color='k',
                       fontsize=fontsize)

    
if __name__=='__main__':
    summarize_skill(sys.argv[1],bboxes=BBOXES)
