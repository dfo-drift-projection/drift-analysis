# Script to compile model-obs drifters into one plot
# Usage: python compare_model_obs.py output_dir
# Nancy Soontiens, May 2018

from collections import namedtuple
import datetime
import os
import sys

import matplotlib.colorbar as mcbar
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import pandas as pd
import xarray as xr


LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

SAVEDIR='/data/data1/OPP/drifters/drift_tool_runs/CMOS/salish_2016_3hr/new_plots'

# Borrowed from ioutils.py - no changes
def drifter_rename_transform(ds):
    if 'description' not in ds.attrs:
        return ds
    desc = ds.description
    if re.match('CONCEPTS Ocean Drifter', desc, re.IGNORECASE):
        renames = {
            'meta': 'trajectory_id',
            'data_date': 'time',
            'longitude': 'lon',
            'latitude': 'lat',
        }
    elif re.match('UBC drifter trajectory file', desc, re.IGNORECASE):
        renames = {
            'longitude': 'lon',
            'latitude': 'lat',
        }
        ds.attrs['buoyid'] = ds.attrs['unique_id']
    elif re.match('IOS drifter trajectory file', desc, re.IGNORECASE):
        renames = {
            'longitude': 'lon',
            'latitude': 'lat',
        }
        ds.attrs['buoyid'] = ds.attrs['unique_id']

    ds.rename(renames, inplace=True)
    return ds


def compare_model_obs(output_dir,launch_hours=['00', '12'],
                      mod_duration_in_seconds=10800.0,
                      vmin=0,vmax=10, axis_fontsize=16,title_fontsize=18,
                      tick_fontsize=14, figsize=(10,10)):
    # Find all unique drifter ids
    drifter_ids = {}
    for dirpath, dirnames, filenames in os.walk(output_dir, followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            # Find drifter id
            data_filename=os.path.join(dirpath, filename)
            with xr.open_dataset(data_filename) as ds:
                uniqueid = ds.obs_unique_id
                if uniqueid not in drifter_ids:
                    drifter_ids[uniqueid] = []
                drifter_ids[uniqueid].append(data_filename)
                ds.close()
    # Plotting
    for drifter_id, filenames in drifter_ids.items():
        model_plotted=False
        # Load all drifters to determine bbox and colorbar
        obs_drifters=[]
        mod_drifters=[]
        for filename in filenames:
            obs_drifters.append(load_drifter_track(filename,
                                                   name_prefix='obs_'))
            mod_drifters.append(load_drifter_track(filename,
                                                   name_prefix='mod_',
                                                   other_vars_keep='sep'))
        all_drifters = obs_drifters.copy()
        all_drifters.extend(mod_drifters)
        bbox = determine_latlon_bbox_for_drifters(all_drifters)
        # Start plotting.....
        fig, ax = plt.subplots(1,1, figsize=figsize)
        # Set up basemap
        basemap_settings = dict(llcrnrlon=bbox.lon_min,
                                llcrnrlat=bbox.lat_min,
                                urcrnrlon=bbox.lon_max,
                                urcrnrlat=bbox.lat_max,
                                ellps='WGS84', resolution='h',
                                projection='merc', lat_ts=20.)
        ax.basemap = Basemap(**basemap_settings)
        # Make basemap pretty
        ax.basemap.drawmapboundary(fill_color="#DDEEFF")
        ax.basemap.fillcontinents()
        ax.basemap.drawcoastlines()
        draw_meridians_parallels(ax.basemap, bbox, fontsize=tick_fontsize)
        # Set up variables for determining start and and of drifter
        start=datetime.datetime(3000,1,1)
        end=datetime.datetime(1,1,1)
        filenames.sort()
        for count, filename in enumerate(filenames):
            ds = xr.open_dataset(filename)
            ds_mod = date_filter(ds, mod_duration_in_seconds)
            # Maybe replace with ioutils.load_drifter_track eventually
            mod_lon = ds_mod.mod_lon.values
            mod_lat = ds_mod.mod_lat.values
            obs_lon = ds.obs_lon.values
            obs_lat = ds.obs_lat.values
            sep = ds_mod.sep
            ax.basemap.plot(obs_lon, obs_lat, '-',
                            c='k',ms=1, zorder=1,
                            latlon='true', label='Observations')
            if count ==0:
                ax.basemap.plot(obs_lon[0], obs_lat[0],
                               '*',c='g',ms=10, zorder=4,
                                latlon='true', mew=1, mec='k')
            # Only plot some of the model - match with launch_hours
            base = os.path.basename(filename)
            date = base.split('_')[2]
            cmap=plt.get_cmap('plasma_r',10)
            for launch_hour in launch_hours:
                if date.endswith(launch_hour):
                    mesh = ax.basemap.scatter(0*mod_lon,0* mod_lat,
                                              c=sep.values/1000,
                                              marker='.', s=30, latlon='true',
                                              vmin=vmin, vmax=vmax, zorder=2,
                                              cmap=cmap)
                    model_plotted=True
            # Determine time range
            times=pd.to_datetime(ds['time'].values)
            start = min(start, times[0])
            end = max(end, times[-1])
            ds.close()
            ds_mod.close()
        # Add title - date range
        ax.set_title('{} to {}'.format(start.strftime('%Y-%m-%d'),
                                       end.strftime('%Y-%m-%d')),
                     fontsize=title_fontsize)
        if model_plotted:
            cbar = plt.colorbar(mesh, ax=ax)
            cbar.set_label('{} [{}]'.format('Separation distance', 'km'),
                           fontsize=axis_fontsize)
            cbar.ax.tick_params(labelsize=tick_fontsize)
            #cbar.remove()
        #plt.show()
        figname = os.path.join(SAVEDIR,'obs',
                               '{}.png'.format(ds.obs_unique_id))
        plt.savefig(figname)
        plt.close()
        print('Figure saved {}'.format(figname))
        
        
def draw_meridians_parallels(bmap, bbox, num_meridians=3, num_parallels=3,
                             color='k', num_decimals=1, fontsize=14):
    # draw lats and lons
    pmin=round(bbox.lat_min, num_decimals)
    pmax=round(bbox.lat_max, num_decimals)
    step=round((pmax-pmin)/num_parallels, num_decimals)
    try:
        parallels=np.arange(pmin,
                            pmax+step,
                            step)
    except ValueError:
        parallels = np.linspace(bbox.lat_min, bbox.lat_max, num=num_parallels)
    bmap.drawparallels(parallels,
                       labels=[True, False, False, False],
                       color='k',
                       fontsize=fontsize)
    pmin=round(bbox.lon_min, num_decimals)
    pmax=round(bbox.lon_max, num_decimals)
    step=round((pmax-pmin)/num_meridians, num_decimals)
    try:
        merdians=np.arange(pmin,pmax+step,step)
    except ValueError:
        merdians = np.linspace(bbox.lon_min, bbox.lon_max, num=num_meridians)
    bmap.drawmeridians(merdians,
                       labels=[False, False, False, True],
                       color='k',
                       fontsize=fontsize)

        
# Based on ioutils.date_filter (but slightly modified)
def date_filter(ds,
                seconds_since_start):
    data_date = pd.to_datetime(ds['time'].values)
    start_idx = 0
    end_date = data_date[0] + datetime.timedelta(seconds=seconds_since_start)
    end_idx = np.where(data_date <= end_date)[0].max()
    ds = ds.isel(time=slice(start_idx, end_idx + 1))
    return ds


# Borrowed from plot_trajectories.py but modified ---> suggest a plotting.utils?
def determine_latlon_bbox_for_drifters(drifters, buffer_lon=0.05, buffer_lat=0.05):
    """Determine the latitude and longitude bounding box for drifter tracks.

    Parameters
    ----------
    drifters : list of xarray.Dataset
        List of drifter tracks. Each drifter must be ``an xarray.Dataset`` with
        variables ``lat`` and ``lon``.
    buffer_lon : float
        buffer in degrees around max/min longitude.
    buffer_lat : float
        buffer in degrees around max/min latitude.

    Returns
    -------
    bbox : LatLonBoundingBox
    """
    # These initial values are outside the valid ranges for latitudes and
    # longitudes, and are guaranteed to change on the first loop below.
    lat_min = 1000.
    lon_min = 1000.
    lat_max = -1000.
    lon_max = -1000.
    for drifter in drifters:
        drifter_lat_min = drifter['lat'].values.min()
        drifter_lat_max = drifter['lat'].values.max()
        drifter_lon_min = drifter['lon'].values.min()
        drifter_lon_max = drifter['lon'].values.max()
        lat_min = min(drifter_lat_min, lat_min)
        lat_max = max(drifter_lat_max, lat_max)
        lon_min = min(drifter_lon_min, lon_min)
        lon_max = max(drifter_lon_max, lon_max)
    lon_min = lon_min - buffer_lon
    lon_max = lon_max + buffer_lon
    lat_min = lat_min - buffer_lat
    lat_max = lat_max + buffer_lat
    return LatLonBoundingBox(lon_min=lon_min, lat_min=lat_min,
                             lon_max=lon_max, lat_max=lat_max)


## Borowwed from ioutils.py but modifieid slightly- maybe replace in ioutils.py
def load_drifter_track(filename, transform_func=drifter_rename_transform,
                       name_prefix=None, other_vars_keep=None, **kwargs):
    """Load drifter track.
    This function loads a dataset and then selectively filters out only those
    variables and attributes whose names begin with a specified prefix or are
    listed in other_var. All other variables and attributes are 
    dropped/removed. The remaining variables and attributes are renamed by
    removing the specified prefix.
          
    If ``name_prefix`` is ``None``, then this function returns the entire
    dataset and behaves identical to ``load_drifter_dataset``.
    """
    ds = load_drifter_dataset(
        filename, transform_func=transform_func, **kwargs)
    if (name_prefix is None) and (other_vars_keep is None):
        return ds

    # Remove variables that do not begin with name_prefix; rename the variables
    # that do begin with name_prefix.
    i = len(name_prefix)
    renames = {}
    vars_to_drop = []
    for v in ds.data_vars:
        if v.startswith(name_prefix):
            renames[v] = v[i:]
        else:
            vars_to_drop.append(v)
    # Keep any variables in other_vars_keep
    if other_vars_keep is not None:
        for other_var in other_vars_keep:
            if other_var in vars_to_drop:
                vars_to_drop.remove(other_var)
    ds = ds.drop(vars_to_drop)
    if renames:
        ds.rename(renames, inplace=True)

    # Remove attributes that do not begin with name_prefix; rename
    # attributes that do begin with name_prefix.
    renames = {}
    attrs_to_drop = []
    for a in ds.attrs:
        if a.startswith(name_prefix):
            renames[a] = a[i:]
        else:
            attrs_to_drop.append(a)
    for a in attrs_to_drop:
        del ds.attrs[a]
    for old, new in renames.items():
        ds.attrs[new] = ds.attrs[old]
        del ds.attrs[old]
    return ds


### Borrowed from ioutils.py - no changes
def load_drifter_dataset(filename, transform_func=drifter_rename_transform,
                                                  **kwargs):
    """Load drifter dataset.
                                                                      
    Parameters
    ----------
    filename : str
         Name of drifter data file.
    transform_func : callable
        Transform function to apply to dataset. This function can do things 
        such as rename variables so that the names match what are expected by
        later processing steps. 
    kwargs                                                                   
        Keyword arguments. These arguments are passed to the transform         
        function.                                                            
    """
    with xr.open_dataset(filename) as ds:
        if transform_func is not None:
            ds = transform_func(ds, **kwargs)
        ds.load()
        return ds


if __name__=='__main__':
    compare_model_obs(sys.argv[1])
