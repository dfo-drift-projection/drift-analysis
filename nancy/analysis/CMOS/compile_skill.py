# Script to compile model skill scores into houlry bins
# Usage: python compare_model_obs.py output_dir save_dir
# output_dir is directory with drift prediction outputs
# save_dir is directory where compiled results are to be saved.
# Nancy Soontiens, June 2018

import datetime
import os
import sys

import pandas as pd
import xarray as xr


def main(output_dir, save_dir):
    # Create pandas data frames for hourly bins
    hour_bins = {}
    skills = ['sep', 'liu', 'molcard',
             'obs_dist', 'obs_disp', 'mod_dist', 'mod_disp']          
    column_names = ['obs_lat_min','obs_lat_max', 'obs_lon_min', 'obs_lon_max',
                    'buoy_id', 'mod_run_name']
    column_names.extend(skills)
    
    for h in range(24):
        tmp_pd = pd.DataFrame(columns=column_names)
        hour_bins[str(h)] = tmp_pd
    for dirpath, dirnames, filenames in os.walk(output_dir,
                                                followlinks=True):
        for filename in filenames:
            if not filename.endswith('.nc'):
                continue
            # Load drifter
            data_filename=os.path.join(dirpath, filename)
            print(data_filename)
            with xr.open_dataset(data_filename) as ds:
                # Resample the data set
                res_mean = resample_hourly(ds, how='mean')
                res_min = resample_hourly(ds, how='min')
                res_max = resample_hourly(ds, how='max')
                for h in range(len(res_mean.time.values)):
                    if h >= 24:
                        print(
                            'Out of model duration {}'.format(data_filename))
                        continue
                    data_dict = {'obs_lat_min': res_min.obs_lat.values[h],
                                 'obs_lat_max': res_max.obs_lat.values[h],
                                 'obs_lon_min': res_min.obs_lon.values[h],
                                 'obs_lon_max': res_max.obs_lon.values[h],
                                 'buoy_id': res_mean.obs_buoyid,
                                 'mod_run_name': res_mean.mod_run_name}
                    for skill in skills:
                        data_dict[skill] = res_mean[skill].values[h]
                    new_pd = pd.DataFrame(data=data_dict,index=[0])
                    new_pd = pd.concat([hour_bins[str(h)], new_pd],
                                       ignore_index=True)
                    hour_bins[str(h)] = new_pd
    # Save each hour bin
    for h, hour_bin in hour_bins.items():
        hour_bin.index.name='index'
        fname=os.path.join(savedir, 'mean_skill-runhour{0:02d}'.format(int(h)))
        hour_bin.to_csv(fname)
        print('Saved {}'.format(fname))
                

def resample_hourly(ds, how='mean'):
    """Resample the dataset to hourly by averaging

    Parameters
    ----------
    ds : xrarray.Dataset
        the dataset to be resample. Requires dimension time and arritrbute
        mod_run_name in format oceanmodel_drfitmodel_YYYYMMDDHH_depth
    how : str
       indicates how the resampling should be performed e.g mean, min, max

    Returns
    -------
    resampled : xarray.Dataset
    """
    run_name = ds.mod_run_name
    start_time = datetime.datetime.strptime(run_name.split('_')[2], '%Y%m%d%H')
    start_hour = start_time.hour
    res = ds.resample(freq='1H', how='mean', dim='time', base=start_hour,
                      keep_attrs=True)
    return res


if __name__=='__main__':
    outdir, savedir = sys.argv[1:]
    main(outdir, savedir)
