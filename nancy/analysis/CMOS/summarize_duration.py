# Script to generate summary skill plots 
# Usage: python summarize_skill.py output_dir
# Nancy Soontiens, June 2018

from collections import namedtuple
import datetime
import os
import glob
import sys

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import numpy as np
import pandas as pd


SAVEDIR='/data/data1/OPP/drifters/drift_tool_runs/CMOS/salish_2016_3hr/new_plots'

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

BBOXES={'Salish Sea': None,
        'Juan de Fuca': LatLonBoundingBox(lon_min=-124.7,
                                          lon_max=-123.6,
                                          lat_min=48.1,
                                          lat_max=48.65),
        'Strait of Georgia': LatLonBoundingBox(lon_min=-125.3,
                                               lon_max=-122.7,
                                               lat_min=48.9,
                                               lat_max=50.1),
        'Gulf': LatLonBoundingBox(lon_min=-123.6,
                                  lon_max=-122.2,
                                  lat_min=48.1,
                                  lat_max=48.9)
}

colors={'Salish Sea': 'firebrick',
        'Juan de Fuca': 'darkgreen',
        'Strait of Georgia': 'dodgerblue',
        'Gulf': 'darkorange'}


def summarize_skill(skill_dir, bboxes=None,
                    tick_fontsize=14, axis_fontsize=16,
                    title_fontsize=18, only_salish=False,
                    plot_quantiles=True):
    """Summarize skills metrics in a skill_dir"""
    f = glob.glob(os.path.join(skill_dir, 'duration_skill'))[0]
    mean_duration_better = {key: [] for key in bboxes}
    mean_ratio={key: [] for key in bboxes}
    q10_ratio={key: [] for key in bboxes}
    q90_ratio={key: [] for key in bboxes}
    duration_better = pd.DataFrame()
    df = pd.read_csv(f)
    df['ratio'] = df['duration_better']/df['total_duration']
    df['hours']= df['duration_better']/3600.
    for key, bbox in bboxes.items():
        if bbox:
            dfnew = truncate_region(df, bbox)
        else:
            dfnew=df.copy()
        duration_better[key] = dfnew.hours
        mean_duration_better[key].append(dfnew.hours.mean())
        mean_ratio[key].append(dfnew.ratio.mean())
        q10_ratio[key].append(dfnew.ratio.quantile(0.1))
        q90_ratio[key].append(dfnew.ratio.quantile(0.9))
    df_better = pd.DataFrame(duration_better)
    df_mean=pd.DataFrame(mean_ratio)
    df_mean_better = pd.DataFrame(mean_duration_better)
    # Plot
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    df.ratio.hist(ax=ax,ec='black',color='firebrick')
    # Make plot pretty
    ax.set_xlabel('Ratio of time model performs better than persistence',
                  fontsize=axis_fontsize)
    ax.set_ylabel('Number of occurrences',
                  fontsize=axis_fontsize)
    plt.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    ax.grid()
    plt.show()
    fname='all_duration_ratio.png'
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()
    # Plot duration better - all
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    df.hours.hist(ax=ax, ec='black',color='firebrick',bins=24)
    # Make plot pretty
    ax.set_xlabel('Amount of time model performs better than persistence [hours]',
                  fontsize=axis_fontsize)
    ax.set_ylabel('Number of occurrences',
                   fontsize=axis_fontsize)
    plt.tick_params(axis='both', which='major', labelsize=tick_fontsize)
    ax.grid()
    plt.show()
    fname='all_duration_better.png'
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()
    #Plot Duration Better - regions
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    df_better.hist(ax=ax, ec='black',bins=24)
    # Make plot pretty
    ax.set_xlabel('Time model performs better than persistence [hours]',
                  fontsize=axis_fontsize)
    ax.set_ylabel('Number of occurrences',
                  fontsize=axis_fontsize)
    ax.grid()
    plt.show()
    fname='regions_duration_better.png'
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()
    #Plot Mean duration Better - regions
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    df_mean_better.plot.bar(ax=ax, color=['darkorange','darkgreen',
                                          'firebrick', 'dodgerblue'])
    # Make plot pretty
    ax.set_ylabel('Mean time better than persistence [hours]',
                  fontsize=axis_fontsize)
    ax.grid()
    plt.tick_params(axis='x', which='both', bottom=False,top=False,
                    labelbottom=False)
    plt.show()
    fname='regions_mean_better.png'
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()
    #Plot Mean ratio Better - regions
    fig, ax = plt.subplots(1,1,figsize=(10,5))
    df_mean.plot.bar(ax=ax, color=['darkorange','darkgreen',
                                   'firebrick', 'dodgerblue'])
    # Make plot pretty
    ax.set_ylabel('Mean ratio of time better than persistence',
                  fontsize=axis_fontsize)
    ax.grid()
    plt.tick_params(axis='x', which='both', bottom=False,top=False,
                    labelbottom=False)
    plt.show()
    fname='regions_mean_ratio.png'
    fig.savefig(os.path.join(SAVEDIR,fname),bbox_inches='tight')
    plt.close()       
    

def truncate_region(df, bbox):
    """Truncate data df based on region defined in bbox.
       That is, exclude data outside of bbox"""
    subset = df[(df.obs_lon_min >= bbox.lon_min) &
                (df.obs_lon_max <= bbox.lon_max) &
                (df.obs_lat_min >= bbox.lat_min) &
                (df.obs_lat_max <= bbox.lat_max)]
    return subset


def draw_meridians_parallels(bmap, bbox, num_meridians=3, num_parallels=3,
                             color='k', num_decimals=1, fontsize=14):
    # draw lats and lons                                                        
    pmin=round(bbox.lat_min, num_decimals)
    pmax=round(bbox.lat_max, num_decimals)
    step=round((pmax-pmin)/num_parallels, num_decimals)
    try:
        parallels=np.arange(pmin,
                            pmax+step,
                            step)
    except ValueError:
        parallels = np.linspace(bbox.lat_min, bbox.lat_max, num=num_parallels)
    bmap.drawparallels(parallels,
                       labels=[True, False, False, False],
                       color='k',
                       fontsize=fontsize)
    pmin=round(bbox.lon_min, num_decimals)
    pmax=round(bbox.lon_max, num_decimals)
    step=round((pmax-pmin)/num_meridians, num_decimals)
    try:
        merdians=np.arange(pmin,pmax+step,step)
    except ValueError:
        merdians = np.linspace(bbox.lon_min, bbox.lon_max, num=num_meridians)
    bmap.drawmeridians(merdians,
                       labels=[False, False, False, True],
                       color='k',
                       fontsize=fontsize)

    
if __name__=='__main__':
    summarize_skill(sys.argv[1],bboxes=BBOXES)
