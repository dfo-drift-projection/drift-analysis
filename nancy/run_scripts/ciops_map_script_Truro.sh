#!/bin/bash

# Script to run RIOPS drift map daily
# Usage: ./riops_script.sh [YYYYMMDD]
# Runs a 48 hour drift_map simulation for the given date
# If date is not given, uses today's date
# Region is defined by bbox (below)
# Nancy Soontiens

source /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_source.sh
MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop
# Set environment                                                                      
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
prod_dir=/home/nso001/data/work2/OPP/riops_daily/DriftMap/output/

# Set date
rundate="$1"
if [[ -z "$rundate" ]]; then
  rundate=$(date +'%Y%m%d')
fi

# Run arameters
ocean_data_dir=/home/nso001/data/work2/models/ciops-e/ciops-e_Truro/2D
ocean_mesh_file=$HOME/data/work2/models/ciops-e/CIOPSE_InitialTest_Surface_mesh.nc
rotation_data_file='/home/nso001/data/work2/rotation_pickles/ciopse/ciopse.rotation.pickle'
drifter_depth=L1c
num_particles_x=15
num_particles_y=10
first_start_date="2020-05-05"
last_start_date="2020-05-05"
start_date_frequency='hourly'
start_date_interval=12
initial_bbox='-64 45 -63 45.4'


experiment_dir=/home/nso001/data/work2/OPP/riops_daily/DriftMap/Truro/

# If Above checks pass then run..

daily_drift_map \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --initial-bbox "$initial_bbox" \
    --drifter-depth $drifter_depth \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --rotation-data-file $rotation_data_file \
    --ocean-mesh-file $ocean_mesh_file \
    --xwatervel 'uos' \
    --ywatervel 'vos' \
    --model-time-ocean 'time_counter' \
    --lon-var 'nav_lon' \
    --lat-var 'nav_lat' \
    --dep-var 'gdepw_1d'

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk' \
  --interval 24 


