#!/bin/bash

# Script to submit the riops evaluation job submission script.
# To be called by hcron
# Nancy Soontiens


# Don't submit if drifter data isn't available
download_date=$(date --date 'yesterday' +'%Y%m%d')
# The path below will need to change if we start drifter runs in SeDOO
output_file="/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_dataset/text_output_files/cron-status-combine_${download_date}.txt"
if [ -f "${output_file}" ]; then
    combinevar=$(<${output_file})
    if [ "${combinevar}" != "0" ]; then
        # drifter download did not complete - exit
        exit
    fi
else
    # File doesn't exist - exit
    exit
fi
# Don't submit job if output already exists
source /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_source.sh
outdir=${results_dir}/DriftEval/${rundate_eval}
if [ -d $outdir ]; then # directory exists
    if [ ! -z "$(ls -A ${outdir})" ]; then # there are files
	exit
    fi
else
    /fs/ssm/main/base/20190814/all/bin/jobsub -c gpsc4 /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_eval_submit.job
fi
