#!/bin/bash

# Script to define variables for RIOPS map and eval
# Nancy Soontiens

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/drift-tool_production

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

drifter_code_dir=/home/nso001/code/drifters/drift-tool # path to drift tool
results_dir=/home/nso001/data/work2/OPP/riops_daily/

# Set dates
date=$(date +'%Y%m%d')
rundate_eval=$(date -d "${date} -3days" +'%Y%m%d')
rundate_map=${date}

# Run arameters
ocean_data_dir_eval=/home/nso001/data/work2/models/riops/continuous
ocean_data_dir_map=/home/nso001/data/work2/models/riops/forecast/
drifter_data_dir_eval=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_dataset/master_files
config_file=/home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops.yaml
