#!/bin/bash

# Script to define variables for CIOPSE map
# Nancy Soontiens

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/ciopse-production

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

results_dir=/home/nso001/data/work2/OPP/ciopse_daily/

# Set dates
date=$(date +'%Y%m%d')
rundate_eval=$(date -d "${date} -3days" +'%Y%m%d')
rundate_map=${date}

# Run arameters
ocean_data_dir_map=/home/nso001/data/work2/models/ciops-e_operational_forecasts
#ocean_data_dir_map=/home/nso001/data/work2/models/riops/forecast/
#drifter_data_dir_eval=/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/gpfs-fs4-dfo-dpnm-dfo_odis/jeh326/cmems_dataset/master_files
atmos_data_dir_map=/home/nso001/data/work2/models/hrdps-forecast
config_file=/home/nso001/code/drifters/drift-analysis/nancy/run_scripts/ciopse_daily/ciopse.yaml
