commit c8bd345f1653c4670b16c32b5ee83c87c673deec (HEAD -> develop, origin/develop)
Merge: a7d5014 5a15041
Author: Nancy Soontiens <nancy.soontiens@dfo-mpo.gc.ca>
Date:   Tue Sep 1 23:04:37 2020 +0000

    Merge branch 'develop' of gitlab.com:dfo-drift-projection/drift-workflow-tool into develop

commit a7d5014f271b14e0edafc88bf2d1902ac970045b
Author: Nancy Soontiens <nancy.soontiens@dfo-mpo.gc.ca>
Date:   Tue Sep 1 23:04:26 2020 +0000

    corrected error where last hourly plot is not created
