#!/bin/bash

# Script to submit the riops evaluation job submission script.
# To be called by hcron
# Nancy Soontiens


source /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_source.sh

rundate=$(date +'%Y%m%d')
ocean_data_dir=${ocean_data_dir_map}/${rundate}00
LOGFILE=/home/nso001/data/work2/OPP/riops_daily/DriftMap/logs/${rundate}

# Check if source files exist before running
if [ -d "${ocean_data_dir}" ]; then
    if [ -z "$(ls -A ${ocean_data_dir})" ]; then
        exit
    fi
else
    exit
fi

# Check that all source files are there before running
files=${ocean_data_dir}/*.nc
if [ "$(echo $files | grep -v '*' | wc -w)" -ne "49" ]; then
    echo "All model files not available at ${rundate}." >> ${LOGFILE}
    echo "Try again later." >> ${LOGFILE}
    exit
fi

# Don't submit job if output already exists
outdir=${results_dir}/DriftMap/${rundate}
if [ -d $outdir ]; then # directory exists
    if [ ! -z "$(ls -A ${outdir})" ]; then # there are files
	exit
    fi
else
    /fs/ssm/main/base/20190814/all/bin/jobsub -c gpsc4 /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_map_submit.job
fi
