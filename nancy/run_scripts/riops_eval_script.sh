#!/bin/bash

# Script to run RIOPS drift evaluate daily
# Usage: ./riops_script.sh [YYYYMMDD]
# Runs a 48 hour drift_predict simulation for three days before the given date
# (delayed mode). Modelled drifters are released every 3 hours in 24 hour
# period.
# If date isn't given, then three days ago is used. 
# Eg if YYYYMMDD = 20190905 then simultion start dates are:
# 20190902 00:00:00
# 20190902 03:00:00
# 20190902 06:00:00
# 20190902 12:00:00
# ...
# 20190903 00:00:00
# Each simulation is 48 hours in duration.
# Nancy Soontiens

source /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_source.sh

# Set environment                                                                      
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


# Set date
rundate="$1"
if [[ -z "$rundate" ]]; then
  rundate=$(date +'%Y%m%d')
fi
rundate=$(date -d "$rundate -3days" +'%Y%m%d')

# Run parameters
ocean_data_dir=$ocean_data_dir_eval
drifter_data_dir=$drifter_data_dir_eval
drifter_depth=15c
first_start_date="$(date -d $rundate +'%Y-%m-%d')"
last_start_date="${first_start_date}"
start_date_frequency='hourly'
start_date_interval=3
drifter_id_attr='buoyid'

experiment_dir=${results_dir}/DriftEval/${rundate}

echo "set arguments. running drift predict..."

drift_predict \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --drifter-depth $drifter_depth \
    --drifter-id-attr $drifter_id_attr \
    --drifter-data-dir $drifter_data_dir

drift_evaluate \
    --data_dir $experiment_dir/output

combine_track_segments \
    --data_dir $experiment_dir/output

plot_aggregated_output \
  --data_dir=$experiment_dir/output/output_per_drifter \
  --etopo_file='/home/sdfo000/sitestore4/opp_drift_fa3/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_ratio_and_track


# Check if run was successful
# Are there files in output? If not, send an email
if [[ ! -e $experiment_dir/output ]]; then
    echo "${rundate} failed" | mail -s "riops eval" nancy.soontiens@dfo-mpo.gc.ca
fi
