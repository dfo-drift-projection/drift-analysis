#!/bin/bash

# Script to run RIOPS drift map daily
# Usage: ./riops_script.sh [YYYYMMDD]
# Runs a 48 hour drift_map simulation for the given date
# If date is not given, uses today's date
# Region is defined by bbox (below)
# Nancy Soontiens

source /home/nso001/code/drifters/drift-analysis/nancy/run_scripts/riops_source.sh

# Set environment                                                                      
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
prod_dir=/home/nso001/data/work2/OPP/riops_daily/DriftMap/output/

# Set date
rundate="$1"
if [[ -z "$rundate" ]]; then
  rundate=$(date +'%Y%m%d')
fi

# Run arameters
ocean_data_dir=${ocean_data_dir_map}/${rundate}00
drifter_depth=0c
num_particles_x=50
num_particles_y=50
first_start_date="$(date -d $rundate +'%Y-%m-%d')"
last_start_date="${first_start_date}"
start_date_frequency='daily'
start_date_interval=1
initial_bbox='-70 43 -40 52'

experiment_dir=/home/nso001/data/work2/OPP/riops_daily/DriftMap/${rundate}

# If Above checks pass then run..

daily_drift_map \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --initial-bbox "$initial_bbox" \
    --drifter-depth $drifter_depth \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/home/sdfo000/sitestore4/opp_drift_fa3/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk' \
  --interval 24 

# If run is successful, copy files to hank
# Otherwise send an email
year=$(date -d ${rundate} +'%Y')
files=$experiment_dir/output/*.nc
count=0
for f in $files; do
    echo "Transfering $f to hank" 
    ssh gpsc-in "ssh nsoontiens@hank.research.cs.dal.ca '[ ! -d drift_tool_output/opp_drift_results/RIOPS/DriftMap/${year} ] && mkdir drift_tool_output/opp_drift_results/RIOPS/DriftMap/${year}/'"
    ssh gpsc-in "rsync -rltp ${f} nsoontiens@hank.research.cs.dal.ca:drift_tool_output/opp_drift_results/RIOPS/DriftMap/${year}/"
    echo "Linking $f to output directory"
    if [ ! -d ${prod_dir}/${year} ]; then
        mkdir ${prod_dir}/${year}
    fi
    ln -s $f ${prod_dir}/${year}/.
    count=$((count+1))
done
# Copy images
ocean_model_name='riopsps'
images=${experiment_dir}/plots/${rundate}00/${ocean_model_name,,}*.png
for image in $images; do
    ssh gpsc-in "rsync -rltp ${image} nsoontiens@hank.research.cs.dal.ca:/tank/data/drift_projection_plots/plots/RIOPS/."
    count=$((count+1))
done
if [ "$count" == "0" ]; then
   echo "${rundate} failed" | mail -s "riops map" nancy.soontiens@dfo-mpo.gc.ca
fi

# Delete uneeded model run files
rm -rf ${experiment_dir}/runs/*/data/land*.nc
