MINICONDA_PATH=/home/sdfo000/sitestore4/opp_drift_fa3/software/drift-tool-miniconda-v4.1.1
export PATH=$MINICONDA_PATH/bin:$PATH

############################################
# General directories for code and outputs #
############################################
code_dir=/home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift
results_dir=/home/nso001/data/work2/OPP/RoutineDrift/

######################################
# Constants related to drifter data  #
######################################
# Path to where raw drifter data and metadate will be downloaded
drifter_download_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/drifters/downloads'
# Path to where SVP data in Northwest Atlantic will be linked
drifter_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/drifters/SVP_NWA'
# Path to logs for downloads
drifter_log_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/drifters/logs/'


#########################################
# Constants related to riops model data #
#########################################
# Path to SeDOO Mirror for riops analysis
riops_src_dir='/home/sdfo501/links/eccc_forecasts/riops_20220621/ru/netcdf/anal/oce'
# Path to where riops data will be copied
riops_model_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/models/riops/anal'
# Path to logs
riops_log_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/models/riops/logs/'


#########################################
# Constants relatd to ciopse model data #
#########################################
# Path to SeDOO Mirror for ciopse pseudo-analysis
ciopse_src_dir='/home/sdfo501/links/eccc_forecasts/ciops_east_20220621/pseudo_anal/netcdf/anal'
# Path to where ciopse data will be copied
ciopse_model_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/models/ciopse/anal'
# Path to logs
ciopse_log_dir='/home/nso001/data/work2/OPP/RoutineDrift/data/models/ciopse/logs/'


#####################################
# Constants related to a simulation #
#####################################
# Start simulations three days ago
num_days_ago=3
riops_eval_log_dir='/home/nso001/data/work2/OPP/RoutineDrift/DriftEval/riops/logs/'
riops_eval_dir='/home/nso001/data/work2/OPP/RoutineDrift/DriftEval/riops/'
ciopse_eval_log_dir='/home/nso001/data/work2/OPP/RoutineDrift/DriftEval/ciopse/logs/'
ciopse_eval_dir='/home/nso001/data/work2/OPP/RoutineDrift/DriftEval/ciopse/'

