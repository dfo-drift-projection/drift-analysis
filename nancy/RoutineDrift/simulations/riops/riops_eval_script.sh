#!/bin/bash

# Script to run RIOPS drift evaluate daily
# Usage: ./riops_script.sh [YYYYMMDD]
# Runs a 24 hour drift_predict simulation for  the given date
# (delayed mode).
# If date isn't given, then three days ago is used. 
# Eg if YYYYMMDD = 20190905 then simultion start dates ais
# 20190902 00:00:00
# Nancy Soontiens

source /home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift/environment.sh

config_file=${code_dir}/simulations/riops/riops.yaml

# Set date
rundate="$1"
if [[ -z "$rundate" ]]; then
  rundate=$(date +'%Y%m%d')
fi
drifter_data_date=$rundate
rundate=$(date -d "$rundate -${num_days_ago}days" +'%Y%m%d')

# Run parameters
ocean_data_dir=$riops_model_dir
drifter_data_dir=$drifter_dir/${drifter_data_date}
drifter_depth=L1
first_start_date="$(date -d $rundate +'%Y-%m-%d')"
last_start_date="${first_start_date}"
start_date_frequency='daily'
start_date_interval=1

experiment_dir=${riops_eval_dir}/${rundate}

echo "set arguments. running drift predict..."

drift_predict \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --drifter-depth $drifter_depth \
    --drifter-data-dir $drifter_data_dir

drift_evaluate \
    --data_dir $experiment_dir/output

combine_track_segments \
  --data_dir=$experiment_dir/output


# Check if run was successful
# Are there files in output? If not, send an email
if [[ ! -e $experiment_dir/output ]]; then
    echo "${rundate} failed" | mail -s "riops eval" nancy.soontiens@dfo-mpo.gc.ca
fi
