#!/bin/bash

# Script to submit the ciopse evaluation job submission script.
# To be called by hcron
# Nancy Soontiens

source /home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift/environment.sh

# Don't submit if drifter data isn't available
date=$(date +'%Y%m%d')
LOG=${ciopse_eval_log_dir}/${date}
if [ ! -d ${drifter_dir}/${date} ]; then # doesn't exist
   echo "Drifter data ${drifter_dir}/${date} does not yet exist." >> $LOG 2>&1
   exit
fi

# Don't submit if model data isn't available
rundate_eval=$(date -d "$date -${num_days_ago}days" +'%Y%m%d')
model_file=${ciopse_model_dir}/${rundate_eval}*
if [ ! -f ${model_files[0]} ]; then
    echo "Model files in $ciopse_model_dir do not yet exist for $rundate_eval" >> $LOG 2>&1
    exit
fi

# Don't submit job if output already exists
outdir=${ciopse_eval_dir}/${rundate_eval}
if [ -d $outdir ]; then # directory exists
    if [ ! -z "$(ls -A ${outdir})" ]; then # there are files
	exit
    fi
else
    /fs/ssm/main/base/20190814/all/bin/jobsub -c gpsc4 ${code_dir}/simulations/ciopse/ciopse_eval_submit.job
fi
