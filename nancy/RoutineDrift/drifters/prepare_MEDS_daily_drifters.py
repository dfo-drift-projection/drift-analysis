# Script to download and prepare daily drifter data from MEDS
# ftp -  ftp://ftp.isdm.gc.ca/pub/openData/Drifting_Buoys/ 
# 1. Downloads all netcdf data files
# 2. Downloads the csv metadata for the Northwest Atlantic (NWA)
# 3. Idenitifies which drifters in the csv have useful data for
# a drift caclulation by excluding drfters without an attached drogue
# at 15m.

# Usage: python prepare_MEDS_daily_drifters download_dir save_dir YYYYMMDD 
# Nancy Soontiens May 2022

import datetime
import glob
import os
import subprocess
import sys

import numpy as np
import pandas as pd
import xarray as xr

# Filtering parameters
STATUS = 'Drogue is attached' # DROGUE_STATUS
DEPTH = 15 # DROGUE_DEPTH


def download_drifters(ftp, download_dir, date):
    # Remote files
    csv = os.path.join(ftp, 'CSV_Version',
                       f'DB_NWA_last5D_{date}.csv')
    netcdf = os.path.join(ftp, 'NETCDF_Version/*.nc')
    # Downloads
    csvout = os.path.join(download_dir, 'csv')
    print(f'Downloading from {csv} to {csvout}')
    os.chdir(csvout)
    subprocess.call(['wget', csv])
    csvfile = os.path.join(csvout, f'DB_NWA_last5D_{date}.csv')
    if not os.path.exists(csvfile):
        print(f'Data for {date} not yet available on ftp')
        sys.exit()
    else:
        netcdfout = os.path.join(download_dir, 'netcdf', date)
        if not os.path.exists(netcdfout):
            os.mkdir(netcdfout)
        print(f'Downloading from {netcdf} to {netcdfout}')
        os.chdir(netcdfout)
        subprocess.call(['wget', netcdf])
    return netcdfout, csvfile
    

def link_drifters(src_dir, csv_file, out_dir):
    """Link the drfiter files in src_dir to out_dir
    if the buoyid is listed in csv_file"""
    all_drifters = glob.glob(os.path.join(src_dir, '*.nc'))
    all_drifters.sort()
    drifters_link = []
    d = pd.read_csv(csv_file)
    # Only drifters with specified DROGUE_STATUS
    d = d[d['DROGUE_STATUS']==STATUS]
    # Only drifters with specific DROGUE_DEPTH
    d = d[d['DROGUE_DEPTH']==DEPTH]
    
    for f in all_drifters:
        b = os.path.basename(f)
        buoyid = b.split('db')[-1].split('.nc')[0]
        wmo = int(b.split('db')[-1].split('D')[0])
        if wmo in list(d['WMO_IDENTIFIER'].values):
            print(f'Copying {f} to {out_dir}') 
            newfile = os.path.join(out_dir, f'{wmo}.nc')
            ds = xr.open_dataset(f)
            ds = rename_transform(ds)
            ds.attrs['buoyid'] = str(wmo)
            # Only unique times
            _, idx = np.unique(ds.TIME, return_index=True)
            ds = ds.isel(TIME=idx)
            ds = ds.resample({'TIME': '1H'}).interpolate()
            ds.attrs['processing_comment'] = 'Positions interpolated to hourly'
            ds.to_netcdf(newfile)
            ds.close()
            

def rename_transform(d):
    renames = {'longitude': 'LONGITUDE',
               'latitude': 'LATITUDE',
               'time': 'TIME'}
    d = d.rename(renames)
    return d

                      
def clean_files(date, download_dir, save_dir, ndays=10):
    # Clean up folders 10 days old
    dt = datetime.datetime.strptime(date,'%Y%m%d')
    clean_date = dt - datetime.timedelta(days=ndays)
    clean_date = clean_date.strftime('%Y%m%d')
    print(f'Cleaning downloads from {clean_date}')
    # CSV file
    csv_clean = glob.glob(os.path.join(download_dir,
                                       'csv',
                                       f'*{clean_date}*'))
    for f in csv_clean:
        os.remove(f)
    #Netcdf files
    netcdf_dir = os.path.join(download_dir,
                              'netcdf',
                              f'{clean_date}')
    netcdf_files = glob.glob(os.path.join(netcdf_dir, '*.nc'))
    for f in netcdf_files:
        os.remove(f)
    if os.path.exists(netcdf_dir):
        os.rmdir(netcdf_dir)
    # Linked files
    linked_dir = os.path.join(save_dir, f'{clean_date}')
    linked_files = glob.glob(os.path.join(linked_dir, '*.nc'))
    for f in linked_files:
        os.remove(f)
    if os.path.exists(linked_dir):
        os.rmdir(linked_dir)


def prepare_MEDS_daily_drifters(ftp, download_dir, save_dir, date, clean=True):
    all_drifters_dir, csv_file = download_drifters(ftp, download_dir, date)
    save_dir_daily = os.path.join(save_dir, date)
    if not os.path.exists(save_dir_daily):
        os.mkdir(save_dir_daily)
    link_drifters(all_drifters_dir, csv_file, save_dir_daily)
    if clean:
        clean_files(date, download_dir, save_dir)


if __name__=='__main__':
    ftp = 'ftp://ftp.isdm.gc.ca/pub/openData/Drifting_Buoys'
    download_dir, save_dir, date = sys.argv[1:]
    prepare_MEDS_daily_drifters(ftp, download_dir, save_dir, date)
                                     
