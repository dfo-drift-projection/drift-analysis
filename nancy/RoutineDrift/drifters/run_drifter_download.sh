#!/bin/bash
# Script to run the drifter downloads
# Downloads and prepares today's data from MEDS ftp site
# Usage: run_drifter_download.sh [YYYYMMDD]
#     if no date is given, runs with today's date

source /home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift/environment.sh

date="$1"
if [[ -z "$date" ]]; then
  date=$(date +'%Y%m%d')
fi
LOG=${drifter_log_dir}/${date}

# Don't run if data already exists
outdir=${drifter_dir}/${date}
if [ -d $outdir ]; then # directory exists
    if [ ! -z "$(ls -A ${outdir})" ]; then # there are files
        exit
    fi
else
    python ${code_dir}/drifters/prepare_MEDS_daily_drifters.py \
        ${drifter_download_dir} \
        ${drifter_dir} \
	${date} \
        > $LOG 2>&1
fi
