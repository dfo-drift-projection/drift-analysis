#!/bin/bash
# Script to prepare riops analysis files for use in the drift tool
# Makes time the record dimension, converts files to netcdf4_classic
# Deletes data 10 days old
# Emails if previous day failed
#
# Usage: prepare_riops.sh [YYYYMMDD]
#        If no argument is given it will run with yesterday's date.
# Author: Nancyh Soontiens
# Date: 2022-05-17

# Source directories and paths
source /home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift/environment.sh

###### Set up ############## #####################
date="$1"
if [[ -z "$date" ]]; then
    date=$(date +'%Y%m%d')
    date=$(date -d "$date -1days" +'%Y%m%d')
fi

# Logfile
LOG=${riops_log_dir}/${date}

# Desired velocity files
u_file=${date}00_000_024_1h_grid_U_2D.nc
v_file=${date}00_000_024_1h_grid_V_2D.nc

##################################################

########## Check the destination #################
# If files exist at destination, don't copy
if [[ -f ${riops_model_dir}/${u_file} ]] && [[ -f ${riops_model_dir}/${v_file} ]]; then
    exit
fi
##################################################

########### Check the source files ###############
# Check if source files exist, otherwise exit
if [[ ! -f ${riops_src_dir}/${u_file} ]] && [[ ! -f ${riops_src_dir}/${v_file} ]]; then
    echo "Files for ${date} in ${riops_src_dir} not yet avaialable" >> ${LOG}
    exit
fi
files=${riops_src_dir}/${date}*_grid_[U,V]_2D.nc
for file in ${files}; do
    base=$(basename "${file}")
    newfile=${riops_model_dir}/${base}
    echo "Modifyng ${base}" >> ${LOG}
    ncks --mk_rec_dmn time_counter ${file} ${newfile}
done

############## Delete data and logs 10 days old ####################
dremove=$(date -d "$date -10days" +'%Y%m%d')
echo "Removing data from ${dremove}" >> ${LOG}
rm -rf ${riops_model_dir}/${dremove}*
# Delete logs
echo "Removing old log ${riops_log_dir}/${dremove}" >> ${LOG}
rm -rf ${riops_log_dir}/${dremove}

######### Email if previous day failed ############################
# directory doesn't exist or is empty
yesterday=$(date -d "$date -1days" +'%Y%m%d')
u_file=${yesterday}00_000_024_1h_grid_U_2D.nc
v_file=${yesterday}00_000_024_1h_grid_V_2D.nc
if [[ ! -f ${riops_model_dir}/${u_file} ]] && [[ ! -f ${riops_model_dir}/${v_file} ]]; then
    echo "${yesterday} failed" | mail -s "prepare riops" nancy.soontiens@dfo-mpo.gc.ca
fi
####################################################################
