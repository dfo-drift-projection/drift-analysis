#!/bin/bash
# Script to prepare ciopse analysis files for use in the drift tool
# Untars 2D U/V files
# Deletes data 10 days old
# Emails if previous day failed
#
# Usage: prepare_ciopse.sh [YYYYMMDD]
#        If no argument is given it will run with yesterday's date.
# Author: Nancyh Soontiens
# Date: 2022-05-17

# Source directories and paths
source /home/nso001/code/drifters/drift-analysis/nancy/RoutineDrift/environment.sh

###### Set up ############## #####################
date="$1"
if [[ -z "$date" ]]; then
    date=$(date +'%Y%m%d')
    date=$(date -d "$date -1days" +'%Y%m%d')
fi

# Logfile
LOG=${ciopse_log_dir}/${date}

# Desired tar file and 2D file names
tar_file=${date}00_000.tar
uf=NEMO_RPN_1h_grid_U_2D.nc
vf=NEMO_RPN_1h_grid_V_2D.nc

ufnew=${date}_${uf}
vfnew=${date}_${vf}

##################################################

########## Check the destination #################
# If files exist at destination, don't copy
if [[ -f ${ciopse_model_dir}/${ufnew} ]] && [[ -f ${ciopse_model_dir}/${vfnew} ]]; then
    exit
fi
##################################################

########### Check the source files ###############
# Check if source files exist, otherwise exit
if [[ ! -f ${ciopse_src_dir}/${tar_file} ]]; then
    echo "Files for ${date} in ${ciopse_src_dir} not yet avaialable" >> ${LOG}
    exit
fi
cp ${ciopse_src_dir}/${tar_file} ${ciopse_model_dir}/.
cd ${ciopse_model_dir}
echo "Extracting U" >> ${LOG}
tar -xzf ${tar_file} ${uf}
mv ${uf} ${ufnew}
echo "Extracting V" >> ${LOG}
tar -xzf ${tar_file} ${vf}
mv ${vf} ${vfnew}
rm -f ${tar_file}


############## Delete data and logs 10 days old ####################
dremove=$(date -d "$date -10days" +'%Y%m%d')
echo "Removing data from ${dremove}" >> ${LOG}
rm -f ${ciopse_model_dir}/${dremove}*
# Delete logs
echo "Removing old log ${ciopse_log_dir}/${dremove}" >> ${LOG}
rm -f ${ciopse_log_dir}/${dremove}

######### Email if previous day failed ############################
# directory doesn't exist or is empty
yesterday=$(date -d "$date -1days" +'%Y%m%d')
u_file=${yesterday}_${uf}
v_file=${yesterday}_${vf}
if [[ ! -f ${ciopse_model_dir}/${u_file} ]] && [[ ! -f ${ciopse_model_dir}/${v_file} ]]; then
    echo "${yesterday} failed" | mail -s "prepare ciopse" nancy.soontiens@dfo-mpo.gc.ca
fi
####################################################################
