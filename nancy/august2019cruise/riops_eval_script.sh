#!/bin/bash

# Script to run RIOPS drift evaluate daily
# Usage: ./riops_eval_script.sh
# Runs multiple 48 hour drift_predict sims beginning Sept 6, 2019 to Sept 12, 2019
# Modelled drifters are released every 3 hours.
# Evaluates model against SVPs (15 m drogue) released on August 2019 cruise.
# Nancy Soontiens

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
MINICONDA_PATH=/space/hall0/sitestore/dfo/odis/nso001/miniconda/envs/opendrift_fork

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

echo "set paths"

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool


# Run arameters
ocean_data_dir=$HOME/data/work2/models/riops/august2019cruise
drifter_data_dir=$HOME/data/work2/OPP/drifter_data/svp_082019/netcdf
ocean_mesh_file='None'
run_option='linear'
ocean_model_name=riopsPS
drifter_depth=15c
first_start_date="2019-09-06"
last_start_date="2019-09-12"
drift_duration=48
start_date_frequency='hourly'
start_date_interval=3
xwatervel='vozocrtx'
ywatervel='vomecrty'
drift_model_name='OpenDrift'
rotation_data_file='None'
utm_zone='9U'
opendrift_map_reader='riops_map_reader.pickle'
alpha_wind=0.
opendrift_dt=180.
lon_var='longitude'
lat_var='latitude'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='depth'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time='time'
drifter_id_attr='buoyid'

experiment_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/august2019cruise/DriftEval/svps

echo "set arguments. running drift predict..."

drift_predict \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --ocean-mesh-file $ocean_mesh_file \
    --ocean-model-name $ocean_model_name\
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drift-duration $drift_duration \
    --run-option $run_option \
    --drifter-depth $drifter_depth \
    --drift-model-name $drift_model_name \
    --rotation-data-file $rotation_data_file \
    --utm-zone $utm_zone \
    --opendrift-map-reader $opendrift_map_reader \
    --alpha-wind $alpha_wind \
    --opendrift-dt $opendrift_dt \
    --lon-var $lon_var \
    --lat-var $lat_var \
    --ulon-var $ulon_var \
    --ulat-var $ulat_var \
    --vlon-var $vlon_var \
    --vlat-var $vlat_var \
    --dep-var $wdep_var \
    --tmask-var $tmask_var \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --xwatervel $xwatervel \
    --ywatervel $ywatervel \
    --zwatervel $zwatervel \
    --temperature $temperature \
    --salinity $salinity \
    --density $density \
    --model-time $model_time \
    --drifter-id-attr $drifter_id_attr\
    --drifter-data-dir $drifter_data_dir

drift_evaluate \
    --data_dir $experiment_dir/output

combine_track_segments \
    --data_dir $experiment_dir/output

plot_aggregated_output \
  --data_dir=$experiment_dir/output/output_per_drifter \
  --etopo_file='/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files/ETOPO1_Bed_g_gmt4.grd' \
  --plot_track_indiv \
  --no-plot_track_persistance \
  --plot_drifter_skillscores \
  --no-plot_average_of_mean_skills \
  --plot_skills_subplots_withindv \
  --no-plot_ratio_combined \
  --no-plot_ratio_and_track

