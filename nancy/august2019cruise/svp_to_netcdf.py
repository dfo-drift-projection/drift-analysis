# Script to convert svp data from csv to netcdf format
# Nancy Soontiens

import datetime
import glob
import os

import numpy as np
import pandas as pd
import xarray as xr

# Source sande save directories
OBS_SRC_DIR='/home/nso001/data/work2/OPP/drifter_data/svp_082019'
SAVE_DIR='/home/nso001/data/work2/OPP/drifter_data/svp_082019/netcdf'

def main():
    """ 
    Main function to save obs from individual drifter into a netcdf file each
    """
    obs_files = glob.glob(os.path.join(OBS_SRC_DIR, '*.csv'))
    # Loop through all obs and model
    for obsf in obs_files:
        obs = read_svp_csv(obsf)
        obs['time'] = pd.to_datetime(obs['time'])
        obs['lon'] = obs['lon'].astype(float)
        obs = obs.sort_values(by=['time'])
        obs = obs.set_index(['time'])
        grouped = obs.groupby('DrifterID')
        for drifter in grouped.groups:
            g = grouped.get_group(drifter)
            obs_array = g.to_xarray()
            print(obs_array)
            outfile = '{}.nc'.format(drifter)
            val, idx = np.unique(obs_array.time, return_index=True)
            obs_array = obs_array.isel(time=idx)
            ds = xr.Dataset(
                coords={'time': obs_array.time.values},
                data_vars={'latitude': ('time', obs_array.lat.values),
                           'longitude': ('time', obs_array.lon.values)
                           }
                )
            ds.latitude.attrs['long_name'] = 'Latitude of observed trajectory'
            ds.latitude.attrs['units'] = 'degrees_north'
           # ds.latitude.attrs['_FillValue'] =\
           #             obs_array.lat.dtype.type(np.nan)
            ds.longitude.attrs['long_name'] = \
                'Longitude of observed trajectory'
            #ds.longitude.attrs['_FillValue'] =\
            #            obs_array.lon.dtype.type(np.nan)
            ds.longitude.attrs['units'] = 'degrees_east'
            ds.attrs['buoyid'] = str(drifter)
            ds.attrs['description'] = 'NAFC drifter data'
            ds.attrs['source'] = 'Xeos'
            ds.attrs['comments'] = 'netCDF file by Nancy Soontiens'
            ds.attrs['approximateDrogueDepth'] = str(15)
            ds.attrs['model'] = 'svp'
            ds.attrs['type'] = 'svp'
            datestr = np.datetime_as_string(obs_array.time.values[0])
            ds.attrs['dataStartDate'] = datestr
            ds.to_netcdf(os.path.join(SAVE_DIR, outfile))
            ds.close()
           

def read_svp_csv(fname):
    """Read drifter data from ROBY or OSKERS. fname is filename (str)"""
    df = pd.read_csv(fname,skiprows=[1,], parse_dates=[1,],
                     usecols=[0,1,2,3], header=0,
                     names=['DrifterID','time','lat', 'lon'])
    
    return df


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


if __name__=='__main__':
    main()
