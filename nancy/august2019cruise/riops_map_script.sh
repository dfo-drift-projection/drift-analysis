#!/bin/bash

# Script to run RIOPS drift map daily
# Usage: ./riops_map_script.sh
# Runs an 8 day drift_map simulation from Sept 6, 2019 to Sept 14, 2019 
# Coincides with hurrican Dorian
# Particles are released at 15 m depth
# Region is defined by bbox (below)
# Nancy Soontiens

# Set environment
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH
MINICONDA_PATH=/space/hall0/sitestore/dfo/odis/nso001/miniconda/envs/opendrift_fork

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

echo "set paths"

drifter_code_dir=$HOME/code/drifters/drift-tool # path to drift tool

# Run arameters
ocean_data_dir=$HOME/data/work2/models/riops/august2019cruise
ocean_mesh_file='None'
run_option='linear'
ocean_model_name=riopsPS
drifter_depth=15c
num_particles_x=20
num_particles_y=20
first_start_date="2019-09-06"
last_start_date="2019-09-06"
drift_duration=192
start_date_frequency='daily'
start_date_interval=1
xwatervel='vozocrtx'
ywatervel='vomecrty'
drift_model_name='OpenDrift'
rotation_data_file='None'
utm_zone='9U'
opendrift_map_reader='riops_map_reader.pickle'
alpha_wind=0.
opendrift_dt=180.
initial_bbox='-60 43 -50 47'
lon_var='longitude'
lat_var='latitude'
ulon_var='glamu'
ulat_var='gphiu'
vlon_var='glamv'
vlat_var='gphiv'
wdep_var='depth'
tmask_var='tmask'
zwatervel='None'
temperature='None'
salinity='None'
density='None'
model_time='time'

experiment_dir=/gpfs/fs4/dfo/dpnm/dfo_odis/nso001/OPP/august2019cruise/DriftMap/svps

echo "set arguments. running drift map..."

daily_drift_map \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --ocean-mesh-file $ocean_mesh_file \
    --ocean-model-name $ocean_model_name\
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drift-duration $drift_duration \
    --num-particles-x $num_particles_x \
    --num-particles-y $num_particles_y \
    --run-option $run_option \
    --initial-bbox "$initial_bbox" \
    --drifter-depth $drifter_depth \
    --drift-model-name $drift_model_name \
    --rotation-data-file $rotation_data_file \
    --utm-zone $utm_zone \
    --opendrift-map-reader $opendrift_map_reader \
    --alpha-wind $alpha_wind \
    --opendrift-dt $opendrift_dt \
    --lon-var $lon_var \
    --lat-var $lat_var \
    --ulon-var $ulon_var \
    --ulat-var $ulat_var \
    --vlon-var $vlon_var \
    --vlat-var $vlat_var \
    --dep-var $wdep_var \
    --tmask-var $tmask_var \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --xwatervel $xwatervel \
    --ywatervel $ywatervel \
    --zwatervel $zwatervel \
    --temperature $temperature \
    --salinity $salinity \
    --density $density \
    --model-time $model_time

plot_drift_map \
  --data_dir $experiment_dir/output \
  --bbox $experiment_dir/namelist_bbox \
  --etopo_file '/space/group/dfo_odis/project/sab002/code/drifters/MLDPn/etopo_file/ETOPO1_Bed_g_gmt4.grd' \
  --plot_dir $experiment_dir/plots \
  --plot_style 'talk'
