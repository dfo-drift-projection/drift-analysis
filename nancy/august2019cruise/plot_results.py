# A script to plot results of august201 cruise and runs.


from collections import namedtuple
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from mpl_toolkits.basemap import Basemap
import glob
import os
import datetime
from matplotlib.lines import Line2D

from driftutils import mpl_util


driftmapdir = ('/home/nso001/data/work2/OPP/august2019cruise/DriftMap/svps/'
               'output')
drifterdir = ('/home/nso001/data/work2/OPP/drifter_data/'
             'svp_082019/netcdf')
LATMIN = 44.5
LATMAX = 47
LONMIN = -59
LONMAX = -56

LatLonBoundingBox = namedtuple('LatLonBoundingBox',
                               ('lat_min', 'lat_max', 'lon_min', 'lon_max'))

bbox=LatLonBoundingBox(lon_min=LONMIN, lon_max=LONMAX,
                       lat_min=LATMIN, lat_max=LATMAX)
etopo = ('/space/group/dfo/dfo_odis/dpnm-gpfs-fs4/sdfo000/software/misc_files'
         '/ETOPO1_Bed_g_gmt4.grd')

start = datetime.datetime(2019,9,6)
end = datetime.datetime(2019,9,10)


def plot_map_and_drifters():
    fig, ax = plt.subplots(1,1,figsize=(10,6))
    bmap = set_up_basemap(ax)
    # plot model
    mfiles = glob.glob(os.path.join(driftmapdir, '*.nc'))
    skip=1
    for f in mfiles:
        print(f)
        ds =xr.open_dataset(f)
        ds= ds.sel(time=slice(start,end))
        x,y = bmap(ds.mod_lon[0::skip,:].values, ds.mod_lat[0::skip,:].values)
        bmap.plot(x,y,'.', markersize=1, c='slategray',
                  alpha=0.2, label='model')
        x,y = bmap(ds.mod_lon[0::skip,0].values, ds.mod_lat[0::skip,0].values)
        bmap.plot(x,y,'o', c='green', markersize=3)

    # plot obs
    dfiles = glob.glob(os.path.join(drifterdir, '*.nc'))
    for f in dfiles:
        print(f)
        ds = xr.open_dataset(f)
        ds = ds.sel(time=slice(start, end))
        x,y = bmap(ds.longitude.values, ds.latitude.values)
        bmap.plot(x,y,'r-', linewidth=1.5, label='obs')
        x,y = bmap(ds.longitude.values[0], ds.latitude.values[0])
        bmap.plot(x,y,'bo')

    pretty_plot(bmap)
    mpl_util.plot_bathymetry(etopo, bmap, bbox)
    ax.set_title('{} to {}'.format(start.strftime('%Y-%m-%d'),
                                   end.strftime('%Y-%m-%d')))
    custom_lines = [Line2D([0], [0], color='slategray', lw=1.5),
                    Line2D([0], [0], color='red', lw=1.5)]
    ax.legend(custom_lines, ['riops', 'obs'])
    plt.show()
    fig.savefig('map2.png', dpi=300)
    
def set_up_basemap(ax):
    basemap_settings = dict(
        llcrnrlon=LONMIN, llcrnrlat=LATMIN,
        urcrnrlon=LONMAX, urcrnrlat=LATMAX,
        ellps='WGS84', resolution='h', projection='merc', lat_ts=20.,
        ax=ax)
    basemap_plot = Basemap(**basemap_settings)
    
    return basemap_plot

def pretty_plot(bmap):
    bmap.fillcontinents(color='lightgrey', alpha=0.7, lake_color='grey')
    bmap.drawcoastlines(linewidth=0.8, color="black")
    
    # inputs for scale (km) on the map
    scale_lon_min = LONMAX - 1
    scale_lon_max = LONMAX - 2.5
    scale_lat_min = LATMAX - 1.8
    scale_lat_max = LATMAX - 1.20
    bmap.drawmapscale(scale_lon_min, scale_lat_min,
                      scale_lon_max, scale_lat_max, 
                      100, fontsize=12)
        
    # parallels and meridiands for the maps
    meridian_dlat = 1
    meridian_dlon = 1
    parallels = np.arange(LATMIN, LATMAX, meridian_dlat)
    bmap.drawparallels(parallels, labels=[1, 0, 0, 0], fontsize=14,
                       color='0.5', linewidth=0.5, fmt='%0.2f')
    meridians = np.arange(LONMIN, LONMAX, meridian_dlon)
    bmap.drawmeridians(meridians, labels=[0, 0, 0, 1], fontsize=14,
                       color='0.5', linewidth=0.5, fmt='%0.2f')

def bigger_plot():
    fig,ax = plt.subplots(1,1)
    basemap_settings = dict(
        llcrnrlon=-70, llcrnrlat=35,
        urcrnrlon=-40, urcrnrlat=55,
        ellps='WGS84', resolution='h', projection='merc', lat_ts=45.,
        ax=ax)
    bmap = Basemap(**basemap_settings)
    bbox2=LatLonBoundingBox(lon_min=-75, lon_max=-30,
                            lat_min=30, lat_max=60)
    mpl_util.plot_bathymetry(etopo, bmap, bbox2)
    X1,Y1=bmap(LONMIN,LATMIN)
    X2,Y2=bmap(LONMAX,LATMAX)
    bmap.plot([X1,X1], [Y1,Y2], 'k-')
    bmap.plot([X1,X2], [Y1,Y1], 'k-')
    bmap.plot([X1,X2], [Y2,Y2], 'k-')
    bmap.plot([X2,X2], [Y1,Y2], 'k-')
    bmap.fillcontinents(color='lightgrey', alpha=0.7, lake_color='grey')
    bmap.drawcoastlines(linewidth=0.8, color="black")
    plt.show()
    fig.savefig('bigmap.png')
    

if __name__=='__main__':
    plot_map_and_drifters()
    bigger_plot()
