# Script to loop through and determine if osker drifters are grounded.
# Plots will pop up on screen which can be used to identify time periods over which data should be removed.

import datetime
import glob
import os

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import pandas as pd
import xarray as xr
import yaml

DATA_DIR='/home/nso001/data/work2/OPP/drifter_data/MEOPAR_TREX/oskers/netcdf_qcd'

SPEED_THRESH_MIN=0.1 #m/s
SPEED_THRESH_MAX=5.144 #m/s

def main():
    files = glob.glob(os.path.join(DATA_DIR, '*.nc'))
    files.sort()
    cutoffs={}
    for f in files:
        d = xr.open_dataset(f)
        try:
            qc=d.attrs['qc_comment']
        except KeyError:
            continue
        buoyid=d.attrs['buoyid']
        if 'grounded' in qc:
            cutoff=d.TIME.values[-1]
            roundedup = cutoff + pd.Timedelta(hours=1)
            roundedup = datetime.datetime(roundedup.year,
                                          roundedup.month,
                                          roundedup.day,
                                          roundedup.hour)
            cutoffs[buoyid] = roundedup.strftime('%Y-%m-%d %H:%M:%S')
    with open('grounded_cutoff_times.yaml', 'w') as f:
        yaml.dump(cutoffs, f)
        
if __name__ == '__main__':
    main()
