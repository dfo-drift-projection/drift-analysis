# Script to submit per drifter jobs

src_dir=/home/nso001/data/work2/OPP/MEOPAR-TREX/CIOPSEv2-correction-factor/drifters
prefix='calc_S02'

for f in ${src_dir}/${prefix}*; do
    drifter=$(basename $f)
    echo ${drifter}
    cp submit_dyanmic_perdrifter_template.job ${drifter}.job
    sed -i "s/{DRIFTER}/${drifter}/" ${drifter}.job
    jobsub -c gpsc4 ${drifter}.job
done
