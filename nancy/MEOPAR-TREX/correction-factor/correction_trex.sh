#! /bin/bash

LOG=/home/nso001/data/work2/OPP/MEOPAR-TREX/logs/oskers-correction-factor

MINICONDA_PATH=$HOME/data/work2/miniconda/envs/develop

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


# Settings
# CIOPSE
OCEAN_DIR='/home/nso001/data/work2/models/ciops-e_operational_forecasts/pseudo-analysis'
OCEAN_MESH_FILE='/home/sdfo000/sitestore4/opp_drift_fa3/share_drift/CIOPSE_SN1500/CIOPSE_mesh_files/mesh_mask_NWA36_Bathymetry_flatbdy_20181109_3_filter_min_7p5.nc'
ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/ciopse/ciopse.rotation.pickle'
OCEAN_MODEL_NAME='CIOPSE-pseudoanalysis'
xwatervel='uos'
ywatervel='vos'

ATMOS_DIR='/home/nso001/data/work2/models/hrdps-forecast/continuous'
ATMOS_MODEL_NAME='HRDPS'
DRIFTER_DIR='/home/nso001/data/work2/OPP/drifter_data/MEOPAR_TREX/oskers/netcdf_qcd'
OUT_DIR='/home/nso001/data/work2/OPP/MEOPAR-TREX/OSKERS-correction-factor'

SDT='2020-09-01'
EDT='2020-09-29'

#cd $HOME/code/drifters/drift-tool/src/driftutils/
drift_correction_factor \
       --experiment-dir $OUT_DIR \
       --drifter-data-dir $DRIFTER_DIR \
       --ocean-data-dir $OCEAN_DIR \
       --ocean-mesh-file $OCEAN_MESH_FILE \
       --ocean-model-name $OCEAN_MODEL_NAME \
       --start-date $SDT \
       --end-date $EDT \
       --atmos-model-name $ATMOS_MODEL_NAME \
       --atmos-data-dir $ATMOS_DIR \
       --rotation-data-file $ROTATION_FILE \
       --xwatervel $xwatervel \
       --ywatervel $ywatervel > $LOG 2>&1
