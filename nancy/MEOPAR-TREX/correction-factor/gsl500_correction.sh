#!/bin/bash

# Script to run CIOPS correction factor for MEOPAR TREX
# Nancy Soontiens

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

# Set environment          
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
experiment_dir=/home/nso001/data/work2/OPP/MEOPAR-TREX/GSL500-correction-factor
config_file=/home/nso001/code/drifters/drift-analysis/nancy/MEOPAR-TREX/correction-factor/gsl500.yaml

drift_correction_factor \
    -c $config_file \
    --experiment-dir $experiment_dir 
