# Script to convert osker data from csv to netcdf format
# Nancy Soontiens

import datetime
import glob
import os

import numpy as np
import pandas as pd
import xarray as xr

# Source sande save directories
OBS_SRC='/home/nso001/code/drifters/drift-analysis/nancy/MEOPAR-TREX/MEOPAR_TREX_osker_20201028.csv'
SAVE_DIR='/home/nso001/data/work2/OPP/drifters/MEOPAR_TREX/oskers/netcdf'
# This codes expects the csv files to be in a certain format.
# Reading the csv is handled by one of three functions:
# read_model_csv, read_SCT_csv or read_other_csv (other is for OSKER or Roby)

def main():
    """ 
    Main function to save obs from individual drifter into a netcdf file each
    """
    obs_files = [OBS_SRC, ]
    # Loop through all obs and model
    for obsf in obs_files:
        obs = read_osker_csv(obsf)
        obs['time'] = pd.to_datetime(obs['time'])
        obs['lon'] = obs['lon'].astype(float)
        obs = obs.sort_values(by=['time'])
        obs = obs.set_index(['time'])
        grouped = obs.groupby('DrifterID')
        for drifter in grouped.groups:
            g = grouped.get_group(drifter)
            obs_array = g.to_xarray()
            print(obs_array)
            drifter = drifter.replace(' ', '')
            outfile = '{}.nc'.format(drifter)
            val, idx = np.unique(obs_array.time, return_index=True)
            obs_array = obs_array.isel(time=idx)
            ds = xr.Dataset(
                coords={'TIME': obs_array.time.values},
                data_vars={'LATITUDE': ('TIME', obs_array.lat.values),
                           'LONGITUDE': ('TIME', obs_array.lon.values),
                           'SPEED': ('TIME', obs_array['speed[km/hr]'].values*1000/3600),
                           'BEARING': ('TIME', obs_array['bearing'].values)
                           }
                )
            ds.LATITUDE.attrs['long_name'] = 'Latitude of observed trajectory'
            ds.LATITUDE.attrs['units'] = 'degrees_north'
           # ds.LATITUDE.attrs['_FillValue'] =\
           #             obs_array.lat.dtype.type(np.nan)
            ds.LONGITUDE.attrs['long_name'] = \
                'Longitude of observed trajectory'
            #ds.LONGITUDE.attrs['_FillValue'] =\
            #            obs_array.lon.dtype.type(np.nan)
            ds.LONGITUDE.attrs['units'] = 'degrees_east'
            ds.SPEED.attrs['long_name'] = 'drifter speed'
            ds.SPEED.attrs['units'] = 'm/s'
            ds.BEARING.attrs['long_name'] = 'drifter bearing'
            ds.BEARING.attrs['units'] = 'degress clockwise of North'
            ds.attrs['buoyid'] = str(drifter)
            ds.attrs['description'] = 'NAFC drifter data'
            ds.attrs['source'] = 'Xeos'
            ds.attrs['comments'] = 'netCDF file by Nancy Soontiens'
            ds.attrs['approximate_drogue_depth'] = str(0)
            ds.attrs['model'] = 'osker'
            ds.attrs['type'] = 'osker'
            datestr = np.datetime_as_string(obs_array.time.values[0])
            ds.attrs['dataStartDate'] = datestr
            ds.to_netcdf(os.path.join(SAVE_DIR, outfile))
            ds.close()
           

def read_osker_csv(fname):
    """Read drifter data from ROBY or OSKERS. fname is filename (str)"""
    df = pd.read_csv(fname, parse_dates=[3,],
                     usecols=[2,3,4,5,10,11], header=0,
                     names=['DrifterID','time','lat', 'lon', 'bearing', 'speed[km/hr]'])
    
    return df


def wrap_to_180(x):
    """Wrap values in degrees into the interval [-180, 180]."""
    x_wrap = np.remainder(x, 360)
    x_wrap[x_wrap > 180] -= 360
    return x_wrap


if __name__=='__main__':
    main()
