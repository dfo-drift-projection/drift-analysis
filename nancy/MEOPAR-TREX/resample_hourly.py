import glob
import os
import subprocess

import numpy as np
import pandas as pd
import xarray as xr

data_dir='/home/nso001/data/work2/OPP/drifter_data/MEOPAR_TREX/oskers/netcdf_qcd'
out_dir='/home/nso001/data/work2/OPP/drifter_data/MEOPAR_TREX/oskers/netcdf_qcd_resampled'

files=glob.glob(os.path.join(data_dir,'*.nc'))
files.sort()

freq='1H' # frequency for resampling

for f in files:
    basename=os.path.basename(f)
    ds = xr.open_dataset(f)
    print('Processing drifter {}'.format(ds.buoyid))
    try:
        dsnew = ds.resample({'TIME': freq}).interpolate('linear').dropna('TIME')
        newfile = os.path.join(out_dir, 'hourly_{}'.format(basename))
        dsnew.attrs['processing_comment'] = 'Data resampled to hourly frequency'
        dsnew.to_netcdf(newfile)
    except:
        continue
