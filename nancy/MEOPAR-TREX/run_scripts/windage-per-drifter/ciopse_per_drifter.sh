#!/bin/bash

# Script to run CIOPS eval for MEOPAR TREX
# Nancy Soontiens

MINICONDA_PATH=/home/nso001/data/work7/miniconda-gpsc7/envs/develop

# Set environment          
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
#experiment_dir=/home/nso001/data/work7/OPP/MEOPAR-TREX/updated-runs/CIOPSEv2-drift-eval-diffusion-uv0.06
drifter_type=$1

# Windages based on DAR calcualtions, but without the 1% stokes correction
alpha_wind=2
if [ "${drifter_type}" = "osker" ]; then
    alpha_wind=3.45
elif [ "${drifter_type}" = "ubc" ]; then
    alpha_wind=0.33
elif [ "${drifter_type}" = "codedavis" ]; then
    alpha_wind=0.36
elif [ "${drifter_type}" = "iSphere" ]; then
    alpha_wind=3.46
elif [ "${drifter_type}" = "ismer" ]; then
    alpha_wind=2.42
elif [ "${drifter_type}" = "sct" ]; then
    alpha_wind=1.00
elif [ "${drifter_type}" = "carthe" ]; then
    alpha_wind=0.70
fi

echo "alpha-wind is ${alpha_wind}"

experiment_dir=/home/nso001/data/work7/OPP/MEOPAR-TREX/updated-runs/per-drifter-windage/CIOPSE/${drifter_type}
config_file=/home/nso001/code/drifters/drift-analysis/nancy/MEOPAR-TREX/run_scripts/ciopsev2-gpsc7.yaml

# Run arameters
#ocean_data_dir=/home/nso001/data/work2/models/IC3-ciopse/ciopsev2/2D
#ocean_data_dir=/home/nso001/data/work7/models/ciops-e_operational_forecasts/pseudo-analysis/
drifter_depth=L1
first_start_date="2020-09-10"
last_start_date="2020-09-26"
start_date_frequency='daily'
start_date_interval=1
atmos_data_dir=/home/nso001/data/work7/models/MEOPAR-TREX/hrdps-1km/netcdf/
drifter_data_dir=/home/nso001/code/drifters/TREX-collab/data/observations/L3/netcdf_drifttool_qc/${drifter_type}
drifter_id_attr='buoyid'
ocean_model_name='CIOPSEv2'
atmos_model_name='HRDPS1km'
current_uncertainty=0.0 # represents KE=1.08 m^2/s


drift_predict \
    -c $config_file \
    --alpha-wind $alpha_wind\
    --experiment-dir $experiment_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drifter-depth $drifter_depth \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --atmos-data-dir $atmos_data_dir \
    --drifter-data-dir $drifter_data_dir \
    --drifter-id-attr $drifter_id_attr \
    --ocean-model-name $ocean_model_name \
    --atmos-model-name $atmos_model_name \
    --current-uncertainty $current_uncertainty

drift_evaluate \
  --data_dir $experiment_dir/output

#combine the output into per drifter files
combine_track_segments \
  --data_dir=$experiment_dir/output
