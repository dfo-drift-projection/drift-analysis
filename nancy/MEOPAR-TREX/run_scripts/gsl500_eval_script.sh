#!/bin/bash

# Script to run gsl500 eval for MEOPAR TREX
# Nancy Soontiens

MINICONDA_PATH=/home/nso001/data/work2/miniconda/envs/develop

# Set environment          
old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/

# production directory - link output here to share with others
experiment_dir=/home/nso001/data/work2/OPP/MEOPAR-TREX/gsl500-drift-eval
config_file=/home/nso001/code/drifters/drift-analysis/nancy/MEOPAR-TREX/run_scripts/gsl500.yaml

# Run arameters
ocean_data_dir=/home/nso001/data/work2/models/MEOPAR-TREX/gsl500_512_TREX_v3/three-hourly-rotated-interp/
drifter_depth=L1
first_start_date="2020-09-10"
last_start_date="2020-09-26"
start_date_frequency='daily'
start_date_interval=1
atmos_data_dir=/home/nso001/data/work2/models/MEOPAR-TREX/hrdps-1km/netcdf/
drifter_data_dir=/home/nso001/code/drifters/TREX-collab/data/observations/L3/netcdf_drifttool_qc/netcdf_2021-09-14/
drifter_id_attr='buoyid'
ocean_model_name='GSL500'
atmos_model_name='HRDPS1km'

drift_predict \
    -c $config_file \
    --experiment-dir $experiment_dir \
    --ocean-data-dir $ocean_data_dir \
    --first-start-date "$first_start_date" \
    --last-start-date "$last_start_date" \
    --drifter-depth $drifter_depth \
    --start-date-frequency $start_date_frequency \
    --start-date-interval $start_date_interval \
    --atmos-data-dir $atmos_data_dir \
    --drifter-data-dir $drifter_data_dir \
    --drifter-id-attr $drifter_id_attr \
    --ocean-model-name $ocean_model_name \
    --atmos-model-name $atmos_model_name

drift_evaluate \
  --data_dir $experiment_dir/output

#combine the output into per drifter files
combine_track_segments \
  --data_dir=$experiment_dir/output
