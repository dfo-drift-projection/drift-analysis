%% Find candidate particle pairs with a significant overlapping data time and
%  initial separation distances below a threshold value

clear

%% Geographic information
% Central latitude, longitude of domain
lon0 = -66;
lat0 = 49.2;

yscale = 6371*pi/180; % Distance in y per degree latitude (km)
xscale = 6371*pi/180*cos(lat0*pi/180); % Distance in x per degree longitude

%% Threshold information
min_overlap_time = 5; % Minimum amount of overlapping time for consideration (days)
max_initial_distance = 1; % Maximum initial distance for candidate pair (km)

%% Load data
load TREX20_SPOT_L2_20201005T2135500.mat

Ndrift = length(drift);

%% Find drifter pairs

pairs = [];
pairs_extent = [];

% Loop over drifters to find pairs
for ii = 1:Ndrift
    myEnd = drift(ii).endDate;
    myStart = drift(ii).launchDate;
    
    % If this drifter wasn't in the water for longer than the minimum
    % pair overlap time, it can't possibly match anything
    if (myEnd - myStart < min_overlap_time)
        continue
    end
    
    % To overlap with drifter (j), (j) must start before (i) ends
    candidates = ii + find([drift((ii+1):end).launchDate] < myEnd);
    % ... and it must end after (i) launches
    idx = find([drift(candidates).endDate] > myStart);
    candidates = candidates(idx);
    
    % Loop over candidate pairs to check for valid overlap times and
    % starting distances
    
    % Performance note: this algorithm is quadratic in the number of
    % drifters because of the selections used.  This is fine for a few
    % hundred drifters, but if we eventually have thousands more clever
    % binning would help.
    for jj = candidates
        pairStart = max(myStart,drift(jj).launchDate);
        pairEnd = min(myEnd,drift(jj).endDate);
        if (pairEnd - pairStart < min_overlap_time)
            continue;
        end
        
        % The pair shares the ocean for long enough, now were they close
        % enough?  Interoplate lat/lon to the start time to check
        startLati = interp1(drift(ii).mtime,drift(ii).lat,pairStart);
        startLatj = interp1(drift(jj).mtime,drift(jj).lat,pairStart);
        startLoni = interp1(drift(ii).mtime,drift(ii).lon,pairStart);
        startLonj = interp1(drift(jj).mtime,drift(jj).lon,pairStart);
        
        startDist = norm([(startLoni - startLonj)*xscale; (startLati - startLatj)*yscale]);
        if (startDist < max_initial_distance)
            fprintf('found pair (%d, %d) overlapping %.2fd, initial distance %.2fkm\n',ii,jj,pairEnd-pairStart,startDist);
            pairs(end+1,:) = [ii jj];
            pairs_extent(end+1)=pairEnd-pairStart;
        end
    end
end

fprintf('%d pairs found matching criteria\n',length(pairs))

%% Sample pair: (147, 182)
ii = 147; jj = 182;
pairStart = max(drift(ii).launchDate,drift(jj).launchDate);
pairEnd = min(drift(ii).endDate,drift(ii).endDate);

clf
mypair = [ii jj];
%colors = colororder;
for pp = 1:2
    kk = mypair(pp);
    lats = drift(kk).lat;
    lons = drift(kk).lon;
    times = drift(kk).mtime;
    goodtimes = (times >= pairStart) & (times <= pairEnd);
    plot(lons(goodtimes), lats(goodtimes), '-');
    hold on
end
title(sprintf('Trajectory pair (%d, %d)',ii,jj))

[sort_extent sort_extenti]=sort(pairs_extent,'descend');
figure(2)
clf
Nbox = 4;
for pairsi=1:Nbox*Nbox
    subplot(Nbox,Nbox,pairsi)
    ii=pairs(sort_extenti(pairsi),1);
    jj=pairs(sort_extenti(pairsi),2);
    pairStart = max(drift(ii).launchDate,drift(jj).launchDate);
    pairEnd = min(drift(ii).endDate,drift(ii).endDate);
    mypair = [ii jj];
    %colors = colororder;
    for pp = 1:2
        % Grab lat/lon coordinates for each drifter in the pair
        kk = mypair(pp);
        lats = drift(kk).lat;
        lons = drift(kk).lon;
        times = drift(kk).mtime;
        % Filter to only observations that are in the valid (matching) time
        % for the pair
        goodtimes = (times >= pairStart) & (times <= pairEnd);
        latnow=lats(goodtimes);
        lonnow=lons(goodtimes);
        tnow=times(goodtimes);
        % Plot the drifter path
        plot(lonnow, latnow, '-');%,'color',colors(pp,:));
        % Look for times the drifter may have been stuck in an eddy, via an
        % arclength approach.
        
        % A drifter is "stuck in an eddy" if it spends a fair amount of
        % time moving but not making forward progress.  This is not
        % something we can observe on an instantaneous basis, but we can
        % reach that conclusion if we observe it over a finite time like a
        % few hours.
        
        % The trial criteria will be the ratio of displacement to
        % arclength -- a low ratio means a very meandering path.
        
        arclength = [0;cumsum(sqrt((diff(latnow)*yscale).^2 + (diff(lonnow)*xscale).^2))];
        eddy_timescale = 48/24; % Eddy timescale, in days (tunable parameter?)
        
        % Since longer timescales seem to behave better, let's take this as
        % a central estimate.
        
        % Filter out times too close to the start or end:
        eddytimes = (tnow > pairStart + eddy_timescale/2) & (tnow < pairEnd - eddy_timescale/2);
        teddy = tnow(eddytimes);
        lateddy = latnow(eddytimes);
        loneddy = lonnow(eddytimes);
        
        % Compute delta-lat and delta-lon between (obs) and (obs+t_eddy)
        lat_displ = interp1(tnow,latnow,teddy+eddy_timescale/2) - ...
                    interp1(tnow,latnow,teddy-eddy_timescale/2);
        lon_displ = interp1(tnow,lonnow,teddy+eddy_timescale/2) - ...
                    interp1(tnow,lonnow,teddy-eddy_timescale/2);
        % Convert to distance
        displacement = sqrt((lat_displ*yscale).^2 + (lon_displ*xscale).^2);
        
        % Compute the arclength (distance travelled) over the same period:
        distance = interp1(tnow,arclength,teddy+eddy_timescale/2) - ...
                   interp1(tnow,arclength,teddy-eddy_timescale/2);
        
        % Identify when the drifter is stuck by a low displacement/distance
        % ratio.  This parameter is also tunable, but 0.5 seems reasonable?
        stuck_in_eddy = (displacement ./ distance) < 0.5;
        
        hold on
        % Plot points where we think we're stuck in the eddy
        plot(loneddy(stuck_in_eddy),lateddy(stuck_in_eddy),'.');%'color',colors(pp,:));
        axis([-70 -63.5 48.5 50])
        hold on
    end
    grid on
end
