# Script to loop through and determine if osker drifters are grounded.
# Plots will pop up on screen which can be used to identify time periods over which data should be removed.

import glob
import os

import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
import xarray as xr

DATA_DIR='/home/nso001/data/work2/OPP/drifter_data/MEOPAR_TREX/oskers/netcdf'

SPEED_THRESH_MIN=0.1 #m/s
SPEED_THRESH_MAX=5.144 #m/s

def main():
    files = glob.glob(os.path.join(DATA_DIR, '*.nc'))
    files.sort()
    for f in files:
        d = xr.open_dataset(f)
        plot_tracks_and_time_series(d)

def plot_tracks_and_time_series(ds):

    fig, axs = plt.subplots(3,2,figsize=(10,10))
    
    ax=axs[0,0]
    lons = ds.LONGITUDE.values
    lats = ds.LATITUDE.values
    min_lon = lons.min()
    max_lon = lons.max()
    min_lat = lats.min()
    max_lat = lats.max()
    buffer_lonslats=0.5

    bmap = Basemap(llcrnrlon=min_lon-buffer_lonslats,
                   urcrnrlon=max_lon+buffer_lonslats,
                   llcrnrlat=min_lat-buffer_lonslats,
                   urcrnrlat=max_lat+buffer_lonslats,
                   projection='merc',
                   resolution='f',ax=ax)
    x,y = bmap(lons, lats)
    bmap.plot(x,y,'-ok')
    
    # positions with small speeds
    ds_small = ds.where(ds.SPEED <= SPEED_THRESH_MIN)
    xs, ys = bmap(ds_small.LONGITUDE.values, ds_small.LATITUDE.values)
    bmap.plot(xs,ys,'ob')

    # positions with large speeds
    ds_big = ds.where(ds.SPEED >= SPEED_THRESH_MAX)
    xs, ys = bmap(ds_big.LONGITUDE.values, ds_big.LATITUDE.values)
    bmap.plot(xs,ys,'or')

    
    ax.set_title('Drifter {}'.format(ds.buoyid))
    bmap.drawcoastlines()

    
    # Time series
    ax=axs[0,1]
    ax.plot(ds.TIME.values, ds.LONGITUDE.values, '-ok', label='__no-label__')
    ax.plot(ds_small.TIME.values,
            ds_small.LONGITUDE.values,
            'ob',label='speed <= {} m/s'.format(SPEED_THRESH_MIN))
    ax.plot(ds_big.TIME.values,
            ds_big.LONGITUDE.values,
            'or',label='speed >= {} m/s'.format(SPEED_THRESH_MAX))

    ax.set_ylabel('Longitude')
    ax.legend()
    ax.grid()

    ax=axs[2,1]
    ax.plot(ds.TIME.values, ds.LATITUDE.values, '-ok', label='__no-label__')
    ax.plot(ds_small.TIME.values,
            ds_small.LATITUDE.values,
            'ob',label='speed <= {} m/s'.format(SPEED_THRESH_MIN))
    ax.plot(ds_big.TIME.values,
            ds_big.LATITUDE.values,
            'or',label='speed >= {} m/s'.format(SPEED_THRESH_MAX))
    ax.set_ylabel('Latitude')
    ax.grid()
    
    ax=axs[1,1]
    ax.plot(ds.TIME.values, ds.SPEED.values, '-ok', label='__no-label__')
    ax.plot(ds_small.TIME.values,
            ds_small.SPEED.values,
            'ob',label='speed <= {} m/s'.format(SPEED_THRESH_MIN))
    ax.plot(ds_big.TIME.values,
            ds_big.SPEED.values,
            'or',label='speed >= {} m/s'.format(SPEED_THRESH_MAX))
    ax.set_ylabel('speed [m/s]')
    ax.grid()
    fig.autofmt_xdate()
    plt.show()

if __name__ == '__main__':
    main()
