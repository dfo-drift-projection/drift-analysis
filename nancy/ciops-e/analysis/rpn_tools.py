# Module with tools for working with NEMO results
# You need to have the following in your environment to use this module
# . ssmuse-sh -x comm/eccc/all/opt/intelcomp/intelpsxe-cluster-19.0.3.199
# . ssmuse-sh -x eccc/mrd/rpn/libs/19.5
# . ssmuse-sh -x eccc/mrd/rpn/MIG/ENV/x/rpnpy/2.1-u1.rc11

import numpy as np

import rpnpy.librmn.all as rmn
from rpnpy.rpndate import RPNDate


def load_2D_std_field(fname, varname, ip1=-1):
    """Load and mask a variable given by varname in a standard rpn file.
    Return variable, lon, lat, and timsstampe"""
    # Open file
    funit = rmn.fstopenall(fname, rmn.FST_RO)
    # Read variable and grid
    var_rec = rmn.fstlir(funit, nomvar=varname, typvar='P@',ip1=ip1)
    var_mask_rec = rmn.fstlir(funit, nomvar=varname, typvar='@@',ip1=ip1)
    lat = np.array(rmn.fstlir(funit, nomvar='^^')['d'])
    lon = np.array(rmn.fstlir(funit, nomvar='>>')['d'])
    dt = get_timestamp(funit, varname)
    # Close file
    rmn.fstcloseall(funit)
    # Create numpy arrray of variable
    var = np.ma.array(var_rec['d'], mask = np.logical_not(var_mask_rec['d']))

    return var, lon, lat, dt


def load_3D_std_field(fname, varname):
    """Load the full 3D profile of a variable.
    Assuming one time slice in each file"""
    funit = rmn.fstopenall(fname, rmn.FST_RO)
    levels, depths = get_vertical_level_codes(funit, varname)
    var3d, mask3d = [], []
    # Read variable and grid
    for k, level in enumerate(levels):
        var_rec = rmn.fstlir(funit, nomvar=varname, typvar='P@',ip1=level)
        var_mask_rec = rmn.fstlir(funit, nomvar=varname, typvar='@@',ip1=level)
        var = np.array(var_rec['d'])
        mask = np.array(var_mask_rec['d'])
        var3d.append(var)
        mask3d.append(mask)
    var3d=np.array(var3d)
    mask3d = np.array(mask3d)
    dt = get_timestamp(funit, varname)
    # Close file
    rmn.fstcloseall(funit)
    # Create numpy arrray of variable
    var3d = np.ma.array(var3d, mask = np.logical_not(mask3d))
    return var3d, depths, dt


def get_vertical_level_codes(funit, varname):
    """Print the vertical levels and the associated ip1 codes
       for a variable varname in an open rpn file funit"""
    keyList = rmn.fstinl(funit, nomvar=varname, typvar='P@')
    levels = []
    depths = []
    for k in keyList:
        recMeta = rmn.fstprm(k)
        (level, ikind) = rmn.convertIp(rmn.CONVIP_DECODE, recMeta['ip1'])
        kindstring     = rmn.kindToString(ikind)
        levels.append(recMeta['ip1'])
        depths.append(level)
    return levels, depths


def get_timestamp(funit, varname):
    """Return the timestamp, as a datetime object, of variable.
       In RPN terms, return the date of validity
       funit is the identifier of a open rpn file
       varname is the name of the variable to look up"""
    key = rmn.fstinf(funit, nomvar=varname)
    datev = rmn.fstprm(key)['datev']
    (yyyymmdd, hhmmsshh) = rmn.newdate(rmn.NEWDATE_STAMP2PRINT, datev)
    rpn1 = RPNDate(yyyymmdd, hhmmsshh)
    dt1 = rpn1.toDateTime()
    return dt1

