# This scripts compares the currents in an RPN file to equivalent netCDF file generated with cstrpn2cdf
# The goad is to determine if the currents are equivalent in the two file formats. For example, are the currents rotated on the rpn file but not on the netcdffile??
# Nancy Soontiens

import numpy as np
import xarray as xr

import rpn_tools


rpn_file='/home/nso001/data/work2/CIOPSE_SN1500/RPN/2015110700_000_1d'
netcdf_file='/home/nso001/data/work2/CIOPSE_SN1500/netcdf/2015110700_000_1d_3D.nc'

# Load rpn
u_rpn, depths_rpn, time_rpn=rpn_tools.load_3D_std_field(rpn_file, 'UUW')
v_rpn, depths_rpn, time_rpn=rpn_tools.load_3D_std_field(rpn_file, 'VVW')
u_rpn = np.transpose(u_rpn, (0,2,1))
v_rpn = np.transpose(v_rpn, (0,2,1))
# Load netCDF
d = xr.open_dataset(netcdf_file)
u_netcdf = d['vozocrtx'].values[0,...]
v_netcdf = d['vomecrty'].values


print(d)
print((u_rpn-u_netcdf ==0).all())
print((v_rpn-v_netcdf ==0).all())
print(time_rpn,d.time_counter)
print(depths_rpn - d['depth'].values)
