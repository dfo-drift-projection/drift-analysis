#! /bin/bash

LOG=/home/nso001/data/work2/OPP/dynamic-windage-test/logs/salishsea-ubc-dec

MINICONDA_PATH=$HOME/data/work2/miniconda/envs/develop

old_PYTHONPATH=$PYTHONPATH
unset PYTHONPATH

export PATH=$MINICONDA_PATH/bin:$PATH
export PYTHONPATH=$MINICONDA_PATH/bin
export PROJ_LIB=$MINICONDA_PATH/share/proj/


# Call your program
# CIOPSW
#OCEAN_DIR='/home/nso001/data/work2/models/ciops-w/CIOPSW-BC12/hourly'
#OCEAN_MESH_FILE='/home/sdfo000/sitestore4/opp_drift_fa3/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
#ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/ciopsw/ciopsw.rotate.pickle'
#OCEAN_MODEL_NAME='CIOPSWBC12'
#xwatervel='uos'
#ywatervel='vos'
#SalishSea
OCEAN_DIR='/home/nso001/data/work2/models/SalishSea-BC12o/hourly'
OCEAN_MESH_FILE='/home/nso001/data/work2/models/SalishSea-BC12/mesh_mask201702MD.nc'
ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/SalishSea-BC12/salishsea.rotate.pickle'
OCEAN_MODEL_NAME='SalishSea'
# RIOPS
#OCEAN_DIR='/home/nso001/data/work2/models/riops_bc12/hourly'
#OCEAN_MESH_FILE='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
#ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/riops/riops.rotate.pickle'


ATMOS_DIR='/home/nso001/data/work2/models/hrdps-west-mshydro/'
ATMOS_MODEL_NAME='HRDPS'
#DRIFTER_DIR='/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/drifter_data_conversion/wpdata/wpdata2016/reprocessed_data_20200721/netcdfs_keep_plus_questionable'
DRIFTER_DIR='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/ubc_type'
#DRIFTER_DIR='/home/nso001/data/work2/OPP/drifter_data/salishsea-ciopsw-compare'
OUT_DIR='/home/nso001/data/work2/OPP/dynamic-windage-test/SalishSea-UBC/dec2016'

SDT='2016-12-01'
EDT='2016-12-31'

#cd $HOME/code/drifters/drift-tool/src/driftutils/
drift_correction_factor \
       --experiment-dir $OUT_DIR \
       --drifter-data-dir $DRIFTER_DIR \
       --ocean-data-dir $OCEAN_DIR \
       --ocean-mesh-file $OCEAN_MESH_FILE \
       --ocean-model-name $OCEAN_MODEL_NAME \
       --start-date $SDT \
       --end-date $EDT \
       --atmos-model-name $ATMOS_MODEL_NAME \
       --atmos-data-dir $ATMOS_DIR \
       --rotation-data-file $ROTATION_FILE  > $LOG 2>&1
