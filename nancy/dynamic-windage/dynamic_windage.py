# Template script for calculating dyanmic windage
# Will be used to build into drift tool eventually
# Nancy Soontiens September 2020

import datetime
import glob
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import pickle
import pyproj
import scipy.interpolate as interp
import xarray as xr

from driftutils.rotate_fields import rot_rep_2017_p
from driftutils.find_nearest_grid_point import find_nearest_grid_point

# CIOPSW
#OCEAN_DIR='/home/nso001/data/work2/models/ciops-w/CIOPSW-BC12/hourly'
#OCEAN_MESH_FILE='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/share_drift/CIOPSW_DI04/CIOPSW_grid_files/mesh_mask_Bathymetry_NEP36_714x1020_SRTM30v11_NOAA3sec_WCTSS_JdeFSalSea.nc'
#ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/ciopsw/ciopsw.rotate.pickle'
#SalishSea
OCEAN_DIR='/home/nso001/data/work2/models/SalishSea-BC12o/hourly'
OCEAN_MESH_FILE='/home/nso001/data/work2/models/SalishSea-BC12/mesh_mask201702MD.nc'
ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/SalishSea-BC12/salishsea.rotate.pickle'
# RIOPS
#OCEAN_DIR='/home/nso001/data/work2/models/riops_bc12/hourly'
#OCEAN_MESH_FILE='/gpfs/fs4/dfo/dpnm/dfo_odis/sdfo000/x/SeDOO/requests/RIOPS_CREG12_ext_data/mesh_mask.nc'
#ROTATION_FILE='/home/nso001/data/work2/rotation_pickles/riops/riops.rotate.pickle'


ATMOS_DIR='/home/nso001/data/work2/models/hrdps-west-mshydro/'
DRIFTER_DIR='/gpfs/fs4/dfo/dpnm/dfo_dpnm/jeh326/projects/drifter_data_conversion/wpdata/wpdata2016/reprocessed_data_20200721/netcdfs_keep_plus_questionable'
#DRIFTER_DIR='/home/nso001/data/work2/OPP/drifter_data/drifters/ubc_drifters/ubc_type'
OUT_DIR='/home/nso001/data/work2/OPP/dynamic-windage-test/SalishSea-IOS'

#drifter must be in this time range
SDT = datetime.datetime(2016,4,1)
EDT = datetime.datetime(2016,6,30)

def dynamic_windage(
        drifter_dir,
        ocean_dir,
        atmos_dir,
        out_dir,
        ocean_mesh_file,
        rotation_data_file='None',
        xwatervel='vozocrtx',
        ywatervel='vomecrty',
        lon_var_ocean='nav_lon',
        lat_var_ocean='nav_lat',
        ulon_var_ocean='glamu',
        ulat_var_ocean='gphiu',
        vlon_var_ocean='glamv',
        vlat_var_ocean='gphiv',
        xwindvel='u_wind',
        ywindvel='v_wind',
        model_time_ocean='time_counter',
        lon_var_atmos='nav_lon',
        lat_var_atmos='nav_lat',
        model_time_atmos='time_counter'
):

    # Open model files
    # refactor to identify model files for each drifter?
    # refactor to identify model files associated with variables
    # Ocean
    fous = glob.glob(os.path.join(ocean_dir, '*grid_U_20160[4,5,6]*.nc'))
    fous.sort()
    dou = xr.open_mfdataset(fous)
    fovs = glob.glob(os.path.join(ocean_dir, '*grid_V_20160[4,5,6]*.nc'))
    fovs.sort()
    dov = xr.open_mfdataset(fovs)
    mesh = xr.open_dataset(ocean_mesh_file)
    try:
        u = dou.isel(depthu=0)[xwatervel] #needs knowledge of name of depth dimension
        v = dov.isel(depthv=0)[ywatervel]
    except:
        # 2D fields
        u = dou[xwatervel]
        v = dov[ywatervel]
    lonu = np.squeeze(mesh[ulon_var_ocean].values)
    latu = np.squeeze(mesh[ulat_var_ocean].values)
    lonv = np.squeeze(mesh[vlon_var_ocean].values)
    latv = np.squeeze(mesh[vlat_var_ocean].values)
    with open(rotation_data_file, 'rb') as f:
        coeffs=pickle.load(f)
    ueast = rot_rep_2017_p(u,v,'U','ij->e', coeffs)
    vnorth = rot_rep_2017_p(u,v,'V','ij->n', coeffs)
    # Atmos
    fas = glob.glob(os.path.join(atmos_dir, '*m0[4,5,6]*.nc'))
    fas.sort()
    da = xr.open_mfdataset(fas)
    u_wind = da[xwindvel]
    v_wind = da[ywindvel]
    
    # start and end times for truncating drifter data
    sdt = max(dou[model_time_ocean].values[0],
              da[model_time_atmos].values[0])
    edt = min(dou[model_time_ocean].values[-1],
              da[model_time_atmos].values[-1])
    
    # Drifter data
    fds=glob.glob(os.path.join(drifter_dir,'*.nc'))
    fds.sort()
    # This is just for reducing processing time since I only want to process certain drifters
    with open('/home/nso001/data/work2/OPP/dynamic-windage-test/drifter_salish.txt') as f:
       drifters= f.read() 
    for fd in fds:
        basename = os.path.basename(fd)
        print("processing drifter file: {}".format(basename))
        d=xr.open_dataset(fd)
        buoyid = d.buoyid
        if buoyid not in drifters:
            continue
        d1 = pd.to_datetime(d.variables['TIME'].values[0]).to_pydatetime()
        d2 = pd.to_datetime(d.variables['TIME'].values[-1]).to_pydatetime()
        if (d1 >=EDT) or (d2 <=SDT):
            continue
        outfile = os.path.join(out_dir, 'dynamic-windage_{}'.format(basename))
        dd = drifter_velocity(xr.open_dataset(fd))
        # Should resampleing be before or after interpolating??
        dres = dd.resample({'time': '1H'}).interpolate('linear').dropna('time')
        dres = dres.sel(time=slice(sdt, edt))
        # Interpolate ocean
        do_uinterp = interpolate_variable_to_obs_track(dres,
                                                       ueast,
                                                       model_time_ocean,
                                                       ulon_var_ocean,
                                                       ulat_var_ocean,
                                                       mesh)
        do_uinterp = do_uinterp.rename({'vari_ts': 'ueast_ocean'})        
        do_vinterp = interpolate_variable_to_obs_track(dres,
                                                       vnorth,
                                                       model_time_ocean,
                                                       vlon_var_ocean,
                                                       vlat_var_ocean,
                                                       mesh)
        do_vinterp = do_vinterp.rename({'vari_ts': 'vnorth_ocean'})
        # Interpolate atmos
        atmos_dict = dict()
        atmos_dict[model_time_atmos] = 0
        da_uinterp = interpolate_variable_to_obs_track(dres,
                                                       u_wind,
                                                       model_time_atmos,
                                                       lon_var_atmos,
                                                       lat_var_atmos,
                                                       da.isel(atmos_dict))
        da_uinterp = da_uinterp.rename({'vari_ts': 'ueast_atmos'})
        da_vinterp = interpolate_variable_to_obs_track(dres,
                                                       v_wind,
                                                       model_time_atmos,
                                                       lon_var_atmos,
                                                       lat_var_atmos,
                                                       da.isel(atmos_dict))
        da_vinterp = da_vinterp.rename({'vari_ts': 'vnorth_atmos'})
        # Check for all nan's - drifter not in domain
        if (np.isnan(da_uinterp.ueast_atmos.values).all()) or \
           (np.isnan(da_vinterp.vnorth_atmos.values).all()) or \
           (np.isnan(do_uinterp.ueast_ocean.values).all()) or \
           (np.isnan(do_vinterp.vnorth_ocean.values).all()):
            print('Drifter not in domain {}'.format(basename))
            continue
        # Calculate alpha
        n = len(do_vinterp.vnorth_ocean.values)
        ocean = np.array([complex(do_uinterp.ueast_ocean.values[i],
                                  do_vinterp.vnorth_ocean.values[i]) for i in range(n)])
        atmos = np.array([complex(da_uinterp.ueast_atmos.values[i],
                                  da_vinterp.vnorth_atmos.values[i]) for i in range(n)])
        drift = np.array([complex(dres.u.values[i],
                                  dres.v.values[i]) for i in range(n)])
        alpha = (drift - ocean)/atmos
        # Output
        dnew = xr.merge([do_uinterp, do_vinterp, da_uinterp, da_vinterp, dres])
        alpha_real = xr.DataArray(alpha.real, dims=['time',],
                                  coords={'time': dres.time.values})
        alpha_imag = xr.DataArray(alpha.imag, dims=['time',],
                                  coords={'time': dres.time.values})
        dnew['alpha_real'] = alpha_real
        dnew['alpha_imag'] = alpha_imag
        print(dnew)
        print("saving {}".format(outfile))
        dnew.to_netcdf(outfile)

                                                 
def drifter_velocity(d):
    """Calculate the time series of drifter velocity given a drifter dataset"""
    lon1 = d.LONGITUDE.values[0:-1]
    lon2 = d.LONGITUDE.values[1:]
    lat1 = d.LATITUDE.values[0:-1]
    lat2 = d.LATITUDE.values[1:]
    times = d.TIME.values[:]
    
    # Define a projection
    g = pyproj.Geod(ellps='WGS84')
    
    azimuth, backazimuth, dist = g.inv(lon1, lat1, lon2, lat2, radians=False)
    
    speed = []
    for i, d in enumerate(dist):
        speed.append(dist[i] /((times[i+1] - times[i])/np.timedelta64(1, 's')))
    speed = np.array(speed)
    
    u = speed*np.sin(np.radians(azimuth))
    v = speed*np.cos(np.radians(azimuth)) 
    
    # perhaps return a dataset? Maybe even save it??
    ds = xr.Dataset(coords={'time': times[0:-1]},
                    data_vars={'lon': ('time', lon1),
                               'lat': ('time', lat1),
                               'u': ('time', u),
                               'v': ('time', v)})
    return ds


def interpolate_time(var, t, ti):
    """Interpolate var(t) to time ti. Linear interpolation in time. 
    var is an array of data with first index time (eg. [time, y, x])
    ti is a np.datetime64 object
    t is an array of np.datetime64 objects
    """
    
    # Define a reference time
    tr = t[0] 
    # t and ti with repsect to epoc in seconds
    ti_tr = (ti-tr)/np.timedelta64(1, 's')
    t_tr = (t-tr)/np.timedelta64(1, 's')
    
    # Identify neighbouring time indices
    diff = ti_tr - t_tr
    i1 = np.argmin(np.abs(diff))
    sign = np.sign(diff[i1])
    i2 = int(i1 + sign*1)
    
    # Ensure i2 is in a valid range. In these cases, extrapolation will occur.
    # perhaps send a warning??
    if i1 == 0:
        i2 = i1 + 1
    elif i1 == len(t_tr) -1:
        i2 = i1 - 1
        
    # Edge case when i1 = i2 (sign is 0)
    if i1 == i2:
        vari = var[i1]
    else:
        # Interpolate
        vari = var[i1] +\
            (var[i2] - var[i1])/(t_tr[i2] - t_tr[i1])*(ti_tr-t_tr[i1])
    return vari


def interpolate_space(var, lon, lat, loni, lati):
    """ Interpolate field var(lon, lat) to (loni, lati)"""
    f = interp.interp2d(lon, lat, var)
    vari=f(loni,lati)
    return vari
    

def interpolate_variable_to_obs_track(
        dobs, dmodel, time_var, lon_var, lat_var, grid_dataset):
    vari_ts_array = []
    lon_mod = np.squeeze(grid_dataset[lon_var].values)
    lon_mod_adjust = lon_mod.copy()
    lon_mod_adjust[lon_mod > 180] -= 360
    lat_mod = np.squeeze(grid_dataset[lat_var].values)
    for t in range(len(dobs.time.values)):
        loni = dobs.lon.values[t]
        lati = dobs.lat.values[t]
        ti = dobs.time.values[t]
        # Interpolate in time
        vari_t = interpolate_time(dmodel, dmodel[time_var].values, ti)
        # Identify closest grid cell and interpolate in space
        dist, j, i, lat_near, lon_near =\
            find_nearest_grid_point(lati, loni,
                                    grid_dataset,
                                    lat_var,lon_var,
                                    n=10)
        Ny = dmodel.shape[-2]
        Nx = dmodel.shape[-1]
        if i[0] <=1 or i[0] >= Nx-2 or j[0] <=1 or j[0] >= Ny-2:
            print("Drifter position ({},{}) outside of domain".format(loni, lati))
            vari_ts = np.empty((1,))
            vari_ts[:] = np.nan
        else:
            vari_ts = interpolate_space(vari_t[j,i],
                                        lon_mod_adjust[j,i],
                                        lat_mod[j,i],
                                        loni,
                                        lati)
        vari_ts_array.append(vari_ts[0])
    ds_vari_ts = xr.Dataset(
        coords={'time': dobs.time.values},
        data_vars={'lon': ('time', dobs.lon.values),
                   'lat': ('time', dobs.lat.values),
                   'vari_ts': ('time', np.array(vari_ts_array))}
    )
    return ds_vari_ts


if __name__=='__main__':
    dynamic_windage(DRIFTER_DIR,
                    OCEAN_DIR,
                    ATMOS_DIR,
                    OUT_DIR,
                    OCEAN_MESH_FILE,
                    rotation_data_file=ROTATION_FILE,
                    xwatervel='vozocrtx',
                    ywatervel='vomecrty')
